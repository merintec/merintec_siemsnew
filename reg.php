<?php
  error_reporting(0);
  require("sistema/comunes/conexion.php");
  require("sistema/comunes/funciones_php.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>.:: SIEMS Instituto Gerencial ::.</title>
    <link href="sistema/css/sm_estilos.css" rel="stylesheet" type="text/css">
    <link href="css/estilos.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="scripts/muestra_oculta.js">
</script>
  </head>
  <body onLoad="muestra_oculta('pop_sesion_top')">
  <div class="contenedor">
          <div id="inicio_sesion_top">
            <?php include('sistema/general/sesion_top.php'); ?>
          </div>
          <div id="botonera_top">
            <?php include('general/botonera_top.php'); ?>
          </div>
          <div id="contenedor_cabecera">
            <?php include('general/cabecera.php'); ?>
          </div>
          <div id="panel">
            <?php include('general/inicio_panel.php'); ?>
          </div>
          <div id="sm_baseformulario">
            <?php $_GET["formulario"]=$_GET["auxiliar"]; include('sistema/general/formulario.php'); ?>
          </div>
          <div id="social_net_side">
            <?php include('general/ntbar.php'); ?>
          </div>
          <div id="copy">
            <?php include('general/copy.php'); ?>
          </div>
  </div>
  </body>
</html>
