<?php
	$url_base = "";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="../imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
        <script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>

		<script src="<?php echo $url_base?>sistema/validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $url_base?>sistema/validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="<?php echo $url_base?>sistema/validacion/css/validationEngine.jquery.css" type="text/css"/>
		<link rel="stylesheet" href="<?php echo $url_base?>sistema/validacion/css/template.css" type="text/css"/>	

		 <!-- validacion en vivo -->
        <script >
          jQuery(document).ready(function(){
	    // binds form submission and fields to the validation engine
              jQuery("#contacto").validationEngine('attach', {bindMethod:"live"});
          });
        </script>
	</head>
	<body>
	<header>

		<div class="hidden-xs" style="font-size:1.8em; position: absolute;	float: right; z-index:99; margin-top: 23em; margin-left:49%;"> <a href="#abajo" style="color:#FFF;"><span class="glyphicon glyphicon-chevron-down">  </span> </a>   </div>

		<?php include ($url_base.'frontend/header.php'); ?>		
	</header>
	<section>
		<a name="abajo" id="abajo"> </a>
		<?php include ($url_base.'frontend/home.php'); ?>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>
