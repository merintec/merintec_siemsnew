<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
        <link rel="stylesheet" href="<?php echo $url_base?>css/swiper.css" type="text/css"/>
        <!-- Demo styles -->
        <style>
		    body {
		    }
		    .swiper-container {
		        width: 100%;
		        height: 85%;
		    }
		    .swiper-slide {
		        text-align: center;
		        width: 60%;
		        /* Center slide text vertically */
		        display: -webkit-box;
		        display: -ms-flexbox;
		        display: -webkit-flex;
		        display: flex;
		        -webkit-box-pack: center;
		        -ms-flex-pack: center;
		        -webkit-justify-content: center;
		        justify-content: center;
		        -webkit-box-align: center;
		        -ms-flex-align: center;
		        -webkit-align-items: center;
		        align-items: center;
		    }
		    .swiper-slide:nth-child(1n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(2n) {
		        width: 35%;
		    }
		    .swiper-slide:nth-child(3n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(4n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(5n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(6n) {
		        width: 35%;
		    }
		    .swiper-slide:nth-child(7n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(8n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(9n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(10n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(11n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(12n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(13n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(14n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(15n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(16n) {
		        width: 80%;
		    }
		    .swiper-slide:nth-child(17n) {
		        width: 35%;
		    }
		    .swiper-slide:nth-child(18n) {
		        width: 45%;
		    }
		    .swiper-slide:nth-child(19n) {
		        width: 40%;
		    }
		    .swiper-slide:nth-child(20n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(21n) {
		        width: 60%;
		    }
		    .swiper-slide:nth-child(22n) {
		        width: 60%;
		    }
		    .imagen{
		    	max-height: 100%;
		    	max-width: 100%;
		    }
	    </style>
	</head>
	<body>
	<header>
		<?php 
			$titulo_primario = "SIEMS INSTITUTO GERENCIAL";
			$texto_slide = '<span class="x05"><b>SIEMS Instituto Gerencial, ofrece programas de educación continua como diplomados, cursos, seminarios y conferencias de alto nivel.</b></span>';
			$imagen_slide = $url_base.'imagenes/page/siems.jpg';
			$menu_active = 'siems';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container">
			<div class="row">
				<br><br>
				<div class="col-md-11 col-xs-10 pull-right">
					<div class="hidden-xs home-titulo x3">¡VIVE LA EXPERIENCIA<BR>EDUCATIVA SIEMS!</div>
					<div class="visible-xs home-titulo x15">¡VIVE LA EXPERIENCIA<BR>EDUCATIVA SIEMS!</div>
					<?php $texto = '<b>Siems es una Institución de Educación contínua</b>, fundada en 1999 que ofrece una oportunidad única de aprendizaje, en la que escucharás de mano de profesionales especialistas, herramientas que permitirán ampliar el observador que eres, vislumbrar oportunidades  y potenciar tu accionar.  <br>Nuestra propuesta educativa, consiste en generar una experiencia donde lo complejo se hace sencillo y aplicable. Imagina el impacto en tu vida y en tu organización,  al promover nuevas prácticas transformadoras, que generan nuevas realidades y te permitan construir un mundo de  posibilidades sin límites!'; ?>
					<div class="hidden-xs outd-texto line-x3 x12"><?php echo $texto; ?></div>
					<div class="visible-xs outd-texto line-x2"><?php echo $texto; ?></div>
					<br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<center>
						<div class="event-titulo x15">Video Insititucional: Siems Instituto Gerencial</div>
						<iframe class="hidden-xs" width="560" height="315" src="https://www.youtube.com/embed/noVt4aHoock?rel=0&controls=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
						<iframe class="visible-xs" src="https://www.youtube.com/embed/noVt4aHoock?rel=0&controls=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
					</center>
				</div>
			</div>
			<br>
			<div class="row">
				<br><br>
				<div class="col-md-11 col-xs-10 pull-right">
					<div class="hidden-xs home-titulo x3">¡Forma parte de nuestra historia!</div>
					<div class="visible-xs home-titulo x15">¡Forma parte de nuestra historia!</div>
					<?php $texto = 'Disfruta de cómodas y modernas instalaciones que harán de tu experintecia de aprendizaje un momento grato, emocionante y divertido que perdurará en tu memoria.'; ?>
					<div class="hidden-xs outd-texto line-x3 x12"><?php echo $texto; ?></div>
					<div class="visible-xs outd-texto line-x2"><?php echo $texto; ?></div>
					<br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-11 col-xs-11 pull-right">
				    <!-- Swiper -->
				    <div class="swipper-prod">
				      <div id="swiper1" class="swiper-container">
				          <div class="swiper-wrapper">
				          	<?php 
				          		$vars_titulos = array("Acogedoras instalaciones", "Moderno estilo", "Eres Único",);
				          		for($i=0;$i<=22;$i++){
					          		echo '<div class="swiper-slide">';
				                    	echo '<img class="imagen" title="'.$vars_titulos[$i].'" src="'.$url_base.'imagenes/page/instalaciones-'.$i.'.jpg">';
				                    echo '</div>';
			                    } 
			                ?>
				          </div>
				          <!-- Add Pagination -->
				          <div class="swiper-pagination"></div>
				          <!-- Add Arrows -->
				          <div class="swiper-button-next"></div>
				          <div class="swiper-button-prev"></div>
				      </div>
				    </div>
				</div>
			</div>
			<br>
			<br>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  
<!-- Swiper JS -->
	<script src="<?php echo $url_base?>js/swiper.min.js"></script>

<!-- Initialize Swiper -->
    <script>
	    var swiper = new Swiper('#swiper1', {
	        pagination: '.swiper-pagination',
	        slidesPerView: 'auto',
	        centeredSlides: true,
	        paginationClickable: true,
	        speed: 2000,
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        spaceBetween: 30,
	        autoplay: 3000,
	        autoplayDisableOnInteraction: false,
	    });
    </script>