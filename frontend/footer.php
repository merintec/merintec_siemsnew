<a name="contactar"></a><div class="footer-divisor">
	<img width="100%" src="<?php echo $url_base?>imagenes/page/divisor.png">
</div>
<div class="footer-contenido">
	<div class="hidden-xs footer-titulo x4">CONTACTOS</div>
	<div class="visible-xs footer-titulo x1.5">CONTACTOS</div>
	<div class="row">
		<div class="col-xs-6">
			<div class="footer-mapa pull-right">
				<iframe class="hidden-xs" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3945.0740881294314!2d-71.1594047872087!3d8.588875498096403!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e64870ff0e91917%3A0x296c77f5124aa197!2sSIEMS%2C+38-138+Av.+2+Obispo+Lora%2C+M%C3%A9rida+5101!5e0!3m2!1ses-419!2sve!4v1445756076213" width="100%" height="280px" frameborder="0" style="border:0" allowfullscreen></iframe>
				<div class="hidden-xs x075 pull-left">RIF: <?php echo $RIF;?> M.P.P.E: <?php echo $MPPE;?></div>
				<div class="visible-xs footer-texto x075"><a href="https://maps.google.com/maps?ll=8.58887,-71.157216&z=15&t=m&hl=en-US&gl=VE&mapclient=embed&q=SIEMS%2038-138%20Av.%202%20Obispo%20Lora%20M%C3%A9rida%205101"><img width="50%" src="<?php echo $url_base; ?>imagenes/page/google-map-icon.png"><br><br>Ver en Google Maps<br><br></a></div>
				<div class="visible-xs x05 pull-left">RIF: <?php echo $RIF;?> M.P.P.E: <?php echo $MPPE;?></div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="footer-social-btn-xs"><div class="social-local-xs">&nbsp;</div></div>
			<span class="hidden-xs footer-texto x15">Prolongación Av. 2 Lora.<br>Sector El Encanto. Edificio Siems.<br>Mérida Venezuela<br>+58 274 - 263.25.30<br>+58 274 - 416.41.43<br>+58 414 - 745.77.22<br><a href="mailto:infosiems@siems.com.ve">infosiems@siems.com.ve</a></span>
			<span class="visible-xs footer-texto x075">Prolongación Av. 2 Lora.<br>Sector El Encanto. Edificio Siems. Mérida Venezuela<br>+58 274 - 263.25.30<br>+58 274 - 416.41.43<br>+58 414 - 745.77.22<br><a href="mailto:infosiems@siems.com.ve">infosiems@siems.com.ve</a></span>
			<div class="hidden-xs">
				<div class="col-xs-2"></div>
				<div class="col-xs-2"><a href="javascript:$zopim.livechat.window.show()" title="Chat"><div class="footer-social-btn"><div class="social-chat">&nbsp;</div></div></a></div>
				<div class="col-xs-2"><a href="https://www.facebook.com/Siems-Instituto-Gerencial-1683148121905224/?ref=br_rs" title="facebook"><div class="footer-social-btn"><div class="social-face">&nbsp;</div></div></a></div>
				<div class="col-xs-2"><a href="https://twitter.com/siems_instituto" title="twitter"><div class="footer-social-btn"><div class="social-twit">&nbsp;</div></div></a></div>
				<div class="col-xs-2"><a href="https://www.instagram.com/siems_instituto/" title="instagram"><div class="footer-social-btn"><div class="social-inst">&nbsp;</div></div></a></div>
				<div class="col-xs-2"></div>
			</div>
		</div>
	</div>
	<div class=" hidden-xs col-xs-12" style="margin-top:3em; font-size: 0.8em;">
				Diseñado por <a class="footer-titulo" href="#"> One Block Brain</a> y Desarrollado por <a target="blank" class="footer-titulo"  href="http://merintec.com.ve" target="blank" > MERINTEC  </a>  para SIEMS. Todos los derechos reservados.
	 </div>
	 	<div class=" visible-xs col-xs-11 pull-right" style="margin-top:1em; font-size: 0.6em;">
				Diseñado por <a class="footer-titulo" href="#"> One Block Brain</a> y Desarrollado por <a target="blank" class="footer-titulo"  href="http://merintec.com.ve" target="blank" >  MERINTEC </a>  para SIEMS. Todos los derechos reservados.
	 </div>
</div>

<div id="footer-fondo">
	<img width="100%" src="<?php echo $url_base?>imagenes/page/footer-fondo.png">
</div>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//cdn.zopim.com/?mPL8zpgRR4YzUng0mExR3eX8lyukQ1Xe';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->