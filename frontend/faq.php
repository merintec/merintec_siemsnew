<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'PREGUNTAS FRECUENTES';
			$imagen_slide = $url_base.'imagenes/page/faq.jpg';
			$menu_active = 'siems';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
	<br>
	<br>

		<div class="container">

			<div class="col-md-12 col-xs-11 pull-right">
				
		
					<div class="faq-sub"> ¿Cuál es el procedimiento para inscribirme? </div><br>
					<div class="faq-text"> El procedimiento es muy simple. </div><br>
					<div class="faq-text line-x2">
						<p align="justify"><span class="faq-num"> 1. </span> Usted puede llamar por teléfono al (0274) 263.2530 / 416.4143 y también, a través de nuestro portal para realizar su preinscripción. </p>
						<p align="justify"><span class="faq-num"> 2. </span> Puede también realizar el pago personalmente en nuestras instalaciones o efectuar depósito o transferencia en las siguientes cuentas bancarias: </p>
					</div>

					<div class="faq-num" style="margin-left:5em;"> 1. Banco Occidental de Descuento </div>
					<div class="faq-text line-x2" style="margin-left:5em;">
						Cuenta Corriente No. 0116-0183-9300-1280-3049<br>
       					Titular: Jorge Alberto Molina Suescun<br>
        				Rif: V-12348522-6

					</div>
					<br>
					<div class="faq-num" style="margin-left:5em;"> 2. Banco Mercantil: </div>
					<div class="faq-text line-x2" style="margin-left:5em;">
						Cuenta Corriente No. 0105-0092-3110-9206-8465 <br>
     					Titular: Siems Servicios Integrados y Estudios Empresariales <br>
    					 Rif: V-12348522-6<br>

					</div>
					<br>
					<div class="faq-text"> <p align="justify"> Una vez efectuado el pago, debes adjuntarlo a email y notificarlo junto con tus datos y el nombre del curso de tu interés. Confirmado el pago, nosotros le enviaremos la factura escaneada. </p> </div>
					<br>
					<div class="faq-sub"> ¿Qué documentos debo presentar para inscribirme? </div>
					<div class="faq-text line-x2"> <p align="justify">Cédula de identidad y en caso de ser estudiante, carnet vigente para obtener precio preferencial. En el caso de las maestrías debe consignar título profesional universitario de licenciatura, ingeniería, etc. Para los diplomados, copia de Título Universitario (TSU o Profesional Universitario).</p></div>
					<br>
					<div class="faq-sub"> ¿Soy Extranjero, puedo realizar un curso en SIEMS? </div>
					<div class="faq-text line-x2"> <p align="justify">Efectivamente. Usted puede tomar cualquiera de nuestros cursos.</p></div>
					<br>
					<div class="faq-sub"> ¿Necesito ir personalmente a SIEMS para inscribirme? </div>
					<div class="faq-text line-x2"> <p align="justify">No hay necesidad de ir personalmente. Puedes preincribirte telefónicamente a través de los teléfono 0274-1632530 /426.41.43 o de nuestra página web y abonar la inversión haciendo depósito o transferencia en nuestras cuentas.</p></div>
					<br>
					<div class="faq-sub"> ¿Puedo pagar con tarjeta de crédito, tarjeta de débito o cheque? </div>
					<div class="faq-text line-x2"> <p align="justify">Si, Siems cuenta con el servicio de punto de venta bancario.</p></div>
					<BR>
					<BR>


				
					
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  