<!-- Logo -->
<a href="<?php echo $url_base; ?>index.php">
  <div id="logo" class="hidden-xs logo" title="<?php echo $logo_title; ?>">&nbsp;</div>
  <div id="logo" class="visible-xs logo-xs" title="<?php echo $logo_title; ?>">&nbsp;</div>
</a>
<!-- Panel superior -->
<div id="panel_top" class="panel_top">
  <div class="container-fluid">
    <div class="row"> 
      <?php include($url_base.'frontend/sesion.php'); ?>
    </div>
    <div class="row">
      <?php include($url_base.'frontend/menu.php'); ?>
    </div>
  </div>
</div>
<!-- Slider -->
<div id="slider">
  <div id="carousel1" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <!-- Social Net -->
      <div id="social-net" class="hidden-xs hidden-xs social">
        <?php include($url_base.'frontend/social.php'); ?>
      </div>
      <div id="social-net" class="visible-xs social social-fixed">
        <?php include($url_base.'frontend/social.php'); ?>
      </div>
      <!-- fin social -->
      <!-- mostrar slide -->
      <div class="item active" style="margin-bottom: -10px;">
          <img src="<?php echo $imagen_slide; ?>" width="100%" title="<?php echo $logo_title; ?>">
          <div class="hidden-xs carouse-titulo-page x4">
            <?php if($titulo_primario){
              echo '<span style="font-size: 0.9em;">'.$titulo_primario.'</span>';
            }
            ?>
            <div class="circulo"></div>
            <div class="linea-vertical"></div>
            <div class="carouse-contenedor-border-titulo">
              <div class="carouse-contenedor-titulo-page"><?php echo $texto_slide; ?></div>
            </div>
          </div>
          <div class="visible-xs carouse-titulo-page x15">
            <div class="carouse-contenedor-border-titulo">
              <div class="carouse-contenedor-titulo-page"><?php echo $texto_slide; ?></div>
            </div>
          </div>
      </div>
    </div>   
  </div>
</div>