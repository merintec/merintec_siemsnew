<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = '<span class=\'x075\'>PROGRAMA DE <BR>CAPACITACIÓN IN COMPANY</span>';
			$imagen_slide = $url_base.'imagenes/page/contacto.jpg';
			$menu_active = 'empresas';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-xs-10 pull-right" style="padding-top: 2em;">
					<?php $texto = '<b>SIEMS</b> ofrece una serie de programas de capacitación in company, que pueden ser adaptados a las necesidades de cada organización en cuanto a objetivos, contenidos, dinámicas y número de participantes. Estos programas están especialmente diseñados para satisfacer las necesidades de formación de las organizaciones del sector público y privado. <br><br>La capacitación del personal es una de las actividades que desarrolla toda gerencia de recursos humanos, comprometida con el crecimiento profesional de su personal y con el mejoramiento del performance de las actividades asociadas a cada cargo, ya que, permite desarrollar un lenguaje común entre todos los miembros de la organización y a generar un espíritu de equipo. <br><br>Además, podemos dictar en la modalidad in company, cualquier programa de capacitación ofertado regularmente en nuestra institución.'; ?>
					<div class="hidden-xs outd-texto line-x3 x12"><?php echo $texto; ?></div>
					<div class="visible-xs outd-texto line-x2"><?php echo $texto; ?></div>
					<br><br>
					<div class="hidden-xs inco-titulo x25">SELECCIONA EL CURSO DE TU PREFERENCIA</div>
					<div class="visible-xs inco-titulo x15"><b>SELECCIONA EL CURSO <br>DE TU PREFERENCIA<b></div>
				</div>
			</div>
		</div>
		<div class="container" style="margin-top: 4em; margin-bottom: 5em;">
			<div class="row hidden-xs">
				<div class="col-md-2">&nbsp;</div>
				<a href="http://es.calameo.com/read/001546779ad0a2f1296ba" target="_blank"><div class="col-md-3"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/vym.png"></div></a>
				<a href="http://es.calameo.com/read/001546779f7ad21fcf50f" target="_blank"><div class="col-md-3"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/vym2.png"></div></a>
				<a href="http://es.calameo.com/read/001546779fc315543271c" target="_blank"><div class="col-md-3"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/dg.png"></div></a>
				<div class="col-md-12" style="margin-top: 3em;">&nbsp;</div>
				<div class="col-md-2">&nbsp;</div>
				<a href="http://es.calameo.com/read/001546779fbdd71a3cbc9" target="_blank"><div class="col-md-3"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/dp.png"></div></a>
				<a href="http://es.calameo.com/read/001546779bff497c8bd23" target="_blank"><div class="col-md-3"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/hys.png"></div></a>
				<a href="../pdf/sectorpublico.pdf" target="_blank"><div class="col-md-3"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/sp.png"></div></a>

			</div>
			<div class="row visible-xs">
				<div class="col-xs-2">&nbsp;</div>
				<a href="http://es.calameo.com/read/001546779ad0a2f1296ba" target="_blank"><div class="col-xs-5"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/vym.png"></div></a>
				<a href="http://es.calameo.com/read/001546779f7ad21fcf50f" target="_blank"><div class="col-xs-5"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/vym2.png"></div></a>
				<div class="col-xs-12" style="margin-top: 3em;">&nbsp;</div>
				<div class="col-xs-2">&nbsp;</div>
				<a href="http://es.calameo.com/read/001546779fc315543271c" target="_blank"><div class="col-xs-5"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/dg.png"></div></a>
				<a href="http://es.calameo.com/read/001546779fbdd71a3cbc9" target="_blank"><div class="col-xs-5"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/dp.png"></div></a>
				<div class="col-xs-12" style="margin-top: 2em;">&nbsp;</div>
				<div class="col-xs-2">&nbsp;</div>
				<a href="http://es.calameo.com/read/001546779bff497c8bd23" target="_blank"><div class="col-xs-5"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/hys.png"></div></a>
				<a href="../pdf/sectorpublico.pdf" target="_blank"><div class="col-xs-5"><img width="90%" title="" src="<?php echo $url_base?>imagenes/page/sp.png"></div></a>

			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  