<!-- Logo -->
<a href="<?php echo $url_base; ?>index.php">
  <div id="logo" class="hidden-xs logo" title="<?php echo $logo_title; ?>">&nbsp;</div>
  <div id="logo" class="visible-xs logo-xs" title="<?php echo $logo_title; ?>">&nbsp;</div>
</a>
<!-- Panel superior -->
<div id="panel_top" class="panel_top">
  <div class="container-fluid">
    <div class="row"> 
      <?php include($url_base.'frontend/sesion.php'); ?>
    </div>
    <div class="row">
      <?php include($url_base.'frontend/menu.php'); ?>
    </div>
  </div>
</div>
<!-- Slider -->
<div id="slider">
  <div id="carousel1" class="carousel slide" data-ride="carousel" style="min-height: 400px;">
    <!-- Wrapper for slides -->

      <!-- mostrar slide -->
      <div class="item active" style="margin-bottom: -10px;">
          <img src="<?php echo $imagen_slide; ?>" width="100%" title="<?php echo $logo_title; ?>">
          <div class="hidden-xs carouse-vineta x25"><b>
            <a name="punto"></a><div id="carouse-icon"></div>
            <div id="carouse-text"></div></b>
          </div>
          <div class="visible-xs carouse-vineta x12 marg-menos2r"><b>
            <div class="pull-right" id="carouse-text-xs"></div></b>
          </div>
      </div>
    </div>   
  </div>
</div>