<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
        <link rel="stylesheet" href="<?php echo $url_base?>css/swiper.css" type="text/css"/>
        <!-- Demo styles -->
	    <style>
		    html, body {
		        position: relative;
		        height: 100%;
		    }
		    body {

		    }
		    .swiper-container {
		        width: 100%;
		        height: 90%;
		    }
		    .swiper-slide {
		        text-align: center;
		        font-size: 18px;
		        /* Center slide text vertically */
		        display: -webkit-box;
		        display: -ms-flexbox;
		        display: -webkit-flex;
		        display: flex;
		        -webkit-box-pack: center;
		        -ms-flex-pack: center;
		        -webkit-justify-content: center;
		        justify-content: center;
		        -webkit-box-align: center;
		        -ms-flex-align: center;
		        -webkit-align-items: center;
		        align-items: center;
		    }
		    .imagen{
		    	max-height: 100%;
		    	max-width: 90%;
		    }
	    </style>
	</head>
	<body>
	<header>
		<?php 
			//$titulo_primario = "SIEMS INSTITUTO GERENCIAL";
			$texto_slide = 'Nuestras Instalaciones';
			$imagen_slide = $url_base.'imagenes/page/siems.jpg';
			$menu_active = 'siems';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container">
			<div class="row">
				<br><br>
				<div class="col-md-11 col-xs-10 pull-right">
					<div class="hidden-xs home-titulo x3">¡SIÉNTETE SIEMS!</div>
					<div class="visible-xs home-titulo x15">¡SIÉNTETE SIEMS!</div>
					<?php $texto = 'En Siems formas parte de nuestra familia. Disfruta de cómodas y modernas instalaciones que harán de tu experintecia de aprendizaje un momento grato, emocionante y divertido que perdurará en tu memoria.'; ?>
					<div class="hidden-xs outd-texto line-x3 x12"><?php echo $texto; ?></div>
					<div class="visible-xs outd-texto line-x2"><?php echo $texto; ?></div>
					<br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-11 col-xs-11 pull-right">
				    <!-- Swiper -->
				    <div class="swipper-prod">
				      <div id="swiper1" class="swiper-container">
				          <div class="swiper-wrapper">
				          	<?php for($i=0;$i<=6;$i++){
				          		echo '<div class="swiper-slide">';
			                    	echo '<img class="imagen" src="'.$url_base.'imagenes/page/instalaciones-'.$i.'.jpg">';
			                    echo '</div>';
			                } ?>
				          </div>
				          <!-- Add Pagination -->
				          <div class="swiper-pagination"></div>
				          <!-- Add Arrows -->
				          <div class="swiper-button-next"></div>
				          <div class="swiper-button-prev"></div>
				      </div>
				    </div>
				</div>
			</div>
			<br>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  
<!-- Swiper JS -->
	<script src="<?php echo $url_base?>js/swiper.min.js"></script>

<!-- Initialize Swiper -->
    <script>
	    var swiper = new Swiper('#swiper1', {
	        pagination: '.swiper-pagination',
	        paginationClickable: true,
	        speed: 2000,
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        spaceBetween: 30,
	        autoplay: 3000,
	        autoplayDisableOnInteraction: false,
	    });
    </script>