<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'DIPLOMADOS Y<BR>PROGRAMAS AVANZADOS';
			$imagen_slide = $url_base.'imagenes/page/contacto.jpg';
			$menu_active = 'diplomados';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<br><br>
		<div style="margin-bottom: 4em;">
			<div class="row">

				<div class="col-md-11 col-xs-10 pull-right">

				<?php 

				$consulta="SELECT * FROM eventos where imag_dipl!='' and pdf_dipl!='' and stat_evnt='Activo' ORDER BY nomb_evnt";
				$consulta=mysql_query($consulta);

				while ($fila=mysql_fetch_array($consulta)) 
				{

					echo '<div class="col-md-6 col-xs-12"> <center>  <a href="'.$url_base.'frontend/evento.php?evento='.$fila[codg_evnt].'&frm=diplomados"> <img class="clientes-img" title="Diplomados" src="'.$url_base.''.$fila[imag_dipl].'" style="margin-bottom: 2em;"> </a> </center>   </div>';					



				}
					
			 	 ?>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  