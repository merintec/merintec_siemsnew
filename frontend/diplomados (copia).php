<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'DIPLOMADOS Y<BR>PROGRAMAS AVANZADOS';
			$imagen_slide = $url_base.'imagenes/page/contacto.jpg';
			$menu_active = 'diplomados';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<br><br>
		<div style="margin-bottom: 4em;">
			<div class="row">
				<div class="col-md-11 col-xs-10 pull-right">
					<a href="<?php echo $url_base?>pdf/01.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/01_dt.png" style="margin-bottom: 2em;"></div></a>					
					<a href="<?php echo $url_base?>pdf/02.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/02_dfg.png" style="margin-bottom: 2em;"></div></a>
					<div class="col-md-12 hidden-xs"></div>
					<a href="<?php echo $url_base?>pdf/03.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/03_dc.png" style="margin-bottom: 2em;"></div></a>
					<a href="<?php echo $url_base?>pdf/04.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/04_th.png" style="margin-bottom: 2em;"></div></a>
					<div class="col-md-12 hidden-xs"></div>					
					<a href="<?php echo $url_base?>pdf/05.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/05_cm.png" style="margin-bottom: 2em;"></div></a>
					<a href="<?php echo $url_base?>pdf/06.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/06_cs.png" style="margin-bottom: 2em;"></div></a>
					<div class="col-md-12 hidden-xs"></div>
					<a href="<?php echo $url_base?>pdf/07.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/07_cg.png" style="margin-bottom: 2em;"></div></a>
					<a href="<?php echo $url_base?>pdf/08.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/08_pm.png" style="margin-bottom: 2em;"></div></a>
					<div class="col-md-12 hidden-xs"></div>
					<a href="<?php echo $url_base?>pdf/09.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/09_lc.png" style="margin-bottom: 2em;"></div></a>					
					<a href="<?php echo $url_base?>pdf/10.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/10_fcd.png" style="margin-bottom: 2em;"></div></a>
					<div class="col-md-12 hidden-xs"></div>
					<div class="col-md-3 hidden-xs"></div>
					<a href="<?php echo $url_base?>pdf/11.pdf" target="_blank"><div class="col-md-6 col-xs-12"><img class="clientes-img" title="Diplomados" src="<?php echo $url_base?>imagenes/page/11_pnl.png" style="margin-bottom: 2em;"></div></a>
					
				</div>
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  