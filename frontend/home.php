<?php 
	include($url_base.'sistema/comunes/conexion.php');
?>
<script>
	function recargar_combo(){
		var parametros = {
			"codg_tipo" : $('#codg_tipo').val(),
			"codg_area" : $('#codg_area').val(),
			"campo_mostrar" : "nomb_evnt",
			"campo_valor" : "codg_evnt",
			"campo_destino" : "codg_evnt", 
			"etiqueta_destino" : "evento_etiqueta",
			"funcion_add" : "recargar_combo()"
		};
		var url= "<?php echo $url_base; ?>comunes/llenar_combo_eventos.php"; 
      	$.ajax
      	({
        	type: "POST",
          	url: url,
          	data: parametros,
          	success: function(data)
          	{
          		console.log(data);
       			$('#lista_eventos').html(data);
          	}
      	});
      	return false;  
    }
    function ira(){
    	if ($('#codg_tipo').val()  || $('#codg_area').val() || $('#codg_evnt').val()){
    		var url = "<?php echo $url_base;?>frontend/evento.php?tipo=" + $('#codg_tipo').val() + "&area=" + $('#codg_area').val() + "&evento=" + $('#codg_evnt').val() + "";
    		if ($('#codg_evnt').val()){
    			url = url + "&frm=home";
    		}
    		url = url + "#punto";
    		window.location=(url);
    	}
    	else{
    		var mensaje = '<b>Debes seleccionar un tipo, un área o un evento<br><br></b>';
    		$('#mensaje_busqueda').html(mensaje);
    	}
    }
    function buscar_cursos(){
    	if ($('#keys').val()){
    		document.getElementById('home_buscar').submit();
    	}
    	else{
    		var mensaje = '<b>Debes escribir al menos una palabra para buscar.<br><br></b>';
    		$('#mensaje_busqueda').html(mensaje);
    	}
    	return false;
    }
</script>
<br><br><br>
<div class="container">
	<div class="row">
		<div class="col-md-12 col-xs-11 pull-right">
			<div class="hidden-xs home-titulo x2"><b>TENEMOS EL PROGRAMA PARA TÍ</b></div>
			<div class="visible-xs home-titulo x1"><b>TENEMOS EL PROGRAMA PARA TÍ</b></div>
			<div class="hidden-xs home-titulo x37">Avanza en tu camino al éxito</div>
			<div class="visible-xs home-titulo x15">Avanza en tu camino al éxito</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3 col-xs-2">&nbsp;</div>
	       	<div class="col-md-6 col-xs-10">
	 			<div class="cajacontacto"  style="margin-top: 2em;">
		 			<div class="row">
		 				<div class="home-cinta"></div>
		 			</div>
		 			<br>
		            <form method="POST" name="home_buscar" id="home_buscar" action="frontend/evento.php" onsubmit="return jQuery(this).validationEngine('validate');">
		                <div class="row">
		                	<div id="mensaje_busqueda"></div>
		                	<div class="input-group">
                                   <input type="text" name="keys" id="keys" class="home-campo text-input form-control"  placeholder="¿En qué estas interesado?">
	                    		   <span class="input-group-addon btn fondo_boton" onclick="buscar_cursos();" > <span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
               				</div>
               				<br>
		                	<div style="position: absolute; float: right; right: 30px;"><input class="validate[required, minSize[3],maxSize[100]] text-input form-control campo_contacto" type="text" id="codg_tipo" value=""></div>
			                <div class="dropdown">
							  <button class="btn dropdown-toggle home-campo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%">
							    <span id="tipo_etiqueta" class="select-etiqueta">Seleccione el Tipo</span>
							    <span class="select-arrow-white pull-right">&nbsp;</span>
							  </button>
							  <ul class="dropdown-menu select-dropdown" aria-labelledby="dropdownMenu1">
							  	<?php 
							  		$sql_tipo = "SELECT * FROM eventos_tipos ORDER BY nomb_tipo";
							  		$bus_tipo = mysql_query($sql_tipo);
							  		while($tipos = mysql_fetch_array($bus_tipo)){
							  			$onc = "$('#codg_tipo').val('".$tipos[codg_tipo]."'); $('#tipo_etiqueta').html('".$tipos[nomb_tipo]."'); recargar_combo();";
							  			echo '<li class="cursor-pointer" onclick="'.$onc.'"><a>'.$tipos[nomb_tipo].'</a></li>';
							  		}
							  	?>
							  </ul>
							</div>
		                </div>
		                <br>
		                <div class="row">
		                	<div style="position: absolute; float: right; right: 30px;"><input class="validate[required, minSize[3],maxSize[100]] text-input form-control campo_contacto" type="text" id="codg_area" value=""></div>
			                <div class="dropdown">
							  <button class="btn dropdown-toggle home-campo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%">
							    <span id="area_etiqueta" class="select-etiqueta">Seleccione el Área</span>
							    <span class="select-arrow-white pull-right">&nbsp;</span>
							  </button>
							  <ul class="dropdown-menu select-dropdown" aria-labelledby="dropdownMenu1">
							  	<?php 
							  		$sql_area = "SELECT * FROM eventos_areas where stat_area='Activo' ORDER BY nomb_area ";
							  		$bus_area = mysql_query($sql_area);
							  		while($areas = mysql_fetch_array($bus_area)){
							  			$onc = "$('#codg_area').val('".$areas[codg_area]."'); $('#area_etiqueta').html('".$areas[nomb_area]."'); recargar_combo();";
							  			echo '<li class="cursor-pointer" onclick="'.$onc.'"><a>'.$areas[nomb_area].'</a></li>';
							  		}
							  	?>
							  </ul>
							</div>
		                </div>
		                <br>
		                <div class="row">
		                	<div style="position: absolute; float: right; right: 30px;"><input class="validate[required, minSize[3],maxSize[100]] text-input form-control campo_contacto" type="text" id="codg_evnt" value=""></div>
			                <div class="dropdown">
							  <button class="btn dropdown-toggle home-campo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%">
							    <span id="evento_etiqueta" class="select-etiqueta">Seleccione el Evento</span>
							    <span class="select-arrow-white pull-right">&nbsp;</span>
							  </button>
							  <ul class="dropdown-menu select-dropdown" aria-labelledby="dropdownMenu1" id="lista_eventos">
							  		<li class="cursor-pointer"><a>Debe seccionar un Tipo o Área</a></li>
							  </ul>
							</div>
		                </div>
		                <br>
			            <div class="row">
							<div align="right"> <button type="button" name="home_ir" id="home_ir" class="btn home-boton" onclick="ira();">&nbsp;&nbsp;&nbsp;&nbsp;IR&nbsp;&nbsp;&nbsp;&nbsp;</button> </div>
			            </div>
		            </form>
	        	</div>
	        	
	    
	    	<div class="row" style="margin-top: -4em; margin-bottom: 6em;">			        	
			        	<div class="col-md-4 col-xs-4 text-left"> <a href="frontend/catalogo_trimestre.php?tipo=trimestral"><button type="button" name="catalogo_t" id="catalogo_t" class="btn boton-panel" > CATÁLOGO <BR> <b> TRIMESTRAL </b> </button> </a></div>
						<div class="col-md-4 col-xs-4 text-center"> <a href="frontend/cursos_areas.php"> <button type="button" name="buscar" id="buscar" class="btn boton-panel" > CURSOS <BR> <b> POR ÁREAS </b> </button> </a></div>
						<div class="col-md-4 col-xs-4 text-right"> <a href="frontend/catalogo_trimestre.php?tipo=anual"><button type="button" name="catalogo_a" id="catalogo_a" class="btn boton-panel" > CATÁLOGO <BR> <b>ANUAL</b> </button></a> </div>
	    	</div>

		</div>
		
	</div>
</div>
		
<div class="img-ppal">
	<div class="texto">
		
			¡El conocimiento te permite <br> ampliar tu observador, <br>  construir nuevas realidades <br> y un mundo de posibilidades <br> sin límites!
		
	</div>
	<!-- NO BORRAR DIV YA QUE DETERMINA EL ALTO DEL DIV PADRE -->
	<div>
		<img width="100%" src="imagenes/page/home-imgppal.jpg" style="visibility: hidden;">
	</div>
</div>
