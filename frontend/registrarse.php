<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	include_once($url_base.'sistema/comunes/funciones_js.php'); 



$boton=$_POST['boton'];
$naci_part=$_POST['naci_part'];
$cedu_part=$_POST['cedu_part'];
$nomb_part=$_POST['nomb_part'];
$apel_part=$_POST['apel_part'];
$sexo_part=$_POST['sexo_part'];
$tlfn_part=$_POST['tlfn_part'];
$corr_part=$_POST['corr_part'];
$fchn_part=$_POST['fchn_part'];
$codg_tpar=$_POST['codg_tpar'];

$codg_tpar=$_POST['codg_tpar'];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "participantes";
$key_entabla = 'codg_part';
$key_enpantalla = $codg_part;
$datos[0] = prepara_datos ("naci_part",$_POST['naci_part'],'');
$datos[1] = prepara_datos ("cedu_part",$_POST['cedu_part'],'');
$datos[2] = prepara_datos ("nomb_part",$_POST['nomb_part'],'');
$datos[3] = prepara_datos ("apel_part",$_POST['apel_part'],'');
$datos[4] = prepara_datos ("sexo_part",$_POST['sexo_part'],'');
$datos[5] = prepara_datos ("tlfn_part",$_POST['tlfn_part'],'');
$datos[6] = prepara_datos ("corr_part",$_POST['corr_part'],'');
$datos[7] = prepara_datos ("fchn_part",$_POST['fchn_part'],'fecha');
$datos[8] = prepara_datos ("codg_tpar",$_POST['codg_tpar'],'');

$tabla2 = "usuarios";
$clave = texto_aleatorio(6);
$datos2[0] = prepara_datos ("cedu_usua",$_POST['cedu_part'],'');
$datos2[1] = prepara_datos ("nomb_usua",$_POST['nomb_part'],'');
$datos2[2] = prepara_datos ("apel_usua",$_POST['apel_part'],'');
$datos2[3] = prepara_datos ("corr_usua",$_POST['corr_part'],'');
$datos2[4] = prepara_datos ("logi_usua",$_POST['corr_part'],'');
$datos2[5] = prepara_datos ("pass_usua",md5($clave),'');
$datos2[6] = prepara_datos ("tipo_usua",'2','');

if ($boton=='GUARDAR'){
	$mensaje=NULL;
	$mensaje_fail=NULL;
	$reg_part=registro_valor("participantes", "*", "WHERE cedu_part='".$datos2[0][1]."' OR corr_part='".$datos2[3][1]."'");
	if($reg_part==''){
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$ejec_guardar_usu = guardar($datos2,$tabla2);
			if($ejec_guardar_usu[0]!=""){
				mail($datos2[3][1], "Datos de Registro en SIEMS", "Hola: ".$datos2[1][1]." ".$datos2[2][1]."! \n A partir de este momento recibirás información sobre los cursos de tu interés. \r\n Le informamos que sus datos para ingreso al sistema de SIEMS son: \r\n Usuario: ".$datos2[4][1]."\r\n Contraseña: ".$clave." \r\n Le recomendamos cambiar su contraseña al ingresar al Sistema. \r\n De nuevo gracias por tu confianza.", 'From: infosiems@siems.com.ve' . "\r\n" .
		'Reply-To: infosiems@siems.com.ve' . "\r\n");
			}
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];

			$mensaje="Los datos se han almacenado con exito, se ha enviado un correo electronico con los datos para su ingreso";
			
		}else{
			$con['cedu_part']=$cedu_part;
			$con['naci_part']=$naci_part;
			$con['nomb_part']=$nomb_part;
			$con['apel_part']=$apel_part;
			$con['sexo_part']=$sexo_part;
			$con['tlfn_part']=$tlfn_part;
			$con['corr_part']=$corr_part;
			$con['fchn_part']=$fchn_part;
			$con['codg_part']=$codg_part;
			$con['codg_tpar']=$codg_tpar;
		}		
	}else{
			$con['cedu_part']=$cedu_part;
			$con['naci_part']=$naci_part;
			$con['nomb_part']=$nomb_part;
			$con['apel_part']=$apel_part;
			$con['sexo_part']=$sexo_part;
			$con['tlfn_part']=$tlfn_part;
			$con['corr_part']=$corr_part;
			$con['fchn_part']=$fchn_part;
			$con['codg_part']=$codg_part;
			$con['codg_tpar']=$codg_tpar;
	
		$ejec_guardar[1]='Error: La Cédula o el Correo ya se encuentran registrados<br><br><img src="sistema/imagenes/icono_rechazado.png" width="50" />';
		$mensaje_fail="Error: La Cédula o el Correo ya se encuentran registrados";
	}
	$mensaje_mostrar=$ejec_guardar[1];

	
}




?>
<html lang="es">
	<head>
				<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="../imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <script src="../bootstrap/js/jquery.js"> </script>

		<script src="../sistema/validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		<script src="../sistema/validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="../sistema/validacion/css/validationEngine.jquery.css" type="text/css"/>
		<link rel="stylesheet" href="../sistema/validacion/css/template.css" type="text/css"/>
		<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
		 <!-- validacion en vivo -->
        <script >
          jQuery(document).ready(function(){
	    // binds form submission and fields to the validation engine
              jQuery("#registrarse").validationEngine('attach', {bindMethod:"live"});
          });
        </script>

       	<script>
			    $(document).ready(function() {
			        $('.datepicker')
			            .datepicker({
			              format: 'dd-mm-yyyy',
			              autoclose: true,
			              language: 'es'
			            });
			    });
    	</script>

	</head>

			
	
	<body>
	<header>
		<?php 
			$texto_slide = 'CONTACTO';
			$imagen_slide = $url_base.'imagenes/page/pantalla_sesion.jpg';
			//$menu_active = 'contacto';
			include ($url_base.'frontend/header3.php'); 
		?>		
	</header>

	

			<div class="posicion_flotante">
			 
				 		<div class="cajasesion">
				 		<?php 
	if ($mensaje!=NULL) 
			{ 
				echo '<div id="mensaje" class="alert alert-info" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$mensaje.'</div>';
				echo '<script>setTimeout(function() { 	$("#mensaje").fadeOut(1500);	},4000); </script>';

			} 

	if ($mensaje_fail!=NULL) 
		{
			echo '<div id="mensaje" class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$mensaje_fail.'</div>';
				echo '<script>setTimeout(function() { 	$("#mensaje").fadeOut(1500);	},4000); </script>';
	    	 
	    }    ?>
				 			<div class="row">
				 				<div class="cinta"></div>
				 			</div>
				 			<br>
				 			<div align="center" class="inco-titulo x37"> Registrate </div>
				 			<br>
				            <form method="POST" name="registrarse" id="registrarse" onsubmit="return jQuery(this).validationEngine('validate');">
				                <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" >  
					                    		<select name="naci_part" id="naci_part"  class="validate[required] combop">
													<option value="V">V</option>
													<option value="E">E</option>
												</select>
												</span>
				                                <input type="text" name="cedu_part" id="cedu_part" placeholder="Cédula de Identidad" class="validate[required, custom[integer] minSize[6],maxSize[13]] text-input form-control campop" >
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>
				                <br>
				                 <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-nombre.png"> </span>
				                                <input type="text" name="nomb_part" id="nomb_part" placeholder="Nombres" class="validate[required, minSize[3],maxSize[30]] text-input form-control campo_sesion" >
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>

				                <br>
				                 <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-nombre.png"> </span>
				                                <input type="text" name="apel_part" id="apel_part" placeholder="Apellidos" class="validate[required, minSize[3],maxSize[30]] text-input form-control campo_sesion" >
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>

				                     <br>
				                 <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/sexo.png"> </span>
												<table class="hidden-xs" width="100%" border="0"> <tr>
												 <td> <label class="fondo_sexo"> Sexo </label></td> 
												 <td> <label class="color_sexo"> Femenino</label>   <input class="validate[required]" type="radio" name="sexo_part" id="femenino" value="f"/></td>						                   			
 												 <td> <label class="color_sexo"> Masculino </label><input class="validate[required]" type="radio" name="sexo_part" id="masculino" value="m"/></td> 
 												</tr> </table>

 												<table class="visible-xs" width="100%" border="0"> 
 												<tr> <td <label class="fondo_sexo"> Sexo </label></td>  </tr>
												<tr> <td> <label class="color_sexo"> Femenino</label>   <input class="validate[required]" type="radio" name="sexo_part" id="femenino" value="f"/></td><tr>						                   			
 												<tr> <td> <label class="color_sexo"> Masculino </label><input class="validate[required]" type="radio" name="sexo_part" id="masculino" value="m"/></td> 
 												</tr> </table>
											</div>				
									</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>
								<br>
				                 <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/estrella_blanca.png"> </span>
				                                <input type="text" name="fchn_part" id="fchn_part" placeholder="Fecha de Nacimiento" class="validate[required,custom[date]]  text-input datepicker form-control  campo_sesion" >
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>

				                <br>
				                 <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-telefono.png"> </span>
				                                <input type="text" name="tlfn_part" id="tlfn_part" placeholder="Teléfono" class="validate[required, custom[phone] minSize[3],maxSize[30]] text-input form-control campo_sesion" >
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>
				                 <br>
				                 <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-correo.png"> </span>
				                                <input type="text" name="corr_part" id="corr_part" placeholder="Email" class="validate[required, custom[email] minSize[3],maxSize[100]] text-input form-control campo_sesion" >
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>
				                <br>
				                <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/v.png"> </span>
					                    		<select name="codg_tpar" id="codg_tpar"  class="validate[required], form-control campo_sesion" >';
													<?php 

													$consulta="SELECT * FROM participantes_tipos order by codg_tpar";
													$con=mysql_query($consulta);
						
													   echo ' <option value="" selected disabled style="display:none;"> Tipo de Participante</option>';
									       			
									       				while($fila=mysql_fetch_array($con))
									                	 {
									                	 	if ($fila[nomb_tpar]=='Estudiante' or $fila[nomb_tpar]=='Profesional' )
									                	 	{
									                	 		 echo "<option value=".$fila[codg_tpar].">".$fila[nomb_tpar]."</option>";


									                	 	}
									                 	 }
															
															echo '</select>';
													?>
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>

				                     
				                <br>

				                

				                <div class="row">
		                
					                        <div align="center"> <input type="submit" name="boton" id="boton" value="GUARDAR" class="btn fondo_boton" >
					            	
					            </div>

					      


				            </form>
				        </div>
		    </div>




	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  