<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
	$tipo = $_GET[tipo];
	if ($tipo==''){
		$tipo = 'trimestral';
	}
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'CATÁLOGO '.strtoupper($tipo);
			$imagen_slide = $url_base.'imagenes/page/cursos.jpg';
			$menu_active = 'cursos';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-11 pull-right">
				<?php 

					if ($tipo=='trimestral'){
						$fecha_ini = date('Y').'-'.date('m').'-01';
						$fecha_fin = strtotime ( '+3 month' , strtotime ( $fecha_ini ) ) ;
						$fecha_fin = strtotime ( '-1 day' , $fecha_fin ) ;
						$fecha_fin = date ( 'Y-m-j' , $fecha_fin );
						//echo 'desde: '.$fecha_ini.' Hasta: '.$fecha_fin;
						$meses= array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
					}
					if ($tipo=='anual'){
						$fecha_ini = date('Y').'-01-01';
						$fecha_fin = date('Y').'-12-31';
						//echo 'desde: '.$fecha_ini.' Hasta: '.$fecha_fin;
						$meses= array('Ene.','Feb.','Mar.','Abr.','May.','Jun.','Jul.','Ago.','Sep.','Oct.','Nov.','Dic.');
					}

					$colores[1] = array('#07328d','#009cdf');
					$colores[2] = array('#009245','#009245');
					$colores[3] = array('#662380','#29225c');
					$colores[4] = array('#921680','#662380');
					$colores[5] = array('#009245','#009245');
					$colores[6] = array('#07328d','#009cdf');
					$colores[7] = array('#662380','#29225c');
					$colores[8] = array('#921680','#662380');
					$colores[9] = array('#009245','#009245');
					$colores[10] = array('#07328d','#009cdf');
					$colores[11] = array('#662380','#29225c');
					$colores[12] = array('#921680','#662380');
					$color = 1;
					$sql_areas = "SELECT *, if(ea.nomb_area='Tributaria', 'Contable', ea.nomb_area) as nomb_area2 FROM eventos e, eventos_areas ea, eventos_apertura eap WHERE e.codg_area=ea.codg_area AND e.codg_evnt = eap.codg_evnt AND fini_aper >= '".date('Y-m-d')."' AND fini_aper >= '".$fecha_ini."' AND fini_aper <= '".$fecha_fin."' GROUP BY nomb_area2 ORDER BY nomb_area2";
					$bus_areas = mysql_query($sql_areas);
					$cuenta = 1;
					$alto = '400px'; 
					$alto2 = '370px';
					echo '<br><br>';
					?>
					<!-- adaptación especial para eventos online -->
					<!-- en el caso de siems acutalmente tipo 4 -->
					<div style="height: <?php echo $alto; ?>;" id="online">
						<div style="background-color: <?php echo $colores[$color][0]; ?>; float:left; height: <?php echo $alto; ?>; width: 150px;" class="titulo-cat-trim">
							<div class="texto-vertical" style="width: <?php echo $alto; ?>; margin-top: <?php echo $alto; ?>;">
								<span class="x17">Sección</span><br>
								<span class="x2">En Línea / On-Line</span>
							</div>
						</div>
						<div style=" width: 88%; position:absolute; float:left; z-index:999; margin-left: 7em; margin-top: 1.1em; background-color: #FFF; height: <?php echo $alto2; ?>; border: 2px solid <?php echo $colores[$color][0]; ?>;padding: 10px; overflow: auto;">
							<div class="">
								<table class="table">
									<tr>
										<th>&nbsp;</th>
										<?php 
											$fecha_max = strtotime($fecha_fin);
											$fecha_act = strtotime($fecha_ini);
											$i = 0;
											while($fecha_act <= $fecha_max){
												$indicador = ceil(date('m',$fecha_act));
												echo '<th><div class="x1.7" style="color: #ffffff; text-align: center; padding: 5px;background-color: '.$colores[$color][$i].'">'.$meses[$indicador-1].'</div></th>';
												$fecha_act = strtotime ( '+1 month', $fecha_act);
												$i += 1;
												if ($i > 1){
													$i = 0;
												}
											}
											//// consultamos las aperturas en el rango de fechas
											$sql_ev = "SELECT * FROM eventos e, eventos_areas ea, eventos_apertura eap WHERE e.codg_area=ea.codg_area AND e.codg_evnt = eap.codg_evnt AND fini_aper >= '".date('Y-m-d')."' AND fini_aper >= '".$fecha_ini."' AND fini_aper <= '".$fecha_fin."' AND codg_tipo = 4 GROUP BY e.codg_evnt ORDER BY e.nomb_evnt ASC";
											$bus_ev = mysql_query($sql_ev);
											$numero_online = mysql_num_rows($bus_ev);
											while($res_ev = mysql_fetch_array($bus_ev)){
												echo '<tr class="x08"><td style="min-width: 300px;">'.$res_ev[nomb_evnt].'<br><b><a href="evento.php?tipo='.$res_ev[codg_tipo].'&area='.$res_ev[codg_area].'&evento='.$res_ev[codg_evnt].'&frm='.$tipo.'" style="color: '.$colores[$color][$i].';">Ver Evento</a></b></td>';
												$fecha_act = strtotime($fecha_ini);
												while($fecha_act <= $fecha_max){
													$mes_aux = ceil(date('m',$fecha_act));
													$anno_aux = ceil(date('Y',$fecha_act));
													$sql_ap = "SELECT * FROM eventos e, eventos_apertura eap WHERE e.codg_evnt = ".$res_ev[codg_evnt]." AND e.codg_evnt = eap.codg_evnt AND fini_aper >= '".date('Y-m-d')."' AND MONTH(fini_aper) = ".$mes_aux." AND YEAR(fini_aper) = ".$anno_aux." ORDER BY eap.fini_aper ASC";
													$bus_ap = mysql_query($sql_ap);
													echo '<td align="center">';
													while($res_ap=mysql_fetch_array($bus_ap)){
														echo date('d',strtotime($res_ap[fini_aper]));
														echo ' al ';
														echo date('d/m',strtotime($res_ap[ffin_aper]));
														echo '<br>';
													}
													echo '</td>';
													$fecha_act = strtotime ( '+1 month', $fecha_act);

												}
												echo '</tr>';
											}
										?>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<br>
					<?php 
						if ($numero_online>0){
							$color++;
						}
					?>
					<!-- adaptación especial para los diplomados -->
					<div style="height: <?php echo $alto; ?>;" id="diplomados">
						<div style="background-color: <?php echo $colores[$color][0]; ?>; float:left; height: <?php echo $alto; ?>; width: 150px;" class="titulo-cat-trim">
							<div class="texto-vertical" style="width: <?php echo $alto; ?>; margin-top: <?php echo $alto; ?>;">
								<span class="x17">Área</span><br>
								<span class="x2">Diplomados</span>
							</div>
						</div>
						<div style=" width: 88%; position:absolute; float:left; z-index:999; margin-left: 7em; margin-top: 1.1em; background-color: #FFF; height: <?php echo $alto2; ?>; border: 2px solid <?php echo $colores[$color][0]; ?>;padding: 10px; overflow: auto;">
							<div class="">
								<table class="table">
									<tr>
										<th>&nbsp;</th>
										<?php 
											$fecha_max = strtotime($fecha_fin);
											$fecha_act = strtotime($fecha_ini);
											$i = 0;
											while($fecha_act <= $fecha_max){
												$indicador = ceil(date('m',$fecha_act));
												echo '<th><div class="x1.7" style="color: #ffffff; text-align: center; padding: 5px;background-color: '.$colores[$color][$i].'">'.$meses[$indicador-1].'</div></th>';
												$fecha_act = strtotime ( '+1 month', $fecha_act);
												$i += 1;
												if ($i > 1){
													$i = 0;
												}
											}
											//// consultamos las aperturas en el rango de fechas
											$sql_ev = "SELECT * FROM eventos e, eventos_areas ea, eventos_apertura eap WHERE e.codg_area=ea.codg_area AND e.codg_evnt = eap.codg_evnt AND fini_aper >= '".date('Y-m-d')."' AND fini_aper >= '".$fecha_ini."' AND fini_aper <= '".$fecha_fin."' AND codg_tipo = 2 GROUP BY e.codg_evnt ORDER BY e.nomb_evnt ASC";
											$bus_ev = mysql_query($sql_ev);
											$numero_diplomado = mysql_num_rows($bus_ev);
											while($res_ev = mysql_fetch_array($bus_ev)){
												echo '<tr class="x08"><td style="min-width: 300px;">'.$res_ev[nomb_evnt].'<br><b><a href="evento.php?tipo='.$res_ev[codg_tipo].'&area='.$res_ev[codg_area].'&evento='.$res_ev[codg_evnt].'&frm='.$tipo.'" style="color: '.$colores[$color][$i].';">Ver Evento</a></b></td>';
												$fecha_act = strtotime($fecha_ini);
												while($fecha_act <= $fecha_max){
													$mes_aux = ceil(date('m',$fecha_act));
													$anno_aux = ceil(date('Y',$fecha_act));
													$sql_ap = "SELECT * FROM eventos e, eventos_apertura eap WHERE e.codg_evnt = ".$res_ev[codg_evnt]." AND e.codg_evnt = eap.codg_evnt AND fini_aper >= '".date('Y-m-d')."' AND MONTH(fini_aper) = ".$mes_aux." AND YEAR(fini_aper) = ".$anno_aux." ORDER BY eap.fini_aper ASC";
													$bus_ap = mysql_query($sql_ap);
													echo '<td align="center">';
													while($res_ap=mysql_fetch_array($bus_ap)){
														echo date('d',strtotime($res_ap[fini_aper]));
														echo ' al ';
														echo date('d/m',strtotime($res_ap[ffin_aper]));
														echo '<br>';
													}
													echo '</td>';
													$fecha_act = strtotime ( '+1 month', $fecha_act);

												}
												echo '</tr>';
											}
										?>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<br>
					<?php
					while($areas=mysql_fetch_array($bus_areas)){
						$color += 1;
						if ($areas[nomb_area2]!='Tributaria'){
							if ($areas[nomb_area2]=='Contable'){
								$areas[nomb_area] = 'Contable y Tributaria';
							}
						?>
							<div style="height: <?php echo $alto; ?>;">
								<div style="background-color: <?php echo $colores[$color][0]; ?>; float:left; height: <?php echo $alto; ?>; width: 150px;" class="titulo-cat-trim">
									<div class="texto-vertical" style="width: <?php echo $alto; ?>; margin-top: <?php echo $alto; ?>;">
										<span class="x17">Área</span><br>
										<span class="x2"><?php echo $areas[nomb_area]; ?></span>
									</div>
								</div>
								<div style=" width: 88%; position:absolute; float:left; z-index:999; margin-left: 7em; margin-top: 1.1em; background-color: #FFF; height: <?php echo $alto2; ?>; border: 2px solid <?php echo $colores[$color][0]; ?>;padding: 10px; overflow: auto;">
									<div class="">
										<table class="table">
											<tr>
												<th>&nbsp;</th>
												<?php 
													$fecha_max = strtotime($fecha_fin);
													$fecha_act = strtotime($fecha_ini);
													$i = 0;
													while($fecha_act <= $fecha_max){
														$indicador = ceil(date('m',$fecha_act));
														echo '<th><div class="x1.7" style="color: #ffffff; text-align: center; padding: 5px;background-color: '.$colores[$color][$i].'">'.$meses[$indicador-1].'</div></th>';
														$fecha_act = strtotime ( '+1 month', $fecha_act);
														$i += 1;
														if ($i > 1){
															$i = 0;
														}
													}
													//// consultamos las aperturas en el rango de fechas
													$sql_ev = "SELECT * FROM eventos e, eventos_areas ea, eventos_apertura eap WHERE e.codg_area=ea.codg_area AND ea.codg_area = ".$areas[codg_area]." AND e.codg_evnt = eap.codg_evnt AND fini_aper >= '".date('Y-m-d')."' AND fini_aper >= '".$fecha_ini."' AND fini_aper <= '".$fecha_fin."' GROUP BY e.codg_evnt ORDER BY e.nomb_evnt ASC";
													$bus_ev = mysql_query($sql_ev);
													while($res_ev = mysql_fetch_array($bus_ev)){
														echo '<tr class="x08"><td style="min-width: 300px;">'.$res_ev[nomb_evnt].'<br><b><a href="evento.php?tipo='.$res_ev[codg_tipo].'&area='.$res_ev[codg_area].'&evento='.$res_ev[codg_evnt].'&frm='.$tipo.'" style="color: '.$colores[$color][$i].';">Ver Evento</a></b></td>';
														$fecha_act = strtotime($fecha_ini);
														while($fecha_act <= $fecha_max){
															$mes_aux = ceil(date('m',$fecha_act));
															$anno_aux = ceil(date('Y',$fecha_act));
															$sql_ap = "SELECT * FROM eventos e, eventos_apertura eap WHERE e.codg_evnt = ".$res_ev[codg_evnt]." AND e.codg_evnt = eap.codg_evnt AND fini_aper >= '".date('Y-m-d')."' AND MONTH(fini_aper) = ".$mes_aux." AND YEAR(fini_aper) = ".$anno_aux." ORDER BY eap.fini_aper ASC";
															$bus_ap = mysql_query($sql_ap);
															echo '<td align="center">';
															while($res_ap=mysql_fetch_array($bus_ap)){
																echo date('d',strtotime($res_ap[fini_aper]));
																echo ' al ';
																echo date('d/m',strtotime($res_ap[ffin_aper]));
																echo '<br>';
															}
															echo '</td>';
															$fecha_act = strtotime ( '+1 month', $fecha_act);

														}
														echo '</tr>';
													}
												?>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<br>
						<?php
						}
					}
				?>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  
<?php 
	if ($numero_online<=0){
		echo '<script>$("#online").hide()</script>';
	} 
	if ($numero_diplomado<=0){
		echo '<script>$("#diplomados").hide()</script>';
	}
?>