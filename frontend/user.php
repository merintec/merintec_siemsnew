<?php
session_start();

	$url_base = "../";
	$url_base2 = "../../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
	include($url_base.'sistema/comunes/verificar_user.php');
	$url_retorno = $_GET[retorno];
	$url_retorno = str_replace('***', '&', $url_retorno);
	$url_retorno = str_replace('**', '?', $url_retorno);

?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
         <script src="../bootstrap/js/jquery.js"> </script>
        <script src="../sistema/validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		<script src="../sistema/validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="../sistema/validacion/css/validationEngine.jquery.css" type="text/css"/>
		<link rel="stylesheet" href="../sistema/validacion/css/template.css" type="text/css"/>
	
		 <!-- validacion en vivo -->
        <script >
          jQuery(document).ready(function(){
	    // binds form submission and fields to the validation engine
              jQuery("#sesion").validationEngine('attach', {bindMethod:"live"});
          });
        </script>
        <script type="text/javascript">
        	function ir_opcion(origen,destino){
        		$('.activo').hide();
        		$('#act_' + origen).show();
	            $(".usuarios-menu .opcion").css("color","");
	            $(".usuarios-menu .opcion").css("background-color","");
	            $("#op_" + origen).css("color","#e5e5e5");
	            $("#op_" + origen).css("background-color","#4645a3");
	            abrir_pagina(destino);
        	}

        	function abrir_pagina(destino){
				var parametros = {
					"aux" : "ejemplo"
				};
	            url =  '<?echo $url_base; ?>sistema/formularios/' + destino + '.php';
		      	$.ajax
		      	({
		        	type: "POST",
		          	url: url,
		          	data: parametros,
		          	beforeSend: function () {
		          		var espera = "<?php include($url_base.'sistema/comunes/cargando.php'); ?>";
				    	$("#usuarios-contenidos").html(espera);
				    },
		          	success: function(data)
		          	{
		       			$('#usuarios-contenidos').html(data);
						$('html,body').animate({
							scrollTop: $("#inicio").offset().top
						}, 1000);
		          	}
		      	});
		      	return false;
		    }
			function pagar_inscripcion(inscripcion){
				var parametros = {
					"codg_insc" : inscripcion
				};
		        url =  '<?echo $url_base; ?>sistema/formularios/inscripcion_pagar_new.php';
		      	$.ajax
		      	({
		        	type: "POST",
		          	url: url,
		          	data: parametros,
		          	beforeSend: function () {
		          		var espera = "<?php include($url_base2.'sistema/comunes/cargando.php'); ?>";
				    	$("#usuarios-contenidos").html(espera);
				    },
		          	success: function(data)
		          	{
		       			$('#usuarios-contenidos').html(data);
		          	}
		      	});
		      	return false;
		    }
        </script>
	</head>
	<body>
	<header>
		<?php 
			$imagen_slide = $url_base.'imagenes/page/pantalla_sesion.jpg';
			//$menu_active = 'contacto';
			include ($url_base.'frontend/header4.php'); 
		?>		
	</header>
	<div id="inicio" class="usuarios-base">
		<div class="usuarios-posicion-flotante">			
			<div class="row-fluid">
				<div class="usuarios-cinta"></div>
			</div>
			<div class="row usuarios-contenedor">
				<div class="col-md-2 col-xs-3 usuarios-menu">
					<div class="opcion" id="op_perfil" onclick="window.location=('user.php')">
						<div class="imagen">
							<span class="glyphicon glyphicon-user"></span>
						</div>
						<div class="etiqueta">PERFIL</div>
						<div class="activo" id="act_perfil"></div>
					</div>
					<div class="opcion" id="op_historico" onclick="ir_opcion('historico','participantes_historial_new');">
						<div class="imagen">
							<span class="glyphicon glyphicon-list-alt"></span>
						</div>
						<div class="etiqueta">HISTÓRICO</div>
						<div class="activo" id="act_historico"></div>
					</div>
					<div class="opcion" id="op_pagos" onclick="ir_opcion('pagos','participantes_pagos_new');">
						<div class="imagen">
							<span class="glyphicon glyphicon-usd"></span>
						</div>
						<div class="etiqueta">PAGOS</div>
						<div class="activo" id="act_pagos"></div>
					</div>
					<div class="opcion" id="op_pass" onclick="ir_opcion('pass','cambio_pass');">
						<div class="imagen">
							<span class="glyphicon glyphicon-asterisk"></span>
						</div>
						<div class="etiqueta">CONTRASEÑA</div>
						<div class="activo" id="act_pass"></div>
					</div>
				</div>
				<div class="col-md-9 col-xs-8">
					<div id="usuarios-contenidos" class="usuarios-contenidos" style="height: 50em;"></div>
				</div>
		    </div>
		</div>
	</div>
	<?php if ($_GET[url]=='pagar'){
			echo "	<script>ir_opcion('pagos','participantes_pagos_new');</script>";
		}
		else{
			echo "	<script>ir_opcion('perfil','participantes_perfil');</script>";	
		}
	?>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  