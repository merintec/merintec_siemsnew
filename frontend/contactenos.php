<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');

	$boton=$_POST['boton'];
	$titulo=$_POST['titulo'];
	$nombre=$_POST['nombre'];
	$telefono=$_POST['telefono'];
	$correo=$_POST['correo'];
	$contenido=$_POST['contenido'];

	if ($boton=='Enviar'){
	mail("siems@siems.com", $titulo, $nombre.": "."\r\n".$contenido, $correo . "\r\n" .$correo. "\r\n");
	$mensaje_mostrar="Su mensaje ha sido enviado con exito";

	//guardamos el mensaje 
	$fecha=DATE('Y-m-d');
	

	 $insertar=mysql_query("INSERT INTO contacto (titulo,nombre,correo,contenido,fecha,telefono) VALUES 
	 	('$titulo','$nombre','$correo','$contenido','$fecha','$telefono') ");

}
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="../imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/estilo.css">
        <script src="../bootstrap/js/jquery.js"> </script>

		<script src="../sistema/validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		<script src="../sistema/validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="../sistema/validacion/css/validationEngine.jquery.css" type="text/css"/>
		<link rel="stylesheet" href="../sistema/validacion/css/template.css" type="text/css"/>
	
		 <!-- validacion en vivo -->
        <script >
          jQuery(document).ready(function(){
	    // binds form submission and fields to the validation engine
              jQuery("#contacto").validationEngine('attach', {bindMethod:"live"});
          });
        </script>

	</head>
	<body>
	<header>
			<?php 
			$texto_slide = 'CONTACTO';
			$menu_active = 'contacto';
			$imagen_slide = $url_base.'imagenes/page/contacto.jpg';
			include ($url_base.'frontend/header2.php'); 
		?>
	</header>
	<section>
	<br>
	<?php 
			if ($mensaje_mostrar!=NULL) 
			{ 
				echo '<div id="mensaje" class="alert alert-info" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$mensaje_mostrar.'</div>';
				echo '<script>setTimeout(function() { 	$("#mensaje").fadeOut(1500);	},4000); </script>';

			} 
	?>
	<div class="container">
	   <div class="col-md-3 col-xs-2">   &nbsp;  </div>

       <div class="col-md-6 col-xs-10">
 		<div class="cajacontacto"  style="margin-top: 2em;">
 			<div class="row">
 				<div class="cinta"></div>
 			</div>
 			<br>
            <form method="POST" name="contacto" id="contacto" onsubmit="return jQuery(this).validationEngine('validate');">
                <div class="row">
                    
                    		<div class="input-group">
	                    		   <span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-asunto.png"> </span>
                                   <input type="text" name="titulo" id="titulo"  placeholder="Asunto" class="validate[required, minSize[3],maxSize[100]] text-input form-control campo_contacto" >
               				</div>
               		
                </div>
                <br>
                <div class="row">
                    
                    		<div class="input-group">
	                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-nombre.png"> </span>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="validate[required, minSize[3],maxSize[100]] text-input form-control campo_contacto" >
                   			</div>
                    
                </div>
                <br>

                <div class="row">
                    
                    		<div class="input-group">
	                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-telefono.png"> </span>
                                <input type="text" name="telefono" id="telefono" placeholder="Teléfono" class="validate[required, custom[phone] , minSize[3],maxSize[100]] text-input form-control campo_contacto " >
                    		</div>
                    
                </div>
                <br>

                <div class="row">
                    
	                    <div class="input-group">
	                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-correo.png"> </span>
	                   			<input type="email" name="correo" id="correo" placeholder="Email" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input form-control campo_contacto" >
	                   	</div>
                    
                </div>
                <br>

                <div class="row">
                	
                				   <div  class="etiqueta_solicitud">  <img src="../imagenes/page/ico-star.png"> <label for="contenido" class="campo_contacto" > Solicitud: </label> </div>
	                               <textarea  name="contenido" id="contenido"    rows="7"  class="validate[required], text-input form-control estilo_text"> </textarea>
	                 

                </div>
                 <br>


	            <div class="row">

	                
	                        <div align="center"> <input type="submit" name="boton" id="boton" value="Enviar" class="btn fondo_boton" >
	            	
	            </div>
            </form>
        </div>
    </div>
   </div>






	</section>
	<footer>
		<?php include ('footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="../bootstrap/js/bootstrap.min.js"> </script>  