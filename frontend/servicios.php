<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'SERVICIOS Y CONSULTORÍA';
			$imagen_slide = $url_base.'imagenes/page/servicios.jpg';
			$menu_active = 'servicios';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container">
			<div class="row">
				<br><br>
				<div class="col-md-11 col-xs-10 pull-right serv-ol">
					<?php $texto = '<div class="serv-subtitulo x12">Servicio de Publicación de Búsquedas Laboral</div>
Siems apoya a sus clientes empresariales, al poner a su disposición sin costo, el servicio de publicación de solicitudes de búsqueda de personal en nuestras bases de datos de estudiantes, egresados y fans en redes sociales.  Con ello se hace certero, ágil este proceso.
<div class="serv-subtitulo x12">Proyectos y Consultoría</div>
A lo largo de nuestra trayectoria como empresa aliada de aprendizaje y consultoría hemos contado con la confianza, receptividad de una importante cantidad de organizaciones. A las que ofrecemos a más de 80 especialistas de distintas áreas, con los cuales prestamos los siguientes servicios:
<ol>
	<li><span class="normal">Proyectos de  Diseño Organizacional: Manuales de Cargos, sistemas y procedimientos.</span></li>
	<li><span class="normal">Proyectos de Gestión de las Calidad  bajo normas ISO 9000.</span></li>
	<li><span class="normal">Proyectos de Implementación de las 5 S: Seguridad Orden y Limpieza </span></li>
	<li><span class="normal">Proyectos de valoración de cargos  y diseño de estructuras salariales</span></li>
	<li><span class="normal">Proyectos de medición de clima organizacional he intervención a través de coaching.</span></li>
	<li><span class="normal">Proyectos de Estructuras de Costos para empresa industriales, comerciales y de servicios</span></li>
	<li><span class="normal">Proyectos de  adaptación de la contabilidad a las Normas Internacionales de Información Financiera NIIF.</span></li>
	<li><span class="normal">Proyectos de generación de marcas y diseño corporativo</span></li>
	<li><span class="normal">Proyectos de Mejoras en Gestión de Almacenes y Control de Inventarios </span></li>
	<li><span class="normal">Proyectos de acompañamiento a emprendedores en el desarrollo de las ideas de negocio, y coaching ejecutivo.</span></li>
	<li><span class="normal">Proyectos de Gestión de Valor Ganado y Project Management</span></li>
	<li><span class="normal">Proyectos de asesoría en materia laboral</span></li>
</ol>'; 
	$texto2 = 'Te apoyamos en el proyecto que requieras.<br class="hidden-xs">
Escríbenos sobre tu necesidad a: <b><a href="mailto:jmolina@siems.com.ve">jmolina@siems.com.ve</a></b> ';
	?>
					<div class="hidden-xs outd-texto line-x3 x12"><?php echo $texto; ?></div>
					<div class="visible-xs outd-texto line-x2"><?php echo $texto; ?></div>
					<div class="hidden-xs serv-titulo-inf line-x2 x2"><?php echo $texto2; ?></div>
					<div class="visible-xs serv-titulo-inf line-x15 x1"><?php echo $texto2; ?></div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  