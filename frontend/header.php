<!-- Logo -->
<a href="<?php echo $url_base; ?>index.php">
  <div id="logo" class="hidden-xs logo" title="<?php echo $logo_title; ?>">&nbsp;</div>
  <div id="logo" class="visible-xs logo-xs" title="<?php echo $logo_title; ?>">&nbsp;</div>
</a>
<!-- Panel superior -->
<div id="panel_top" class="panel_top">
  <div class="container-fluid">
    <div class="row"> 
      <?php include($url_base.'frontend/sesion.php'); ?>
    </div>
    <div class="row">
      <?php include($url_base.'frontend/menu.php'); ?>
    </div>
  </div>
</div>
<!-- Slider -->
<div id="slider">
  <div id="carousel1" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <!-- Social Net -->
      <div id="social-net" class="hidden-xs hidden-xs social">
        <?php include($url_base.'frontend/social.php'); ?>
      </div>
      <div id="social-net" class="visible-xs social social-fixed">
        <?php include($url_base.'frontend/social.php'); ?>
      </div>
      <!-- fin social -->
      <!-- incio de generación de slides seguin Base de datos -->
      <?php
        // buscar el slide indicado si no se indica slide el buscará el activo
        if ($show_slide){
          $sql_slides = "SELECT * FROM slides s, slides_images sm WHERE s.nomb_slide='".$show_slide."' AND s.codg_slide=sm.codg_slide AND stat_imag='Activa' ORDER BY ordn_imag";
        }
        else{        
          $sql_slides = "SELECT * FROM slides s, slides_images sm WHERE s.pred_slide='S' AND s.codg_slide=sm.codg_slide AND stat_imag='Activa' ORDER BY ordn_imag";
        }
        $bus_slides = mysql_query($sql_slides);
        $cuenta_slides = 0;
        while ($slides = mysql_fetch_array($bus_slides)){
          $cuenta_slides += 1;
          $activo = '';
          $direccion = '';
          if ($cuenta_slides==1){
            $activo = 'active';
          }
          $direccion=split("://", $slides[urls_imag]);
          if ($direccion[0]=='http' || $direccion[0]=='HTTP'){
            $direccion = $slides[urls_imag];
          }
          else{
            $direccion = $url_base.''.$direccion[0];
          }
      ?>
          <div class="item <?php echo $activo; ?>" style="margin-bottom: -10px;">
            <a href="<?php echo $direccion; ?>" target="<?php echo $slides[targ_imag]; ?>">
              <img src="<?php echo $url_base; ?><?php echo $slides[imag_imag]; ?>" width="100%">
              <div class="hidden-xs carousel-caption carouse-titulo x25 marg-menos3r"><?php echo $slides[titl_imag]; ?></div>
              <div class="visible-xs carousel-caption carouse-titulo x1 marg-menos1r"><?php echo $slides[titl_imag]; ?></div>
            </a>
          </div>
      <?php
        }
      ?>
      <!-- fin de slides -->
      <!-- Indicators -->
      <?php
        if ($cuenta_slides>1) {
          echo '<ol class="carousel-indicators">';
          for ($i=1;$i<=$cuenta_slides;$i++){
            echo '<li data-target="#carousel1" data-slide-to="'.($i-1).'" '; if ($i==1) { echo 'class="active"'; } echo '></li>';            
          } 
          echo '</ol>';
          //<!-- Left and right controls -->
          //echo '<a class="left carousel-control" href="#carousel1" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>';
          echo '<a class="right carousel-control" href="#carousel1" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>';
        } 
      ?>
    </div>   
  </div>
</div>