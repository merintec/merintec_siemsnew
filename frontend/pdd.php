<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'POLÍTICAS DE REEMBOLSO';
			$imagen_slide = $url_base.'imagenes/page/faq.jpg';
			$menu_active = 'siems';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
	<br>
	<br>

		<div class="container">

			<div class="col-md-12 col-xs-11 pull-right">
				

<div class="faq-sub">TÉRMINOS DE REEMBOLSO ESTÁNDAR</div>
<br>
<div class="faq-text line-x2">Los servicios que se adquieran de <a href="index.php">siems.com.ve</a>, se pueden reembolsar solo  si se cancelan en el siguiente período de tiempo:
<br>
<span class="faq-num">Cursos:</span> dentro de los 30 días posteriores a la fecha de la transacción si el curso no se lleva a cabo.
<br>
<span class="faq-num">Congresos:</span> dentro de los 90 posteriores a la fecha de la transacción si el congreso se suspende por razones atribuibles a SIEMS/Fundaceo.
<br><br>
<span class="faq-num">Fecha de la transacción:</span> a los fines de esta Política de reembolso, se refiere a la fecha de pago de cualquier producto o servicio.
<br><br>Puedes cancelar un servicio en cualquier momento, pero el reembolso solo se emitirá si la cancelación se solicita dentro del período de tiempo de reembolso especificado para el producto correspondiente, y el servicio no se realiza.
</div>
<br><br>
Revisado: 13/05/16
<br><br>
<center>Copyright © 2016  <a href="index.php">www.siems.com.ve</a>,  Todos los derechos reservados.</center>
					
					<BR>
					<BR>


				
					
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  