<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'CLIENTES';
			$imagen_slide = $url_base.'imagenes/page/clientes.jpg';
			$menu_active = 'siems';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container" style="margin-top: 4em; margin-bottom: 4em;">
			<div class="row">
				<div class="col-md-12 col-xs-11 pull-right">
					<?php $titulo = "A LO LARGO DE NUESTRA TRAYECTORIA COMO<br>INSTITUCIÓN HEMOS CONTADO CON LA CONFIANZA Y RECEPTIVIDAD<br> DE MUCHAS ORGANIZACIONES, ENTRE LAS QUE DESTACAN:"; ?>
					<?php $titulo_xs = "A LO LARGO DE NUESTRA TRAYECTORIA COMO INSTITUCIÓN HEMOS CONTADO CON LA CONFIANZA Y RECEPTIVIDAD DE MUCHAS ORGANIZACIONES, ENTRE LAS QUE DESTACAN:"; ?>
					<div class="hidden-xs home-titulo x17 line-x25"><?php echo $titulo; ?></div>
					<div class="visible-xs home-titulo x1"><?php echo $titulo_xs; ?></div>
				</div>	
			</div>
		</div>
		<br><br>
		<div class="container" style="margin-bottom: 4em;">
			<div class="row">
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/01.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/02.png"></div>
				<div class="visible-xs col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/03.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/04.png"></div>
				<div class="col-md-12 col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/05.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/06.png"></div>
				<div class="visible-xs col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/07.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/08.png"></div>
				<div class="col-md-12 col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/09.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/10.png"></div>
				<div class="visible-xs col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/11.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/12.png"></div>
				<div class="col-md-12 col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/13.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/14.png"></div>
				<div class="visible-xs col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/15.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/16.png"></div>
				<div class="col-md-12 col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/17.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/18.png"></div>
				<div class="visible-xs col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/19.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/20.png"></div>
				<div class="col-md-12 col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/21.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/22.png"></div>
				<div class="visible-xs col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/23.png"></div>
				<div class="col-md-3 col-xs-5 centrado-centrado"><img class="clientes-img" title="Clientes" src="<?php echo $url_base?>imagenes/clientes/24.png"></div>
				<div class="col-md-12 col-xs-12">&nbsp;</div>
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  