<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'CURSOS';
			$imagen_slide = $url_base.'imagenes/page/cursos.jpg';
			$menu_active = 'cursos';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-11 pull-right">
				<?php 
					$sql_areas = "SELECT * FROM eventos_areas WHERE stat_area = 'Activo' ORDER BY nomb_area";
					$bus_areas = mysql_query($sql_areas);
					$cuenta = 1;
					echo '<br><br>';
					while($areas=mysql_fetch_array($bus_areas)){
						if ($areas[nomb_area]!='Tributaria'){
							if ($areas[nomb_area]=='Contable'){
								$areas[nomb_area] = 'Contable y Tributaria';
							}
							echo '<a href="evento.php?tipo=&area='.$areas[codg_area].'&evento=#punto"><div class="col-md-3 col-xs-6" style="margin-bottom: 30px;">
								<div class="curso-ico-contenido centrado-centrado">
									<img class="visible-xs" height="50px" src="'.$url_base.''.$areas[imag_area].'" style="margin: auto auto; margin-bottom: 10px">
									<img class="hidden-xs" height="100px" src="'.$url_base.''.$areas[imag_area].'"><br class="hidden-xs"><br class="hidden-xs">
									<span class="hidden-xs x12"><b>'.$areas[nomb_area].'<b></span>
									<span class="visible-xs x075"><b>'.$areas[nomb_area].'<b></span>
								</div>
								<div class="curso-ico">		
									<img width="100%" src="'.$url_base.'imagenes/page/fondo-cursos.png">
								</div>
							</div></a>';
						}
					}
				?>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  