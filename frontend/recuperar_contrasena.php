<?php
$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	include_once($url_base.'sistema/comunes/funciones_js.php'); 

$boton=$_POST['boton'];
$correo=$_POST['correo'];

/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "usuarios";

if ($boton=='Enviar'){
	$mensaje_mostrar='';
	$reg=registro_valor($tabla, '*', 'WHERE corr_usua="'.$correo.'"');
	if($reg!=""){
		$clave = texto_aleatorio(6);
		$sql='update usuarios set pass_usua="'.md5($clave).'"WHERE corr_usua="'.$correo.'"';
		if(mysql_query($sql)){
			$envio = mail($correo, "Recuperacion de Contraseña SIEMS", "Estimado usuario: ".$reg[nomb_usua]." ".$reg[apel_usua]." \n Le informamos que sus datos para ingreso a sistema de SIEMS son: \r\n Usuario: <b>".$reg[logi_usua]."</b> \r\n Nueva Contraseña: <b>".$clave."</b> \r\n Le recomendamos cambiar su clave al ingresar al Sistema.", 'From: infosiems@siems.com.ve' . "\r\n" .'Reply-To: infosiems@siems.com.ve' . "\r\n");
			$mensaje_mostrar="Nueva Contraseña enviada correctamente, favor verifique su email";
			$accion='info';
		}
	}
	else {
		$mensaje_mostrar="El correo especificado no se encuentra registrado. <br>Intente Nuevamente";
		$accion='danger';
	} 
}

?>


<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
        <script src="../bootstrap/js/jquery.js"> </script>

		<script src="../sistema/validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		<script src="../sistema/validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="../sistema/validacion/css/validationEngine.jquery.css" type="text/css"/>
		<link rel="stylesheet" href="../sistema/validacion/css/template.css" type="text/css"/>
	
		 <!-- validacion en vivo -->
        <script >
          jQuery(document).ready(function(){
	    // binds form submission and fields to the validation engine
              jQuery("#contacto").validationEngine('attach', {bindMethod:"live"});
          });
        </script>

	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'CONTACTO';
			$imagen_slide = $url_base.'imagenes/page/pantalla_sesion.jpg';
			//$menu_active = 'contacto';
			include ($url_base.'frontend/header3.php'); 
		?>		
	</header>


	


			<div class="posicion_flotante">
						
			 
				 		<div class="cajasesion">
				 		<?php 
						if ($mensaje_mostrar!=NULL) 
						{ 
							echo '<div id="mensaje" class="alert alert-'.$accion.'" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$mensaje_mostrar.'</div>';
							echo '<script>setTimeout(function() { 	$("#mensaje").fadeOut(1500);	},4000); </script>';

						} 
							?>
				 			<div class="row">
				 				<div class="cinta"></div>
				 			</div>
				 			<br>
				 			<div align="center" class="inco-titulo x37"> Recuperar Contraseña </div>
				 			<br>
				            <form method="POST" name="contacto" id="contacto" onsubmit="return jQuery(this).validationEngine('validate');">
				                <div class="row">
				                	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                	<div class="col-md-8 col-xs-9">
				                    
				                    		<div class="input-group">
					                    		<span class="input-group-addon fondo_boton" > <img width="20" src="../imagenes/page/ico-correo.png"> </span>
				                                <input type="text" name="correo" id="correo" placeholder="Email" class="validate[required, custom[email] minSize[3],maxSize[100]] text-input form-control campo_sesion" >
				                   			</div>
				                   	</div>
				                   	<div class="col-md-2 col-xs-1">
				                		&nbsp;
				                	</div>
				                    
				                </div>
				                <br>

				                

				                <div class="row">
		                
					                        <div align="center"> <input type="submit" name="boton" id="boton" value="Enviar" class="btn fondo_boton" >
					            	
					            </div>

					      


				            </form>
				        </div>
		    </div>




	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->

<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  