
<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'ACREDITACIONES';
			$imagen_slide = $url_base.'imagenes/page/acreditaciones.jpg';
			$menu_active = 'acreditaciones';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="hidden-xs home-titulo x37">SIEMS CUENTA CON <BR> LAS SIGUIENTES <BR> AUTORIZACIONES Y CONVENIOS :</div>
				<div class="visible-xs home-titulo x15">SIEMS CUENTA CON <BR> LAS SIGUIENTES <BR> AUTORIZACIONES Y CONVENIOS :</div>
				<br>
				<br>
			</div>
			<div class="row">

				<div class="col-md-12 col-xs-12">

						<div class="hidden-xs" style="margin-left:3em;"> <li class="vineta">  <span style="color:#000;" class="x12 letrauno"> Autorización del <b> Ministerio de Educación. </b>    </span>  </li> </div> 
						<div class="visible-xs" style="margin-left:1em;"> <li class="vineta">  <span style="color:#000;" class="x04 letrauno"> Autorización del <b> Ministerio de Educación. </b>    </span>  </li> </div> 

						<br>
						<div class="acreditacion1">
							
							<div style="padding: 3em;" align="center">
									<img class="img-responsive" src="../imagenes/page/acreditacion1.jpg">
							</div>

						</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="acreditacion2">

				<div class="col-md-8 col-xs-9">

						<div class="hidden-xs" style="margin-left:5em; margin-top:12em;"> <li class="vineta">  <span style="color:#000;" aling="center" class="x15 letrauno"> Autorización de <b> INCES </b> para Efectuar Adiestramiento Deducible a empresas a través de nuestra fundación FUNDACEO.    </span>  </li> </div> 
						<div class="visible-xs" style="margin-left:3.5em;"> <li class="vineta">  <span style="color:#000;" aling="center"  class="x04 letrauno"> Autorización de <b> INCES </b> para Efectuar Adiestramiento Deducible a empresas a través de nuestra fundación FUNDACEO.    </span>  </li> </div> 

						
				</div>
				<div class="col-md-4 col-xs-3">

						<div>
							
							<div class="hidden-xs" style="margin-top: 7em; margin-bottom: 7em;"  align="center">
									<img class="img-responsive" src="../imagenes/page/inces.jpg">
							</div>
							<div class="visible-xs" style="margin-top: 3em; margin-bottom: 1em;"   align="center">
									<img class="img-responsive" src="../imagenes/page/inces.jpg">
							</div>


						</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="acreditacion3 hidden-xs">
				<div class="col-md-4 col-xs-4">

					<div class="hidden-xs" style="margin-left:4em; margin-top:2.5em;"> <span style="color:#FFF;" aling="left" class="x3 letrauno"> CONVENIO <BR> DE <BR> CERTIFICACIÓN <BR> NACIONAL <BR>   </span>  <SPAN style="color:#FFF;" aling="left" class="x15"> EN DIPLOMADO <BR> EN COACHING ONTOLÓGICO </SPAN> </div> 


				</div>
				<div class="col-md-8 col-xs-8">

					<div class="hidden-xs" style="margin-top: 1.8em;"  align="center">
									<img class="img-responsive" src="../imagenes/page/upel.jpg">
					</div>


				</div>

			</div>


			<div class="acreditacion3c visible-xs">
				<div class="col-md-6 col-xs-6">


					<div class="visible-xs" style="margin-left:4em; margin-top:2.5em;"> <span style="color:#FFF;" aling="left" class="x04 letrauno"> CONVENIO <BR> DE <BR> CERTIFICACIÓN <BR> NACIONAL <BR>   </span>  <SPAN style="color:#FFF;" aling="left" class="x04 letrauno"> EN DIPLOMADO <BR> EN COACHING ONTOLÓGICO </SPAN> </div> 

				</div>
				<div class="col-md-6 col-xs-6">

					<div class="visible-xs" style="margin-top: 1.8em;"  align="center">
									<img class="img-responsive" src="../imagenes/page/upel.jpg">
					</div>

				</div>

			</div>
			
		</div>

		<div class="row">
			<div class="acreditacion2">

				<div class="col-md-4 col-xs-4">

					    <div class="hidden-xs" style="margin-left:7em; margin-top:7em;"> <span style="color:#5f4582;"  aling="left" class="x3 letrauno"> CONVENIO <BR> DE <BR> <B>CERTIFICACIÓN <BR> INTERNACIONAL </B>   </span>   </div> 

						 <div class="visible-xs" style="margin-left:3.5em; margin-top:3em;"> <span style="color:#5f4582;" aling="left" class="x04 letrauno"> CONVENIO <BR> DE <BR> <B>CERTIFICACIÓN <BR> INTERNACIONAL </B>   </span>   </div> 

				</div>
				<div class="col-md-8 col-xs-8">

						<div>
							
							<div class="hidden-xs" style="margin-top: 7em; margin-bottom: 7em;"  align="center">
									<img class="img-responsive" src="../imagenes/page/internacional.jpg">
							</div>
							<div class="visible-xs" style="margin-top: 2em; margin-bottom: 1em;"   align="center">
									<img class="img-responsive" src="../imagenes/page/internacional.jpg">
							</div>


						</div>
				</div>
			</div>
		</div>






	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  