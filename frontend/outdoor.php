<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = 'PROGRAMAS DE<BR>FORMACIÓN OUTDOOR';
			$imagen_slide = $url_base.'imagenes/page/outdoor.jpg';
			$menu_active = 'empresas';
			include ($url_base.'frontend/header2.php'); 
		?>		
	</header>
	<section>
		<?php $texto = 'Un programa de entrenamiento al aire libre va más allá de ofrecer cursos y juegos al aire libre, ya que se busca que el personal  asuma un compromiso de mejora con su empresa. Que el grupo detecte los puntos fuertes y débiles derivados de los comportamientos del equipo y vincular estas actitudes a las conductas habituales en el trabajo. Finalmente, se elabora un plan de acción con los diferentes aspectos a mejorar aplicables a la empresa.

<div class="outd-subtitulo">¿Cómo se desarrolla el proceso a través de ésta metodología?</div>
El entrenamiento al aire libre es un medio que consiste en juegos o actividades al aire libre o espacios abiertos, con una metodología propia de la educación experiencial, es decir, aprendizaje netamente vivencial, que presenta una secuencia lógica de actividades donde se extraen conclusiones que ayudan a mejorar el entorno personal y profesional. 
Es llevada a cabo facilitadores que basan el aprendizaje a través de la experiencia en un clima distendido, donde se llevan a cabo actividades que combinan la competitividad, el trabajo en equipo, el liderazgo y la comunicación, es decir aspectos relevantes para la gestión del capital social. 

<div class="outd-subtitulo">¿Qué beneficios brinda ésta metodología a las empresas?</div>
Los beneficios más relevantes están relacionados con las habilidades interpersonales propias de la inteligencia emocional que deben estar presente en las empresas, como son los siguientes:
· Estimula la confianza y desarrolla/potencia el espíritu de trabajo en equipo, para lograr la cooperación y complementación entre los miembros.
· Potencia el liderazgo y la capacidad de delegar responsabilidades.
· Motiva a los participantes en los compromisos del trabajo diario.
· Fomenta la comunicación e integración entre áreas y miembros de una organización.
· Aumenta la resistencia al estrés.
· Mejora la reacción de sagacidad en ingenio ante situaciones de presión y cambio.
· Aumenta la confianza entre compañeros.
· Entrena el enfoque de la atención y visión.';

$texto2 = '<div class="outd-titulo-inf">La riqueza que se obtiene de este tipo de experiencias impacta de manera positiva
en los participantes. Su gran aporte personal y grupal  establece un diferencial
con instancias tradicionales de capa citación orientadas a un estilo académico.
<br>¡ANÍMATE A VIVIRLO!</div>'; ?>
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-xs-10 pull-right" style="padding-top: 2em;">
					<div class="hidden-xs outd-titulo x3">EL ENTRENAMIENTO AL AIRE LIBRE:<br>MUCHO MÁS QUE UN JUEGO</div>
					<div class="visible-xs outd-titulo x1">EL ENTRENAMIENTO AL AIRE LIBRE:<br>MUCHO MÁS QUE UN JUEGO</div>
					<div class="hidden-xs outd-texto line-x3 x12"><?php echo $texto; ?></div>
					<div class="visible-xs outd-texto line-x2"><?php echo $texto; ?></div>
					<div class="hidden-xs outd-titulo-inf line-x2 x15"><?php echo $texto2; ?></div>
					<div class="visible-xs outd-titulo-inf line-x15 x1"><?php echo $texto2; ?></div>
				</div>
			</div>
		</div>
		<div>
			<div class="out-mod-contenido">
	          <div class="carouse-titulo-page x4">
	            <div class="carouse-contenedor-border-titulo" style="margin-left: 16%; margin-right: 16%">
	              <div class="hidden-xs carouse-contenedor-titulo-page" style="padding-top: 15px; padding-bottom: 15px;">MODALIDAD</div>
	              <div class="visible-xs carouse-contenedor-titulo-page x05" style="padding-top: 5px; padding-bottom: 5px;">MODALIDAD</div>
	            </div>
	            <div class="hidden-xs linea-vertical" style="height: 20px"></div>
	            <div class="hidden-xs circulo"></div>
	          </div>
			</div>
			<div>
				<img width="100%" src="<?php echo $url_base?>imagenes/page/outdoor-modalidad.jpg">
			</div>
		</div>
		<div class="container" style="margin-top: 4em; margin-bottom: 4em;">
			<div class="row">
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5"><a href="<?php echo $url_base?>pdf/Outdoor_Rafting_y_Tirolina.pdf" boder="0px" target="_blank"><img border="0px" width="90%" title="Raftign y Triola" src="<?php echo $url_base?>imagenes/page/outdoor-rafting.png"></a></div>
				<div class="col-md-3 col-xs-5"><a href="<?php echo $url_base?>pdf/Outdoor_Zona_Liquida.pdf" boder="0px" target="_blank"><img boder="0px" width="90%" title="Zona Líquida" src="<?php echo $url_base?>imagenes/page/outdoor-zona.png"></a></div>
				<div class="visible-xs col-xs-12">&nbsp;</div>
				<div class="visible-xs col-xs-2"></div>
				<div class="col-md-3 col-xs-5"><a href="<?php echo $url_base?>pdf/Outdoor_Sendero_del_Equipo.pdf" boder="0px" target="_blank"><img boder="0px" width="90%" title="Sendero del Equipo" src="<?php echo $url_base?>imagenes/page/outdoor-sendero.png"></a></div>
				<div class="col-md-3 col-xs-5"><a href="<?php echo $url_base?>pdf/Outdoor_Tecnicas_Manejo_Estres_Laboral.pdf" boder="0px" target="_blank"><img boder="0px" width="90%" title="Estrés Laboral" src="<?php echo $url_base?>imagenes/page/outdoor-stres.png"></a></div>
			</div>
		</div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/jquery.js"> </script>
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>