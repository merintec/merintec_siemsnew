<?php
	$url_base = "../";
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	include_once($url_base.'sistema/comunes/funciones_js.php'); 
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base?>css/estilo.css">
        <script src="../bootstrap/js/jquery.js"> </script>

		<script src="../sistema/validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		<script src="../sistema/validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="../sistema/validacion/css/validationEngine.jquery.css" type="text/css"/>
		<link rel="stylesheet" href="../sistema/validacion/css/template.css" type="text/css"/>
	
		 <!-- validacion en vivo -->
        <script >
          jQuery(document).ready(function(){
	    // binds form submission and fields to the validation engine
              jQuery("#registrar_interesado").validationEngine('attach', {bindMethod:"live"});
          });
        </script>


        <script>
        	function reservar(tipo,apertura){
        		var parametros = {
        			"codg_aper" : apertura
        		};
        		if (tipo=='usuario'){
        			var url = "<?php echo $url_base; ?>sistema/formularios/reservar_new.php"; 
        		}
        		else{
        			var url = "<?php echo $url_base; ?>sistema/formularios/empresa_reservar_new.php";
        		}
        		$.ajax
		      	({
		        	type: "POST",
		          	url: url,
		          	data: parametros,
		          	beforeSend: function () {
		          		//console.log('probando');
		          		var espera = "<?php include($url_base.'sistema/comunes/cargando.php'); ?>";
				    	$("#contenido_modal").html(espera);
				    },
		          	success: function(data)
		          	{
		          		//console.log(data);
		       			$('#contenido_modal').html(data);
		          	}
		      	});
		      	return false;
        	}
        	function registro_reserva(){
        		var tipo_insc = '';
        		if ($('input:radio[name=tipo_insc]:checked').val()){
        			tipo_insc = $('input:radio[name=tipo_insc]:checked').val();
        		}
        		var parametros = {
					"elegir" : $('#elegir').val(),
					"tipo_insc" : tipo_insc,
					"base_insc" : $('#base_insc').val(),
					"codg_aper" : $('#codg_aper').val(),
					"boton" : "Inscribir"
				};
       			var url = "<?php echo $url_base; ?>sistema/formularios/reservar_new.php"; 
		      	$.ajax
		      	({
		        	type: "POST",
		          	url: url,
		          	data: parametros,
		          	beforeSend: function () {
		          		//console.log('probando');
		          		var espera = "<?php include($url_base.'sistema/comunes/cargando.php'); ?>";
				    	$("#mensaje_espera").html(espera);
				    	$("#mensaje_espera").show();
				    },
		          	success: function(data)
		          	{
		          		$("#mensaje_espera").html('');
		          		$("#mensaje_espera").hide();	
		          		data = data.split(":::");
		          		//console.log(data);
		       			$('#mensaje_resultado').html(data[0]);
		       			if (data[1]!='NO'){
			       			$('#formulario').hide();
		       			}
		          	}
		      	});
		      	return false;
        	}

        	function registro_interesado(){
        		if ($('#registrar_interesado').validationEngine('validate')){
        		var parametros = {
					"nomb_intr" : $('#nomb_intr').val(),
					"tlfn_intr" : $('#tlfn_intr').val(),
					"corr_intr" : $('#corr_intr').val(),
					"apertura" : $('#codg_aper').val(),
					"codg_evnt" : "<?php echo $_GET[evento]; ?>",
					"boton" : "Enviar"
				};
				var url = "<?php echo $url_base; ?>sistema/formularios/conocer_mas_new.php"; 
		      	$.ajax
		      	({
		        	type: "POST",
		          	url: url,
		          	data: parametros,
		          	beforeSend: function () {
		          		//console.log('probando');
		          		var espera = "<?php include($url_base.'sistema/comunes/cargando.php'); ?>";
				    	$("#mensaje_resultado").html(espera);
				    },
		          	success: function(data)
		          	{
		          		//console.log(data);
		       			$('#mensaje_resultado').html(data);
		       			$('#formulario').hide();
		          	}
		      	});
		      	return false;
        		}
        	}
        	function abrir_interesado(apertura,evento,inicio,fin){
				var parametros = {
					"apertura" : "'" + apertura + "'",
					"evento" : "'" + evento + "'",
					"inicio" : "" + inicio + "",
					"codg_evnt" : "<?php echo $_GET[evento]; ?>",
					"fin" : "" + fin + ""
				};
				var url= "<?php echo $url_base; ?>sistema/formularios/conocer_mas_new.php"; 
		      	$.ajax
		      	({
		        	type: "POST",
		          	url: url,
		          	data: parametros,
		          	beforeSend: function () {
		          		//console.log('probando');
		          		var espera = "<?php include($url_base.'sistema/comunes/cargando.php'); ?>";
				    	$("#contenido_modal").html(espera);
				    },
		          	success: function(data)
		          	{
		          		//console.log(data);
		       			$('#contenido_modal').html(data);
		          	}
		      	});
		      	return false;
		    }
    	</script>
	</head>
	<body>
	<header>
		<?php 
			$texto_slide = '';
			$imagen_slide = $url_base.'imagenes/page/cursos.jpg';
			$menu_active = 'cursos';
			if ($_GET[nomb_tipo] == 'diplomados'){
				$menu_active = 'diplomados';
				$_GET[frm]='diplomados';
			}
			include ($url_base.'frontend/header3.php'); 
		?>		
	</header>
	<section>
		<?php 
			$codg_tipo = $_GET[tipo];
			$codg_area = $_GET[area];
			$codg_evnt = $_GET[evento];
			if ($_GET[nomb_tipo] == 'diplomados'){
				$sql_temp = "SELECT * FROM eventos_tipos WHERE nomb_tipo LIKE '%diplomados%' ORDER BY codg_tipo LIMIT 1";
				$res = mysql_fetch_array(mysql_query($sql_temp));
				$codg_tipo = $res[codg_tipo];
				$imagen_dip = '<img height="100px" src="'.$url_base.'sistema/imagenes/imagenes_areas/icon-dip.png">';
			}
		?>
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-xs-11 pull-right">
				<br>
				<br>
				<?php
					if ($codg_evnt) {
						$sql_ev = "SELECT * FROM eventos e, eventos_areas ea WHERE e.codg_evnt = ".$codg_evnt." AND e.codg_area = ea.codg_area AND e.stat_evnt = 'Activo'";
						if ($sql_ev = mysql_fetch_array(mysql_query($sql_ev))){
							echo '<script>$(\'#carouse-text\').html(\''.$sql_ev[nomb_area].'\')</script>';
							echo '<script>$(\'#carouse-icon\').html(\'<img height="100px" src="'.$url_base.''.$sql_ev[imag_area].'">\')</script>';
							echo '<script>$(\'#carouse-text-xs\').html(\''.$sql_ev[nomb_area].'\')</script>';
							echo '<div class="hidden-xs event-titulo x3">'.$sql_ev[nomb_evnt].'</div>';
							echo '<div class="visible-xs event-titulo x2">'.$sql_ev[nomb_evnt].'</div>';
							echo '<br><br>';
							$sql="select * from eventos_apertura where codg_evnt=".$codg_evnt." AND stat_aper='Activo' AND fini_aper >= '".date("Y-m-d")."' order by fini_aper ASC";
							$busq=mysql_query($sql);
							$cuenta_aper = 1;
							while($reg=mysql_fetch_array($busq)){
								if ($cuenta_aper==1){
									echo '<div class="row event-aper-cont">';
								}
								if($cuenta_aper > 1 && $cuenta_aper != mysql_num_rows($busq)) {
									echo '<div class="row event-aper-cont" style="margin-bottom:0px; padding-top: 0px; padding-bottom:5px;">';
								}
								if ($cuenta_aper > 1 && $cuenta_aper == mysql_num_rows($busq)){
									echo '<div class="row event-aper-cont" style="padding-top: 0px; padding-bottom:25px;">';	
								}
									if ($cuenta_aper==2){
										echo '<br><b>OTRAS FECHAS PROGRAMADAS</b><br>';
									}
									if ($cuenta_aper>=2){
										echo '<div class="event-datos-titulo cursor-pointer padd-mas1l" onclick="muestra_oculta(\'aper_'.$reg[codg_aper].'\'); muestra_oculta(\'icon_b_'.$reg[codg_aper].'\'); muestra_oculta(\'icon_r_'.$reg[codg_aper].'\');">&nbsp;
											<span id="icon_b_'.$reg[codg_aper].'" class="glyphicon glyphicon-triangle-bottom pull-left" style="display: none;">&nbsp;</span>
											<span id="icon_r_'.$reg[codg_aper].'" class="glyphicon glyphicon-triangle-right pull-left" style="display: \'\'">&nbsp;</span>
											<span class="pull-left"><b>Del '.ordernar_fecha($reg[fini_aper]).' al '.ordernar_fecha($reg[ffin_aper]).'</b></span>';
										echo '</div>';
										$oculto = 'style="display: none;"';
									}
									echo '<div id="aper_'.$reg[codg_aper].'" '.$oculto.'>';
										echo '<br>';
										echo '<div class="col-md-6 col-xs-12">';
											echo '<div class="event-datos-titulo x15">Fecha del Evento</div>';
											echo '<div class="event-datos x12">Del '.ordernar_fecha($reg['fini_aper']).' al '.ordernar_fecha($reg['ffin_aper']).'</div>';
										echo '</div>';

										echo '<div class="col-md-6 col-xs-12">';
											echo '<div class="event-datos-titulo x15">Duración</div>';
											echo '<div class="event-datos x12">'.$reg['dura_aper'].'</div>';
										echo '</div>';

										echo '<div class="col-md-12 col-xs-12">';
											echo '<div class="event-datos-titulo x15">Horario</div>';
											echo '<div class="event-datos x12  table-responsive">'; 
												echo '<table id="event-table" width="100%" border="0">';
													$sql_secc = "SELECT * FROM eventos_secciones WHERE codg_aper='".$reg['codg_aper']."'";
													$bus_secc = mysql_query($sql_secc);
													if (mysql_num_rows($bus_secc) == 0 ){
														echo '<tr align="center" ><td><b>POR DEFINIR</b></td></tr>';														
													}
													while ($res = mysql_fetch_array($bus_secc)){
														echo '<tr align="center" ><td >Sección "'.$res[nomb_secc].'"</td><td>'.strtoupper($res[lugr_secc]).'</td><td>'.$res[hora_secc].'</td></tr>';
													}
												echo '</table>';
											echo'</div>';
										echo '</div>';

										// inversión de contado
										if ($reg[desc_aper]>0) { 
					          				if ($reg[estu_desc]!=0){ $condicion = ' Estudiantes'; }
					          				if ($reg[estu_desc]!=0 && $reg[prof_desc]!=0 && $reg[empr_desc]==0){ $condicion .= ' y'; }
					          				if ($reg[estu_desc]!=0 && $reg[prof_desc]!=0 && $reg[empr_desc]!=0){ $condicion .= ','; }
					          				if ($reg[prof_desc]!=0){ $condicion .= ' Profesionales '; }
					          				if ($reg[estu_desc]!=0 || $reg[prof_desc]!=0 && $reg[empr_desc]!=0){ $condicion .= ' y '; }
					          				if ($reg[empr_desc]!=0){ $condicion .= ' Empresas a partir de '.$reg[empr_desc].' participantes'; }
				          				}
				          				if ($reg[ppre_aper]=='NO'){
	   										echo '<div class="col-md-12 col-xs-12">';
												echo '<div class="event-datos-titulo x15">Inversión</div>';
												echo '<div class="event-datos x12  table-responsive">'; 
													echo '<table id="event-table" width="100%" border="0">';
															echo '<tr align="center" ><td>Consulta con nuestros asesores <br>(<a href="#contactar">ver contacto</a>)</td></tr>';														
													echo '</table>';
												echo'</div>';
											echo '</div>';
										}
				          				else{
											echo '<div class="col-md-12 col-xs-12">';
												echo '<div class="event-datos-titulo x15">Inversión de Contado</div>';
												echo '<div class="event-datos x12 table-responsive">';
													echo '<table id="event-table" width="100%" border="0" align="center">';
														echo '<tr><td colspan="2">Inversión General</td><td align="right">Bs.&nbsp;'.number_format($reg['pgen_aper'],2,",",".").'&nbsp;</td></tr>';
														echo '<tr><td>Estudiantes</td><td align="right">'; if ($reg["prec_aper"] > 0) { echo '&nbsp;Descuento:&nbsp;'.$reg["prec_aper"].'%'; } echo '</td><td align="right">Bs.&nbsp;'.number_format(($reg['pgen_aper']-($reg['pgen_aper']*$reg['prec_aper']/100)),2,",",".").'&nbsp;</td></tr>';
														echo '<tr><td>Profesionales</td><td align="right">'; if ($reg["prep_aper"] > 0) { echo '&nbsp;Descuento:&nbsp;'.$reg["prep_aper"].'%'; } echo '</td><td align="right">Bs.&nbsp;'.number_format(($reg['pgen_aper']-($reg['pgen_aper']*$reg['prep_aper']/100)),2,",",".").'&nbsp;</td></tr>';
														echo '<tr><td>Empresas <br class="visible-xs">(x Persona)</td><td align="right">'; if ($reg["prem_aper"] > 0) { echo '&nbsp;Descuento:&nbsp;'.$reg["prem_aper"].'%'; } echo '</td><td align="right">Bs.&nbsp;'.number_format(($reg['pgen_aper']-($reg['pgen_aper']*$reg['prem_aper']/100)),2,",",".").'&nbsp;</td></tr>';
														if ($reg[desc_aper]>0){
															echo '<tr align="center"><td colspan="4" class="event-datos-desc">'.number_format($reg[desc_aper],2,',','').'% descuento para '.$condicion.'<br>en pagos de contado.</td></tr>';
														}
													echo '</table>';
												echo '</div>';
											echo '</div>';
											///// inversion en cuotas
											$cuotas = false;
											$sql_cuotas = "SELECT * FROM aperturas_cuotas WHERE codg_aper ='".$reg['codg_aper']."'";
											$sql_bus = mysql_query($sql_cuotas);
											if (mysql_num_rows($sql_bus)){											
												echo '<div class="col-md-12 col-xs-12">';
													echo '<div class="event-datos-titulo x15">Inversión Financiada</div>';
													echo '<div class="event-datos x12 table-responsive">';
														echo '<table id="event-table" width="100%" border="0" align="center">';
														echo '<tr>
															<th colspan="2" style="text-align: left;">Descripción del Pago</th>
															<th width="150px">Fecha Límite</th>
															<th width="120px">Estudiante</th>
															<th width="120px">Profesionales</th>
															<th width="120px">Empresas</th>';
															while ($res_cuotas = mysql_fetch_array($sql_bus)){
																echo '<tr>
																	<td colspan="2" >&nbsp;'.$res_cuotas[conc_cuot].'</td>
																	<td  align="center">&nbsp;'.ordernar_fecha($res_cuotas[ftop_cuot]).'</td>
																	<td  align="right"> '; if ($res_cuotas[estu_cuot] > 0) { echo 'Bs.&nbsp;'.number_format($res_cuotas[estu_cuot],2,',','.'); }else { echo 'N/A'; } echo '&nbsp;</td>
																	<td  align="right"> '; if ($res_cuotas[prof_cuot] > 0) { echo 'Bs.&nbsp;'.number_format($res_cuotas[prof_cuot],2,',','.'); }else { echo 'N/A'; } echo '&nbsp;</td>
																	<td  align="right"> '; if ($res_cuotas[empr_cuot] > 0) { echo 'Bs.&nbsp;'.number_format($res_cuotas[empr_cuot],2,',','.'); }else { echo 'N/A'; } echo '&nbsp;</td>
																</tr>';

																$totalizar1 += $res_cuotas[estu_cuot];
																$totalizar2 += $res_cuotas[prof_cuot];
																$totalizar3 += $res_cuotas[empr_cuot];
																$totalizar1 = number_format($totalizar1,2,'.','');
																$totalizar2 = number_format($totalizar2,2,'.','');
																$totalizar3 = number_format($totalizar3,2,'.','');
															}
															echo '<tr>
																<th colspan="3">TOTAL DE LA INVERSIÓN FINANCIADA</th>
																<th style="text-align: right;">Bs. '.number_format($totalizar1,2,',','.').'&nbsp;</th>
																<th style="text-align: right;">Bs. '.number_format($totalizar2,2,',','.').'&nbsp;</th>
																<th style="text-align: right;">Bs. '.number_format($totalizar3,2,',','.').'&nbsp;</th>
															</tr>';
														echo '</table>';
													echo '</div>';
												echo '</div>';
											}
										}
										if ($_SESSION['tipo_usuario'] != 2 && $_SESSION['tipo_usuario'] != 3){
											$retornar = $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'];
											$retornar = str_replace('?', '**', $retornar);
											$retornar = str_replace('&', '***', $retornar);
											$onclick_reservar = "window.location=('".$url_base."frontend/iniciar_sesion.php?retorno=".$retornar."')";
										}
										else{
											if ($_SESSION['tipo_usuario'] == 2){
												$tipo_reserva = 'usuario';
											}
											else{
												$tipo_reserva = 'empresa';
											}
											$onclick_reservar = "reservar('".$tipo_reserva."',".$reg[codg_aper].");";
										}
										echo '<div class="event-img"><img src="'.$url_base.''.$reg['imag_aper1'].'" width="100%"/></div>';
										if ($sql_ev[pdf_dipl]){
											echo '<div class="hidden-xs col-md-12" style="margin-bottom:1em; text-align:center;"><a href="'.$url_base.''.$sql_ev[pdf_dipl].'" target="_blank"><button class="btn event-boton" style="width: 20em">INFORMACIÓN DETALLADA</button></a></div>';										
										}
											echo '<div class="hidden-xs col-md-2">&nbsp;</div>';
											
											echo '<div class="col-md-4 col-xs-12"><button class="btn event-boton" data-toggle="modal" data-target="#modal" onclick="abrir_interesado('.$reg[codg_aper].',\''.$sql_ev[nomb_evnt].'\',\''.$reg[fini_aper].'\',\''.$reg[ffin_aper].'\')">REGISTRARME INTERESADO</button></div>';
										echo '<div class="col-md-4 col-xs-12"><button class="btn event-boton" data-toggle="modal" data-target="#modal" onclick="'.$onclick_reservar.'">RESERVAR</button></div>';
									echo '</div>';
									include($url_base.'frontend/evento_videos.php');
								echo '</div>';
								$cuenta_aper+=1;
							}
							if (mysql_num_rows($busq)==0){
								echo '<div class="row event-aper-cont">';
									echo '<div class="event-img"><img src="'.$url_base.''.$sql_ev['imag_evnt'].'" width="100%"/></div>';									
									echo '<div class="col-md-12 col-xs-12 event-titulo x12">Actualmente no tenemos programado este evento pero si te registras como interesado<br class="hidden-xs"> te informaremos si lo incluimos en nuestra programación.</div>';
									echo '<div class="col-md-12 col-xs-12 event-titulo x12">&nbsp;</div>';
									echo '<div class="hidden-xs col-md-4">&nbsp;</div>';
									echo '<div class="col-md-4 col-xs-12"><button class="btn event-boton" data-toggle="modal" data-target="#modal" onclick="abrir_interesado(\''.$reg[codg_aper].'\',\''.$sql_ev[nomb_evnt].'\',\''.$reg[fini_aper].'\',\''.$reg[ffin_aper].'\')">REGISTRARME INTERESADO</button></div>';
									include($url_base.'frontend/evento_videos.php');
								echo '</div>';
							}	
							if ($_GET[frm]=='home' && $codg_evnt){
								$backto = $url_base.'index.php';
							}	
							if ($_GET[frm]=='eventos'){
								$backto = $url_base.'frontend/eventos.php';
							}	
							if ($_GET[frm]=='diplomados'){
								$backto = $url_base.'frontend/diplomados.php';
							}
							if ($_GET[frm]=='anual' || $_GET[frm]=='trimestral'){
								$backto = $url_base.'frontend/catalogo_trimestre.php?tipo='.$_GET[frm];	
							}
							if ($_GET[frm]=='perfil'){
								if ($_SESSION[codg_empr]){
									$backto = $url_base.'frontend/bussiness.php';
								}
								else{
									$backto = $url_base.'frontend/user.php';
								}
							}
							if (!$_GET[frm]){
								$backto = $url_base.'frontend/evento.php?tipo='.$_GET[tipo].'&area='.$_GET[area].'&evento=#punto';
							}
							echo '<div class="hidden-xs col-md-4">&nbsp;</div>';
							echo '<div class="col-md-4 col-xs-12"><button class="btn event-boton" onclick="window.location=(\''.$backto.'\')">REGRESAR</button></div>';
							echo '<div class="col-md-11 col-xs-12" style="margin-bottom: 30px;">&nbsp;</div>';
						}
					}
					else{
						if ($codg_area && !$codg_evnt || ($codg_tipo && !$codg_area && !$codg_event) || $_POST['keys']){
							$cuenta_eventos = 1;
							$ol1 = '<ol>';
							$ol2 = '';
							$sql_eventos_area = "SELECT * FROM eventos e, eventos_areas ea WHERE ea.codg_area = ".$codg_area." AND ea.codg_area = e.codg_area AND e.stat_evnt = 'Activo' ORDER BY e.codg_area ASC, e.nomb_evnt";
							if ($codg_tipo && $codg_area){
								$sql_eventos_area = "SELECT * FROM eventos e, eventos_areas ea, eventos_tipos et WHERE ea.codg_area = ".$codg_area." AND ea.codg_area = e.codg_area AND e.codg_tipo=et.codg_tipo AND et.codg_tipo = ".$codg_tipo." AND e.stat_evnt = 'Activo' ORDER BY e.codg_area ASC, e.nomb_evnt";							
							}
							if ($codg_tipo && !$codg_area){
								$sql_eventos_area = "SELECT * FROM eventos e, eventos_areas ea, eventos_tipos et WHERE ea.codg_area = e.codg_area AND e.codg_tipo=et.codg_tipo AND et.codg_tipo = ".$codg_tipo." AND e.stat_evnt = 'Activo' ORDER BY e.codg_area ASC, e.nomb_evnt";
							}
							if ($_POST['keys']){
								$_GET[frm] = 'home';
								$buscara = $_POST['keys'];
								$buscara = str_replace(', ', ' ', $buscara);
								$buscara = str_replace('. ', ' ', $buscara);
								$buscara = explode (' ', $_POST['keys']);
								$buscara;
								$sql_eventos_area = '';
								foreach ($buscara as $key => $value) {
									if (strlen($value)>2){
										$sql_eventos_area .= "SELECT e.*, ea.*, et.* FROM eventos e, eventos_areas ea, eventos_tipos et WHERE ea.codg_area = e.codg_area AND e.codg_tipo=et.codg_tipo AND e.stat_evnt = 'Activo' AND (et.nomb_tipo LIKE '%".$value."%' OR ea.nomb_area LIKE '%".$value."%' OR e.nomb_evnt  LIKE '%".$value."%' OR e.desc_evnt LIKE '%".$value."%')";
										$sql_eventos_area .= ' UNION ';
									}
								}
								$sql_eventos_area .= ' UNION ';
								$sql_eventos_area = str_replace(' UNION  UNION ', '', $sql_eventos_area);
								$sql_eventos_area .= " ORDER BY nomb_evnt";
							}
							$bus_eventos_area = mysql_query($sql_eventos_area);
							//echo mysql_error();
							$cant_res = mysql_num_rows($bus_eventos_area);
							if ($cant_res>0){
								if ($cant_res > $EVNT_PAGE){
									$tope_event = number_format(($cant_res / 2),0,'','');
									$tope_event += 1;
									$ol2 = '<ol start="'.($tope_event + 1).'">';
								}
								$temp = mysql_fetch_array(mysql_query($sql_eventos_area));
								if ($temp[nomb_area] == 'Contable' || $temp[nomb_area] == 'Tributaria'){
									$tope = '';
									$ol1 = '<ol>';
									$sql_eventos_area2 = "SELECT * FROM eventos e, eventos_areas ea WHERE ea.nomb_area = 'Contable' AND ea.codg_area = e.codg_area AND e.stat_evnt = 'Activo' ORDER BY e.codg_area ASC, e.nomb_evnt";
									$bus_eventos_area2 = mysql_query($sql_eventos_area2);
									while($res_eventos_area2 = mysql_fetch_array($bus_eventos_area2)){
										$ol1 .= '<a href="evento.php?tipo='.$_GET[tipo].'&area='.$codg_area.'&evento='.$res_eventos_area2[codg_evnt].'#punto"><li class="marg-mas2b"><span class="normal">'.$res_eventos_area2[nomb_evnt].'</span></li></a>';
										$imagen = $res_eventos_area2[imag_area];
										$imagen = '<img height="100px" src="'.$url_base.''.$imagen.'">';
									}
									$ol2 = '<ol>';								
									$sql_eventos_area3 = "SELECT * FROM eventos e, eventos_areas ea WHERE ea.nomb_area = 'Tributaria' AND ea.codg_area = e.codg_area AND e.stat_evnt = 'Activo' ORDER BY e.codg_area ASC, e.nomb_evnt";
									$bus_eventos_area3 = mysql_query($sql_eventos_area3);
									while($res_eventos_area3 = mysql_fetch_array($bus_eventos_area3)){
										$ol2 .= '<a href="evento.php?tipo='.$_GET[tipo].'&area='.$codg_area.'&evento='.$res_eventos_area3[codg_evnt].'#punto"><li class="marg-mas2b"><span class="normal">'.$res_eventos_area3[nomb_evnt].'</span></li></a>';
									}
									$titulo = "Área Contable y Tributaria";
									$titulo_tipo = $res_eventos_area[nomb_tipo];
									$cinta1 = '<div class="event-cinta1 x2">Contable</div>';
									$cinta2 = '<div class="event-cinta2 x2">Tributaria</div>';
								}
								else{							
									while($res_eventos_area = mysql_fetch_array($bus_eventos_area)){
										$dest = 'ol1';
										if ($tope_event && $cuenta_eventos > $tope_event){
											$dest = 'ol2';
										}
										$$dest .= '<a href="evento.php?tipo='.$_GET[tipo].'&area='.$codg_area.'&evento='.$res_eventos_area[codg_evnt].'&frm='.$_GET[frm].'#punto"><li class="marg-mas2b"><span class="normal">'.$res_eventos_area[nomb_evnt].'</span></li></a>';
										$cuenta_eventos += 1;
										$titulo = $res_eventos_area[nomb_area];
										$titulo_tipo = $res_eventos_area[nomb_tipo];
										$imagen = $res_eventos_area[imag_area];
										$imagen = '<img height="100px" src="'.$url_base.''.$imagen.'">';
									}
								}
								$ol1 .= '</ol>';
								if ($ol2){
									$ol2 .= '</ol>';
								}
								else{
									echo '<div class="hidden-xs col-md-3">&nbsp;</div>';
								}
								echo '<div class="col-md-5 col-xs-12 event-ol" style="height:"><div id="cinta1" class="event-cinta">'.$cinta1.'</div><div class="event-listado1" >'.$ol1.'<div class="visible-xs">'.$ol2.'</div></div></div>';
								if ($ol2){
									echo '<div class="hidden-xs col-md-1">&nbsp;</div>';
									echo '<div class="hidden-xs col-md-5 event-ol"><div id="cinta2" class="event-cinta">'.$cinta2.'</div><div class="event-listado2">'.$ol2.'</div></div>';
								}
								if ($codg_tipo && !$codg_area){
									$titulo = $titulo_tipo;
									$imagen = '';	
								}
								if ($imagen_dip){
									$imagen = $imagen_dip;
								}
								echo '<div class="col-md-12 col-xs-12" style="margin-bottom: 30px;">&nbsp;</div>';
								if($_POST['keys']){
									$imagen = '<span class="glyphicon glyphicon-search" aria-hidden="true"></span>';
									echo '<script>$(\'#carouse-text\').html(\'Resultado de la Búsqueda\')</script>';
									echo '<script>$(\'#carouse-text-xs\').html(\'Resultado de la Búsqueda\')</script>';
									echo '<script>$(\'#carouse-icon\').html(\''.$imagen.'\')</script>';
								}
								else{								
									echo '<script>$(\'#carouse-text\').html(\''.$titulo.'\')</script>';
									echo '<script>$(\'#carouse-text-xs\').html(\''.$titulo.'\')</script>';
									echo '<script>$(\'#carouse-icon\').html(\''.$imagen.'\')</script>';
								}
							}
							else{
								echo '<div style="width: 100%; margin-top: 50px; margin-bottom: 50px; text-align: center;" class="inco-titulo x3"><b>NO HAY RESULTADOS</b></div>';
							}
						}
					}
				?>
				</div>
			</div>
		</div>
		<!-- Modal para login -->
	    <div class="modal fade" id="modal" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    	<div class="modal-dialog">
	    		<div class="modal-content">
	    			<div class="modal-body">
		            	<div class="cinta">&nbsp;</div>
		            	<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove-circle "></span></button>
			            <div id="contenido_modal">

			            </div>
	    			</div>
	    		</div>  
	    	</div>    
	    </div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>  
