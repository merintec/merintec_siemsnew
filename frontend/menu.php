    <!-- Static navbar -->
    <nav class="navbar menu">
	    <div class="container-fluid">
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed menu-boton" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
	            </button>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	            <ul class="nav navbar-nav navbar-right">
		            <li class="<?php if($menu_active=='home' || $menu_active=='') { echo 'active'; } ?>"><a href="<?php echo $url_base; ?>index.php">HOME<?php if($menu_active=='home' || $menu_active=='') { echo '<span class="menu-active"></span>'; } ?></a></li>
		            <li class="hidden-xs menu-separador"></li>
					<li class="dropdown">
	                	<a href="#" class="dropdown-toggle <?php if($menu_active=='siems') { echo 'active'; } ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SIEMS <span class="caret"></span><?php if($menu_active=='siems') { echo '<span class="menu-active"></span>'; } ?></a>
		                <ul class="dropdown-menu">
		                  <li><a href="<?php echo $url_base?>frontend/siems.php">Siems</a></li>
		                  <li><a href="<?php echo $url_base?>frontend/acreditaciones.php">Acreditaciones</a></li>
		                  <li><a href="<?php echo $url_base?>frontend/clientes.php">Clientes</a></li>
		                  <li><a href="<?php echo $url_base?>frontend/faq.php">Preguntas Frecuentes</a></li>
		                  <li><a href="<?php echo $url_base?>frontend/pdd.php">Políticas de Reembolso</a></li>
		                </ul>
	              	</li>
	              	<li class="hidden-xs menu-separador"></li>
					<li class="<?php if($menu_active=='diplomados') { echo 'active'; } ?>"><a href="<?php echo $url_base?>frontend/diplomados.php">DIPLOMADOS<?php if($menu_active=='diplomados') { echo '<span class="menu-active"></span>'; } ?></a></li>
					<li class="hidden-xs menu-separador"></li>

					<li class="hidden-xs <?php if($menu_active=='cursos') { echo 'active'; } ?>"><a href="<?php echo $url_base?>frontend/cursos.php">CURSOS<?php if($menu_active=='cursos') { echo '<span class="menu-active"></span>'; } ?></a></li>
					<li class="visible-xs <?php if($menu_active=='cursos') { echo 'active'; } ?>"><a href="<?php echo $url_base?>frontend/cursos_areas.php">CURSOS<?php if($menu_active=='cursos') { echo '<span class="menu-active"></span>'; } ?></a></li>

					<li class="hidden-xs menu-separador"></li>
					<li class="<?php if($menu_active=='eventos') { echo 'active'; } ?>"><a href="<?php echo $url_base?>frontend/eventos.php">EVENTOS<?php if($menu_active=='eventos') { echo '<span class="menu-active"></span>'; } ?></a></li>
		            <li class="hidden-xs menu-separador"></li>
					<li class="dropdown">
	                	<a href="#" class="dropdown-toggle <?php if($menu_active=='empresas') { echo 'active'; } ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EMPRESAS <span class="caret"></span>		                <?php if($menu_active=='empresas') { echo '<span class="menu-active"></span>'; } ?></a>
		                <ul class="dropdown-menu">
		                  <li><a href="<?php echo $url_base?>frontend/incompany.php">In Company</a></li>
		                  <li><a href="<?php echo $url_base?>frontend/outdoor.php">Outdoor</a></li>
		                  <li><a target="blanck" href="<?php echo $url_base?>pdf/catalogoanual2016.pdf">Cátalogo Anual</a></li>


		                </ul>
	              	</li>
	              	<li class="hidden-xs menu-separador"></li>
	              	<li class="<?php if($menu_active=='servicios') { echo 'active'; } ?>"><a href="<?php echo $url_base?>frontend/servicios.php">SERVICIOS<?php if($menu_active=='servicios') { echo '<span class="menu-active"></span>'; } ?></a></li>
	              	<li class="hidden-xs menu-separador"></li>
	              	<li class="<?php if($menu_active=='contacto') { echo 'active'; } ?>"><a href="<?php echo $url_base?>frontend/contactenos.php">CONTACTO<?php if($menu_active=='contacto') { echo '<span class="menu-active"></span>'; } ?></a></li>
	            </ul>
          	</div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>