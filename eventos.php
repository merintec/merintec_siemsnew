<?php
	$url_base = '../';
	include($url_base.'comunes/variables.php');
	include($url_base.'sistema/comunes/conexion.php');
	//include_once($url_base.'sistema/comunes/funciones_php.php');
	include_once($url_base.'sistema/comunes/funciones_js.php');
?>
<html lang="es">
	<head>
		<!-- meta -->
	    <meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="<?php echo $page_descripcion; ?>" />
		<meta name="keywords" content="<?php echo $page_keywords; ?>" />
		<meta name="author" content="<?php echo $page_autor; ?>" />
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="robots" content="all">
		<!-- Favicon-->
		<link href="<?php echo $url_base; ?>imagenes/favicon.ico" rel="shortcut icon">
	    <!-- titulo de la pagina -->
	    <title><?php echo $page_nombre; ?></title>
	    <!-- Estilos -->
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base; ?>bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $url_base; ?>css/estilo.css">
        <script src="../bootstrap/js/jquery.js"> </script>
	</head>
	<body>
	<header>
		<?php 
			
			$texto_slide = '';
			$imagen_slide = $url_base.'imagenes/page/cursos.jpg';
			$menu_active = 'eventos';
			include ($url_base.'frontend/header3.php'); 
			echo '<script>$(\'#carouse-text\').html(\'Eventos\')</script>';
			echo '<script>$(\'#carouse-icon\').html(\'<img height="100px" src="'.$url_base.'sistema/imagenes/imagenes_areas/icon-evn.png">\')</script>';
			echo '<script>$(\'#carouse-text-xs\').html(\'Eventos\')</script>';
		?>		
	</header>
	<section>
		<div class="container">
			<br><br><br>
			<div id="slider2">
			  <div id="carousel2" class="carousel slide" data-ride="carousel2">
			    <!-- Wrapper for slides -->
			    <div class="carousel-inner" role="listbox">
			      <!-- incio de generación de slides seguin Base de datos -->
			      <?php
			        // buscar el slide indicado si no se indica slide el buscará el activo
			      	$sql_slides = "SELECT * FROM eventos e, eventos_apertura ea, eventos_tipos et WHERE e.codg_evnt = ea.codg_evnt AND ea.stat_aper = 'Activo' AND e.stat_evnt = 'Activo' AND ea.imag_aper2 != '' AND e.codg_tipo = et.codg_tipo AND evnt_aper = 'SI' AND ea.fini_aper >= '".date('Y-m-d')."' ORDER BY ea.fini_aper ASC";
			        $bus_slides = mysql_query($sql_slides);
			        $cuenta_slides = 0;
			        while ($slides = mysql_fetch_array($bus_slides)){
				        $cuenta_slides += 1;
				        $activo = '';
				        $direccion = '';
				        if ($cuenta_slides==1){
				        	$activo = 'active';
				        }
		            	$direccion = $url_base.'frontend/evento.php?tipo=&area='.$slides[codg_area].'&evento='.$slides[codg_evnt].'&frm=eventos';
			      ?>
			          <div class="item <?php echo $activo; ?>" style="margin-bottom: -10px;">
			            <a href="<?php echo $direccion; ?>">
			              <img src="<?php echo $url_base; ?><?php echo $slides[imag_aper2]; ?>" width="100%" style="margin-bottom: 10px;">
			            </a>
			          </div>
			      <?php
			        }
			      ?>
			      <!-- fin de slides -->
			      <!-- Indicators -->
			      <?php
			        if ($cuenta_slides>1) {
				        //<!-- Left and right controls -->
			          	echo '<a class="left carousel-control" href="#carousel2" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>';
			          	echo '<a class="right carousel-control" href="#carousel2" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>';
			        } 
			      ?>
			    </div>
			    <!-- Indicators -->
			      <?php
			        if ($cuenta_slides>1) {
				        echo '<ol class="carousel-indicators carousel-indicators-override">';
				        for ($i=1;$i<=$cuenta_slides;$i++){
				        	echo '<li data-target="#carousel2" data-slide-to="'.($i-1).'" '; if ($i==1) { echo 'class="active"'; } echo '></li>';
				        } 
				        echo '</ol>';
			        } 
			      ?>
			  </div>
			</div>			
		</div>
		<br><br><br><br><br>
		<!-- Modal para login -->
	    <div class="modal fade" id="modal" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    	<div class="modal-dialog">
	    		<div class="modal-content">
	    			<div class="modal-body">
		            	<div class="cinta">&nbsp;</div>
		            	<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove-circle "></span></button>
			            <div id="contenido_modal">

			            </div>
	    			</div>
	    		</div>  
	    	</div>    
	    </div>
	</section>
	<footer>
		<?php include ($url_base.'frontend/footer.php'); ?>
	</footer>
	</body>
</html>
<!-- Bootstrap -->
<script src="<?php echo $url_base?>bootstrap/js/bootstrap.min.js"> </script>