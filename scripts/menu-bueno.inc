<link rel='stylesheet' type='text/css' href='css/styles.css' />
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<div id='cssmenu' style="z-index:999;"> 
<ul>
   <li><a href='index.php'><span>Inicio</span></a></li>
   <li class='has-sub'><a href='#'><span>La Institución</span></a>
      <ul>
         <li class='has-sub'><a href='index.php?pagina=quienes'><span>¿Quiénes Somos?</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=acreditaciones'><span>Acreditaciones y Convenios</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=clientes'><span>Nuestros Clientes</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=valor'><span>Valor Agregado</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=contacto'><span>Contacto Directo</span></a></li>
      </ul>
   </li>
   <li><a href='#'><span>Diplomados</span></a>
   <ul>
         <li class='has-sub'><a href='index.php?pagina=presenciales'><span>Presenciales</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=semipres'><span>Semi-presenciales</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=online'><span>100% On-Line</span></a></li>
      </ul>
   </li>
   <li class='last'><a href='#'><span>Cursos</span></a>
   <ul>
         <li class='has-sub'><a href='#'><span>Administración y Gerencia</span></a></li>
         <li class='has-sub'><a href='#'><span>Contabilidad y Tributos</span></a></li>
         <li class='has-sub'><a href='#'><span>Informática Práctica</span></a></li>
         <li class='has-sub'><a href='#'><span>Higiene y Seguridad Laboral</span></a></li>
         <li class='has-sub'><a href='#'><span>Hotelería y Turismo</span></a></li>
         <li class='has-sub'><a href='#'><span>Especializados</span></a></li>
      </ul>
   
   </li>
   <li class='last'><a href='index.php?pagina=catalogo'><span>Catálogo</span></a></li>
   <li class='last'><a href='#'><span>Para Empresas</span></a>
      <ul>
         <li class='has-sub'><a href='index.php?pagina=outdoor'><span>Outdoor</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=incompany'><span>Incompany</span></a></li>
      </ul>
   </li>
   <li class='last'><a href='#'><span>Servicios</span></a>
      <ul>
         <li class='has-sub'><a href='index.php?pagina=proyectos'><span>Proyectos</span></a></li>
         <li class='has-sub'><a href='index.php?pagina=consultorias'><span>Consultorías</span></a></li>
      </ul>
   </li>
<!--   <li class='last'><a href='#'><span>Contacto</span></a>
      <ul>
         <li class='has-sub'><a href='#'><span>Formulario</span></a></li>
         <li class='has-sub'><a href='#'><span>Redes Sociales</span></a></li>
      </ul>
   </li>-->
</ul>
</div>
