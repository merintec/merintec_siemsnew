<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<style type="text/css" title="currentStyle">
			@import "sistema/datatables/media/css/demo_page.css";
			@import "sistema/datatables/media/css/demo_table.css";
		</style>
		<script type="text/javascript" language="javascript" src="sistema/datatables/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="sistema/datatables/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			function pasar_valor(valor, campo)
			{
				if (document.form1[campo].value==valor){
					document.form1[campo].value = '';
				}
				else {
					document.form1[campo].value = valor;
				}
				if (document.form1[campo].value==''){
					document.all['adicionales'].style.display = "none";
				}
				else{
					document.all['adicionales'].style.display = "block";
				}
			}
			var oTable;			
			$(document).ready(function() {
				/* Add a click handler to the rows - this could be used as a callback */
				$("#example tbody tr").click( function( e ) {
					if ( $(this).hasClass('row_selected') ) {
						$(this).removeClass('row_selected');						
						pasar_valor('','<?php echo $key_entabla; ?>');
					}
					else {
						oTable.$('tr.row_selected').removeClass('row_selected');
						$(this).addClass('row_selected');
						var anSelected = fnGetSelected( oTable );
						var codigo = anSelected[0].id;
						pasar_valor(codigo,'<?php echo $key_entabla; ?>');
					}
				});
				
				/* Add a click handler for the delete row */
				$('#delete').click( function() {
					var anSelected = fnGetSelected( oTable );
					if ( anSelected.length !== 0 ) {
						oTable.fnDeleteRow( anSelected[0] );
					}
				} );
				
				oTable = $('#example').dataTable( {
				"sPaginationType": "full_numbers",
					"oLanguage": {
						"sLengthMenu": "Mostrar _MENU_ registros por página",
						"sZeroRecords": "Nada encontrado - Intenta nuevamente",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Showing 0 to 0 of 0 records",
			                        "sSearch": "Buscar:",
						"sInfoFiltered": "(filtados de _MAX_ registros)",
						"oPaginate": {
				                        "sFirst": "Primera",
				                        "sPrevious": "Anterior",
				                        "sNext": "Siguiente",
				                        "sLast": "Última"
				                 }
					}
				} );
			} );
			
			/* Get the rows which are currently selected */
			function fnGetSelected( oTableLocal )
			{
				return oTableLocal.$('tr.row_selected');
			}
		</script>
	</head>
	<body id="dt_example" class="ex_highlight_row">
		<div id="container">
			<div class="full_width big">
				Resultados de la Búsqueda indicada: "<?php echo $_POST['parametro']; ?>"
			</div>
			<div id="demo">
			<form name="form1" id="form1" method="POST">
				<div align="center">		
				   <table>
				   	<tr>
				   		<td>
				   			<input type="hidden" value="" name="<?php echo $key_entabla; ?>" id="<?php echo $key_entabla; ?>">
								<input type="submit" name="boton" id="boton" value="Nuevo" class="boton_iniciar">
				   		</td>
				   		<td id="adicionales" style="display: none;">
								<input type="submit" name="boton" id="boton" value="Modificar" class="boton_iniciar">
								<input type="submit" name="boton" id="boton" value="Eliminar" class="boton_iniciar">				   		
				   		</td>
				   	</tr>
				   </table>
				</div>
			</form>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<?php
				$nparam_v = count($buscar_varios);	
				for($i=0;$i<$nparam_v;$i++)
				{
					$cabeceras .= '<th>'.$buscar_varios[$i][0].'</th>';
				}
				echo $cabeceras;
		   ?>
		</tr>
	</thead>
	<tbody>
		<?php 
			while($res = mysql_fetch_array($buscando[0])){
				echo "<tr class'gradeX' id='".$res[$key_entabla]."'>";
		   	for($i=0;$i<$nparam_v;$i++)
				{
					
					echo "<td align='".$buscar_varios[$i][3]."'>";
					if ($buscar_varios[$i][2]=='fecha')
					{
						echo ordernar_fecha($res[$buscar_varios[$i][1]]); 
					}
					elseif($buscar_varios[$i][2]!='' && $res[$buscar_varios[$i][1]]!=''){
						$mostrar = $buscar_varios[$i][2];
						$sql_aux = "SELECT * FROM ".$mostrar[0]." WHERE ".$mostrar[1]." = ".$res[$buscar_varios[$i][1]];
						$mostrar_datos = mysql_fetch_array(mysql_query($sql_aux));
						$encontrado = $mostrar_datos[$mostrar[2]];

						if ($mostrar[3]!=''){
							$sql_aux = "SELECT * FROM ".$mostrar[3]." WHERE ".$mostrar[4]." = ".$mostrar_datos[$mostrar[2]];
							$mostrar_datos = mysql_fetch_array(mysql_query($sql_aux));
							$encontrado = $mostrar_datos[$mostrar[5]];
						}
						if ($mostrar[5]=='fecha') { $encontrado = ordernar_fecha($encontrado);}
						echo $encontrado;
					}
					else {
					 	echo $res[$buscar_varios[$i][1]];
					}
					echo "</td>";
				}
				echo "</tr>";
		   } 
		?>
	</tbody>
	<tfoot>
		<tr>
			<?php   
				echo $cabeceras;
		   ?>
		</tr>
	</tfoot>
</table>
			</div>
		</div>
	</body>
</html>
