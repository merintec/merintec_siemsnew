<?php $key_entabla = 'codg_intr'; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<style type="text/css" title="currentStyle">
			@import "../datatables/media/css/demo_page.css";
			@import "../datatables/media/css/demo_table.css";
		</style>
		<script type="text/javascript" language="javascript" src="../datatables/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="../datatables/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			function pasar_valor(valor, campo)
			{
				if (document.form1[campo].value==valor){
					document.form1[campo].value = '';
				}
				else {
					document.form1[campo].value = valor;
				}
				if (document.form1[campo].value==''){
					document.all['adicionales'].style.display = "none";
				}
				else{
					document.all['adicionales'].style.display = "block";
				}
			}
			var oTable;			
			$(document).ready(function() {
				/* Add a click handler to the rows - this could be used as a callback */
				$("#example tbody tr").click( function( e ) {
					if ( $(this).hasClass('row_selected') ) {
						$(this).removeClass('row_selected');						
						pasar_valor('','<?php echo $key_entabla; ?>');
					}
					else {
						oTable.$('tr.row_selected').removeClass('row_selected');
						$(this).addClass('row_selected');
						var anSelected = fnGetSelected( oTable );
						var codigo = anSelected[0].id;
						pasar_valor(codigo,'<?php echo $key_entabla; ?>');
					}
				});
				
				/* Add a click handler for the delete row */
				$('#delete').click( function() {
					var anSelected = fnGetSelected( oTable );
					if ( anSelected.length !== 0 ) {
						oTable.fnDeleteRow( anSelected[0] );
					}
				} );
				
				oTable = $('#example').dataTable( {
				"sPaginationType": "full_numbers",
					"oLanguage": {
						"sLengthMenu": "Mostrar _MENU_ registros por página",
						"sZeroRecords": "Nada encontrado - Intenta nuevamente",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Showing 0 to 0 of 0 records",
			                        "sSearch": "Buscar:",
						"sInfoFiltered": "(filtados de _MAX_ registros)",
						"oPaginate": {
				                        "sFirst": "Primera",
				                        "sPrevious": "Anterior",
				                        "sNext": "Siguiente",
				                        "sLast": "Última"
				                 }
					}
				} );
			} );
			
			/* Get the rows which are currently selected */
			function fnGetSelected( oTableLocal )
			{
				return oTableLocal.$('tr.row_selected');
			}
		</script>
	</head>
	<body id="dt_example" class="ex_highlight_row">
		<div id="container">
			<div id="demo">
			<form name="form1" id="form1" method="POST" action="" >
				<div align="center" id="adicionales" style="display: none;">		
				   <table width="100%" height="300px" border="0" cellpadding="0" cellspacing="0">
				        <tr align="center">
				                <td>
				                        <textarea cols="60" rows="5" name="perf_faci" id="perf_faci" class="validate[required, minSize[3],maxSize[5]] text-input, cajas_texto" placeholder="Observación a registrar"></textarea>
				                </td>
				        </tr>
				   	<tr height="20px" align="center">
				   		<td>
        				   		        <input type="hidden" value="" name="<?php echo $key_entabla; ?>" id="<?php echo $key_entabla; ?>">
								<input type="submit" name="boton" id="boton" value="Guardar" class="boton_iniciar">
								<input type="button" name="boton1" id="boton1" value="Cerrar" class="boton_iniciar" OnClick="document.all['adicionales'].style.display = 'none';">			   		
				   		</td>
				   	</tr>
				   </table>
				</div>
			</form>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<?php
				$nparam_v = 8;	
                                $cabeceras = '<th>Evento</th><th>Participante</th><th width="50px">Tipo</th><th width="50px">TLF</th><th width="50px">mail</th><th width="50px">SMS</th><th>Última Acción</th>';
				echo $cabeceras;
		   ?>
		</tr>
	</thead>
	<tbody>
		<?php 
                        $sql_contactos = "SELECT *, 'interesado' as tipo_intr FROM interesados inte, eventos_apertura ev_ap, eventos ev WHERE inte.codg_aper = ev_ap.codg_aper AND ev_ap.codg_evnt=ev.codg_evnt";
	                $bus_contactos =  mysql_query($sql_contactos);
			while($res = mysql_fetch_array($bus_contactos)){
				echo "<tr class'gradeX' id='".$res['codg_intr'].";".$res['tipo_intr']."'>";
					echo "<td>";
                			        echo $res['nomb_evnt'];
				                echo "</td>";
                			        echo "<td>";
                			        echo $res['nomb_intr'];
				                echo "</td>";
                			        echo "<td>";
                			        echo $res['tipo_intr'];
				                echo "</td>";
				                echo "<td align='center'>tlf</td>";
				                echo "<td align='center'>mail</td>";
				                echo "<td align='center'>SMS</td>";
                			        echo "<td>";
                			        echo $res['nomb_intr'];
				                echo "</td>";
				echo "</tr>";
		   } 
		?>
	</tbody>
	<tfoot>
		<tr>
			<?php   
				echo $cabeceras;
		   ?>
		</tr>
	</tfoot>
</table>
			</div>
		</div>
	</body>
</html>
