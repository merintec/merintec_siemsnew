<?php 
	$url_base = "../../";
	$url_base2 = "../";
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	/*for ($i=0; $i<=$espera_tiempo; $i++){	}*/
?>
<?php 
include($url_base."sistema/comunes/verificar_empresa.php");
$codg_aper=$_POST['codg_aper'];
$cedula_usuario=$_SESSION['cedula_usuario'];
$vinculo_correo="<a href='http://siems.com.ve/frontend/iniciar_sesion.php?url=pagar'>Registrar Pago</a>";
$usuario = buscar("usuarios","logi_usua",$_SESSION['usuario_logueado'],'individual');
$usuario = $usuario[0];
$codg_empr =$_SESSION['codg_empr'];

if($usuario[1]>=1){
	$sql_aper="select * from vista_secciones_aperturas where codg_aper='".$codg_aper."'";
	$busq_aper=mysql_query($sql_aper);
	$reg_aper=mysql_fetch_array($busq_aper);
	$precio_evento = $reg_aper['pgen_aper']-number_format(($reg_aper['pgen_aper']*$reg_aper['prem_aper']/100),2,'.',''); }
?>
<script type="text/javascript" src="../scripts/muestra_oculta.js"></script>
<script type="text/javascript">
	function registrar(codg_part){
		var codg_insc = $('#' + codg_part + '_codg_insc').val();	
		if (codg_insc){
			eliminar(codg_part, codg_insc);
		}
		else{
			var tipo = $('input:radio[name=tipo_insc]:checked').val();
			if (!tipo){
				alert('Debes Seleccionar Tipo de Inscripción');
			}
			else{
				guardar(codg_part);
			}
		}
	}
	function eliminar(codg_part, codg_insc){
		var parametros = {
			"var_tabla" : "inscripcion",
			"var_id" : "codg_insc",
			"var_id_val" : codg_insc
		};
		var url="../sistema/comunes/funcion_eliminar.php"; 
		$.ajax
		({
		    type: "POST",
		    url: url,
          	data: parametros,
          	beforeSend: function () {

		    },
		    success: function(data)
		    {
				var codigo, datatemp, mensaje;
				datatemp=data;
				datatemp=datatemp.split(":::");
				codigo=datatemp[0];
				mensaje=datatemp[1];
				if (codigo=001){
					$('#' + codg_part + '_codg_insc').val('');
					muestra_oculta(codg_part + '_exito');
					muestra_oculta(codg_part + '_exito_off');
				}
		    }
		});
		return false;
	}
	function guardar(codg_part) {
		var precio = 0.00 ;
		var tipo = $('input:radio[name=tipo_insc]:checked').val();
		if (tipo = 'Contado'){ precio = $('#base_insc').val(); }
		var parametros = {
			"var_tabla" : "inscripcion",
			"codg_part" : codg_part,
			"codg_aper" : $('#codg_aper').val(),
			"codg_secc" : $('#' + codg_part + '_codg_secc').val(),
			"fech_insc" : '<?php echo date('Y-m-d'); ?>',
			"apro_insc" : 'E',
			"codg_empr" : '<?php echo $_SESSION[codg_empr]; ?>',
			"tipo_insc" : tipo,
			"base_insc" : precio
		};
		var url="../sistema/comunes/funcion_guardar.php"; 
		$.ajax
		({
		    type: "POST",
		    url: url,
		    data: parametros,
          	beforeSend: function () {

		    },
		    success: function(data)
		    {
		      var codigo, datatemp, mensaje;
		      datatemp=data;
		      datatemp=datatemp.split(":::");
		      codigo=datatemp[0];
		      mensaje=datatemp[1];
		      respuesta = datatemp[2];
      		  if(codigo==001){
					$('#' + codg_part + '_codg_insc').val(respuesta);
					muestra_oculta(codg_part + '_exito');
					muestra_oculta(codg_part + '_exito_off');
      		  }
		    }
		});
		return false;
	}
</script>
<meta charset="utf-8" />
<div class="interesa">
	<div class="interesa-titulo x2" align="center">RESERVACIÓN</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
	  <table width="80%" cellpadding="0" cellspacing="0" border="0" align="center" style="font-size: 11px;">
      </br>
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
	      echo "<input type='hidden' name='codg_aper' id='codg_aper' value='".$codg_aper."'>";
         ////////////////////////////////////////       		  	   
	      ?>
	    <div id="mensaje_resultado"></div>
		<table width="100%">
			<tr>
				<td width="100px">
					<div class="interesa-etiqueta x12">Evento</div>
				</td>
				<td>
					<div class="interesa-resultado x12"><?PHP echo $reg_aper['nomb_evnt']; ?></div>			
				</td>
			</tr>				
		</table>
		<br>
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<table width="100%">
					<tr>
						<td width="100px">
							<div class="interesa-etiqueta x12">Inicia</div>
						</td>
						<td>
							<div class="interesa-resultado x12"><?php echo ordernar_fecha($reg_aper['fini_aper']); ?></div>
						</td>
					</tr>				
				</table>
			</div>
			<div class="visible-xs col-xs-12">&nbsp;</div>
			<div class="col-md-6 col-xs-12">
				<table width="100%">
					<tr>
						<td width="100px">
							<div class="interesa-etiqueta x12">Finaliza</div>
						</td>
						<td>
							<div class="interesa-resultado x12"><?php echo ordernar_fecha($reg_aper['fini_aper']); ?></div>			
						</td>
					</tr>				
				</table>
			</div>
				<?php
				$select_financiar = '';
				$sql_cuotas = "SELECT * FROM aperturas_cuotas WHERE codg_aper = ".$codg_aper." AND empr_cuot >0 ";
				$bus_cuotas = mysql_query($sql_cuotas);
				while ($res_cuotas = mysql_fetch_array($bus_cuotas)){
					$select_financiar = '<input name="tipo_insc" type="radio" value="Financiada">Financiada</label>';
					$contado_selected = '';
				}
				$contado_selected = 'checked="checked"';
				if (!$mostrar_opcion) {
					echo '<div class="col-md-12 col-xs-12" style="margin-top: 30px;">';
						echo '<div class="event-datos-titulo x15"><b>Selecciona la Modalidad</b></div>';
						echo '<div class="event-datos x12 table-responsive">';
							echo '<table id="event-table" width="100%" border="0" align="center">';
								echo '<tr><td ><label>
									<input type="hidden" name="base_insc" id="base_insc" value="'.$precio_evento.'">
									<input id="tipo_insc" name="tipo_insc" type="radio" value="Contado" '.$contado_selected.'>&nbsp;De Contado
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$select_financiar.'</td></tr>';
							echo '</table>';
						echo '</div>';
					echo '</div>';
				}
				echo '<div class="col-md-12 col-xs-12">';
					echo '<div class="event-datos-titulo x15"><b>Secciones Disponibles</b></div>';
					echo '<div class="event-datos x12 table-responsive">';
						echo '<table id="event-table" width="100%" border="0" align="center">';
							echo '<tr> <td width="9%">&nbsp;Secci&oacute;n</td> <td>&nbsp;Horario</td> <td width="25%">&nbsp;Lugar</td></tr>'; 
								$i=0;
								do{
									$i+=1;
									echo '<tr class="x075">
											<td align="center">&nbsp;'.$reg_aper[nomb_secc].'</td>
											<td>'.$reg_aper[hora_secc].'</td>
											<td>'.$reg_aper[lugr_secc].'</td>
										  </tr>';
							  		$opciones .= '<option value="'.$reg_aper[codg_secc].'">'.$reg_aper[nomb_secc].'</option>';
								}while($reg_aper=mysql_fetch_array($busq_aper));
							echo '</table>';
					echo '</div>';
				echo '</div>';
				$sql = "SELECT * FROM participantes WHERE codg_empr = ".$usuario[codg_empr]." ORDER BY nomb_part, apel_part";
				$busq = mysql_query($sql);
				echo '<div class="col-md-12 col-xs-12">';
					echo '<div class="event-datos-titulo x15"><b>Selecciona los Participantes que desea inscribir</b></div>';
					echo '<div class="event-datos x12 table-responsive">';
						echo '<table id="event-table" width="100%" border="0" align="center" style="font-size: 12px; padding-top: 0px;">';
								$i=0;
								while ($resp = mysql_fetch_array($busq)){
									$i+=1;
									echo '
									<tr align="center">
									  <td style="padding-top: 0px; padding-bottom: 0px;" width="9%">&nbsp;'; echo $resp["cedu_part"]; echo '</td>
								  	  <td style="padding-top: 0px; padding-bottom: 0px;" width="29%">&nbsp;'.$resp[nomb_part].'</td>
									  <td style="padding-top: 0px; padding-bottom: 0px;" width="25%">&nbsp;'.$resp[apel_part].'</td>
									  <td style="padding-top: 5px; padding-bottom: 5px;">&nbsp;<select name="'.$resp[codg_part].'_codg_secc" id="'.$resp[codg_part].'_codg_secc" class="combo_seccion form-control" style="font-size: 12px;">'.$opciones.'</select></td>
									  <td style="padding-top: 0px; padding-bottom: 0px;" width="13%">&nbsp;';
										echo'<img id="'.$resp[codg_part].'_exito" src="'.$url_base2.'sistema/imagenes/exito.png" style="width: 25px; cursor: pointer; float: right;" onclick="registrar('.$resp[codg_part].');return false;">';
										echo'<img id="'.$resp[codg_part].'_exito_off" src="'.$url_base2.'sistema/imagenes/exito_off.png" style="width: 25px; cursor: pointer;  float: right;"  onclick="registrar('.$resp[codg_part].');return false;">';
										$sql_ex = "SELECT * FROM inscripcion WHERE codg_aper = ".$codg_aper." AND codg_part = ".$resp[codg_part];
										if ($res_ex = mysql_fetch_array(mysql_query($sql_ex))){
											echo '<script type="text/javascript">muestra_oculta("'.$resp[codg_part].'_exito_off");</script>';
										}
										else{
											echo '<script type="text/javascript">muestra_oculta("'.$resp[codg_part].'_exito");</script>';
										}
										echo '<input type="hidden" id="'.$resp[codg_part].'_codg_insc" name="'.$resp[codg_part].'_codg_insc" value="'.$res_ex[codg_insc].'">';
									echo'</td>
							     		</tr>';
								}
							echo '</table>';
					echo '</div>';
				echo '</div>';
				?>
			</div>
		</div>
<script type="text/javascript"></script>
	</div>
</form>