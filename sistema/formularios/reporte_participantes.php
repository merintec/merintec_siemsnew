<?php 
error_reporting(0);
include("../sistema/comunes/verificar_admin.php");
include("../comunes/verificar_sesion.php");
include("../comunes/conexion.php");
include("../comunes/funciones_php.php");

$codg_aper=$_POST['codg_aper'];
$codg_secc=$_POST['codg_secc'];

//CONSULTAS COMBOS
$consulta_eventos_aperturas = mysql_query("SELECT * FROM eventos_apertura, eventos where eventos_apertura.codg_evnt=eventos.codg_evnt AND eventos_apertura.codg_aper!='$codg_aper' order by eventos.nomb_evnt ");
if ($codg_aper!='')
{
	$consulta_eventos_aperturas1 = mysql_query("SELECT * FROM eventos_apertura, eventos where eventos_apertura.codg_aper='$codg_aper' and eventos.codg_evnt=eventos_apertura.codg_evnt ");
	$conev=mysql_fetch_assoc($consulta_eventos_aperturas1);
	$nomb_evnt=$conev[nomb_evnt]." del ".$conev[fini_aper]." al ".$conev[ffin_aper];
}

if ($codg_aper!='')
{      
	$consulta_apertura_secciones = mysql_query("SELECT * FROM eventos_secciones WHERE codg_aper='$codg_aper' ORDER BY nomb_secc");
}

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>.:: SIEMS Instituto Gerencial ::.</title>
        
        <script src="../validacion/js/jquery-1.8.2.min.js" type="text/javascript"></script>
		  <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		  <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		  <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
		  <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
		  <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    </head>
    

<link href="../css/sm_estilos.css" rel="stylesheet" type="text/css">

<!-- validacion en vivo -->
<script >

	jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
		});

</script>
<!-- CALENDARIO-->
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			$( ".datepicker" ).datepicker();
			jQuery("form1").validationEngine();
		});
</script>

<body>

	<div class="titulo_formulario" align="center">REPORTE DE PARTICIPANTES</div>
	<?php include('../general/mensaje.php'); ?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="reporte_participantes.php">
	<table cellpaddig="0" cellspacing="0" border="0" align="center">
	      </br>
	      </br>	
      		<?php 
      		echo '
		<tr>
			<td align="center">
				<select name="codg_aper" id="codg_aper"  class="validate[required], combo_form" onchange="submit()" >';
				echo ' <option value="" selected disabled style="display:none;">Seleccione la apertura de evento</option>';
				if($nomb_evnt){
					echo' <option    value="'.$codg_aper.'" selected>'.$nomb_evnt.'</option> ';
				}
       				while($fila=mysql_fetch_array($consulta_eventos_aperturas))
				{
					echo "<option value=".$fila[codg_aper].">".$fila[nomb_evnt]." del ".$fila[fini_aper]." al ".$fila[ffin_aper]."</option>";
                  		}
				echo '</select>
			</td>
			<td align="center">
				<select name="codg_secc" id="codg_secc"  class="validate[required], combo_form" onchange="submit()" >';
				echo ' <option value="" selected disabled style="display:none;">Seleccione la Sección</option>';
       				while($fila=mysql_fetch_array($consulta_apertura_secciones))
				{
					if ($fila[codg_secc]==$_POST[codg_secc]) { $add_sel = "selected"; } else { $add_sel=''; } 
					echo "<option ".$add_sel." value=".$fila[codg_secc].">".$fila[nomb_secc]."</option>";

                  		}
				echo '</select>

			</td>

		</tr>';
		echo '</table>';         		  	   
		?>


		<!--//// funcion para guardar mediante JQuery -->
		<script>
			function realizaProceso(observacion,codg_insc,i,accion){
			    var parametros = {
			    	"observacion" : observacion,
			    	"codg_insc" :codg_insc,
			    	"i":i,
			    	"accion":accion
 
 
					};

					if (observacion)
					{
						$.ajax({
							data:  parametros,
						    url:   'guardar_reporte_participantes.php',
						    type:  'post',
						    beforeSend: function () 
						    {
						        $("#resultante" + i).html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/acciones/cargando_gray.gif' width='30px'></td>");
						    },
						    success:  function (response) 
						    {
						    	$("#resultante" + i).html(response);
						    }
						});
					}
					else
					{
						alert('Debe Indicar la observación ');
					}
					
			}
		
		</script>
		<!--/////////////////////////////////////////-->



	<?php if ($_POST[codg_secc] && $_POST[codg_aper]) { ?>
	<table width="100%" border="1" align="center" cellspacing="0" id="listados">
		<tr align="center"  style="font-size: 11px;">
			<th> Nº </th><th>Cédula</th><th align="left">Nombre y Apellido</th> <th align="left">Tipo Participante</th> <th align="left">Empresa</th>  <th align="left">Teléfono</th> <th align="left">Email</th><th align="left">Monto Cancelado</th><th align="left">Monto Deuda</th>     <th align="left">Status Inscripción</th>  <th align="left">Descuento</th> <th align="left" style="min-width: 140px;">Observaciones</th>    
		</tr>
		<?php
			$sql_part = "SELECT *, (SELECT nomb_empr FROM empresas WHERE codg_empr=vi.codg_empr) as empresa FROM vista_inscripciones vi, participantes part WHERE vi.codg_aper = ".$codg_aper." AND vi.codg_secc=".$codg_secc." AND part.codg_part=vi.codg_part ORDER BY vi.apro_insc" ;
			$bus_part = mysql_query($sql_part);

			$i=0;
			
			while ($res_part = mysql_fetch_array($bus_part)){
				$i++;
            //para sacar el tipo de participante				
			 $consulta_tipo_participante = mysql_query("SELECT * FROM  participantes_tipos where codg_tpar=$res_part[codg_tpar] ");
	         $contpar=mysql_fetch_assoc($consulta_tipo_participante);
		
		if (!$res_part[empresa] && $res_part[codg_tpar]==3){							
			 $consulta_tipo_participante = mysql_query("SELECT * FROM  participantes_tipos where codg_tpar=2 ");
		         $contpar=mysql_fetch_assoc($consulta_tipo_participante);
		}
	         
	         //sumar el total que ha cancelado en los pagos de una inscripcion
	          
	         if ($res_part[empresa])
	         {
	         	  $consulta_total_cancelado = mysql_query("SELECT SUM(mont_pago) as total_cancelado, fech_pago, nfac_pago, apro_pago FROM  pagos where codg_aper='$res_part[codg_aper]' and codg_empr='$res_part[codg_empr]'");

	      		  $contcan=mysql_fetch_assoc($consulta_total_cancelado);
	      		
	      		 // echo "SELECT SUM(mont_pago) as total_cancelado, fech_pago, nfac_pago, apro_pago FROM  pagos where codg_insc='$res_part[codg_insc]' and codg_aper='$res_part[codg_aper]' and codg_empr='$res_part[codg_empr]'";
	        	//verificar si tiene descuento
	        	 $consulta_descuentos = mysql_query("SELECT * FROM  aperturas_descuentos where codg_aper='$res_part[codg_aper]' and codg_empr='$res_part[codg_empr]'  ");
	       		 $condes=mysql_fetch_assoc($consulta_descuentos);
	       		  

	         }
	         else
	         {
	         	  $consulta_total_cancelado = mysql_query("SELECT SUM(mont_pago) as total_cancelado, fech_pago, nfac_pago, apro_pago FROM  pagos where codg_insc='$res_part[codg_insc]' ");
	         	  $contcan=mysql_fetch_assoc($consulta_total_cancelado);
	         	  //echo "SELECT SUM(mont_pago) as total_cancelado, fech_pago, nfac_pago, apro_pago FROM  pagos where codg_insc='$res_part[codg_insc]' ";
	         	  //verificar si tiene descuento
	              $consulta_descuentos = mysql_query("SELECT * FROM  aperturas_descuentos where codg_insc=$res_part[codg_insc] ");
	              $condes=mysql_fetch_assoc($consulta_descuentos);     
	         }
	         $descuentoval=$condes[mont_desc].''.$condes[tipo_desc];

	         //evaluar si tiene o no empresa para colocar una linea de relleno para que no quede el campo tan vacio 
	         if ($res_part[empresa]==NULL)
	         {
	         	$empresa='----------';
	         }
	         else 
	         {
	         	$empresa=$res_part[empresa];
	         }

///////
$res_part[deuda_final] = 0;
$deuda = 0;
$totalizar = 0;
$descuento = 0;
$total_conformado = 0;
//// pagos que debe efectuar
$participante = buscar("participantes","cedu_part",$res_part[cedu_part],'individual');

if ($participante[0][codg_tpar]==1) { $precio = 'prec_aper'; $tcuot='estu_cuot';}
if ($participante[0][codg_tpar]==2) { $precio = 'prep_aper'; $tcuot='prof_cuot';}
if ($participante[0][codg_tpar]==3) { $participante[0][codg_tpar]=2; $precio = 'prep_aper'; $tcuot='prof_cuot';}

$sql_deuda = "SELECT vi.tipo_insc, vi.base_insc, vi.codg_aper, vi.pgen_aper, vi.prec_aper, vi.prep_aper, vi.prem_aper, (SELECT SUM(".$tcuot.") FROM aperturas_cuotas WHERE codg_aper = vi.codg_aper ) as monto_cuotas FROM vista_inscripciones vi WHERE codg_insc = ".$res_part[codg_insc];
$bus_deuda = mysql_query($sql_deuda);
$res_deuda = mysql_fetch_array($bus_deuda);

if ($res_deuda[tipo_insc]=='Contado'){
	$deuda = $res_deuda[base_insc];
}
if ($res_deuda[tipo_insc]=='Financiada'){
	$deuda = $res_deuda[monto_cuotas];
	$sql_cuotas = "SELECT * FROM aperturas_cuotas WHERE codg_aper = ".$res_deuda[codg_aper];
	$bus_cuotas = mysql_query($sql_cuotas);
	while ($res_cuotas = mysql_fetch_array($bus_cuotas)){
		$totalizar += $res_cuotas[$tcuot];
	}
}
///// Descuento recibido en esta Apertura
$sql_desc = "SELECT * from aperturas_descuentos WHERE codg_insc = ".$res_part[codg_insc].";";
$bus_desc = mysql_query($sql_desc);
if ($res_desc = mysql_fetch_array($bus_desc)){
	if ($res_desc[tipo_desc]=='%'){ $mostrar_desc = '('.$res_desc[mont_desc].'%)'; $descuento = $deuda * $res_desc[mont_desc] / 100; }
	if ($res_desc[tipo_desc]=='Bs.') { $descuento = $res_desc[mont_desc]; }
	$deuda = $deuda - $descuento;
} 
///// pagos efectuados
 		$sql_pago="select pa.*, CONCAT(bn.nomb_banc,' (',bn.numr_cuen,')') as banc_pago  FROM pagos pa, banco bn  where pa.codg_insc='".$res_part[codg_insc]."' AND pa.codg_banc=bn.codg_banc ORDER BY pa.fech_pago,pa.refe_pago ASC";
		$busq_pago=mysql_query($sql_pago);
		if($reg_pago=mysql_fetch_array($busq_pago)){
			$h=0;
			do{
				$h+=1;
				$res=$h%2;
				if($res==0){ $clase="lista_tabla2"; }else{ $clase="lista_tabla1"; }
				if($reg_pago[apro_pago]=="A"){ $total_conformado += $reg_pago[mont_pago]; }
			}while($reg_pago=mysql_fetch_array($busq_pago));
		}
///// totalizando
$res_part[deuda_final] = $deuda-$total_conformado;
$add_back = '';
$res_part[deuda_final];
if 	($res_part[deuda_final] > 0){
	$add_back = 'background-color: #e7d8d8;';
}else { $add_back = ''; }
/////////
	         
			echo '
			<tr style="'.$add_back.' font-size: 11px;">
				<td>'.$i.' </td> <td align="right">'.number_format($res_part[cedu_part],0,'','.').'&nbsp;</td><td>&nbsp;'.$res_part[nomb_part].' '.$res_part[apel_part].'</td><td>'.$contpar[nomb_tpar].'</td><td align="center">'.$empresa.'</td><td>'.$res_part[tlfn_part].'</td><td>'.$res_part[corr_part].'</td><td  align="right">'.number_format($contcan[total_cancelado],2,',','.').'</td><td align="right">'.number_format($res_part[deuda_final],2,',','.').'</td>  <td>'.$res_part[apro_insc].'</td> <td>'.$descuentoval.'</td>';
			     echo '<td align="left" width="12%">';
				
				    include ('aux_reporte_participantes.php');
				echo '</td>';
			 
			 echo'</tr>';


			}
		?>
			</form>
	</table>
	<?php } ?>
	</body>
</html>
