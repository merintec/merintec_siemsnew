<?php 
	session_start();
	$url_base = "../../";
	$url_base2 = "../";
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	$codg_aper=$_GET['codg_aper'];
	$codg_part=$_SESSION['codigo_part'];
	$cedula_usuario=$_SESSION['cedula_usuario'];
	$participante = buscar("participantes","cedu_part",$cedula_usuario,'individual');
	$reg_enc=0;
	if($participante[1]>=1){
		$sql_aper="select * from vista_inscripciones where codg_part='".$codg_part."' and apro_insc='A'";
		$busq_aper=mysql_query($sql_aper);
		if($reg_aper=mysql_fetch_array($busq_aper)){
			$reg_enc=1;
		}
	}
?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
</head>
<body>
    <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">		
		<span class="titulo-perfil">Historial de Eventos Inscritos</span><br>
		<span class="subtitulo-perfil">Si efectuó el registro de un pago recientemente, es posible que existan inscripciones por aprobar.</span>
		<div id="resultado"></div>
		<div class="row-fluid">
			<div class="col-md-11 col-xs-11">&nbsp;</div>
		</div>
		<div class="row-fluid">
			<div class="col-md-11 col-xs-11">
				<div class="table-responsive"> 
				  	<table width="100%" id="lista-table" class="x1">
						<tr class="cajas_entrada" align="center">
						  <th>&nbsp;Evento&nbsp;</th>
						  <th width="50px">&nbsp;Secci&oacute;n&nbsp;</th>
						  <th width="110px">&nbsp;Inscripci&oacute;n&nbsp;</th>
						  <th width="110px">&nbsp;Inicio&nbsp;</th>
						  <th width="110px">&nbsp;Fin&nbsp;</th>
					   	</tr>
						<?PHP 
							if($reg_enc==1){
								$i=0;
								do{
									$i+=1;
									$res=$i%2;
									if($res==0){ $clase="lista_tabla2"; }else{ $clase="lista_tabla1"; }
									echo '<tr class="'.$clase.'">
											<td>&nbsp;'.$reg_aper[nomb_evnt].'</td>
											<td align="center">&nbsp;'.$reg_aper[nomb_secc].'</td>
											<td align="center">&nbsp;'.ordernar_fecha($reg_aper[fech_insc]).'</td>
											<td align="center">&nbsp;'.ordernar_fecha($reg_aper[fini_aper]).'</td>
											<td align="center">&nbsp;'.ordernar_fecha($reg_aper[ffin_aper]).'</td>
										  </tr>';
								}while($reg_aper=mysql_fetch_array($busq_aper));
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</form>
</body>
</html>