<?php 
include("sistema/comunes/verificar_admin_diseno.php");
$boton=$_POST['boton'];

/// preparando para la imagen del area
$archivo = $_FILES["imag_area"]["tmp_name"];
$tami_area = $_FILES["imag_area"]["size"];
$tipi_area = $_FILES["imag_area"]["type"];
$nomi_area = $_FILES["imag_area"]["name"];
$imgr_area=$_POST['imgr_area'];

if($tami_area>0){
	if($imgr_area!=""){
		unlink($imgr_area);
	}
    $destino = "sistema/imagenes/imagenes_areas/".$nomi_area;
	$destino = str_replace(" ","_",$destino);
    if(move_uploaded_file($archivo, $destino)) {
		echo "";
	}else{
		echo "archivo no movido";
	}
}



$nomb_area=$_POST['nomb_area'];
$codg_area=$_POST['codg_area'];
$parametro=$_POST['parametro'];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "eventos_areas";
$key_entabla = 'codg_area';
$key_enpantalla = $codg_area;
$datos[0] = prepara_datos ("nomb_area",$_POST['nomb_area'],'');
$datos[1] = prepara_datos ("imag_area",$destino,'');
$datos[2] = prepara_datos ("stat_area",$_POST['stat_area'],'');
if ($boton=='Guardar'){
	$buscando = buscar($tabla,'nomb_area',$_POST[nomb_area],'individual');
	if ($buscando[1]<1) {
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
			$mensaje_mostrar=$ejec_guardar[1];
		}
	}
	else {
		$mensaje_mostrar = 'Error: El Área '.$_POST[nomb_area].' ya existe intente nuevamente';
		$boton = '';
	}
}
if ($boton=='Eliminar')
{
	$buscando_tipo = buscar('eventos','codg_area',$_POST['codg_area'],'individual');
	if ($buscando_tipo[1]<1) {
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}else{
		$mensaje_mostrar='Área no puede eliminarse debido a que hay eventos asociados';
		$boton='Eliminando';
	}
}
if ($boton=='Actualizar')
{
	if($tami_area==0 and $imgr_area!=""){
		$datos[1][1]=$imgr_area;
	}	
	$buscando = buscar($tabla,'nomb_area',$_POST[nomb_area].' and codg_area = '.$_POST[codg_area],'individual');
	if ($buscando[1]<1) {
			$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
			$existente='si';        
			$mensaje_mostrar=$ejec_actualizar[1];
			$$key_entabla = $ejec_actualizar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
	}
	else {
		$mensaje_mostrar = 'Error: El Área '.$_POST[nomb_area].' ya existe intente nuevamente';
		$iramodificar="si";
		$boton = 'Modificar';		
	}
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'individual');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	if($iramodificar){ $mensaje_mostrar .= "<br><br>No ha efectuado cambios o ya existe el Área"; }
	$existente='no';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}
?>
<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE TIPOS DE AREAS</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Área";
		$buscar_varios[0][1]="nomb_area";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="" enctype="multipart/form-data">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
		<?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
			{
				echo '<input type="hidden" name="codg_area" id="codg_area" value="'.$con[codg_area].'">';	
				echo '
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[nomb_area].'" id="nomb_area" name="nomb_area" placeholder="Nombre del Area" />
					</td>
				</tr>
				<td aling="center"> <input type="file" class="cajas_entrada"  id="imag_area" name="imag_area" placeholder="Imagen del �rea" />   </td> </tr>
				<tr><td>&nbsp;<input name="imgr_area" type="hidden" value="'.$con['imag_area'].'" /></td></tr>'; 
				echo '
			   <tr>
					<td>
						<label id="etiqueta">Status:</label> ';
        					echo '<input class="validate[required] radio" type="radio" name="stat_area" id="Activo"';  if ($con[stat_area]=='Activo') { echo 'checked="checked"'; } echo 'value="Activo"/> <label id="etiqueta">Activo</label>';
        					echo '<input class="validate[required] radio" type="radio" name="stat_area" id="Desactivo"';  if ($con[stat_area]=='Desactivo') { echo 'checked="checked"'; } echo 'value="Desactivo"/> <label id="etiqueta">Desactivo</label>';
                		echo '
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>';
			}
			else 
			{
				echo '<input type="hidden" name="codg_area" id="codg_area" value="'.$con["codg_area"].'">';	
				echo '
				<tr>
					<td align="left" colspan="3">
						<label id="etiqueta"> Nombre de Area: </label> <label id="resultado">'.$con["nomb_area"].' </label>
					</td>
				</tr>
				<tr><td><br><center><img src="'.$con['imag_area'].'" ></center></td></tr>
				<tr><td>&nbsp;</td></tr>';
         		 echo '<tr>
         			<td align="left">
         				<label id="etiqueta"> Status:</label> <label id="resultado"> '.$con['stat_area'].' </label>
         			</td>
         		</tr>';


			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Área de Evento";
		$buscar_parm[0][1]="nomb_area";
		include('sistema/general/busqueda.php');?>
