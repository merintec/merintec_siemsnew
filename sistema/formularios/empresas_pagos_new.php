<?php 
	session_start();
	$url_base = "../../";
	$url_base2 = "../";
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	$codg_empr=$_SESSION['codg_empr'];

	$sql_aper="select vi.*, count(vi.codg_part) as inscritos, (SELECT sum(mont_pago) FROM pagos WHERE codg_empr=vi.codg_empr AND codg_aper=vi.codg_aper AND apro_pago='A' GROUP BY vi.codg_aper) as pagado from vista_inscripciones vi where vi.codg_empr='".$codg_empr."' GROUP BY vi.codg_aper ORDER BY pagado,vi.fini_aper, vi.nomb_evnt";
	$busq_aper=mysql_query($sql_aper);
	$reg_aper=mysql_fetch_array($busq_aper);
?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
   	<script>
   		//Precargar imagen timer
		jQuery.preloadImages = function() {
			for(var i = 0; i<arguments.length; i++){
				jQuery("<img>").attr("src", arguments[i]);
			}
		}
		$.preloadImages('../sistema/imagenes/cargando.gif');

	    $(document).ready(function() {
	        $('.datepicker')
	            .datepicker({
	              format: 'dd-mm-yyyy',
	              autoclose: true,
	              language: 'es'
	            });
	    });

	</script>
</head>
<body>
    <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">		
		<span class="titulo-perfil">Pago de Reservaciones de la Empresa</span><br>
		<div id="resultado"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-7 col-xs-11">
				<span class="subtitulo-perfil">También puedes visitar nuestros eventos</span>

				<?php 
					//CONSULTA DE EVENTOS PARA EL COMBO DE RESERVACION
					$consulta_eventos_aperturas = mysql_query("SELECT * FROM eventos_apertura, eventos where eventos_apertura.codg_evnt=eventos.codg_evnt  AND stat_evnt='Activo' AND stat_aper='Activo' order by eventos.nomb_evnt ");
					echo '<select name="busq" id="busq"  class="validate[required], text-input form-control" onchange="window.location=(\''.$url_base2.'frontend/evento.php?tipo=&area=&evento=\' + $(\'#busq\').val() + \'&frm=perfil\')">';
						echo ' <option value="" selected disabled style="display:none;">Seleccione la apertura de evento</option>';
		   				while($fila=mysql_fetch_array($consulta_eventos_aperturas))
						{
							echo "<option value=".$fila[codg_evnt].">".$fila[nomb_evnt]." del ".$fila[fini_aper]." al ".$fila[ffin_aper]."</option>";
		           		}
					echo '</select>';
				?>				
			</div>
			<div class="row-fluid"><div class="col-md-11 col-xs-11">&nbsp;</div>
			<div class="row-fluid">
				<div class="col-md-11 col-xs-11">
					<div class="table-responsive"> 

						<table width="100%" id="lista-table" class="x1">
							<tr>
							  <th >&nbsp;Evento&nbsp;</th>
							  <th width="98px">&nbsp;Inicio&nbsp;</th>
							  <th width="50px">&nbsp;Inversi&oacute;n&nbsp;</th>
							  <th width="50px">&nbsp;Acci&oacute;n&nbsp;</th>
						   	</tr>
							<?PHP 
								$i=0;
								do{
									$i+=1;
									$tcuot = 'empr_cuot';
									if ($reg_aper[tipo_insc]=='Contado'){
										$precio_evento = $reg_aper[base_insc]; 
									}
									if ($reg_aper[tipo_insc]=='Financiada'){
										// consultamos si es por cuotas
										$sql_cuotas = "SELECT SUM(".$tcuot.") as total_cuotas FROM aperturas_cuotas WHERE codg_aper = ".$reg_aper[codg_aper]." GROUP BY codg_aper";
										$bus_cuotas = mysql_query($sql_cuotas);
										if ($res_cuotas = mysql_fetch_array($bus_cuotas)){
											$precio_evento = $res_cuotas[total_cuotas];
										} 				
									}
									// Consultamos si existe un descuento
									$precio_pagar = $precio_evento * $reg_aper[inscritos];
									$sql_desc = "SELECT * FROM aperturas_descuentos WHERE codg_empr = ".$codg_empr." AND codg_aper=".$reg_aper[codg_aper].";";
									$bus_desc = mysql_query($sql_desc);
									if ($res_desc = mysql_fetch_array($bus_desc)){
										if ($res_desc[tipo_desc]=='%'){ $mostrar_desc = '('.$res_desc[mont_desc].'%)'; $descuento =  $precio_pagar * $res_desc[mont_desc] / 100; }
										if ($res_desc[tipo_desc]=='Bs.') { $descuento = $res_desc[mont_desc]; }
										$precio_pagar = $precio_pagar - $descuento;
									}
									$add_url = '&pago=no';
									if ($reg_aper[pagado]<$precio_pagar) {
										$add_url = '';			
									}
									echo '<tr>
											<td align="left">&nbsp;<b>'.$reg_aper[nomb_evnt].'</b></td>
											<td align="center">&nbsp;'.ordernar_fecha($reg_aper[fini_aper]).'</td>
											<td align="right">&nbsp;'.number_format($precio_pagar,2,',','.').'&nbsp;</td>
											<td align="center">'; 
											if ($reg_aper[pagado]<$precio_evento) {
												echo '<button class="btn boton-peq" onclick="pagar_inscripcion('.$reg_aper[codg_aper].');return false;">Pagar</button';
											}
											else {
												echo '<img height="30px" title="El pago se ha aprobado" src="'.$url_base2.'sistema/imagenes/exito.png">';
											}										  echo '</td>
										  </tr>';
								}while($reg_aper=mysql_fetch_array($busq_aper));
							?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>