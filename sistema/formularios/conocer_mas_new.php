<?php 
	$url_base = "../../";
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	/*for ($i=0; $i<=$espera_tiempo; $i++){	}*/
?>
<?php
$codg_aper= str_replace("'", "", $_POST['apertura']);
$nomb_evnt= str_replace("'", "", $_POST['evento']);
$nomb_evnt= str_replace("\\", "", $_POST['evento']);
$nomb_evnt= str_replace("'", "", $nomb_evnt);
$fini_evnt= str_replace("'", "", $_POST['inicio']);
$ffin_evnt= str_replace("'", "", $_POST['fin']);
$codg_evnt=$_POST['codg_evnt'];

$nomb_intr=$_POST['nomb_intr'];
$tlfn_intr=$_POST['tlfn_intr'];
$corr_intr=$_POST['corr_intr'];
$boton=$_POST['boton'];

if($boton=='Enviar'){
	/// Preparando datos para guardar
	$tabla = "interesados";
	$key_entabla = 'codg_intr';
	$key_enpantalla = $codg_intr;
	$datos[0] = prepara_datos ("nomb_intr",$nomb_intr,'');
	$datos[1] = prepara_datos ("tlfn_intr",$tlfn_intr,'');
	$datos[2] = prepara_datos ("corr_intr",$corr_intr,'');
	$datos[3] = prepara_datos ("codg_curso",$codg_evnt,'');
	$datos[4] = prepara_datos ("fech_intr",date('Y-m-d'),'fecha');
	$datos[5] = prepara_datos ("codg_aper",$codg_aper,'');

	$ejec_guardar = guardar($datos,$tabla);
	$mensaje_mostrar = '<span class="interesa-titulo x15">Error... No se ha almacenando la información.</span><br><img src="../sistema/imagenes/icono_rechazado.png" width="50" /><br><br>';;
	if ($ejec_guardar[0]!=''){
		$existente='si';
		$$key_entabla = $ejec_guardar[0];
		$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
		$con=$con2[0];
		$auditoria=$con2[3];
	    $mensaje_mostrar = '<span class="interesa-titulo x15">Los datos se han guardado con éxito</span><br><img src="../sistema/imagenes/exito.png" width="50" /><br><br>';
	}
	echo $mensaje_mostrar;
}
else {
?>
<meta charset="utf-8" />	
	<div class="interesa">
		<div class="interesa-titulo x3">Conocer Más</div>
		<div id="mensaje_resultado"></div>
		<table width="100%">
			<tr>
				<td width="100px">
					<div class="interesa-etiqueta x12">Evento</div>
				</td>
				<td>
					<div class="interesa-resultado x12"><?php echo $nomb_evnt; ?></div>			
				</td>
			</tr>				
		</table>
		<br>
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<table width="100%">
					<tr>
						<td width="100px">
							<div class="interesa-etiqueta x12">Inicia</div>
						</td>
						<td>
							<div class="interesa-resultado x12"><?php if ($fini_evnt==""){ echo 'Por Definir'; } else { echo ordernar_fecha($fini_evnt); } ?></div>
						</td>
					</tr>				
				</table>
			</div>
			<div class="visible-xs col-xs-12">&nbsp;</div>
			<div class="col-md-6 col-xs-12">
				<table width="100%">
					<tr>
						<td width="100px">
							<div class="interesa-etiqueta x12">Finaliza</div>
						</td>
						<td>
							<div class="interesa-resultado x12"><?php if ($ffin_evnt==""){ echo 'Por Definir'; } else { echo ordernar_fecha($ffin_evnt); } ?></div>			
						</td>
					</tr>				
				</table>
			</div>
			<div class="col-md-12 col-xs-12"><hr></div>
			<div class="hidden-xs col-md-3">&nbsp;</div>
			<div class="col-md-6 col-xs-12">
				<div id="formulario">
			        <form method="POST" name="registrar_interesado" id="registrar_interesado" onsubmit="return jQuery(this).validationEngine('validate');">
			            <div class="row">                 
			        		<div class="input-group">
			            		<span class="input-group-addon interesa-fondo-boton" > <img width="20" src="../imagenes/page/ico-nombre.png"> </span>
			                    <input type="text" name="nomb_intr" id="nomb_intr" placeholder="Nombre" class="validate[required, minSize[3],maxSize[100]] text-input form-control campo_contacto" >
			                    <input type="hidden" name="codg_aper" id="codg_aper" value="<?php echo $codg_aper; ?>" placeholder="Código de Apertura" class="validate[required, minSize[3],maxSize[100]] text-input form-control campo_contacto" >
			       			</div>
			            </div>
			            <br>
			            <div class="row">
			        		<div class="input-group">
			            		<span class="input-group-addon interesa-fondo-boton" > <img width="20" src="../imagenes/page/ico-telefono.png"> </span>
			                    <input type="text" name="tlfn_intr" id="tlfn_intr" placeholder="Teléfono" class="validate[required, custom[phone] , minSize[3], maxSize[100]] text-input form-control campo_contacto " >
			        		</div>
			            </div>
			            <br>
			            <div class="row">
			                <div class="input-group">
			                		<span class="input-group-addon interesa-fondo-boton" > <img width="20" src="../imagenes/page/ico-correo.png"> </span>
			               			<input type="email" name="corr_intr" id="corr_intr" placeholder="Email" class="validate[required, custom[email] , minSize[3], maxSize[100]] text-input form-control campo_contacto" >
			               	</div>
			            </div>
			            <br>
			            <div class="row">               
			                 <button class="btn interesa-boton" onclick="registro_interesado();return(false);">ENVIAR</button>
			            </div>
			        </form>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
