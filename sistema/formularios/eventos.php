<?php 
include("sistema/comunes/verificar_admin_diseno.php");
$boton=$_POST['boton'];
$codg_evnt=$_POST['codg_evnt'];
$nomb_evnt=$_POST['nomb_evnt'];
$desc_evnt=$_POST['desc_evnt'];
$codg_area=$_POST['codg_area'];
$codg_tipo=$_POST['codg_tipo'];
$parametro=$_POST['parametro'];
$stat_evnt=$_POST['stat_evnt'];
$imgr_evnt=$_POST['imgr_evnt'];
$imgr_evnt2=$_POST['imgr_evnt2'];
$imgr_evnt3=$_POST['imgr_evnt3'];

$archivo = $_FILES["imag_evnt"]["tmp_name"];
$tami_evnt = $_FILES["imag_evnt"]["size"];
$tipi_evnt = $_FILES["imag_evnt"]["type"];
$nomi_evnt = $_FILES["imag_evnt"]["name"];

$archivo2 = $_FILES["imag_dipl"]["tmp_name"];
$tami_evnt2 = $_FILES["imag_dipl"]["size"];
$tipi_evnt2= $_FILES["imag_dipl"]["type"];
$nomi_evnt2 = $_FILES["imag_dipl"]["name"];

$archivo3 = $_FILES["pdf_dipl"]["tmp_name"];
$tami_evnt3 = $_FILES["pdf_dipl"]["size"];
$tipi_evnt3= $_FILES["pdf_dipl"]["type"];
$nomi_evnt3 = $_FILES["pdf_dipl"]["name"];

//para la imagen del evento

if($tami_evnt>0){
	if($imgr_evnt!=""){
		unlink($imgr_evnt);
	}
    $destino = "sistema/imagenes/imagenes_publicaciones/".$nomi_evnt;
	$destino = str_replace(" ","_",$destino);
    if(move_uploaded_file($archivo, $destino)) {
		echo "";
	}else{
		echo "archivo no movido";
	}
}

//para la imagen del diplomado

if($tami_evnt2>0){
	if($imgr_evnt2!=""){
		unlink($imgr_evnt2);
	}
    $destino2 = "sistema/imagenes/imagenes_publicaciones/".$nomi_evnt2;
	$destino2 = str_replace(" ","_",$destino2);
    if(move_uploaded_file($archivo2, $destino2)) {
		echo "";
	}else{
		echo "archivo no movido";
	}
}


//Pdf para del diplomado

if($tami_evnt3>0){
	if($imgr_evnt3!=""){
		unlink($imgr_evnt3);
	}
    $destino3 = "sistema/imagenes/imagenes_publicaciones/".$nomi_evnt3;
	$destino3 = str_replace(" ","_",$destino3);
    if(move_uploaded_file($archivo3, $destino3)) {
		echo "";
	}else{
		echo "archivo no movido";
	}
}

/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "eventos";
$key_entabla = 'codg_evnt';
$key_enpantalla = $codg_evnt;

$datos[0] = prepara_datos ("nomb_evnt",$_POST['nomb_evnt'],'');
$datos[1] = prepara_datos ("desc_evnt",$_POST['desc_evnt'],'');
$datos[2] = prepara_datos ("codg_area",$_POST['codg_area'],'');
$datos[3] = prepara_datos ("codg_tipo",$_POST['codg_tipo'],'');
$datos[4] = prepara_datos ("stat_evnt",$_POST['stat_evnt'],'');
$datos[5] = prepara_datos ("imag_evnt",$destino,'');
$datos[6] = prepara_datos ("imag_dipl",$destino2,'');
$datos[7] = prepara_datos ("pdf_dipl",$destino3,'');


if ($boton=='Guardar'){
	$buscando = buscar($tabla,'nomb_evnt',$_POST[nomb_evnt],'individual');
	if ($buscando[1]<1) {
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
			$mensaje_mostrar=$ejec_guardar[1];
		}
	}
	else {
		$mensaje_mostrar = 'Error: El evento '.$_POST[nomb_evnt].' ya existe intente nuevamente';
		$boton = '';
	}
}
if ($boton=='Eliminar')
{	
	$buscando_tipo = buscar('eventos_apertura','codg_evnt',$_POST['codg_evnt'],'individual');
	if ($buscando_tipo[1]<1) {
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}else{
		$mensaje_mostrar='Evento no puede eliminarse debido a que hay aperturas asociadas';
		$boton='Eliminando';
	}
}
if ($boton=='Actualizar')
{
	if($tami_evnt==0 and $imgr_evnt!=""){
		$datos[5][1]=$imgr_evnt;
	}
	if($tami_evnt2==0 and $imgr_evnt2!=""){
		$datos[6][1]=$imgr_evnt2;
	}

	if($tami_evnt3==0 and $imgr_evnt3!=""){
		$datos[7][1]=$imgr_evnt3;
	}
	$buscando = buscar($tabla,'nomb_evnt',$_POST[nomb_evnt]."' AND codg_evnt<> '".$_POST['codg_evnt'],'individual');
	if ($buscando[1]<1) {
			$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
			$existente='si';        
			$mensaje_mostrar=$ejec_actualizar[1];
			$$key_entabla = $ejec_actualizar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
	}
	else {
		$mensaje_mostrar = 'Error: El evento '.$_POST[nomb_evnt].' ya existe intente nuevamente';
		$iramodificar="si";
		$boton = 'Actualizando';
		$con2 = buscar($tabla,$key_entabla,$codg_evnt,'individual');
		$con=$con2[0];
		$auditoria=$con2[3];		
	}
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'individual');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	if($iramodificar){ $mensaje_mostrar .= "<br><br>No ha efectuado cambios o ya existe el evento"; }
	$existente='no';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}
if ($boton=='Actualizando')
{
	$boton = 'Modificar';
}

//CONSULTAS COMBOS

$consulta_areas = mysql_query("SELECT * FROM eventos_areas order by nomb_area ");
$consulta_tipo = mysql_query("SELECT * FROM eventos_tipos order by nomb_tipo ");

if ($con[codg_tipo]!='')
{
	       $codg_tipo=$con[codg_tipo];
       	 $consulta_tipo1 = mysql_query("SELECT * FROM eventos_tipos where codg_tipo='$codg_tipo' ");
       	 $cont1=mysql_fetch_assoc($consulta_tipo1);
       	 $nomb_tipo=$cont1[nomb_tipo];

}

if ($con[codg_area]!='')
{
	      	$codg_area=$con[codg_area];
       	   $consulta_areas1 = mysql_query("SELECT * FROM eventos_areas where codg_area='$codg_area' ");
       		$cona1=mysql_fetch_assoc($consulta_areas1);
       		$nomb_area=$cona1[nomb_area];
	
}




?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE EVENTOS</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="&Aacute;rea";
		$buscar_varios[0][1]="codg_area";
		$buscar_varios[0][2]=array("eventos_areas","codg_area","nomb_area");
		$buscar_varios[0][3]="center";
		$buscar_varios[1][0]="Nombre";
		$buscar_varios[1][1]="nomb_evnt";
		$buscar_varios[2][0]="Descripción";
		$buscar_varios[2][1]="desc_evnt";
		$buscar_varios[3][0]="Status";
		$buscar_varios[3][1]="stat_evnt";
		$buscar_varios[3][0]="center";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<script type="text/javascript">
		function guardar_video(){
			if ($("#titulo_video").validationEngine('validate')==false && $("#embed_video").validationEngine('validate')==false)
          	{
				var titulo = $('#titulo_video').val();
				var embed = $('#embed_video').val();
				var evento = $('#codg_evnt').val();
				var parametros = {
					"var_tabla" : "eventos_youtube",
					"codg_evnt" : evento,
					"titulo_video" : titulo,
					"embed_video" : embed
				};
				var url="sistema/comunes/funcion_guardar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: parametros,
		          	beforeSend: function () {

				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      respuesta = datatemp[2];
				      $('#resultado_video').html(mensaje);
				      setTimeout(function() {
					  	$("#msg_act").fadeOut(1500);
     				  },3000);
     				  $('#titulo_video').val('');
     				  $('#embed_video').val('');
		      		  if(codigo==001){
		      		  	recargar_videos();
		      		  }
				    }
				});
				return false;
			}
		}
		function recargar_videos(){
			var evento = $('#codg_evnt').val();
			var parametros = {
				"codg_evnt" : evento
			};
			var url="sistema/formularios/lista_youtube.php";
			$.ajax
			({
			    type: "POST",
			    url: url,
			    data: parametros,
	          	beforeSend: function () {

			    },
			    success: function(data)
			    {
			      $('#lista_youtube').html(data);
			    }
			});
			return false;
		}
		function eliminar_videos(video){
			var parametros = {
				"var_tabla" : "eventos_youtube",
				"var_id" : "codg_video",
				"var_id_val" : video
			};
			var url="sistema/comunes/funcion_eliminar.php";
			$.ajax
			({
			    type: "POST",
			    url: url,
			    data: parametros,
	          	beforeSend: function () {

			    },
			    success: function(data)
			    {
			    	var codigo, datatemp, mensaje;
			      	datatemp=data;
				    datatemp=datatemp.split(":::");
				    codigo=datatemp[0];
				    mensaje=datatemp[1];
				    respuesta = datatemp[2];
					$('#resultado_video').html(mensaje);
				    setTimeout(function() {
					  	$("#msg_act").fadeOut(1500);
     				},3000);
			    	recargar_videos();
			    }
			});
			return false;
		}
	</script>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="" enctype="multipart/form-data">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
				echo '<input type="hidden" name="codg_evnt" id="codg_evnt" value="'.$con['codg_evnt'].'">';	
      		echo '
					<tr>
          		<td  align="center">
						<input type="text" class="validate[required, minSize[5],maxSize[255]] text-input, cajas_entrada" value="'.$con[nomb_evnt].'" id="nomb_evnt" name="nomb_evnt" placeholder="Nombre de Evento" />
         		</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, minSize[5],maxSize[250]] text-input,  cajas_entrada" value="'.$con[desc_evnt].'" id="desc_evnt" name="desc_evnt" placeholder="Descripción" />
					</td>
				</tr>
				<tr>
					<td align="center">
						
						<select name="codg_area" id="codg_area"  class="validate[required], combo_form" >';
						if ($con[codg_area]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione el Área</option>';
       				}
       				else 
       				{
      					
       					echo' <option    value="'.$codg_area.'" >'.$nomb_area.'</option> ';
       				}
       				while($fila=@mysql_fetch_array($consulta_areas))
                  {
                      echo "<option value=".$fila[codg_area].">".$fila[nomb_area]."</option>";
                  }
						
						echo '</select>
						
					</td>
				</tr>
				<tr>
					<td align="center">
						
						<select name="codg_tipo" id="codg_tipo"  class="validate[required], combo_form" >';
						if ($con[codg_tipo]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione el Tipo Evento</option>';
       				}
       				else 
       				{
      					
       					echo' <option    value="'.$codg_tipo.'" >'.$nomb_tipo.'</option> ';
       				}
       				while($fila=mysql_fetch_array($consulta_tipo))
                  {
                      echo "<option value=".$fila[codg_tipo].">".$fila[nomb_tipo]."</option>";
                  }
						
						echo '</select>
						
					</td>
				</tr>
				<tr><td><label id="etiqueta"> Imagen para el Evento </label></td></tr>

				<tr> <td aling="center"> <input type="file" class="cajas_entrada"  id="imag_evnt" name="imag_evnt" placeholder="Imagen del Evento" />   </td> </tr>

				<tr><td><label id="etiqueta"> Imagen para Diplomado </label></td></tr>

				<tr> <td aling="center"> <input type="file" class="cajas_entrada"  id="imag_dipl" name="imag_dipl" placeholder="Imagen del Diplomado" />   </td> </tr>
				<tr><td><label id="etiqueta"> Pdf de Información Detallada </label></td></tr>

				<tr> <td aling="center"> <input type="file" class="cajas_entrada"  id="pdf_dipl" name="pdf_dipl" placeholder="Pdf del Diplomado" />   </td> </tr>


			   <tr>
					<td>
						<label id="etiqueta">Status:</label> ';
        					echo '<input class="validate[required] radio" type="radio" name="stat_evnt" id="Activo"';  if ($con[stat_evnt]=='Activo') { echo 'checked="checked"'; } echo 'value="Activo"/> <label id="etiqueta">Activo</label>';
        					echo '<input class="validate[required] radio" type="radio" name="stat_evnt" id="Desactivo"';  if ($con[stat_evnt]=='Desactivo') { echo 'checked="checked"'; } echo 'value="Desactivo"/> <label id="etiqueta">Desactivo</label>';
                		echo '
					</td>
				</tr>
				
				
				
				<tr><td>&nbsp;<input name="imgr_evnt" type="hidden" value="'.$con['imag_evnt'].'" /></td></tr>
				<input name="imgr_evnt2" type="hidden" value="'.$con['imag_dipl'].'" />
				<input name="imgr_evnt3" type="hidden" value="'.$con['pdf_dipl'].'" />';

			}
			else
			{
				
				echo '<input type="hidden" name="codg_evnt" id="codg_evnt" value="'.$con['codg_evnt'].'">';	
				echo '
					<tr>
						<td align="left">
							<label id="etiqueta" > Nombre: </label> <label id="etiqueta"></label> <label id="resultado">'.$con[nomb_evnt].' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Descripción: </label> <label id="resultado">'.$con[desc_evnt].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left" > <label id="etiqueta"> Área: </label> <label id="resultado">'.$nomb_area.' </label> </td> 
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Tipo de Evento:</label> <label id="resultado"> '.$nomb_tipo.' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>';
         		 echo '<tr>
         			<td align="left">
         				<label id="etiqueta"> Status:</label> <label id="resultado"> '.$con['stat_evnt'].' </label>
         			</td>
         		</tr>';
         	   echo '<tr><td><br><center><label id="etiqueta"> Imagen para el Evento </label> </center></td></tr>';

               echo '<tr><td><br><center><img src="'.$con['imag_evnt'].'" width="500"  /></center></td></tr>';
               echo '<tr><td><br><center><label id="etiqueta"> Imagen de Diplomado </label> </center></td></tr>';

               echo '<tr><td><br><center><img src="'.$con['imag_dipl'].'" width="500"  /></center></td></tr>';
               echo '<tr><td><br><center><label id="etiqueta"> Pdf de Diplomado </label> </center></td></tr>';
               echo '<tr><td><br><center> <a target="_blank" href="'.$con['pdf_dipl'].'" > <img src="sistema/imagenes/pdf.png" width="80" > </a></center></td></tr>';
         		
         		echo '<tr><td>&nbsp;</td></tr>';
			}
			if ($boton == 'Modificar'){
				echo '<tr><td>
				<center>
					<div id="etiqueta" style="text-align: center; color: #000;">Agrega Videos <img style="position: relative; top: 10px;" height="30px" src="sistema/imagenes/youtube.png"></div>
					<div id="resultado_video"> </div>
					<input style="width: 150px;" type="text" class="validate[required, minSize[5],maxSize[255]] text-input, cajas_entrada" value="" id="titulo_video" name="titulo_video" placeholder="Titulo del Video" />
					<input  style="width: 250px;" type="text" class="validate[required, minSize[5]] text-input, cajas_entrada" value="" id="embed_video" name="embed_video" placeholder="Código del Video. Ejem: UXasXQ8htXY"  title="Ve a la direeción y copia el codigo luego de watch?v=">
					<img src="sistema/imagenes/agrega.png" style="position: relative; top: 15px; height: 40px; cursor: pointer;" title="agregar video youtube" onclick="guardar_video();">
					<div id="lista_youtube">';
						include('sistema/formularios/lista_youtube.php');
					echo '</div>
				</center>
				</td></tr>';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Nombre";
		$buscar_parm[0][1]="nomb_evnt";
		$buscar_parm[1][0]="Descripción";
		$buscar_parm[1][1]="desc_evnt";
		include('sistema/general/busqueda.php');?>
