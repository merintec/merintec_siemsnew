<?php 
	session_start();
	$url_base = "../";
	include($url_base.'comunes/conexion.php');
?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
   	<script>
   		//Precargar imagen timer
		jQuery.preloadImages = function() {
			for(var i = 0; i<arguments.length; i++){
				jQuery("<img>").attr("src", arguments[i]);
			}
		}
		$.preloadImages('../sistema/imagenes/cargando.gif');

		function actualiza_pass(){
			if ($("#form1").validationEngine('validate')){
				var parametros = {
		   			"pass_act" : $('#pass_act').val(),
		  			"pass_new" : $('#pass_new').val(),
		  			"pass_ree" : $('#pass_act').val(),
				}
				var url="../sistema/comunes/actualizar_pass.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_boton').html('Guardando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_boton').html('Guardar Cambios');
	          		  if(codigo==001){
	          		  	$("#form1")[0].reset();
	          		  	$('input[name=pass_act]').focus();
	          		  }
				    }
				});
				return false;
			}
		} 
	</script>
</head>
<body>
    <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">		
		<span class="titulo-perfil">Cambio de Clave de Acceso</span><br>
		<div id="resultado"></div>
		<div class="row-fluid" style="margin-top: 3em;">
			<div class="col-md-2 hidden-xs"></div>
			<div class="col-md-6 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" >
						<img width="20" src="../imagenes/page/llave.png">
		    		</span>
	                <input type="password" name="pass_act" id="pass_act" placeholder="Contraseña Actual" class="validate[required,minSize[4],maxSize[100]]  text-input form-control campo_sesion">
				</div>
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" >
						<img width="20" src="../imagenes/page/llave.png">
		    		</span>
	                <input type="password" name="pass_new" id="pass_new" placeholder="Contraseña Nueva" class="validate[required,minSize[4],maxSize[100],equals[pass_ree]]  text-input form-control campo_sesion">
				</div>
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" >
						<img width="20" src="../imagenes/page/llave.png">
		    		</span>
	                <input type="password" name="pass_ree" id="pass_ree" placeholder="Reescribe Contraseña Nueva" class="validate[required,minSize[4],maxSize[100],equals[pass_new]]  text-input form-control campo_sesion">
				</div>
		    </div>
			<div class="col-md-2 hidden-xs"></div>
		</div>
		<div class="row-fluid">
			<div class="col-md-10 col-xs-11" style="margin-top: 2em;">
				<div class="text-center"><button id="guardar" onclick="actualiza_pass()" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_boton">Guardar Cambios</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button></div>	
		    </div>
		</div>
		<div class="col-md-11 col-xs-11">&nbsp;</div>
	</form>
	<script type="text/javascript">$('input[name=pass_act]').focus();</script>
</body>
</html>