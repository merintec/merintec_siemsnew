<?php 
	session_start();
	$url_base = "../";
	include($url_base.'comunes/conexion.php');
?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>

   	<script>
   		//Precargar imagen timer
		jQuery.preloadImages = function() {
			for(var i = 0; i<arguments.length; i++){
				jQuery("<img>").attr("src", arguments[i]);
			}
		}
		$.preloadImages('../sistema/imagenes/cargando.gif');

	    $(document).ready(function() {
	        $('.datepicker')
	            .datepicker({
	              format: 'dd-mm-yyyy',
	              autoclose: true,
	              language: 'es'
	            });
	    });
	    function guarda(){
			if ($("#form1").validationEngine('validate')){
				var url="../sistema/comunes/funcion_guardar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_guardar').html('Guardando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_guardar').html('Guardar');
	          		  if(codigo==001){
	          		  	$("#form1")[0].reset();
	          		  }
	          		  setTimeout(function() {
	          		  	ir_opcion('presupuesto','solicitud_presupuesto_new');
				      },3000);
				    }
				});
				return false;
			}
		}
		function llenar(id_pres,tema_pres,desc_pres,npar_pres,fech_pres,obsr_pres){
			$('#var_id_val').val(id_pres);
			$('#tema_pres').val(tema_pres);
			$('#desc_pres').val(desc_pres);
			$('#npar_pres').val(npar_pres);
			$('#fech_pres').val(fech_pres);
			$('#obsr_pres').val(obsr_pres);
			$('#guardar').hide();
			$('#actualizar').show();
			$('#eliminar').show();
			$('#cancelar').show();
			$('html,body').animate({
				scrollTop: $("#inicio").offset().top
			}, 1000);
		}
		function cancela(){			
			$('#var_id_val').val('');
			$('#tema_pres').val('');
			$('#desc_pres').val('');
			$('#npar_pres').val('');
			$('#fech_pres').val('');
			$('#obsr_pres').val('');	
			$('#guardar').show();
			$('#actualizar').hide();
			$('#eliminar').hide();
			$('#cancelar').hide();
			$('html,body').animate({
				scrollTop: $("#inicio").offset().top
			}, 1000);
		}

		function actualiza(){
			if ($("#form1").validationEngine('validate')){
				var url="../sistema/comunes/funcion_actualizar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_actualizar').html('Actualizando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_actualizar').html('Actualizar');
	          		  if (codigo=001){          		  	
					      setTimeout(function() {
							ir_opcion('presupuesto','solicitud_presupuesto_new');
					      },3200);
	          		  }
				    }
				});
				return false;
			}
		}
		function elimina(){
			var url="../sistema/comunes/funcion_eliminar.php"; 
			$.ajax
			({
			    type: "POST",
			    url: url,
			    data: $("#form1").serialize(),
	          	beforeSend: function () {
	          		$('#etiqueta_eliminar').html('Eliminando...');
			    },
			    success: function(data)
			    {
			      var codigo, datatemp, mensaje;
			      datatemp=data;
			      datatemp=datatemp.split(":::");
			      codigo=datatemp[0];
			      mensaje=datatemp[1];
			      $("#resultado").html(mensaje);
		              $('html,body').animate({
			        scrollTop: $("#inicio").offset().top
			      }, 1000);
			      setTimeout(function() {
			        $("#msg_act").fadeOut(1500);
			      },3000);
          		  $('#etiqueta_eliminar').html('Eliminar');
          		  if (codigo=001){          		  	
				      setTimeout(function() {
		          		  ir_opcion('presupuesto','solicitud_presupuesto_new');
				      },3200);
          		  }
			    }
			});
			return false;
		}
	</script>
</head>
<body>
    <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">		
		<span class="titulo-perfil">Solicitud de Presupuestos</span><br>
		<div id="resultado"></div>
		<span class="subtitulo-perfil">Planifica con nosotros la capacitación de Tu Personal</span>
		<input type="hidden" name="var_tabla" id="var_tabla" value="solicitud_presupuesto">
		<input type="hidden" name="codg_empr" id="codg_empr" value="<?php echo $_SESSION[codg_empr]; ?>">
		<input type="hidden" name="var_id" id="var_id" value="id_pres">
		<input type="hidden" name="var_id_val" id="var_id_val" value="">
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Tema</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="tema_pres" id="tema_pres" placeholder="Tema a tratar" class="validate[required, minSize[3],maxSize[250]] text-input form-control"  value="">
				</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Descripción</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="desc_pres" id="desc_pres" placeholder="Descripción del tema" class="validate[required, minSize[3],maxSize[250]] text-input form-control"  value="">
				</div>
		    </div>
		</div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Participantes</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="npar_pres" id="npar_pres" placeholder="Nº Participantes" class="validate[required, custom[integer] minSize[1],maxSize[5]] text-input form-control"  value="">
				</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Fecha</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="fech_pres" id="fech_pres" placeholder="Posible Fecha" class="validate[required, custom[date]] datepicker text-input form-control"  value="">
				</div>
		    </div>
		</div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-10 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Observaciones</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="obsr_pres" id="obsr_pres" placeholder="Información adicional" class="validate[required, minSize[3],maxSize[250] ], text-input form-control"  value="">
				</div>
		    </div>
		</div>


		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid">
			<div class="col-md-10 col-xs-11" style="margin-top: 2em;">
				<div class="text-right">
					<button id="cancelar" onclick="cancela(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_cancelar">Cancelar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-repeat"></span></button>
					<button id="eliminar" onclick="elimina(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_eliminar">Eliminar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-trash"></span></button>
					<button id="actualizar" onclick="actualiza(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_actualizar">Actualizar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button>
					<button id="guardar" onclick="guarda(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_guardar">Guardar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button>
					<script>$('#actualizar').hide();$('#cancelar').hide();$('#eliminar').hide();</script>
				</div>
		    </div>
		</div>
	</form>
	<div class="row-fluid">
		<div class="col-md-11 col-xs-11">&nbsp;</div>
	</div>
	<div class="row-fluid">
		<div class="col-md-11 col-xs-11">
			<div class="text-center titulo-perfil">Presupuestos Solicitados</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row-fluid">
		<div class="table-responsive"> 	
			<table align="left" width="100%" cellspacing="0" id="lista-table" style="font-size: 12px;">
				<tr>
					<th style="text-align: right; padding-right: 5px;">#</th>
					<th style="text-align: left;">Fecha</th>
					<th style="text-align: left;">Tema</th>
					<th style="text-align: left;">Descripción</th>
					<th>Acción</th>
				</tr>
				<?php
					$sql_pres = "SELECT * FROM solicitud_presupuesto WHERE codg_empr = ".$_SESSION[codg_empr]." ORDER BY fech_pres DESC, tema_pres ASC";
					$bus_pres = mysql_query($sql_pres);
					$i = 0;
					while($res_pres = mysql_fetch_array($bus_pres)){
						$i++;
						echo '
						<tr>
							<td style="text-align: right; padding-right: 5px;">'.$i.'</td>
							<td style="padding-left: 5px;">'.date('d-m-Y', strtotime($res_pres[fech_pres])).'</td>
							<td style="padding-left: 5px;">'.$res_pres[tema_pres].'</td>
							<td style="padding-left: 5px;">'.$res_pres[desc_pres].'</td>
							<td align="center"><button class="btn fondo_boton" onclick="llenar(\''.$res_pres[id_pres].'\',\''.$res_pres[tema_pres].'\',\''.$res_pres[desc_pres].'\',\''.$res_pres[npar_pres].'\',\''.date('d-m-Y',strtotime($res_pres[fech_pres])).'\',\''.$res_pres[obsr_pres].'\')" style="font-weight: bold; font-size:12px;">Editar&nbsp;<span class="glyphicon glyphicon-pencil"></span></button></td>
						</tr>';
					}
				?>
			</table>
		</div>
	</div>
</body>
</html>