<?php 
	session_start();
	$url_base = "../";
	include($url_base.'comunes/conexion.php');
?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>

   	<script>
   		//Precargar imagen timer
		jQuery.preloadImages = function() {
			for(var i = 0; i<arguments.length; i++){
				jQuery("<img>").attr("src", arguments[i]);
			}
		}
		$.preloadImages('../sistema/imagenes/cargando.gif');

	    $(document).ready(function() {
	        $('.datepicker')
	            .datepicker({
	              format: 'dd-mm-yyyy',
	              autoclose: true,
	              language: 'es'
	            });
	    });
	    //Foto Perfil
		$(document).ready(function(){
			var base_dir = "../sistema/imagenes/empresas/";
			var nom_arch = '<?php echo $_SESSION[codg_empr]; ?>';
		    var parametros = {
		            "nom_arch" : nom_arch,
		            "base_dir" : base_dir
		    };
		    var button = $('#imagen_perfil');
		    new AjaxUpload('#imagen_perfil', {
		        type: "POST", 
		        data: parametros,
		        action: '../comunes/subir_imagen.php',
		        onSubmit : function(file , ext){
			        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
			            // extensiones permitidas
			            var mensaje;
			            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
			             $("#resultado").html(mensaje);
			            // cancela upload
			            return false;
			        } else {
			            $('#status_imagen').html('Cargando...');
			            $('#imag_perfil_act').attr('src', '../sistema/imagenes/cargando.gif');
			            //this.disable();
			        }
		        },
		        onComplete: function(file, response){
		        	// separar resultado
		        	datatemp=response;
              		datatemp=datatemp.split(":::");
              		var mensaje = datatemp[2];
              		var res = datatemp[1];
              		var archivo = datatemp[0];
		            $('#status_imagen').html('Cambiar');
		            // habilito upload button
		            $('#resultado').html(mensaje);
		            // evaluamos el resultado
		            if (res==001){	            	
			            // Agrega archivo a la lista
			            $('#imag_part').val(archivo);
			            d = new Date();
			            $('#imag_perfil_act').attr('src', base_dir + '' + archivo + '?'+d.getTime());
			            setTimeout(function() {
	                		$("#msg_res").fadeOut(1500);
	              		},3000);
		            }
		            if (res==002){
		            	var act = $('#imag_part').val();
		            	if (!act){
		            		act = '../sistema/imagenes/usuarios.png';
		            	}
		            	else {
			            	act = base_dir + '' + $('#imag_part').val();
		            	}
			            $('#imag_perfil_act').attr('src', act);
		            }
		            $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				    }, 1000);
		        }
		    });
		});

		function actualiza_perfil(){
			if ($("#form1").validationEngine('validate')){
				var url="../sistema/comunes/funcion_actualizar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_boton').html('Guardando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_boton').html('Guardar Cambios');
				    }
				});
				return false;
			}
		} 

	</script>
</head>
<body>
    <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">		
    	<?php 
			$sql_perfil = "SELECT * FROM empresas WHERE codg_empr = ".$_SESSION[codg_empr];
			$res_perfil = mysql_fetch_array(mysql_query($sql_perfil));
		?>
		<span class="titulo-perfil">Datos de la Empresa</span><br>
		<div id="resultado"></div>
		<span class="subtitulo-perfil">Comparte con nosotros algunos detalles de la Empresa</span>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-3 col-xs-11">
				<div class="foto-perfil text-center"><img src="<?php if ($res_perfil[imag_empr]) { echo '../sistema/imagenes/empresas/'.$res_perfil[imag_empr];} else { echo '../sistema/imagenes/logo_gen.png'; } ?>" id="imag_perfil_act" style="max-width: 100%;"></div>
				<div>
					<input type="hidden" id="var_id" name="var_id" value="codg_empr">
					<input type="hidden" id="var_id_val" name="var_id_val" value="<?php echo $_SESSION[codg_empr]; ?>">
					<input type="hidden" id="var_tabla" name="var_tabla" value="empresas"><input type="hidden" id="imag_empr" name="imag_empr" value="<?php echo $res_perfil[imag_empr]; ?>"><button id="imagen_perfil" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold; width: 100%;"><span id="status_imagen">Cambiar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-camera"></span></button>
				</div>
			</div>
			<div class="col-md-7 col-xs-11">
	            	<label class="perfil-etiquetas1">Razón Social:</label>
		            <input type="text" name="nomb_empr" id="nomb_empr" placeholder="Nombre o Razón social" class="validate[required, custom[onlyLetterSp] minSize[3],maxSize[60]] text-input form-control" value="<?php echo $res_perfil[nomb_empr]; ?>">
	               	<label class="perfil-etiquetas1">Dirección:</label>
	                <input type="text" name="dirc_empr" id="dirc_empr" placeholder="Dirección" class="validate[required, minSize[3],maxSize[255]] text-input form-control"  value="<?php echo $res_perfil[dirc_empr]; ?>">
	                <label class="perfil-etiquetas1">e-mail:</label>
	                <input readonly="readonly" type="text" name="emai_empr" id="emai_empr" placeholder="Dirección" class="validate[required, custom[email] minSize[3],maxSize[70]] text-input form-control"  value="<?php echo $res_perfil[emai_empr]; ?>">
			</div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
	    		<div class="input-group" style="margin-top: 0.8em;">
	        		<span class="input-group-addon fondo_boton" >  
	        		<select name="trif_empr" id="trif_empr"  class="validate[required] combop negritas" style="height: 1em;">
						<option value="V" <?php if ($res_perfil[trif_empr] == 'V') { echo 'selected'; } ?>>V</option>
						<option value="E" <?php if ($res_perfil[trif_empr] == 'E') { echo 'selected'; } ?>>E</option>
						<option value="J" <?php if ($res_perfil[trif_empr] == 'J') { echo 'selected'; } ?>>J</option>
						<option value="G" <?php if ($res_perfil[trif_empr] == 'G') { echo 'selected'; } ?>>G</option>
					</select>
					</span>
	                <input type="text" name="rifd_empr" id="rifd_empr" placeholder="Número de RIF" class="validate[required, custom[integer] minSize[9],maxSize[9]] text-input form-control"  value="<?php echo $res_perfil[rifd_empr]; ?>">
	   			</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Teléfono</span><span class="visible-xs glyphicon glyphicon-earphone" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="tlfn_empr" id="tlfn_empr" placeholder="Teléfono" class="validate[required, custom[phone] minSize[3],maxSize[30]] text-input form-control"  value="<?php echo $res_perfil[tlfn_empr]; ?>">
				</div>
		    </div>
		</div>
		<div class="col-md-11 col-xs-11">&nbsp;</div>
		<div class="row-fluid" style="margin-top: -1em;">
			<div class="col-md-12 col-xs-12">
				<label class="perfil-etiquetas1">Datos de Persona Contacto:</label>
			</div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Nombre</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="cont_empr" id="cont_empr" placeholder="Nombre Contacto" class="validate[required, custom[onlyLetterSp] minSize[3],maxSize[60]] text-input form-control"  value="<?php echo $res_perfil[cont_empr]; ?>">	        		
	   			</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Teléfono</span><img class="visible-xs" style="margin-left: 4px; margin-right: 4px; max-height: 20px;" src="../imagenes/page/ico-telefono.png"></span>
	                <input type="text" name="tlfc_empr" id="tlfc_empr" placeholder="Teléfono" class="validate[required, custom[phone] minSize[3],maxSize[30]] text-input form-control"  value="<?php echo $res_perfil[tlfc_empr]; ?>">
				</div>
		    </div>
		</div>
		<div class="row-fluid">
			<div class="col-md-10 col-xs-11" style="margin-top: 2em;">
				<div class="text-right"><button id="guardar" onclick="actualiza_perfil()" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_boton">Guardar Cambios</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button></div>	
		    </div>
		</div>
		<div class="col-md-11 col-xs-11">&nbsp;</div>
	</form>
</body>
</html>