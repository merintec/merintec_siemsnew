﻿<?php   
error_reporting(0);
include('../comunes/conexion.php');
include('../comunes/funciones_php.php');
include("../comunes/verificar_admin_vendedor_gestion.php");
$key_entabla = 'codg_part'; 
$key_entabla1 = 'tipo_part'; 
$key_entabla2 = 'nomb_part'; 
$key_entabla3 = 'nomb_even';
$key_entabla4 = 'parti_nomb_part';
$key_entabla5 = 'parti_apel_part';
$key_entabla6 = 'parti_tlfn_part';
$key_entabla7 = 'parti_corr_part';
$campos_pasa = $key_entabla.'|-|'.$key_entabla1.'|-|'.$key_entabla2.'|-|'.$key_entabla3.'|-|'.$key_entabla4.'|-|'.$key_entabla5.'|-|'.$key_entabla6.'|-|'.$key_entabla7;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>.:: SIEMS Instituto Gerencial ::.</title>
		<style type="text/css" title="currentStyle">
			@import "../datatables/media/css/demo_page.css";
			@import "../datatables/media/css/demo_table.css";
		</style>
		<script type="text/javascript" language="javascript" src="../datatables/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="../datatables/media/js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="../comunes/calendario/jquery-ui.css">
	        <script src="../comunes/calendario/jquery-ui.min.js"></script>


		<script type="text/javascript" charset="utf-8">
			function guardar_contacto(){
				if ($('#form1').validationEngine('validate')){				
					var id = $("#codg_part").val();
					var tipo_part = $("#tipo_part").val();
					var obsr_gest = $("#obsr_gest").val();
					var destino = '';
	        		if ($('input:radio[name=destino]:checked').val()){
	        			destino = $('input:radio[name=destino]:checked').val();
	        		}
	        		var fecha = '<?php echo date('Y-m-d'); ?>';
					var codg_usua = $("#codg_usua").val();		
					var archivar = '';
	        		if ($('input:checkbox[name=archivar]:checked').val()){
	        			archivar = $('input:checkbox[name=archivar]:checked').val();
	        		}
					var parametros = {
				    	"id" : id,
				    	"tipo_part" : tipo_part,
				    	"obsr_gest" : obsr_gest,
				    	"destino" : destino,
				    	"fecha" : fecha,
				    	"codg_usua" : codg_usua,
				    	"archivar" : archivar
				   	};
					$.ajax({
						data:  parametros,
					    url:   'guardar_contacto_exalumno.php',
					    type:  'post',
					    success:  function (response) 
					    {
					    	$("#obsr"+id).html(obsr_gest);
					    	$("#"+destino+id).attr('src',"../imagenes/gestion/"+destino+"_on.png");
					    	$("#obsr_gest").val('');
					    	$("#archivar").prop("checked", false);
					    	$('input:radio[name=destino]').prop("checked", false);
					    	if (archivar!=''){
						    	$("#archivar"+id).attr('src',"../imagenes/gestion/stat_on.png");
					    	}
					    	if (archivar==''){
					    		$("#archivar"+id).attr('src',"../imagenes/gestion/stat_off.png");
					    	}
					    	//alert('Guardado con éxito');
					    	document.all['contacto_adicionales'].style.display = 'none';
					    }
					});
				}
				return;
			} 
			function pasar_valor(valores, campos)
			{
				var form_origen = $("#form2"); 
				var viene_curso = $("[name='codg_evnt']", form_origen).val();
				var viene_inicio = $("[name='fini_rep']", form_origen).val();
				var viene_fin = $("[name='ffin_rep']", form_origen).val();
             	var form = $("#form1"); 
              	$("[name='codg_evnt']", form).val(viene_curso);
              	$("[name='fini_rep']", form).val(viene_inicio);
              	$("[name='ffin_rep']", form).val(viene_fin);

				var recibe_valor = valores;
				var val_elem = recibe_valor.split('|-|');
					codigo = val_elem[0];
					tipoin = val_elem[1];
					nombre = val_elem[2];
					evento = val_elem[3];
				var recibe_campos = campos;
				var cam_elem = recibe_campos.split('|-|');
					codigo_input = cam_elem[0];
					tipoin_input = cam_elem[1];
					nombre_input = cam_elem[2];
					evento_input = cam_elem[3];
				var url_open="../../user.php?menu=participantes&formulario=participantes&"+cam_elem[4]+'='+val_elem[4]+'&'+cam_elem[5]+'='+val_elem[5]+'&'+cam_elem[6]+'='+val_elem[6]+'&'+cam_elem[7]+'='+val_elem[7]+"&sub=si";
				if (document.form1[codigo_input].value==codigo){
					document.form1[codigo_input].value = '';
					document.form1[tipoin_input].value = '';
					document.form1[nombre_input].value = '';
					document.form1[evento_input].value = '';
					document.form1['url'].value = '';
				}
				else {
					document.form1[codigo_input].value = codigo;
					document.form1[tipoin_input].value = tipoin;
					document.form1[nombre_input].value = nombre;
					document.form1[evento_input].value = evento;
					document.form1['url'].value = url_open;
					document.form1['archivar'].checked = false;
				}
				if (document.form1[codigo_input].value==''){
					document.all['contacto_adicionales'].style.display = "none";
				}
				else{
					document.all['contacto_adicionales'].style.display = "block";
				}
			}
		</script>
		<!-- <script src="../validacion/js/jquery-1.8.2.min.js" type="text/javascript"></script> -->
	<script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
	<script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
        <link rel="stylesheet" href="../comunes/calendario/jquery-ui.css">
	<link href="../css/sm_estilos.css" rel="stylesheet" type="text/css">
        <script src="../comunes/calendario/jquery-ui.min.js"></script> 
        <!-- validacion en vivo -->
<script type="text/javascript" charset="utf-8">
			var oTable;			
			$(document).ready(function() {
				/*<!-- Acción sobre el botón con id=boton y actualizamos el div con id=capa -->
				$("#boton").click(function(event) {
                    $("#capa").load("gestion_exalumnos_history.php",{valor1:'primer valor', valor2:'segundo valor'}, function(response, status, xhr) {
                          if (status == "error") {
                            var msg = "Error!, algo ha sucedido: ";
                            $("#capa").html(msg + xhr.status + " " + xhr.statusText);
                          }
                        });
                });*/
				
			    // binds form submission and fields to the validation engine
                jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
                
                $("#boton_correos").click(function() {
                	var tabla_objeto = $('#example tbody tr');
					var texto = '';
					for (var i = 1; i <= tabla_objeto.length; i++) {
						var actual = $('#example tr:nth-child(' + i + ') td:nth-child(5)').text();
						if (texto.search(actual) < 0 ){ texto = texto + actual + ','; }
					}
					texto = texto + ',';
					texto = texto.replace(",,","");
					$('#correos').val(texto);
					$('#correos').select();
				});

                $("#boton_telef").click(function() {
                	var tabla_objeto = $('#example tbody tr');
					var texto = '';
					for (var i = 1; i <= tabla_objeto.length; i++) {
						var actual = $('#example tr:nth-child(' + i + ') td:nth-child(6)').text();
						if (texto.search(actual) < 0 ){ texto = texto + actual + ','; }
					}
					texto = texto + ',';
					texto = texto.replace(",,","");
					$('#telefonos').val(texto);
					$('#telefonos').select();
				});

                $("#boton_clear").click(function() {
					$('#correos').val('');
					$('#telefonos').val('');
				});

				$("#example tbody tr").click( function( e ) {
					if ( $(this).hasClass('row_selected') ) {
						$(this).removeClass('row_selected');						
						pasar_valor('','<?php echo $campos_pasa; ?>');
					}
					else {
						oTable.$('tr.row_selected').removeClass('row_selected');
						$(this).addClass('row_selected');
						var anSelected = fnGetSelected( oTable );
						var codigo = anSelected[0].id;
						var recibe_codigo = codigo;
						var val_codg = recibe_codigo.split('|-|');
						var codigo1 = val_codg[0];
						var codigo2 = val_codg[1];
						pasar_valor(codigo,'<?php echo $campos_pasa; ?>');
						$("#capa").load("gestion_exalumnos_history.php",{valor1:codigo1, valor2:codigo2}, function(response, status, xhr) {
                          if (status == "error") {
                            var msg = "Cargando información...";
                            $("#capa").html(msg + xhr.status + " " + xhr.statusText);
                          }
                        });
					}
				});	
				/* Add a click handler for the delete row */
				$('#delete').click( function() {
					var anSelected = fnGetSelected( oTable );
					if ( anSelected.length !== 0 ) {
						oTable.fnDeleteRow( anSelected[0] );
					}
				} );
				
				oTable = $('#example').dataTable( {
				"sPaginationType": "full_numbers",
					"oLanguage": {
						"sLengthMenu": "Mostrar _MENU_ registros por página",
						"sZeroRecords": "Nada encontrado - Intenta nuevamente",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Showing 0 to 0 of 0 records",
			                        "sSearch": "Buscar:",
						"sInfoFiltered": "(filtados de _MAX_ registros)",
						"oPaginate": {
				                        "sFirst": "Primera",
				                        "sPrevious": "Anterior",
				                        "sNext": "Siguiente",
				                        "sLast": "Última"
				                 }
					}
				} );
			} );
			
			/* Get the rows which are currently selected */
			function fnGetSelected( oTableLocal )
			{
				return oTableLocal.$('tr.row_selected');
			}
		</script>
        <!-- CALENDARIO-->
  <script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			$( ".datepicker" ).datepicker({
				firstDay: 1,
				closeText: 'Cerrar',
				nextText: 'Sig ->',
				currentText: 'Hoy',
				prevText: '<- Ant',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
				dateFormat: 'dd/mm/yy',			
				changeYear: true
			});
			jQuery("form1").validationEngine();
		});	
	</script>
    <link href="../css/sm_estilos.css" rel="stylesheet" type="text/css">
    <link href="../../css/estilos.css" rel="stylesheet" type="text/css">
    </head>
  <body id="dt_example" class="ex_highlight_row">
<?php
/*if ($_POST['boton']=='Guardar'){
	$sql_guardar = "INSERT INTO gestion_exalumnos (codg_rela,orgn_rela,obsr_gest,dest_gest,fcha_gest,codg_usua,stat_gest) VALUES (".$_POST[$key_entabla].",'".$_POST[$key_entabla1]."','".$_POST['obsr_gest']."','".$_POST['destino']."','".date('Y-m-d')."','".$_POST['codg_usua']."','".$_POST['archivar']."')";
	mysql_query($sql_guardar);
};*/
?>
	<table border="0" cellpadding="1" cellspacing="1" width="100%">
		<tr>
			<td align="center" id="contacto_cabecera">
				Módulo de Gestión de Exalumnos
			</td>
		</tr>	
		<tr>
			<td align="center" id="contacto_opciones" style="font-size: 20px;">
				<form name="form2" id="form2" method="POST" action="" >
					<?php $consulta_eventos = mysql_query("SELECT * FROM eventos order by nomb_evnt "); 
						echo 'Del sistema: ';
						echo '<select name="codg_evnt" id="codg_evnt"  class="validate[required], combo_form" onchange="submit()">';
						 	echo ' <option value="">Seleccione el Evento</option>';
       						while($fila=mysql_fetch_array($consulta_eventos))
                  				{
							if ($_POST[codg_evnt]==$fila[codg_evnt]){ $add_sel='selected'; }else {$add_sel='';}
                      					echo "<option value=".$fila[codg_evnt]." ".$add_sel.">".$fila[nomb_evnt]."</option>";
                  				}
						echo '</select>';
						$consulta_eventos = mysql_query("SELECT * FROM exalumnos_xls order by nomb_evnt "); 
						echo ' Importados de xls: ';
						echo '<select name="nomb_evnt" id="nomb_evnt"  class="validate[required], combo_form" onchange="submit()">';
					 	echo ' <option value="">Seleccione el Evento</option>';
       						while($fila=mysql_fetch_array($consulta_eventos))
                  				{
							if ($_POST[nomb_evnt]==$fila[nomb_evnt]){ $add_sel='selected'; }else {$add_sel='';}
                      				echo "<option value='".$fila[nomb_evnt]."' ".$add_sel.">".$fila[nomb_evnt]."</option>";
                  				}
						echo '</select>';


						if (!$_POST[fini_rep]){ 
							$fecha = date('Y-m-j');
							$nuevafecha = strtotime ( '-6 month' , strtotime ( $fecha ) ) ;
							$fecha_inicio = date ( 'Y-m-j' , $nuevafecha ); 
						} else {$fecha_inicio = $_POST[fini_rep]; } 
						if (!$_POST[ffin_rep]){ $fecha_final = date('d-m-Y'); } else {$fecha_final = $_POST[ffin_rep]; }
					?>

					<!--Del <input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" name="fini_rep" id="fini_rep" value="<?php echo $fecha_inicio; ?>" placeholder="Fecha de Inicio" style="width: 100px; text-align:center;" onchange="submit()">
					Al <input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" name="ffin_rep" id="ffin_rep" value="<?php echo $fecha_final; ?>" placeholder="Fecha de Fin" style="width: 100px; text-align:center;" onchange="submit()">-->
					<input type="checkbox" name="mostrar_archivados" value="SI" <?php if ($_POST['mostrar_archivados']){ echo 'checked'; }?> onclick="submit();"> Mostrar Archivados
				</form>
			</td>
		</tr>
	</table>
	<div id="container">
	<div id="demo">
	<form name="form1" id="form1" method="POST" action="" >
				<div align="center" id="contacto_adicionales" style="display: none;">
					<div style="position:absolute; right: 10px;">
						<input type="hidden" id="url">
						<input type="button" name="boton" id="boton" value="Registrar como Participante" class="boton_iniciar" style="width:200px" onclick="window.open(url.value,'_blank','scrollbars=yes,status=no,toolbar=no,directories=no,menubar=no,resizable=no,width=800,height=500')">
					</div>
				   	<table width="500px" height="300px" border="0" cellpadding="0" cellspacing="0" align="center">
						<tr height="20px">
							<td colspan="4">
								<input type="hidden" name="codg_evnt" id="codg_evnt">
								<input type="hidden" name="fini_rep" id="fini_rep">
								<input type="hidden" name="ffin_rep" id="ffin_rep">
								<span class="contacto_etiquetas">Tipo de Contacto:</span> <input class="contacto_resultado" style="border: 0px #FFFFFF; width: 250px; background: transparent;" readonly="yes" type="text" value="" name="<?php echo $key_entabla1; ?>" id="<?php echo $key_entabla1; ?>">
				            </td>
						</tr>
				        <tr height="20px">
				        	<td colspan="4">
								<span class="contacto_etiquetas">Nombre:</span> <input class="contacto_resultado" style="border: 0px #FFFFFF; width: 320px; background: transparent;" readonly="yes"  type="text" value="" name="<?php echo $key_entabla2; ?>" id="<?php echo $key_entabla2; ?>">
				            </td>
					</tr>
				        <tr height="20px">
				        	<td colspan="4">										
								<span class="contacto_etiquetas">Evento:</span> <textarea class="contacto_resultado" style="border: 0px #FFFFFF; resize: none;  background: transparent;" cols="60" rows="2" name="<?php echo $key_entabla3; ?>" id="<?php echo $key_entabla3; ?>" readonly="readonly"></textarea>
							</td>
					</tr>
				        <tr height="20px">
				        	 <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="telefono"><img src='../imagenes/gestion/telefono_on.png' title='Contactado vía telefónica' border='0'></td>
				             <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="mail"><img src='../imagenes/gestion/mail_on.png' title='Contactado vía e-mail' border='0'></td>
				             <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="sms"><img src='../imagenes/gestion/sms_on.png' title='Contactado vía SMS' border='0'></td>
				             <td align='center'><input class="validate[required] radio"type="radio" name="destino" value="skype"><img src='../imagenes/gestion/skype_on.png' title='Contactado vía Skype' border='0'></td>
					</tr>
				        <tr align="center">
				        	<td colspan="4">							
				            	<textarea cols="60" rows="5" maxlength="255" name="obsr_gest" id="obsr_gest" class="validate[required, minSize[5],maxSize[255]] text-input, cajas_texto" placeholder="Observación a registrar"></textarea><input type="hidden" name="codg_usua" id="codg_usua" value="<?php echo $_SESSION['codg_usuario']; ?>"></td>
				        </tr>
					<tr height="20px">
				        	<td colspan="4">										
								<span class="contacto_etiquetas"><input type="checkbox" name="archivar" id="archivar" value="SI">Archivar este contacto (No Mostrar Nuevamente)</span><br><br> 
							</td>
					</tr>
				   	<tr height="20px" align="center">
				   		<td colspan="4">
        				   		<input type="hidden" value="" name="<?php echo $key_entabla; ?>" id="<?php echo $key_entabla; ?>">
								<input type="button" name="boton" id="boton" value="Guardar" class="boton_iniciar" onclick="guardar_contacto();">
								<input type="button" name="boton1" id="boton1" value="Cerrar" class="boton_iniciar" OnClick="document.all['contacto_adicionales'].style.display = 'none';">			   								
				   		</td>
				   	</tr>
				   </table>
				   	<div id="contacto_history_titulo">HISTORIAL DE CONTACTO</div>
					<div id="contacto_history_cabecera">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr align="center">
								<td width="70px">FECHA</td><td width="50px">MEDIO</td><td>OBSERVACIÓN</td><td td width="150px">USUARIO</td>
							</tr>
						</table>
					</div>
				   	<div id="capa" class="contacto_history">Historial sobre el contacto</div>
				</div>
			</form>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<?php
				$nparam_v = 8;	
                                $cabeceras = '<th width="180px">Fecha</th><th width="120px">Evento</th><th width="150px">Nombre de Contacto</th><th width="100px">Tipo</th><th width="5px">e-mail</th><th width="50px">Teléfono</th><th width="5px">TLF</th><th width="5px">mail</th><th width="5px">SMS</th><th width="5px">Skype</th><th width="5px">Archivo</th><th>Última Acción</th>';
				echo $cabeceras;
		   ?>
		</tr>
	</thead>
	<tbody>
		<?php 
			$fecha_ini = fecha_formato($fecha_inicio, 2);
			$fecha_fin = fecha_formato($fecha_final, 2);
			$sql_insc = "SELECT part.codg_part, part.nomb_part, part.apel_part, vi.nomb_evnt, part.tlfn_part, part.corr_part, vi.fech_insc,'Exalumnos' as tipo_part FROM vista_inscripciones vi, participantes part WHERE codg_insc > 0 AND vi.icodg_evnt ='".$_POST[codg_evnt]."' AND vi.apro_insc='A' AND vi.fini_aper < '".date('Y-m-d')."' AND vi.codg_part=part.codg_part";
			if (!$_POST['mostrar_archivados']){
				$sql_insc .= " AND ('' = (SELECT stat_gest FROM gestion_exalumnos WHERE codg_rela=part.codg_part ORDER BY codg_gest DESC LIMIT 1) OR part.codg_part NOT IN (SELECT codg_rela FROM gestion_exalumnos) ) ";
			}
			$sql_insc .= " UNION SELECT part.codg_part, part.nomb_part, part.apel_part, part.nomb_evnt, part.tlfn_part, part.corr_part, part.fech_insc,'Exalumnos xls' as tipo_part FROM exalumnos_xls part WHERE nomb_evnt ='".$_POST[nomb_evnt]."'";
			if (!$_POST['mostrar_archivados']){
				$sql_insc .= " AND ('' = (SELECT stat_gest FROM gestion_exalumnos WHERE codg_rela=part.codg_part ORDER BY codg_gest DESC LIMIT 1) OR part.codg_part NOT IN (SELECT codg_rela FROM gestion_exalumnos) ) ";
			}
			$sql_contactos = $sql_insc;         
            $bus_contactos =  mysql_query($sql_contactos);
			while($res = mysql_fetch_array($bus_contactos)){
			    $n_mail = 'off';
			    $n_tlfn = 'off';
			    $n_sms = 'off';
			    $n_skype = 'off';
			    $n_stat = 'off';
				$obsr_gest = '&nbsp';
			    $sql_gest = "SELECT g.*, 'off' as n_mail, if ((select count(codg_gest) from gestion_exalumnos where codg_rela=g.codg_rela and dest_gest='mail')>0,'on','off') as n_mail, if ((select count(codg_gest) from gestion_exalumnos where codg_rela=g.codg_rela and dest_gest='telefono')>0,'on','off') as n_tlfn, if((select count(codg_gest) from gestion_exalumnos where codg_rela=g.codg_rela and dest_gest='skype')>0,'on','off') as n_skype, if((select count(codg_gest) from gestion_exalumnos where codg_rela=g.codg_rela and dest_gest='sms')>0,'on','off') as n_sms, if((SELECT stat_gest FROM gestion_exalumnos WHERE codg_rela=g.codg_rela ORDER BY codg_gest DESC LIMIT 1)='SI','on','off') as stat_gest FROM gestion_exalumnos g WHERE g.codg_rela = ".$res['codg_part']." AND g.orgn_rela='".$res['tipo_part']."' ORDER BY codg_gest DESC LIMIT 1";
//echo '<BR>';				
$bus_gest = mysql_query($sql_gest);
				if ($res_gest = mysql_fetch_array($bus_gest)){ 
					$n_mail = $res_gest['n_mail'];
					$n_tlfn = $res_gest['n_tlfn'];
					$n_sms = $res_gest['n_sms'];
					$n_skype = $res_gest['n_skype'];
					$n_stat = $res_gest['stat_gest'];
					$obsr_gest = $res_gest['obsr_gest'];
				}		
				echo "<tr class'gradeX' id='".$res['codg_part']."|-|".$res['tipo_part']."|-|".$res['nomb_part']." ".$res['apel_part']."|-|".$res['nomb_evnt']."|-|".$res['nomb_part']." ".$res['apel_part']."|-|".$res['nomb_part']." ".$res['apel_part']."|-|".$res['tlfn_part'] ."|-|".$res['corr_part']."'>";
						echo "<td>";
                			        echo fecha_formato($res['fech_insc'], 2);
				                echo "</td>";
						echo "<td>";
                			        echo $res['nomb_evnt'];
				                echo "</td>";
                			        echo "<td>";
                			        echo $res['nomb_part'].' '.$res['apel_part'];
				                echo "</td>";
                			        echo "<td>";
                			        echo $res['tipo_part'];
				                echo "</td>";
   				                echo "<td align='center'>".$res['corr_part']."</td>";
   				                echo "<td align='center'>".$res['tlfn_part']."</td>";
				                echo "<td align='center'><img id='telefono".$res['codg_part']."' src='../imagenes/gestion/telefono_".$n_tlfn.".png' title='Contactado vía telefónica ".$res['tlfn_part']."' border='0'></td>";
				                echo "<td align='center'><img id='mail".$res['codg_part']."' src='../imagenes/gestion/mail_".$n_mail.".png' title='Contactado vía e-mail ".$res['corr_part']."' border='0'></td>";
				                echo "<td align='center'><img id='sms".$res['codg_part']."' src='../imagenes/gestion/sms_".$n_sms.".png' title='Contactado vía SMS ".$res['tlfn_part']."' border='0'></td>";
				                echo "<td align='center'><img id='skype".$res['codg_part']."' src='../imagenes/gestion/skype_".$n_skype.".png' title='Contactado vía Skype ' border='0'></td>";
                			        echo "<td align='center'><img id='archivar".$res['codg_part']."' src='../imagenes/gestion/stat_".$n_stat.".png' title='Archivado' alt='Archivado' border='0'></td>";
                			        echo "<td>";
                			        echo "<div id='obsr".$res['codg_part']."'>".$obsr_gest."</div>";
				                echo "</td>";
				echo "</tr>";
		   } 
		?>
	</tbody>
	<tfoot>
		<tr>
			<?php   
				echo $cabeceras;
		   ?>
		</tr>
	</tfoot>
</table>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><input class="buscar_curso" style="" type="button" id="boton_correos" name="boton_correos" value="Obtener Correos"><td>
		<td><textarea id="correos" name="correos" rows="4" cols="35" readonly="readonly"></textarea><br><b>Nota:</b> Se obviarán los e-mails repetidos</td>
		<td><input class="buscar_curso" type="button" id="boton_telef" name="boton_telef" value="Obtener Teléfonos"><td>
		<td><textarea id="telefonos" name="telefonos" rows="4" cols="35" readonly="readonly"></textarea><br><b>Nota:</b> Se obviarán los teléfonos repetidos</td>
		<td><input class="buscar_curso" type="button" id="boton_clear" name="boton_clear" value="Limpiar" title="Vaciar ambas cajas de texto"><td>
	</tr>
</table>
			</div>
		</div>
  </body>
</html>