<?php 
include("sistema/comunes/verificar_empresa.php");
$boton=$_POST['boton'];
$boton2=$_GET['boton'];
$trif_empr=$_POST['trif_empr'];
$rifd_empr=$_POST['rifd_empr'];
$nomb_empr=$_POST['nomb_empr'];
$dirc_empr=$_POST['dirc_empr'];
$tlfn_empr=$_POST['tlfn_empr'];
$emai_empr=$_POST['emai_empr'];
$cont_empr=$_POST['cont_empr'];
$cont_empr=$_POST['cont_empr'];
$tlfc_empr=$_POST['tlfc_empr'];
$codg_empr=$_POST['codg_empr'];
$parametro=$_POST['parametro'];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "empresas";
$key_entabla = 'codg_empr';
$key_enpantalla = $codg_empr;
$datos[0] = prepara_datos ("trif_empr",$_POST['trif_empr'],'');
$datos[1] = prepara_datos ("rifd_empr",$_POST['rifd_empr'],'');
$datos[2] = prepara_datos ("nomb_empr",$_POST['nomb_empr'],'');
$datos[3] = prepara_datos ("dirc_empr",$_POST['dirc_empr'],'');
$datos[4] = prepara_datos ("tlfn_empr",$_POST['tlfn_empr'],'');
$datos[5] = prepara_datos ("emai_empr",$_POST['emai_empr'],'');
$datos[6] = prepara_datos ("cont_empr",$_POST['cont_empr'],'');
$datos[7] = prepara_datos ("tlfc_empr",$_POST['tlfc_empr'],'');

$tabla2 = "usuarios";
$clave = texto_aleatorio(6);
$datos2[0] = prepara_datos ("cedu_usua",$_POST['rifd_empr'],'');
$datos2[1] = prepara_datos ("nomb_usua",$_POST['nomb_empr'],'');
$datos2[2] = prepara_datos ("apel_usua",$_POST['nomb_empr'],'');
$datos2[3] = prepara_datos ("corr_usua",$_POST['emai_empr'],'');
$datos2[4] = prepara_datos ("logi_usua",$_POST['emai_empr'],'');
$datos2[5] = prepara_datos ("pass_usua",md5($clave),'');
$datos2[6] = prepara_datos ("tipo_usua",'2','');
$datos2[7] = prepara_datos ("stat_usua",'Activo','');

if ($boton=='Guardar'){
	$ejec_guardar = guardar($datos,$tabla);
	if ($ejec_guardar[0]!=''){
		$ejec_guardar_usu = guardar($datos2,$tabla2);
		if($ejec_guardar_usu[0]!=""){
			mail($datos2[3][1], "Datos de Ingreso SIEMS", "Estimado usuario: ".$datos2[1][1]." \n Le informamos que sus datos para ingreso a sistema de SIEMS son: \r\n Usuario: ".$datos2[4][1]."\r\n Contraseña: ".$clave." \r\n Le recomendamos cambiar su clave al ingresar al Sistema.", 'From: merintec@merintec.com.ve' . "\r\n" .
    'Reply-To: merintec@merintec.com.ve' . "\r\n");
		}
		$existente='si';
		$$key_entabla = $ejec_guardar[0];
		$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
		$con=$con2[0];
		$auditoria=$con2[3];
	}
	$mensaje_mostrar=$ejec_guardar[1];
}
if ($boton=='Eliminar')
{
	$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
	$mensaje_mostrar=$ejec_eliminar;
	$boton='';
	$auditoria='';
}
if ($boton=='Actualizar')
{
	$buscando2 = buscar($tabla,'$emai_empr',$_POST[$emai_empr],'individual');	
	$buscando = buscar($tabla,'rifd_empr',$_POST[rifd_empr],'individual');
	if ($buscando[1]<1 && $buscando2[1]<1) {
			$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
			$existente='si';        
			$mensaje_mostrar=$ejec_actualizar[1];
			$$key_entabla = $ejec_actualizar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
	}
	else {
		$mensaje_mostrar = 'Error: El rif '.$_POST[nomb_tipo].' ya existe intente nuevamente';
		$iramodificar="si";
		$boton = 'Modificar';		
	}
}
if ($boton2=='Buscar')
{
	// verificar con que empresa esta relacionado el usuario
	$buscando0 = buscar('usuarios','logi_usua',$_SESSION['usuario_logueado'],'individual');
	$con0=$buscando0[0];
	// Buscamos la empresa relacionada
	$buscando = buscar($tabla,$key_entabla,$con0[$key_entabla],'individual');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	//$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	if($iramodificar){ $mensaje_mostrar .= "<br><br>No ha efectuado cambios o ya existe el rif y/o correo"; }
	$existente='no';
}
?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE EMPRESAS</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Rif";
		$buscar_varios[0][1]="rifd_empr";
		$buscar_varios[1][0]="Razon Social";
		$buscar_varios[1][1]="nomb_empr";
		$buscar_varios[2][0]="Telefono";
		$buscar_varios[2][1]="tlfn_empr";
		$buscar_varios[3][0]="Email";
		$buscar_varios[3][1]="emai_empr";
		$buscar_varios[4][0]="Contacto";
		$buscar_varios[4][1]="cont_empr";
		$buscar_varios[5][0]="Tlfn Contacto";
		$buscar_varios[5][1]="tlfc_empr";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	}else{?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
				echo '<input type="hidden" name="codg_empr" id="codg_empr" value="'.$con['codg_empr'].'">';	
      		echo '
				<tr>
					<td align="left	">
						<select name="trif_empr" id="trif_empr"  class="validate[required], combo_identificador">
							<option value="'.$con[trif_empr].'">'.$con[trif_empr].'</option>
							<option value="V">V</option>
							<option value="J">J</option>
							<option value="G">G</option>
						</select>   
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[6],maxSize[13]] text-input, cajas_entrada2" value="'.$con[rifd_empr].'" id="rifd_empr" name="rifd_empr" placeholder="Rif de la empresa" />
					</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp], minSize[3],maxSize[60]] text-input, cajas_entrada" value="'.$con[nomb_empr].'" id="nomb_empr" name="nomb_empr" placeholder="Razon Social" />
         		</td>
				</tr>
				<tr>
					<td>
						<input type="text" class="validate[required, custom[onlyLetterSp] , minSize[0],maxSize[255]] text-input,  cajas_entrada" value="'.$con[dirc_empr].'" id="dirc_empr" name="dirc_empr" placeholder="Direccion" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[phone], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[tlfn_empr].'" id="tlfn_empr" name="tlfn_empr" placeholder="Teléfono" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[email] , minSize[3],maxSize[70]] text-input,  cajas_entrada" value="'.$con[emai_empr].'" id="emai_empr" name="emai_empr" placeholder="Correo Electrónico" />
					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[cont_empr].'" id="cont_empr" name="cont_empr" placeholder="Persona de Contacto" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[phone] , minSize[3],maxSize[100]] text-input,  cajas_entrada" value="'.$con[tlfc_empr].'" id="tlfc_empr" name="tlfc_empr" placeholder="Teléfono de la Persona de Contacto" />
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>';
			}
			else
			{
				echo '<input type="hidden" name="codg_empr" id="codg_empr" value="'.$con['codg_empr'].'">';	
				echo '
					<tr>
						<td align="left">
							<label id="etiqueta" > Rif: </label> <label id="etiqueta"></label> <label id="resultado">'.$con[trif_empr].'-'.$con[rifd_empr].'</label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Razon Social: </label> <label id="resultado">'.$con[nomb_empr].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left" > <label id="etiqueta"> Dirección: </label> <label id="resultado">'.$con[dirc_empr].' </label> </td> 
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Teléfono: </label> <label id="resultado"> '.$con[tlfn_empr].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Email: </label> <label id="resultado"> '.$con[emai_empr].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td> </tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Persona de Contacto:</label> <label id="resultado"> '.$con[cont_empr].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Teléfono de la Persona de Contacto :</label> <label id="resultado"> '.$con[tlfc_empr].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr> ';
			}
			echo '<tr><td>';
			include('sistema/general/botonera2.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
