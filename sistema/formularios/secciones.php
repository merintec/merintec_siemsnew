<?php 
include("sistema/comunes/verificar_admin.php");
$boton=$_POST['boton'];
$codg_secc=$_POST['codg_secc'];
$nomb_secc=$_POST['nomb_secc'];
$hora_secc=$_POST['hora_secc'];
$lugr_secc=$_POST['lugr_secc'];
$obsr_secc=$_POST['obsr_secc'];
$codg_aper=$_POST['codg_aper'];
$codg_faci=$_POST['codg_faci'];
$parametro=$_POST['parametro'];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "eventos_secciones";
$key_entabla = 'codg_secc';
$key_enpantalla = $codg_secc;
$datos[0] = prepara_datos ("nomb_secc",$_POST['nomb_secc'],'');
$datos[1] = prepara_datos ("hora_secc",$_POST['hora_secc'],'');
$datos[2] = prepara_datos ("lugr_secc",$_POST['lugr_secc'],'');
$datos[3] = prepara_datos ("obsr_secc",$_POST['obsr_secc'],'');
$datos[4] = prepara_datos ("codg_aper",$_POST['codg_aper'],'');
$datos[5] = prepara_datos ("codg_faci",$_POST['codg_faci'],'');

if ($boton=='Guardar'){
	$ejec_guardar = guardar($datos,$tabla);
	if ($ejec_guardar[0]!=''){
		$existente='si';
		$$key_entabla = $ejec_guardar[0];
		$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
		$con=$con2[0];
		$auditoria=$con2[3];
		$mensaje_mostrar=$ejec_guardar[1];
	}
}
if ($boton=='Eliminar')
{

	$buscando_tipo = buscar('inscripcion','codg_secc',$_POST['codg_secc'],'individual');
	if ($buscando_tipo[1]<1) {
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}else{
		$mensaje_mostrar='Sección no puede eliminarse debido a que hay Inscripciones asociadas';
		$boton='Eliminando';
	}
}
if ($boton=='Actualizar')
{
	$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
	$existente='si';        
	$mensaje_mostrar=$ejec_actualizar[1];
	$$key_entabla = $ejec_actualizar[0];
	$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
	$con=$con2[0];
	$auditoria=$con2[3];
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'individual');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	$existente='no';
}

//CONSULTAS COMBOS

$consulta_eventos_aperturas = mysql_query("SELECT * FROM eventos_apertura, eventos where eventos_apertura.codg_evnt=eventos.codg_evnt order by eventos.nomb_evnt ");


if ($con[codg_aper]!='')
{
	       $codg_aper=$con[codg_aper];
       	 $consulta_eventos_aperturas1 = mysql_query("SELECT * FROM eventos_apertura, eventos where eventos_apertura.codg_aper='$codg_aper' and eventos.codg_evnt=eventos_apertura.codg_evnt ");
       	 $conev=mysql_fetch_assoc($consulta_eventos_aperturas1);
       	 $nomb_evnt=$conev[nomb_evnt]." del ".$conev[fini_aper]." al ".$conev[ffin_aper];

}

 $consulta_facilitadores = mysql_query("SELECT * FROM facilitadores order by nomb_faci ");
 if ($con[codg_faci]!='')
{
	       $codg_faci=$con[codg_faci];
       	 $consulta_facilitadores1 = mysql_query("SELECT * FROM facilitadores where codg_faci='$codg_faci'");
       	 $confaci=mysql_fetch_assoc($consulta_facilitadores1);
       	 $nomb_faci=$confaci[cedu_faci]."->".$confaci[nomb_faci]." ".$confaci[apel_faci];

}







?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">CREACIÓN DE SECCIONES</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Evento";
		$buscar_varios[0][1]="codg_aper";
		///                          tabla    codigo en tabla destino    campo que se quiere mostrar
		$buscar_varios[0][2]=array("eventos_apertura","codg_aper","codg_evnt","eventos","codg_evnt","nomb_evnt");


		$buscar_varios[1][0]="Sección";
		$buscar_varios[1][1]="nomb_secc";
		$buscar_varios[1][3]="center";

		$buscar_varios[2][0]="Inicio";
		$buscar_varios[2][1]="codg_aper";
		$buscar_varios[2][2]=array("eventos_apertura","codg_aper","fini_aper",'','','fecha');
		$buscar_varios[2][3]="center";

		$buscar_varios[3][0]="Fin";
		$buscar_varios[3][1]="codg_aper";
		$buscar_varios[3][2]=array("eventos_apertura","codg_aper","ffin_aper",'','','fecha');
		$buscar_varios[3][3]="center";

		$buscar_varios[4][0]="Hora";
		$buscar_varios[4][1]="hora_secc";
		$buscar_varios[5][0]="Lugar";
		$buscar_varios[5][1]="lugr_secc";

		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
				echo '<input type="hidden" name="codg_secc" id="codg_secc" value="'.$con['codg_secc'].'">';	
      		echo '
				<tr>
					<td align="center">
						
						<select name="codg_aper" id="codg_aper"  class="validate[required], combo_form" >';
						if ($con[codg_aper]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione la apertura de evento</option>';
       				}
       				else 
       				{
      					
       					echo' <option    value="'.$codg_aper.'" >'.$nomb_evnt.'</option> ';
       				}
       				while($fila=mysql_fetch_array($consulta_eventos_aperturas))
                  {
                      echo "<option value=".$fila[codg_aper].">".$fila[nomb_evnt]." del ".$fila[fini_aper]." al ".$fila[ffin_aper]."</option>";
                  }
						
						echo '</select>
						
					</td>
				</tr>
								<tr>
					<td align="center">
						
						<select name="codg_faci" id="codg_faci"  class="validate[required], combo_form" >';
						if ($con[codg_faci]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione el facilitador</option>';
       				}
       				else 
       				{
      					
       					echo' <option    value="'.$codg_faci.'" >'.$nomb_faci.'</option> ';
       				}
       				while($fila=mysql_fetch_array($consulta_facilitadores))
                  {
                      echo "<option value=".$fila[codg_faci].">".$fila[cedu_faci]."->".$fila[nomb_faci]." ".$fila[apel_faci]."</option>";
                  }
						
						echo '</select>
						
					</td>
				</tr>

				<tr>
          		<td  align="left">
				<select name="nomb_secc" id="nomb_secc"  class="validate[required], combo_form">
					'; if ($con[nomb_secc]==NULL){
							echo '<option value="">Seleccione nombre de seccion</option>'; 
						}else{
							echo '<option value="'.$con[nomb_secc].'">'.$con[nomb_secc].'</option>';
						} echo '					
					<option value="A">A</option>
					<option value="B">B</option>
					<option value="C">C</option>
				</select>
         		</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required, minSize[1],maxSize[120]] text-input, cajas_entrada" value="'.$con[hora_secc].'" id="hora_secc" name="hora_secc" placeholder="Horario" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[1],maxSize[120]] text-input, cajas_entrada" value="'.$con[lugr_secc].'" id="lugr_secc" name="lugr_secc" placeholder="Lugar" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[custom[onlyLetterSp], minSize[3],maxSize[255]] text-input, cajas_entrada" value="'.$con[obsr_secc].'" id="obsr_secc" name="obsr_secc" placeholder="Observación" />
         		</td>
				</tr>
				
				
				
				<tr><td>&nbsp;</td></tr>';
			}
			else
			{
				
				echo '<input type="hidden" name="codg_secc" id="codg_secc" value="'.$con['codg_secc'].'">';	
				echo '
		
         		<tr>
         			<td align="left" > <label id="etiqueta"> Evento: </label> <label id="resultado">'.$nomb_evnt.' </label> </td> 
         		</tr>
         		<tr><td>&nbsp;</td></tr>

         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Facilitador: </label> <label id="resultado"> '.$nomb_faci.' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Sección:</label> <label id="resultado"> '.$con[nomb_secc].' </label>
         			</td>
         		</tr>         		
         		<tr><td>&nbsp;</td></tr>
         							<tr>
         			<td align="left">
         				<label id="etiqueta"> Hora:</label> <label id="resultado"> '.$con[hora_secc].' </label>
         			</td>
         		</tr>         	
         		<tr><td>&nbsp;</td></tr>
         							<tr>
         			<td align="left">
         				<label id="etiqueta"> Lugar:</label> <label id="resultado"> '.$con[lugr_secc].' </label>
         			</td>
         		</tr>         		
         		<tr><td>&nbsp;</td></tr>
         				<tr>
         			<td align="left">
         				<label id="etiqueta"> Observación:</label> <label id="resultado"> '.$con[obsr_secc].' </label>
         			</td>
         		</tr>         		
         		<tr><td>&nbsp;</td></tr>
         		';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Sección";
		$buscar_parm[0][1]="nomb_secc";
		$buscar_parm[1][0]="Hora";
		$buscar_parm[1][1]="hora_secc";
		$buscar_parm[2][0]="Observación";
		$buscar_parm[2][1]="obsr_secc";
		include('sistema/general/busqueda.php');?>
