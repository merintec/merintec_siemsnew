<?php 
	session_start();
	$url_base = "../";
	include($url_base.'comunes/conexion.php');
?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>

   	<script>
   		//Precargar imagen timer
		jQuery.preloadImages = function() {
			for(var i = 0; i<arguments.length; i++){
				jQuery("<img>").attr("src", arguments[i]);
			}
		}
		$.preloadImages('../sistema/imagenes/cargando.gif');

	    $(document).ready(function() {
	        $('.datepicker')
	            .datepicker({
	              format: 'dd-mm-yyyy',
	              autoclose: true,
	              language: 'es'
	            });
	    });
	    //Foto Perfil
		$(document).ready(function(){
			var base_dir = "../sistema/imagenes/perfiles/";
			var nom_arch = '<?php echo $_SESSION[codigo_part]; ?>';
		    var parametros = {
		            "nom_arch" : nom_arch,
		            "base_dir" : base_dir
		    };
		    var button = $('#imagen_perfil');
		    new AjaxUpload('#imagen_perfil', {
		        type: "POST", 
		        data: parametros,
		        action: '../comunes/subir_imagen.php',
		        onSubmit : function(file , ext){
			        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
			            // extensiones permitidas
			            var mensaje;
			            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
			             $("#resultado").html(mensaje);
			            // cancela upload
			            return false;
			        } else {
			            $('#status_imagen').html('Cargando...');
			            $('#imag_perfil_act').attr('src', '../sistema/imagenes/cargando.gif');
			            //this.disable();
			        }
		        },
		        onComplete: function(file, response){
		        	// separar resultado
		        	datatemp=response;
              		datatemp=datatemp.split(":::");
              		var mensaje = datatemp[2];
              		var res = datatemp[1];
              		var archivo = datatemp[0];
		            $('#status_imagen').html('Cambiar');
		            // habilito upload button
		            $('#resultado').html(mensaje);
		            // evaluamos el resultado
		            if (res==001){	            	
			            // Agrega archivo a la lista
			            $('#imag_part').val(archivo);
			            d = new Date();
			            $('#imag_perfil_act').attr('src', base_dir + '' + archivo + '?'+d.getTime());
			            setTimeout(function() {
	                		$("#msg_res").fadeOut(1500);
	              		},3000);
		            }
		            if (res==002){
		            	var act = $('#imag_part').val();
		            	if (!act){
		            		act = '../sistema/imagenes/usuarios.png';
		            	}
		            	else {
			            	act = base_dir + '' + $('#imag_part').val();
		            	}
			            $('#imag_perfil_act').attr('src', act);
		            }
		            $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				    }, 1000);
		        }
		    });
		});

		function actualiza_perfil(){
			if ($("#form1").validationEngine('validate')){
				var url="../sistema/comunes/funcion_actualizar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_boton').html('Guardando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_boton').html('Guardar Cambios');
				    }
				});
				return false;
			}
		} 

	</script>
</head>
<body>
    <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">		
    	<?php 
			$sql_perfil = "SELECT * FROM participantes WHERE codg_part = ".$_SESSION[codigo_part];
			$res_perfil = mysql_fetch_array(mysql_query($sql_perfil));
		?>
		<span class="titulo-perfil">Perfil del Usuario</span><br>
		<div id="resultado"></div>
		<span class="subtitulo-perfil">Comparte con nosotros algunos detalles de tu perfil</span>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-3 col-xs-11">
				<div class="foto-perfil text-center"><img src="<?php if ($res_perfil[imag_part]) { echo '../sistema/imagenes/perfiles/'.$res_perfil[imag_part];} else { echo '../sistema/imagenes/usuarios.png'; } ?>" id="imag_perfil_act" style="max-width: 100%;"></div>
				<div>
					<input type="hidden" id="var_id" name="var_id" value="codg_part">
					<input type="hidden" id="var_id_val" name="var_id_val" value="<?php echo $_SESSION[codigo_part]; ?>">
					<input type="hidden" id="var_tabla" name="var_tabla" value="participantes"><input type="hidden" id="imag_part" name="imag_part" value="<?php echo $res_perfil[imag_part]; ?>"><button id="imagen_perfil" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold; width: 100%;"><span id="status_imagen">Cambiar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-camera"></span></button>
				</div>
			</div>
			<div class="col-md-7 col-xs-11">
	            	<label class="perfil-etiquetas1">Nombres:</label>
		            <input type="text" name="nomb_part" id="nomb_part" placeholder="Nombres" class="validate[required, custom[onlyLetterSp] minSize[3],maxSize[30]] text-input form-control" value="<?php echo $res_perfil[nomb_part]; ?>">
	               	<label class="perfil-etiquetas1">Apellidos:</label>
	                <input type="text" name="apel_part" id="apel_part" placeholder="Apellidos" class="validate[required, custom[onlyLetterSp] minSize[3],maxSize[30]] text-input form-control"  value="<?php echo $res_perfil[apel_part]; ?>">
			</div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
	    		<div class="input-group" style="margin-top: 0.8em;">
	        		<span class="input-group-addon fondo_boton" >  
	        		<select name="naci_part" id="naci_part"  class="validate[required] combop negritas" style="height: 1em;">
						<option value="V" <?php if ($res_perfil[naci_part] == 'V') { echo 'selected'; } ?>>V</option>
						<option value="E" <?php if ($res_perfil[naci_part] == 'E') { echo 'selected'; } ?>>E</option>
					</select>
					</span>
	                <input type="text" name="cedu_part" id="cedu_part" placeholder="Cédula de Identidad" class="validate[required, custom[integer] minSize[6],maxSize[13]] text-input form-control"  value="<?php echo $res_perfil[cedu_part]; ?>">
	   			</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Sexo</span><img class="visible-xs" width="20" src="../imagenes/page/sexo.png"></span>
		    		<select name="sexo_part" id="sexo_part" class="validate[required] text-input form-control">
		    			<option value="f" <?php if ($res_perfil[sexo_part] == 'f') { echo 'selected'; } ?>>Femenino</option>
		    			<option value="m" <?php if ($res_perfil[sexo_part] == 'm') { echo 'selected'; } ?>>Masculino</option>
		    		</select>
				</div>
			</div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">email</span><img class="visible-xs" width="20" src="../imagenes/page/ico-correo.png"></span>
	                <input type="email" name="corr_part" id="corr_part" placeholder="Email" class="validate[required, custom[email] minSize[3],maxSize[100]] text-input form-control"  value="<?php echo $res_perfil[corr_part]; ?>">
				</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Teléfono</span><img class="visible-xs" style="margin-left: 4px; margin-right: 4px; max-height: 20px;" src="../imagenes/page/ico-telefono.png"></span>
	                <input type="text" name="tlfn_part" id="tlfn_part" placeholder="Teléfono" class="validate[required, custom[phone] minSize[3],maxSize[30]] text-input form-control"  value="<?php echo $res_perfil[tlfn_part]; ?>">
				</div>
		    </div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-10 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Fecha de Nacimiento</span><img class="visible-xs" width="20" src="../imagenes/page/estrella_blanca.png"></span>
	                <input type="text" name="fchn_part" id="fchn_part" placeholder="Fecha de Nacimiento" class="validate[required,custom[date]]  text-input datepicker form-control"  value="<?php echo date('d-m-Y', strtotime($res_perfil[fchn_part])); ?>">
				</div>
		    </div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid">
			<div class="col-md-10 col-xs-11" style="margin-top: 2em;">
				<div class="text-right"><button id="guardar" onclick="actualiza_perfil()" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_boton">Guardar Cambios</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button></div>	
		    </div>
		</div>
		<div class="col-md-11 col-xs-11">&nbsp;</div>
	</form>
</body>
</html>