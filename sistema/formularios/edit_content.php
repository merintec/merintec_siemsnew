<!DOCTYPE html>
<?php 
error_reporting(0);
include('../comunes/conexion.php');
include('../comunes/funciones_php.php');
include("../comunes/verificar_admin_vendedor_gestion.php");
//// consultamos la pagina seleccionada
if($_POST['var_pagina_edit']){
  $sql_act = "SELECT * FROM contenido WHERE codigo=".$_POST['var_pagina_edit'];
  $res_act = mysql_fetch_array(mysql_query($sql_act));
  $contenido = 'Has eliminado completamente el contenido ten cuidado';
  $codigo = $res_act['codigo'];
}
else{
  $contenido = 'Primero debes seleccionar una sección a editar';
  $codigo = '';
}
?>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
  <title>Editar Contenido</title>
  <!-- include jquery -->
  <script src="../summernote/js/jquery-1.11.3.min.js"></script> 

  <!-- include libraries BS3 -->
  <link rel="stylesheet" href="../summernote/js/bootstrap.min.css" />
  <script type="text/javascript" src="../summernote/js/bootstrap.min.js"></script>

  <!-- include summernote -->
  <link rel="stylesheet" href="../summernote/dist/summernote.css">
  <script type="text/javascript" src="../summernote/dist/summernote.js"></script>
  <script type="text/javascript" src="../summernote/lang/summernote-es-ES.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('.summernote').summernote({
        height: 300,
        tabsize: 2,
        fontNames: ['ralewaythin','ralewayregular', 'Roboto Light', 'Roboto Regular', 'Roboto Bold'],
		fontNamesIgnoreCheck: ['ralewaythin','ralewayregular', 'Roboto Light', 'Roboto Regular', 'Roboto Bold'],
		placeholder: '<?php echo $contenido; ?>',
		toolbar: [
                ["style", ["style"]],
                ["font", ["bold", "underline", "clear"]],
                ["fontname", ["fontname"]],
                //['fontsize', ['fontsize']],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["table", ["table"]],
                ["insert", ["link", "picture", "video"]],
                //["view", ["fullscreen", "codeview", "help"]]
                ["view", ["fullscreen", "codeview"]]
            ],
        lang: 'es-ES'
      });
    });
    $('.summernote').summernote({
        oninit: function() {
        $('.note-editable').addClass('home-titulo');
    }
    });
  </script>
  <script type="text/javascript">
    function actualiza_pagina(){
        var url="../comunes/funcion_actualizar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form2").serialize(),
            beforeSend: function () {
                  $('#mensaje').html('Guardando...');
            },
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              $("#mensaje").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
            }
        });
        return false;
    } 
  </script>
  <!-- Estilo Propio -->
  <link href="../css/sm_estilos.css" rel="stylesheet" type="text/css">
  <link href="../../css/estilos.css" rel="stylesheet" type="text/css">
  <link href="../../css/estilo.css" rel="stylesheet" type="text/css">
</head>
<body>
  <form name="form2" id="form2" method="POST" action="" >
    <table border="0" cellpadding="1" cellspacing="1" width="100%">
      <tr>
        <td align="center" id="contacto_cabecera">
          Módulo de Edición de Contenido
        </td>
      </tr> 
      <tr>
        <td align="center" style="color: #FFFFFF; font-size: 18px; padding-top: 10px;">
          <input type="hidden" name="var_tabla" name="var_tabla" value="contenido">
          <input type="hidden" name="var_id" name="var_id" value="codigo">
          <input type="hidden" name="var_id_val" name="var_id_val" value="<?php echo $codigo; ?>">
          Seleccione la sección que desea editar: 
          <select name="var_pagina_edit" id="var_pagina_edit" class="combo_form" onchange="submit();">
            <option value="0">Seleccione...</option>
            <?php 
              $sql_paginas = "SELECT * FROM contenido ORDER BY titulo";
              $bus_paginas = mysql_query($sql_paginas);
              while($res_paginas = mysql_fetch_array($bus_paginas)){
                if($codigo==$res_paginas['codigo']){
                  $add_sel = ' selected="selected" ';
                }
                else{
                  $add_sel = ''; 
                }
                echo '<option '.$add_sel.' value="'.$res_paginas['codigo'].'">'.$res_paginas['titulo'].'</option>';
              }
            ?>
          </select>        
        </td>
      </tr>
    </table>
    <textarea class="summernote" name="contenido" id="summernote">
      <?php echo $res_act["contenido"];?>
    </textarea>
    <button name="boton" id="boton" class="boton_iniciar" onclick="actualiza_pagina(); return false;">Guardar</button>
    <span id="mensaje"></span>
  </form>
</body>
</html>
