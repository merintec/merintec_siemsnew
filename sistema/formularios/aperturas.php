<?php 
include("sistema/comunes/verificar_admin_diseno.php");
$boton=$_POST['boton'];
// para las secciones
$nomb_secc=$_POST['nomb_secc'];
$hora_secc=$_POST['hora_secc'];
$lugr_secc=$_POST['lugr_secc'];
$obsr_secc=$_POST['obsr_secc'];
$codg_faci=$_POST['codg_faci'];

// para las aperturas
$evnt_aper=$_POST['evnt_aper'];
$codg_aper=$_POST['codg_aper'];
$fini_aper=$_POST['fini_aper'];
$ffin_aper=$_POST['ffin_aper'];
$obsr_aper=$_POST['obsr_aper'];
$codg_evnt=$_POST['codg_evnt'];
$pgen_aper=$_POST['pgen_aper'];
$prec_aper=$_POST['prec_aper'];
$prep_aper=$_POST['prep_aper'];
$prem_aper=$_POST['prem_aper'];
$pmin_aper=$_POST['pmin_aper'];
$hras_aper=$_POST['hras_aper'];
$desc_aper=$_POST['desc_aper'];
$estu_desc=$_POST['estu_desc'];
$prof_desc=$_POST['prof_desc'];
$empr_desc=$_POST['empr_desc'];
$dura_aper=$_POST['dura_aper'];
$ciudad_aper=$_POST['ciudad_aper'];
$parametro=$_POST['parametro'];
$stat_aper=$_POST['stat_aper'];
$imgr_aper1=$_POST['imgr_aper1'];
$archivo1 = $_FILES["imag_aper1"]["tmp_name"];
$tami_aper1 = $_FILES["imag_aper1"]["size"];
$tipi_aper1 = $_FILES["imag_aper1"]["type"];
$nomi_aper1 = $_FILES["imag_aper1"]["name"];
$imgr_aper2=$_POST['imgr_aper2'];
$archivo2 = $_FILES["imag_aper2"]["tmp_name"];
$tami_aper2 = $_FILES["imag_aper2"]["size"];
$tipi_aper2 = $_FILES["imag_aper2"]["type"];
$nomi_aper2 = $_FILES["imag_aper2"]["name"];

if($tami_aper1>0){
	if($imgr_aper1!=""){
		unlink($imgr_aper1);
	}
    $destino1 = "sistema/imagenes/imagenes_publicaciones/".$nomi_aper1;
	$destino1 = str_replace(" ","_",$destino1);
    if(move_uploaded_file($archivo1, $destino1)) {
		echo "";
	}else{
		echo "archivo no movido";
	}
}

if($tami_aper2>0){
	if($imgr_aper2!=""){
		unlink($imgr_aper2);
	}
    $destino2 = "sistema/imagenes/imagenes_publicaciones/".$nomi_aper2;
	$destino2 = str_replace(" ","_",$destino2);
    if(move_uploaded_file($archivo2, $destino2)) {
		echo "";
	}else{
		echo "archivo no movido";
	}
}
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "eventos_apertura";
$key_entabla = 'codg_aper';
$key_enpantalla = $codg_aper;

$datos[0] = prepara_datos ("fini_aper",$_POST['fini_aper'],'fecha');
$datos[1] = prepara_datos ("ffin_aper",$_POST['ffin_aper'],'fecha');
$datos[2] = prepara_datos ("obsr_aper",$_POST['obsr_aper'],'');
$datos[3] = prepara_datos ("codg_evnt",$_POST['codg_evnt'],'');
$datos[4] = prepara_datos ("pgen_aper",$_POST['pgen_aper'],'');
$datos[5] = prepara_datos ("prec_aper",$_POST['prec_aper'],'');
$datos[6] = prepara_datos ("prep_aper",$_POST['prep_aper'],'');
$datos[7] = prepara_datos ("prem_aper",$_POST['prem_aper'],'');
$datos[8] = prepara_datos ("hras_aper",$_POST['hras_aper'],'');
$datos[9] = prepara_datos ("stat_aper",$_POST['stat_aper'],'');
$datos[10] = prepara_datos ("imag_aper1",$destino1,'');
$datos[11] = prepara_datos ("imag_aper2",$destino2,'');
$datos[12] = prepara_datos ("ciudad_aper",$ciudad_aper,'');
$datos[13] = prepara_datos ("pmin_aper",$_POST['pmin_aper'],'');
$datos[14] = prepara_datos ("desc_aper",$_POST['desc_aper'],'');
$datos[15] = prepara_datos ("estu_desc",$_POST['estu_desc'],'');
$datos[16] = prepara_datos ("prof_desc",$_POST['prof_desc'],'');
$datos[17] = prepara_datos ("empr_desc",$_POST['empr_desc'],'');
$datos[18] = prepara_datos ("dura_aper",$_POST['dura_aper'],'');
$datos[19] = prepara_datos ("evnt_aper",$_POST['evnt_aper'],'');
$datos[20] = prepara_datos ("ppre_aper",$_POST['ppre_aper'],'');

$tabla2 = "eventos_secciones";
$datos2[0] = prepara_datos ("nomb_secc",$_POST['nomb_secc'],'');
$datos2[1] = prepara_datos ("hora_secc",$_POST['hora_secc'],'');
$datos2[2] = prepara_datos ("lugr_secc",$_POST['lugr_secc'],'');
$datos2[3] = prepara_datos ("obsr_secc",$_POST['obsr_secc'],'');
$datos2[4] = prepara_datos ("codg_aper",$_POST['codg_aper'],'');
$datos2[5] = prepara_datos ("codg_faci",$_POST['codg_faci'],'');

if ($boton=='Guardar'){
	$ejec_guardar = guardar($datos,$tabla);
	if ($ejec_guardar[0]!=''){
		$datos2[4][1]=$ejec_guardar[0];
		$ejec_guardar2 = guardar($datos2,$tabla2);
		$existente='si';
		$$key_entabla = $ejec_guardar[0];
		$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
		$con=$con2[0];
		$auditoria=$con2[3];
		$ejec_guardar[1].= '<br><br> Si el evento es de larga duración debe <br>especificar ahora las cuotas correspondientes<br> para ello ir al final del formulario';		
	}
	$mensaje_mostrar=$ejec_guardar[1];
}
if ($boton=='Eliminar')
{	
	$buscando_tipo = buscar('eventos_secciones','codg_aper',$_POST['codg_aper'],'individual');
	if ($buscando_tipo[1]<1) {
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}else{
		$mensaje_mostrar='Apertura no puede eliminarse debido a que hay secciones asociadas';
		$boton='Eliminando';
	}
}
if ($boton=='Actualizar')
{
	if($tami_aper1==0 and $imgr_aper1!=""){
		$datos[10][1]=$imgr_aper1;
	}
	if($tami_aper2==0 and $imgr_aper2!=""){
		$datos[11][1]=$imgr_aper2;
	}
	$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
	$existente='si';        
	$mensaje_mostrar=$ejec_actualizar[1];
	$$key_entabla = $ejec_actualizar[0];
	$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
	$con=$con2[0];
	$auditoria=$con2[3];
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'individual');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	$existente='no';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}


//CONSULTAS COMBOS

$consulta_eventos = mysql_query("SELECT * FROM eventos order by nomb_evnt ");
if ($con[codg_evnt]!='')
{
	       $codg_evnt=$con[codg_evnt];
       	 $consulta_eventos1 = mysql_query("SELECT * FROM eventos where codg_evnt='$codg_evnt' ");
       	 $conev=mysql_fetch_assoc($consulta_eventos1);
       	 $nomb_evnt=$conev[nomb_evnt];

}

 $consulta_facilitadores = mysql_query("SELECT * FROM facilitadores order by nomb_faci ");
 if ($con[codg_faci]!='')
{
	       $codg_faci=$con[codg_faci];
       	 $consulta_facilitadores1 = mysql_query("SELECT * FROM facilitadores where codg_faci='$codg_faci'");
       	 $confaci=mysql_fetch_assoc($consulta_facilitadores1);
       	 $nomb_faci=$confaci[cedu_faci]."->".$confaci[nomb_faci]." ".$confaci[apel_faci];

}



?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">APERTURA DE EVENTOS</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Evento";
		$buscar_varios[0][1]="codg_evnt";
		///                          tabla    codigo en tabla destino    campo que se quiere mostrar
		$buscar_varios[0][2]=array("eventos","codg_evnt","nomb_evnt");
		$buscar_varios[0][3]="center";

		$buscar_varios[1][0]="Fecha Inicio";
		$buscar_varios[1][1]="fini_aper";
		$buscar_varios[2][0]="Fecha Fin";
		$buscar_varios[2][1]="ffin_aper";
		$buscar_varios[3][0]="Observación";
		$buscar_varios[3][1]="obsr_aper";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="" enctype="multipart/form-data">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
				echo '<input type="hidden" name="codg_aper" id="codg_aper" value="'.$con['codg_aper'].'">';	
      		echo '		
				<tr>
					<td align="center">
						
						<select name="codg_evnt" id="codg_evnt"  class="validate[required], combo_form" >';
						if ($con[codg_evnt]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione el Evento</option>';
       				}
       				else 
       				{
      					
       					echo' <option    value="'.$codg_evnt.'" >'.$nomb_evnt.'</option> ';
       				}
       				while($fila=mysql_fetch_array($consulta_eventos))
                  {
                      echo "<option value=".$fila[codg_evnt].">".$fila[nomb_evnt]."</option>";
                  }
						
						echo '</select>
						
					</td>
				</tr>
				<tr>
					<td align="center">

						<input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" value="'.ordernar_fecha($con[fini_aper]).'" name="fini_aper" id="fini_aper" placeholder="Fecha de Inicio"/>

					</td>
				</tr>
				<tr>
					<td align="center">

						<input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" value="'.ordernar_fecha($con[ffin_aper]).'" name="ffin_aper" id="ffin_aper" placeholder="Fecha de Culminación"/>

					</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[minSize[3],maxSize[255]] text-input, cajas_entrada" value="'.$con[ciudad_aper].'" id="ciudad_aper" name="ciudad_aper" placeholder="Ciudad" />
         		</td>
				</tr>				
				
				<tr>
          		<td  align="center">
						<input type="text" class="validate[minSize[3],maxSize[255]] text-input, cajas_entrada" value="'.$con[obsr_aper].'" id="obsr_aper" name="obsr_aper" placeholder="Observación" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
					<input type="text" class="validate[custom[number]] text-input, cajas_entrada" value="'.$con[pgen_aper].'" id="pgen_aper" name="pgen_aper" placeholder="Precio General del Evento" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
				<input type="text" class="validate[custom[number]] text-input, cajas_entrada" value="'.$con[prec_aper].'" id="prec_aper" name="prec_aper" placeholder="% Descuento a Estudiantes" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
				<input type="text" class="validate[custom[number]] text-input, cajas_entrada" value="'.$con[prep_aper].'" id="prep_aper" name="prep_aper" placeholder="% Descuento a Profesionales" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
				<input type="text" class="validate[custom[number]] text-input, cajas_entrada" value="'.$con[prem_aper].'" id="prem_aper" name="prem_aper" placeholder="% Descuento Corporativo" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
          			<table align="center" cellspacing="0" border="0">
          				<tr>
          					<td colspan="2" align="center"><label id="etiqueta"> % Descuento por pago de contado</label></td>
          				</tr>
          				<tr>
          					<td align="center">
  								<input type="text" class="validate[custom[number]] text-input, cajas_entrada" value="'.$con[desc_aper].'" id="desc_aper" name="desc_aper" placeholder="%" style="width: 30px; text-align:center;" maxlength="2"/>
		          			</td>
		          			<td>';
		          				if ($con[estu_desc]!=0){ $mar_estu = ' checked '; }
		          				if ($con[prof_desc]!=0){ $mar_prof = ' checked '; }
		          				if ($con[empr_desc]!=0){ $mar_empr = ' checked '; }
		          				echo '<input type="checkbox" name="estu_desc" id="estu_desc" value="1" '.$mar_estu.'><label id="etiqueta">Estudiantes</label>
		          				<br><input type="checkbox" name="prof_desc" id="prof_desc" value="1" '.$mar_prof.'><label id="etiqueta">Profesionales</label>
								<br><input type="checkbox" name="empr_desc_aux" id="empr_desc_aux" '.$mar_empr.'><label id="etiqueta">Empresas a partir de: </label><input type="text" class="validate[minSize[1],maxSize[2]] text-input, cajas_entrada" value="'.$con[empr_desc].'" id="empr_desc" name="empr_desc" style="width:20px; height: 15px;" maxlength="2"/>
							</td>
						</tr>
					</table>
         		</td>
				</tr>				
				<tr align="center">
					<td  align="center">
						<label id="etiqueta">Publicar los precios:</label> ';
        					echo '<input class="validate[required] radio" type="radio" name="ppre_aper"';  if ($con[ppre_aper]!='NO') { echo 'checked="checked"'; } echo 'value="SI"/> <label id="etiqueta">SI</label>';
        					echo '<input class="validate[required] radio" type="radio" name="ppre_aper"';  if ($con[ppre_aper]=='NO') { echo 'checked="checked"'; } echo 'value="NO"/> <label id="etiqueta">NO</label>';
                		echo '
					</td>
				</tr>
         		 <tr><td>&nbsp;</td></tr>
				<tr>
          		<td  align="center">
				<input type="text" class="validate[custom[number]] text-input, cajas_entrada" value="'.$con[pmin_aper].'" id="pmin_aper" name="pmin_aper" placeholder="Pago mínimo para aprobar inscripción" />
         		</td>
				</tr>

				<tr>
          		<td  align="center">
				<input type="text" class="validate[required, custom[integer]] text-input, cajas_entrada" value="'.$con[hras_aper].'" id="hras_aper" name="hras_aper" placeholder="Horas Academicas" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[minSize[3],maxSize[255]] text-input, cajas_entrada" value="'.$con[dura_aper].'" id="dura_aper" name="dura_aper" placeholder="Duración en días, Meses, Horas"/>
         		</td>
				</tr>
				<tr align="center"><td aling="center"><label id="etiqueta">Imagen General de la apertura</label><br><input type="file" class="cajas_entrada"  id="imag_aper1" name="imag_aper1" placeholder="Imagen Apertura uno" />   </td> </tr>
            <tr align="center"> <td aling="center"><label id="etiqueta">Imagen para ser usada en el carrusel</label><br><input type="file" class="cajas_entrada"  id="imag_aper2" name="imag_aper2" placeholder="Imagen Apertura dos" />   </td> </tr>
			   <tr align="center">
					<td  align="center">
						<label id="etiqueta">Status:</label> ';
        					echo '<input class="validate[required] radio" type="radio" name="stat_aper" id="Activo"';  if ($con[stat_aper]=='Activo') { echo 'checked="checked"'; } echo 'value="Activo"/> <label id="etiqueta">Activo</label>';
        					echo '<input class="validate[required] radio" type="radio" name="stat_aper" id="Inactivo"';  if ($con[stat_aper]=='Inactivo') { echo 'checked="checked"'; } echo 'value="Inactivo"/> <label id="etiqueta">Inactivo</label>';
                		echo '
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr align="center">
					<td  align="center">
						<label id="etiqueta">Publicar en Eventos:</label> ';
        					echo '<input class="validate[required] radio" type="radio" name="evnt_aper" id="SI"';  if ($con[evnt_aper]=='SI') { echo 'checked="checked"'; } echo 'value="SI"/> <label id="etiqueta">SI</label>';
        					echo '<input class="validate[required] radio" type="radio" name="evnt_aper" id="NO"';  if ($con[evnt_aper]=='NO') { echo 'checked="checked"'; } echo 'value="NO"/> <label id="etiqueta">NO</label>';
                		echo '
					</td>
				</tr>
				
				<tr><td  align="center">&nbsp;<input name="imgr_aper1" type="hidden" value="'.$con['imag_aper1'].'" />&nbsp;
				<br><input name="imgr_aper2" type="hidden" value="'.$con['imag_aper2'].'" /></td></tr>'; 

if($boton!='Actualizar' && $boton!='Modificar'){
echo '<tr><td><label id="etiqueta">Datos de la primera Sección</label></td></tr>
<tr><td><label id="etiqueta"><hr></label></td></tr>
<tr>
					<td align="center">
						
						<select name="codg_faci" id="codg_faci"  class="validate[required], combo_form" >';
						if ($con[codg_faci]==NULL)
						{ 
						 echo ' <option value="" selected disabled style="display:none;">Seleccione el facilitador</option>';
       				}
       				else 
       				{
      					
       					echo' <option    value="'.$codg_faci.'" >'.$nomb_faci.'</option> ';
       				}
       				while($fila=mysql_fetch_array($consulta_facilitadores))
                  {
                      echo "<option value=".$fila[codg_faci].">".$fila[cedu_faci]."->".$fila[nomb_faci]." ".$fila[apel_faci]."</option>";
                  }
						
						echo '</select>
						
					</td>
				</tr>				
				<tr>
          		<td  align="center">
				<select name="nomb_secc" id="nomb_secc"  class="validate[required], combo_form">									
					<option value="A" selected>A</option>
				</select>
         		</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required, minSize[1],maxSize[120]] text-input, cajas_entrada" value="'.$con[hora_secc].'" id="hora_secc" name="hora_secc" placeholder="Horario" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[1],maxSize[120]] text-input, cajas_entrada" value="'.$con[lugr_secc].'" id="lugr_secc" name="lugr_secc" placeholder="Lugar" />
         		</td>
				</tr>
				<tr>
          		<td  align="center">
						<input type="text" class="validate[minSize[3],maxSize[255]] text-input, cajas_entrada" value="'.$con[obsr_secc].'" id="obsr_secc" name="obsr_secc" placeholder="Observación" />
         		</td>
				</tr>
				
				
				
				<tr><td>&nbsp;</td></tr>				
				
				';
	}
			}
			else
			{
				
				echo '<input type="hidden" name="codg_aper" id="codg_aper" value="'.$con['codg_aper'].'">';	
				echo '
		
         		<tr>
         			<td align="left" > <label id="etiqueta"> Evento: </label> <label id="resultado">'.$nomb_evnt.' </label> </td> 
         		</tr>
         		<tr><td>&nbsp;</td></tr>
      		   <tr>
         			<td align="left">
         				<label id="etiqueta"> Ciudad: </label> <label id="resultado"> '.$con[ciudad_aper].' </label>
         			</td>
         		</tr>
         		 <tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Fecha de Inicio: </label> <label id="resultado"> '.ordernar_fecha($con[fini_aper]).' </label>
         			</td>
         		</tr>
         		 <tr><td>&nbsp;</td></tr>
         		 
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Fecha de Culminación: </label> <label id="resultado"> '.ordernar_fecha($con[ffin_aper]).' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Precio General del Evento:</label> <label id="resultado"> '.$con[pgen_aper].' </label>
         			</td>
         		</tr> 
         		 <tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Descuento a Estudiantes:</label> <label id="resultado"> '.$con[prec_aper].' %</label>
         			</td>
         		</tr> 
				<tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Descuento a Profesionales:</label> <label id="resultado"> '.$con[prep_aper].' %</label>
         			</td>
         		</tr> 
				<tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Descuento Corporativo:</label> <label id="resultado"> '.$con[prem_aper].' %</label>
         			</td>
         		</tr>
				<tr><td>&nbsp;</td></tr>      		  
				 <tr>
         			<td align="left">
         				<label id="etiqueta"> Descuento por Pago de Contado: </label> <label id="resultado"> '.$con[desc_aper].' </label>
         			</td>
         		</tr>
         		 <tr><td>&nbsp;</td></tr>      		   
         		 <tr>
         			<td align="left">
         				<label id="etiqueta"> Condición para descuento por pago de contado: </label>';		          				
		          				if ($con[estu_desc]!=0){ $condicion = ' Estudiantes'; }
		          				if ($con[estu_desc]!=0 && $con[prof_desc]!=0 && $con[empr_desc]==0){ $condicion .= ' y'; }
		          				if ($con[estu_desc]!=0 && $con[prof_desc]!=0 && $con[empr_desc]!=0){ $condicion .= ','; }
		          				if ($con[prof_desc]!=0){ $condicion .= ' Profesionales '; }
		          				if ($con[estu_desc]!=0 || $con[prof_desc]!=0 && $con[empr_desc]!=0){ $condicion .= ' y '; }
		          				if ($con[empr_desc]!=0){ $condicion .= ' Empresas a partir de '.$con[empr_desc].' participantes'; }

         				echo '<br><label id="resultado"><br>'.$condicion.' </label>
         			</td>
         		</tr>
         		 <tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Precios Publicados:</label> <label id="resultado"> '.$con[ppre_aper].' </label>
         			</td>
         		</tr>
				<tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Monto Mínimo para Aprobar:</label> <label id="resultado"> '.$con[pmin_aper].' Bs.</label>
         			</td>
         		</tr>
				<tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Horas Academicas:</label> <label id="resultado"> '.$con[hras_aper].' </label>
         			</td>
         		</tr> 
				<tr><td>&nbsp;</td></tr>      		   
         		 <tr>
         			<td align="left">
         				<label id="etiqueta"> Duración en días, meses, años: </label> <label id="resultado"><br>'.$con[dura_aper].' </label>
         			</td>
         		</tr>
         		 <tr><td>&nbsp;</td></tr>
					<tr>
         			<td align="left">
         				<label id="etiqueta"> Observación:</label> <label id="resultado"> '.$con[obsr_aper].' </label>
         			</td>
         		</tr>         		
         		<tr><td>&nbsp;</td></tr>
         		';
         		echo '<tr>
         			<td align="left">
         				<label id="etiqueta"> Status:</label> <label id="resultado"> '.$con[stat_aper].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>';
         		echo '<tr>
         			<td align="left">
         				<label id="etiqueta"> Publicado para Eventos:</label> <label id="resultado"> '.$con[evnt_aper].' </label>
         			</td>
         		</tr>';
         		echo '<tr align="center"><td  align="center"><br><label id="etiqueta">Imagen General de la apertura</label><br><center><img src="'.$con['imag_aper1'].'" width="500"  /></center></td></tr>
         		';
         		echo '<tr  align="center"><td  align="center"><br><label id="etiqueta">Imagen para ser usada en el carrusel</label><center><img src="'.$con['imag_aper2'].'" width="500"  /></center></td></tr>
         		<tr><td>&nbsp;</td></tr>';     		
			}
			if (($boton=="Buscar" || $boton=="Modificar" || $boton=="Guardar") && codg_aper!=''){ 
				?>
				<script>
					function realizaProceso(apertura, concepto, fecha, monto, cuota, estu, prof, empr){
					    var parametros = {
					    	"apertura" : apertura,
					        "concepto" : concepto,
							"fecha" : fecha,
							"monto" : monto,
							"cuota" : cuota,
							"estu_cuot" : estu,
							"prof_cuot" : prof,
							"empr_cuot" : empr
					    };
					    if (concepto && fecha && (estu || prof || empr)){
					        $.ajax({
					                data:  parametros,
					                url:   'sistema/formularios/agregar_cuota.php',
					                type:  'post',
					                beforeSend: function () {
					                        $("#resultado_nuevo").html("Procesando, espere por favor...");
					                },
					                success:  function (response) {
					                        $("#resultado_nuevo").html(response);
					                }
					        });
					    }
					    else { alert('Debe suministrar todos los datos'); }
					}
				</script>
				<?php
				$param = $codg_aper.", $('#conc_cuot').val(), $('#ftop_cuot').val(), $('#mont_cuot').val(), '', $('#estu_cuot').val(), $('#prof_cuot').val(), $('#empr_cuot').val()";
				echo '<tr>
						<td>							
							<div class="titulo_formulario" align="center">CUOTAS PARA EVENTOS DE LARGA DURACIÓN</div><BR>
							<table  width="100%" border="0" align="center" cellspacing="0">
								<tr>
									<td><input type="text" class="validate[minSize[3],maxSize[255]] text-input, cajas_entrada" style="width:220px" value="" id="conc_cuot" name="conc_cuot" placeholder="Concepto de la Cuota" /></td>
									<td><input  class="validate[custom[date]] text-input datepicker cajas_entrada es" style="width:82px; text-align: center;" type="text" value="" name="ftop_cuot" id="ftop_cuot" placeholder="Fecha Límite"/></td>
									<td><input  class="validate[custom[number]] text-input cajas_entrada es" style="width:70px; text-align: right;" type="hidden" value="0" name="mont_cuot" id="mont_cuot" placeholder="Monto"/></td>
									<td><input  class="validate[custom[number]] text-input cajas_entrada es" style="width:70px; text-align: right;" type="text" value="" name="estu_cuot" id="estu_cuot" placeholder="Estudiante"/></td>
									<td><input  class="validate[custom[number]] text-input cajas_entrada es" style="width:70px; text-align: right;" type="text" value="" name="prof_cuot" id="prof_cuot" placeholder="Profesional"/></td>
									<td><input  class="validate[custom[number]] text-input cajas_entrada es" style="width:70px; text-align: right;" type="text" value="" name="empr_cuot" id="empr_cuot" placeholder="Empresas"/></td>
									<td><img href="javascript:;" onclick="realizaProceso('.$param.'); return false;" src="sistema/imagenes/agrega.png"  height="35px" title="Agregar Cuota"></td>
								</tr>
							</table>
							<span id="resultado_nuevo">
								<table id="listados" width="100%" border="0" align="center" cellspacing="0" >
									<tr>
										<th>Concepto</th>
										<th width="100px">Fecha Límite</th>
										<th width="100px">Estudiante</th>
										<th width="100px">Profesional</th>
										<th width="100px">Empresa (p/p)</th>
									</tr>';
									$sql_coutas = "SELECT * FROM aperturas_cuotas WHERE codg_aper = ".$codg_aper." ORDER BY ftop_cuot";
									$bus_cuotas = mysql_query($sql_coutas);
									$total_acum = 0;
									while ($res_cuotas = mysql_fetch_array($bus_cuotas)){
										$param_cuota = $codg_aper.",'".$res_cuotas[conc_cuot]."','".$res_cuotas[ftop_cuot]."',".$res_cuotas[mont_cuot].",".$res_cuotas[codg_cuot].",".$res_cuotas[estu_cuot].",".$res_cuotas[prof_cuot].",".$res_cuotas[empr_cuot];
										$total_acum += $res_cuotas[mont_cuot];
										$total_acum1 += $res_cuotas[estu_cuot];
										$total_acum2 += $res_cuotas[prof_cuot];
										$total_acum3 += $res_cuotas[empr_cuot];

										echo '<tr>
												<td>'.$res_cuotas[conc_cuot].'</td>
												<td align="center">'.ordernar_fecha($res_cuotas[ftop_cuot]).'</td>
												<td align="right">'.number_format($res_cuotas[estu_cuot],2,',','.').'&nbsp;</td>
												<td align="right">'.number_format($res_cuotas[prof_cuot],2,',','.').'&nbsp;</td>
												<td align="right">'.number_format($res_cuotas[empr_cuot],2,',','.').'&nbsp;</td>
												<td align="right" width="30px"><img href="javascript:;" onclick="realizaProceso('.$param_cuota.'); return false;" src="sistema/imagenes/acciones/elimina.png"  title="Agregar Cuota">&nbsp;</td>
													</tr>';
									}
									echo '<tr><th align="right" colspan="2">TOTAL</th><th align="right">'.number_format($total_acum1,2,',','.').'&nbsp;</th><th align="right">'.number_format($total_acum2,2,',','.').'&nbsp;</th><th align="right">'.number_format($total_acum3,2,',','.').'&nbsp;</th></tr>';
								echo '</table>
							</span>
						</td>
					</tr>';
			}
			echo '<tr><td>&nbsp;</td></tr>';
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Fecha Inicio";
		$buscar_parm[0][1]="fini_aper";
		$buscar_parm[1][0]="Fecha Fin";
		$buscar_parm[1][1]="ffin_aper";
		$buscar_parm[2][0]="Observación";
		$buscar_parm[2][1]="obsr_aper";
		include('sistema/general/busqueda.php');?>
