<?php 
include("sistema/comunes/verificar_empresa.php");
	// verificar con que empresa esta relacionado el usuario
	$buscando0 = buscar('usuarios','logi_usua',$_SESSION['usuario_logueado'],'individual');
	$con0=$buscando0[0];
	$codg_empr = $con0['codg_empr'];
$boton=$_POST['boton'];

$id_pres=$_POST[id_pres];
$con['tema_pres']=$_POST[tema_pres];
$con['desc_pres']=$_POST[desc_pres];
$con['npar_pres']=$_POST[npar_pres];
$con['fech_pres']=$_POST[fech_pres];
$con['obsr_pres']=$_POST[obsr_pres];
$con['id_pres']=$_POST[id_pres];
$con['codg_empr']=$_POST[codg_empr];
/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "solicitud_presupuesto";
$key_entabla = 'id_pres';
$key_enpantalla = $id_pres;
$datos[0] = prepara_datos ("tema_pres",$_POST['tema_pres'],'');
$datos[1] = prepara_datos ("desc_pres",$_POST['desc_pres'],'');
$datos[2] = prepara_datos ("npar_pres",$_POST['npar_pres'],'');
$datos[3] = prepara_datos ("fech_pres",$_POST['fech_pres'],'fecha');
$datos[4] = prepara_datos ("obsr_pres",$_POST['obsr_pres'],'');
$datos[5] = prepara_datos ("codg_empr",$_POST['codg_empr'],'');

if ($boton=='Guardar'){
	$buscando = buscar($tabla,'id_pres',$_POST[id_pres],'individual');
	if ($buscando[1]<1) {
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
			$mensaje_mostrar=$ejec_guardar[1];
		}
	}else{
		$mensaje_mostrar = 'Error: El Tema '.$_POST[tema_pres].' ya existe intente nuevamente';
		$boton = '';
	}	
}

if ($boton=='Eliminar')
{
	$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
	$mensaje_mostrar=$ejec_eliminar;
	$boton='';
	$auditoria='';
}
if ($boton=='Actualizar')
{
	//$buscando = buscar($tabla,'id_pres',$id_pres,'individual');
	if ($buscando[1]<1) {
			$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
			$existente='si';        
			$mensaje_mostrar=$ejec_actualizar[1];
			$$key_entabla = $ejec_actualizar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
	}
	 
	
	
}
if ($boton=='Buscar')
{
	$parametro = $_POST[parametro];	
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'general');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	if($iramodificar){ $mensaje_mostrar .= "<br><br>No ha efectuado cambios o ya existe el Número"; }
	$existente='no';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}

?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">SOLICITUD DE PRESUPUETO</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Tema";
		$buscar_varios[0][1]="tema_pres";
		$buscar_varios[1][0]="Descripción";
		$buscar_varios[1][1]="desc_pres";
		$buscar_varios[2][0]="Cantidad Participantes";
		$buscar_varios[2][1]="npar_pres";
		$buscar_varios[3][0]="Fecha";
		$buscar_varios[3][1]="fchn_part";
 		$buscar_varios[4][0]="Observaciones";
		$buscar_varios[4][1]="obsr_pres";
	 
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
				echo '<input type="hidden" name="id_pres" id="id_pres" value="'.$con['id_pres'].'">';	
				echo '<input type="hidden" name="codg_empr" id="codg_empr" value="'.$codg_empr.'">';	
      		echo '
				<tr>
          		<td  align="center">
						<input type="text" class="validate[required], minSize[3],maxSize[250]] text-input, cajas_entrada" value="'.$con[tema_pres].'" id="tema_pres" name="tema_pres" placeholder="Tema a Tratar" />
         		</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required], minSize[3],maxSize[250]] text-input,  cajas_entrada" value="'.$con[desc_pres].'" id="desc_pres" name="desc_pres" placeholder="Descripción" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required],custom[integer], minSize[1],maxSize[5]] text-input,  cajas_entrada" value="'.$con[npar_pres].'" id="npar_pres" name="npar_pres" placeholder="Número de Participantes" />
					</td>
				</tr>
 				<tr>
					<td>

						<input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" value="'.ordernar_fecha($con[fech_pres]).'" name="fech_pres" id="fech_pres" placeholder="Posible Fecha del Evento"/>

					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[obsr_pres].'" id="obsr_pres" name="obsr_pres" placeholder="Observaciones" />
					</td>
				</tr>

				<tr><td>&nbsp;</td></tr>';
			}
			else
			{
				echo '<input type="hidden" name="codg_empr" id="codg_empr" value="'.$codg_empr.'">';	
				echo '<input type="hidden" name="id_pres" id="id_pres" value="'.$con['id_pres'].'">';	
				echo '
					<tr>
						<td align="left">
							<label id="etiqueta" > Tema a Tratar: </label> <label id="etiqueta"></label> <label id="resultado">'.$con[tema_pres].' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Descripción: </label> <label id="resultado">'.$con[desc_pres].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left" > <label id="etiqueta"> Cantidad de Participantes: </label> <label id="resultado">'.$con[npar_pres].' </label> </td> 
         		</tr>
           		<tr><td>&nbsp;</td></tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Fecha del Evento: </label> <label id="resultado"> '.ordernar_fecha($con[fech_pres]).' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td> </tr>
         		<tr>
         			<td align="left">
         				<label id="etiqueta"> Observaciones:</label> <label id="resultado"> '.$con[obsr_pres].' </label>
         			</td>
         		</tr>
         		<tr><td>&nbsp;</td></tr> ';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';         		  	   
		?>
	</form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Tema";
		$buscar_parm[0][1]="tema_pres";
		$buscar_parm[1][0]="Fecha";
		$buscar_parm[1][1]="fech_pres";
		include('sistema/general/busqueda.php');
?>
