<?php 
include("sistema/comunes/verificar_admin.php");
$boton=$_POST['boton'];
$nacionalidad=$_POST['nacionalidad'];
$cedula=$_POST['cedula'];
$nombre=$_POST['nombre'];
$apellido=$_POST['apellido'];
$telefono=$_POST['telefono'];
$celular=$_POST['celular'];
$observacion=$_POST['observacion'];
$correo=$_POST['correo'];
$codg_faci=$_POST['codg_faci'];
$parametro=$_POST['parametro'];
$titu_faci=$_POST['titu_faci'];
$perf_faci=$_POST['perf_faci'];
$fnac_faci=$_POST['fnac_faci'];

$con['naci_faci']=$_POST['nacionalidad'];
$con['cedu_faci']=$_POST['cedula'];
$con['nomb_faci']=$_POST['nombre'];
$con['apel_faci']=$_POST['apellido'];
$con['tlfn_faci']=$_POST['telefono'];
$con['celu_faci']=$_POST['celular'];
$con['obsr_faci']=$_POST['observacion'];
$con['emai_faci']=$_POST['correo'];
$con['codg_faci']=$_POST['codg_faci'];
$con['titu_faci']=$_POST['titu_faci'];
$con['perf_faci']=$_POST['perf_faci'];
$con['fnac_faci']=$_POST['fnac_faci'];

/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "facilitadores";
$key_entabla = 'codg_faci';
$key_enpantalla = $codg_faci;
$datos[0] = prepara_datos ("naci_faci",$_POST['nacionalidad'],'');
$datos[1] = prepara_datos ("cedu_faci",$_POST['cedula'],'');
$datos[2] = prepara_datos ("nomb_faci",$_POST['nombre'],'');
$datos[3] = prepara_datos ("apel_faci",$_POST['apellido'],'');
$datos[4] = prepara_datos ("tlfn_faci",$_POST['telefono'],'');
$datos[5] = prepara_datos ("celu_faci",$_POST['celular'],'');
$datos[6] = prepara_datos ("emai_faci",$_POST['correo'],'');
$datos[7] = prepara_datos ("obsr_faci",$_POST['observacion'],'');
$datos[8] = prepara_datos ("titu_faci",$_POST['titu_faci'],'');
$datos[9] = prepara_datos ("perf_faci",$_POST['perf_faci'],'');
$datos[10] = prepara_datos ("fnac_faci",$_POST['fnac_faci'],'fecha');
if ($boton=='Guardar'){
	$buscando = buscar($tabla,'cedu_faci',$_POST['cedula'],'individual');
	if ($buscando[1]<1) {
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
			$mensaje_mostrar=$ejec_guardar[1];
		}
	}
	else {
		$mensaje_mostrar = 'Error: La Cédula '.$_POST[cedu_faci].' ya existe intente nuevamente';
		$boton = '';
	}	
	
	
}
if ($boton=='Eliminar')
{	
	$buscando_tipo = buscar('eventos_secciones','codg_faci',$_POST['codg_faci'],'individual');
	if ($buscando_tipo[1]<1) {
		$ejec_eliminar = eliminar($tabla,$key_entabla,$key_enpantalla,$auditoria);
		$mensaje_mostrar=$ejec_eliminar;
		$boton='';
		$auditoria='';
	}else{
		$mensaje_mostrar='Facilitador no puede eliminarse debido a que hay secciones asociadas';
		$boton='Eliminando';
	}
}
if ($boton=='Actualizar')
{	
	$buscando = buscar($tabla,'cedu_faci',$_POST['cedula']."' AND codg_faci<>'".$_POST['codg_faci'],'individual');
	if ($buscando[1]<1) {
			$ejec_actualizar = actualizar($datos,$tabla,$key_entabla,$key_enpantalla,$auditoria);
			$existente='si';        
			$mensaje_mostrar=$ejec_actualizar[1];
			$$key_entabla = $ejec_actualizar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_actualizar[0],'individual');
			$con=$con2[0];
			$auditoria=$con2[3];
	}
	else {
		$mensaje_mostrar = 'Error: La Cédula '.$_POST[cedu_faci].' ya existe intente nuevamente';
		$boton = 'Actualizando';		
	}	
	
	
}
if ($boton=='Buscar')
{
	$buscando = buscar($tabla,$_POST['criterio'],$parametro,'individual');
	$con=$buscando[0];
	$nresultados=$buscando[1];
	$mensaje_mostrar=$buscando[2];
	$auditoria=$buscando[3];
	$$key_entabla = $con[$key_entabla];
	if ($$key_entabla!=NULL) 
	{
		$existente='si';
        }
	else 
	{
		$existente='no';
		$boton='';
	}
}
if ($boton=='Nuevo')
{
	$con = array();
	$existente='no';
	$boton='';
   $auditoria='';
}
if ($boton=='Modificar')
{
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$mensaje_mostrar = "Cambie la información que requiera y presione Actualizar";
	if($iramodificar){ $mensaje_mostrar .= "<br><br>No ha efectuado cambios o ya existe la cédula"; }
	$existente='no';
}
if ($boton=='Actualizando')
{
	$boton = 'Modificar';
}
if ($boton=='Eliminando')
{
	$existente='si';
	$con = buscar($tabla,$key_entabla,$key_enpantalla,'individual');
	$con=$con[0];
	$boton='Buscar';
}
?>
<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRO DE FACILITADORES</div>
	<?php include('sistema/general/mensaje.php'); ?>
	<?php if ($nresultados>1){ 
		// definimos los parametros a mostrar en el resultado múltiple
		$buscar_varios[0][0]="Cédula";
		$buscar_varios[0][1]="cedu_faci";
		$buscar_varios[1][0]="Nombres";
		$buscar_varios[1][1]="nomb_faci";
		$buscar_varios[2][0]="Apellidos";
		$buscar_varios[2][1]="apel_faci";
		$buscar_varios[3][0]="Titulo";
		$buscar_varios[3][1]="titu_faci";
		$buscar_varios[4][0]="Perfil";
		$buscar_varios[4][1]="perf_faci";
		$buscar_varios[4][4]="center";
		include('sistema/general/busqueda_varios.php'); 
		echo '<br>'; 
	} 
	else {?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="">
		<table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      </br>	
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////
			if ($existente!='si')
         {
         	echo '<input type="hidden" name="codg_faci" id="codg_faci" value="'.$con['codg_faci'].'">';	
				echo '
				<tr>
					<td align="center" colspan="2">
						<select name="nacionalidad" id="nacionalidad"  class="validate[required], combo_identificador">
							<option value="'.$con[naci_faci].'">'.$con[naci_faci].'</option>
							<option value="V">V</option>
							<option value="E">E</option>
						</select>
						<input type="text" class="validate[required, custom[onlyLetterNumber], minSize[6],maxSize[13]] text-input, cajas_entrada2" value="'.$con[cedu_faci].'" id="cedula" name="cedula" placeholder="Cédula de Identidad" />
					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[nomb_faci].'" id="nombre" name="nombre" placeholder="Nombres" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[onlyLetterSp] , minSize[3],maxSize[30]] text-input,  cajas_entrada" value="'.$con[apel_faci].'" id="apellido" name="apellido" placeholder="Apellidos" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" value="'.ordernar_fecha($con[fnac_faci]).'" name="fnac_faci" id="fnac_faci" placeholder="Fecha de Nacimiento"/>
					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[required, custom[phone], minSize[3],maxSize[30]] text-input, cajas_entrada" value="'.$con[tlfn_faci].'" id="telefono" name="telefono" placeholder="Teléfono" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[phone] , minSize[3],maxSize[100]] text-input,  cajas_entrada" value="'.$con[celu_faci].'" id="celular" name="celular" placeholder="Celular" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input,  cajas_entrada" value="'.$con[emai_faci].'" id="correo" name="correo" placeholder="Correo Electrónico" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[minSize[3],maxSize[100]] text-input,  cajas_entrada" value="'.$con[obsr_faci].'" id="observacion" name="observacion" placeholder="Observaciones" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, minSize[3],maxSize[50]] text-input,  cajas_entrada" value="'.$con[titu_faci].'" id="titu_faci" name="titu_faci" placeholder="Titulo o especilidad" />
					</td>
				</tr>
				<tr>
				
				
					<td align="center">
                   <textarea cols="38" rows="4" name="perf_faci" id="perf_faci" class="validate[required] text-input, cajas_texto" placeholder="Perfil Profesional">'.$con[perf_faci].'</textarea>
					</td>
				</tr>
				<tr><td>&nbsp;</td> </tr>'; 
			}
			else 
			{
				echo '<input type="hidden" name="codg_faci" id="codg_faci" value="'.$con['codg_faci'].'">';	
				echo '
					<tr>
						<td align="left">
							<label id="etiqueta" > Cédula de Identidad: </label> <label id="etiqueta"></label> <label id="resultado">'.$con[naci_faci].'-'.$con[cedu_faci].' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="left">
							<label id="etiqueta"> Nombres: </label> <label id="resultado">'.$con[nomb_faci].' </label>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr> 
					<tr>
						<td align="left"> 
							<label id="etiqueta"> Apellidos: </label> <label id="resultado">'.$con[apel_faci].' </label> 
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
			  		<tr>
			 			<td align="left">
			 				<label id="etiqueta"> Fecha de Nacimiento: </label> <label id="resultado"> '.ordernar_fecha($con[fnac_faci]).' </label>
			 			</td>
			 		</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="left">
							<label id="etiqueta"> Teléfono:</label> <label id="resultado"> '.$con[tlfn_faci].' </label>
						</td>
					<tr/>
					<tr><td>&nbsp;</td></tr>
					<tr> 
						<td align="left">
							<label id="etiqueta"> Celular :</label> <label id="resultado"> '.$con[celu_faci].' </label>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="left">
							<label id="etiqueta"> Correo:</label> <label id="resultado"> '.$con[emai_faci].' </label>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="left">
							<label id="etiqueta"> Observaciones :</label> <label id="resultado"> '.$con[obsr_faci].' </label>
						</td>
					</tr>
						<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="left">
							<label id="etiqueta"> Titulo :</label> <label id="resultado"> '.$con[titu_faci].' </label>
						</td>
					</tr>
						<tr><td>&nbsp;</td></tr>
					<tr>
						<td align="left">
							<label id="etiqueta"> Perfil Profesional :</label> <label id="resultado"> '.$con[perf_faci].' </label>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>';
			}
			echo '<tr><td>';
			include('sistema/general/botonera.php');
			echo '</td></tr></table>';
                   ?>
   	   </form>
	<?php } ?>
	<?php // definimos los parametros de busqueda 
		$buscar_parm[0][0]="Cédula";
		$buscar_parm[0][1]="cedu_faci";
		$buscar_parm[1][0]="Nombre";
		$buscar_parm[1][1]="nomb_faci";
		$buscar_parm[2][0]="Titulo";
		$buscar_parm[2][1]="titu_faci";
		$buscar_parm[3][0]="Perfil";
		$buscar_parm[3][1]="perf_faci";
		include('sistema/general/busqueda.php');?>
