<?php 
	session_start();
	$url_base = "../";
	include($url_base.'comunes/conexion.php');
?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>

   	<script>
   		//Precargar imagen timer
		jQuery.preloadImages = function() {
			for(var i = 0; i<arguments.length; i++){
				jQuery("<img>").attr("src", arguments[i]);
			}
		}
		$.preloadImages('../sistema/imagenes/cargando.gif');

	    $(document).ready(function() {
	        $('.datepicker')
	            .datepicker({
	              format: 'dd-mm-yyyy',
	              autoclose: true,
	              language: 'es'
	            });
	    });
	    function guardar_participante(){
			if ($("#form1").validationEngine('validate')){
				var url="../sistema/comunes/funcion_guardar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_guardar').html('Guardando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_guardar').html('Guardar');
	          		  if(codigo==001){
	          		  	$("#form1")[0].reset();
	          		  }
	          		  setTimeout(function() {
	          		  	ir_opcion('participantes','empresas_participantes_new');
				      },3000);
				    }
				});
				return false;
			}
		}
		function llenar(codg_part,nomb_part,apel_part,naci_part,cedu_part,sexo_part,corr_part,tlfn_part,fchn_part){
			$('#var_id_val').val(codg_part);
			$('#nomb_part').val(nomb_part);
			$('#apel_part').val(apel_part);
			$('#naci_part').val(naci_part);
			$('#cedu_part').val(cedu_part);
			$('#sexo_part').val(sexo_part);
			$('#corr_part').val(corr_part);
			$('#tlfn_part').val(tlfn_part);
			$('#fchn_part').val(fchn_part);
			$('#guardar').hide();
			$('#actualizar').show();
			$('#eliminar').show();
			$('#cancelar').show();
			$('html,body').animate({
				scrollTop: $("#inicio").offset().top
			}, 1000);
		}
		function cancela(){
			$('#var_id_val').val('');
			$('#nomb_part').val('');
			$('#apel_part').val('');
			$('#naci_part').val('');
			$('#cedu_part').val('');
			$('#sexo_part').val('');
			$('#corr_part').val('');
			$('#tlfn_part').val('');
			$('#fchn_part').val('');	
			$('#guardar').show();
			$('#actualizar').hide();
			$('#eliminar').hide();
			$('#cancelar').hide();
			$('html,body').animate({
				scrollTop: $("#inicio").offset().top
			}, 1000);
		}

		function actualizar_participante(){
			if ($("#form1").validationEngine('validate')){
				var url="../sistema/comunes/funcion_actualizar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_actualizar').html('Actualizando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_actualizar').html('Actualizar');
	          		  if (codigo=001){          		  	
					      setTimeout(function() {
			          		  ir_opcion('participantes','empresas_participantes_new');
					      },3200);
	          		  }
				    }
				});
				return false;
			}
		}
		function eliminar_participante(){
			$('#codg_empr').val('NULL');
			$('#codg_tpar').val('2');
			var url="../sistema/comunes/funcion_actualizar.php"; 
			$.ajax
			({
			    type: "POST",
			    url: url,
			    data: $("#form1").serialize(),
	          	beforeSend: function () {
	          		$('#etiqueta_eliminar').html('Eliminando...');
			    },
			    success: function(data)
			    {
			      var codigo, datatemp, mensaje;
			      datatemp=data;
			      datatemp=datatemp.split(":::");
			      codigo=datatemp[0];
			      mensaje=datatemp[1];
			      $("#resultado").html(mensaje);
		              $('html,body').animate({
			        scrollTop: $("#inicio").offset().top
			      }, 1000);
			      setTimeout(function() {
			        $("#msg_act").fadeOut(1500);
			      },3000);
          		  $('#etiqueta_eliminar').html('Eliminar');
          		  if (codigo=001){          		  	
				      setTimeout(function() {
		          		  ir_opcion('participantes','empresas_participantes_new');
				      },3200);
          		  }
			    }
			});
			return false;
		}
	</script>
</head>
<body>
    <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">		
		<span class="titulo-perfil">Participantes Asociados a la Empresa</span><br>
		<div id="resultado"></div>
		<span class="subtitulo-perfil">Registra el personal de tu empresa</span>
		<input type="hidden" name="var_tabla" id="var_tabla" value="participantes">
		<input type="hidden" name="codg_empr" id="codg_empr" value="<?php echo $_SESSION[codg_empr]; ?>">
		<input type="hidden" name="codg_tpar" id="codg_tpar" value="3">
		<input type="hidden" name="var_id" id="var_id" value="codg_part">
		<input type="hidden" name="var_id_val" id="var_id_val" value="">
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Nombres</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="nomb_part" id="nomb_part" placeholder="Nombres" class="validate[required, custom[onlyLetterSp] minSize[3],maxSize[30]] text-input form-control"  value="">
				</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Apellidos</span><span class="visible-xs glyphicon glyphicon-user" style="margin-left: 4px; margin-right: 4px; max-height: 20px;"></span></span>
	                <input type="text" name="apel_part" id="apel_part" placeholder="Apellidos" class="validate[required, custom[onlyLetterSp] minSize[3],maxSize[30]] text-input form-control"  value="">
				</div>
		    </div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
	    		<div class="input-group" style="margin-top: 0.8em;">
	        		<span class="input-group-addon fondo_boton" >  
	        		<select name="naci_part" id="naci_part"  class="validate[required] combop negritas" style="height: 1em;">
						<option value="V" <?php if ($res_perfil[naci_part] == 'V') { echo 'selected'; } ?>>V</option>
						<option value="E" <?php if ($res_perfil[naci_part] == 'E') { echo 'selected'; } ?>>E</option>
					</select>
					</span>
	                <input type="text" name="cedu_part" id="cedu_part" placeholder="Cédula de Identidad" class="validate[required, custom[integer] minSize[6],maxSize[13]] text-input form-control"  value="">
	   			</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Sexo</span><img class="visible-xs" width="20" src="../imagenes/page/sexo.png"></span>
		    		<select name="sexo_part" id="sexo_part" class="validate[required] text-input form-control">
		    			<option value="" selected disabled style="display:none;">Seleccione...</option>';
		    			<option value="f" <?php if ($res_perfil[sexo_part] == 'f') { echo 'selected'; } ?>>Femenino</option>
		    			<option value="m" <?php if ($res_perfil[sexo_part] == 'm') { echo 'selected'; } ?>>Masculino</option>
		    		</select>
				</div>
			</div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">email</span><img class="visible-xs" width="20" src="../imagenes/page/ico-correo.png"></span>
	                <input type="email" name="corr_part" id="corr_part" placeholder="Email" class="validate[required, custom[email] minSize[3],maxSize[100]] text-input form-control"  value="">
				</div>
		    </div>
			<div class="col-md-5 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Teléfono</span><img class="visible-xs" style="margin-left: 4px; margin-right: 4px; max-height: 20px;" src="../imagenes/page/ico-telefono.png"></span>
	                <input type="text" name="tlfn_part" id="tlfn_part" placeholder="Teléfono" class="validate[required, custom[phone] minSize[3],maxSize[30]] text-input form-control"  value="">
				</div>
		    </div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-10 col-xs-11">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><span class="hidden-xs negritas">Fecha de Nacimiento</span><img class="visible-xs" width="20" src="../imagenes/page/estrella_blanca.png"></span>
	                <input type="text" name="fchn_part" id="fchn_part" placeholder="Fecha de Nacimiento" class="validate[required,custom[date]]  text-input datepicker form-control" value="">
				</div>
		    </div>
		</div>
		<div class="col-md-11 col-xs-11"></div>
		<div class="row-fluid">
			<div class="col-md-10 col-xs-11" style="margin-top: 2em;">
				<div class="text-right">
					<button id="cancelar" onclick="cancela(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_cancelar">Cancelar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-repeat"></span></button>
					<button id="eliminar" onclick="eliminar_participante(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_eliminar">Eliminar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-trash"></span></button>
					<button id="actualizar" onclick="actualizar_participante(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_actualizar">Actualizar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button>
					<button id="guardar" onclick="guardar_participante(); return false;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_guardar">Guardar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button>
					<script>$('#actualizar').hide();$('#cancelar').hide();$('#eliminar').hide();</script>
				</div>
		    </div>
		</div>
	</form>
	<div class="row-fluid">
		<div class="col-md-11 col-xs-11">&nbsp;</div>
	</div>
	<div class="row-fluid">
		<div class="col-md-11 col-xs-11">
			<div class="text-center titulo-perfil">Participantes Registrados</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row-fluid">
		<div class="table-responsive"> 	
			<table align="left" width="100%" cellspacing="0" id="lista-table" style="font-size: 12px;">
				<tr>
					<th style="text-align: right; padding-right: 5px;">#</th>
					<th style="text-align: left;">Cédula</th>
					<th style="text-align: left;">Nombres y Apellidos</th>
					<th>Acción</th>
				</tr>
				<?php
					$sql_part = "SELECT * FROM participantes WHERE codg_empr = ".$_SESSION[codg_empr]." ORDER BY apel_part ASC, nomb_part ASC";
					$bus_part = mysql_query($sql_part);
					$i = 0;
					while($res_part = mysql_fetch_array($bus_part)){
						$i++;
						echo '
						<tr>
							<td style="text-align: right; padding-right: 5px;">'.$i.'</td>
							<td style="padding-left: 5px;">'.number_format($res_part[cedu_part],0,'','.').'</td>
							<td style="padding-left: 5px;">'.$res_part[apel_part].' '.$res_part[nomb_part].'</td>
							<td align="center"><button class="btn fondo_boton" onclick="llenar(\''.$res_part[codg_part].'\',\''.$res_part[nomb_part].'\',\''.$res_part[apel_part].'\',\''.$res_part[naci_part].'\',\''.$res_part[cedu_part].'\',\''.$res_part[sexo_part].'\',\''.$res_part[corr_part].'\',\''.$res_part[tlfn_part].'\',\''.date('d-m-Y',strtotime($res_part[fchn_part])).'\')" style="font-weight: bold; font-size:12px;">Editar&nbsp;<span class="glyphicon glyphicon-pencil"></span></button></td>
						</tr>';
					}
				?>
			</table>
		</div>
	</div>
</body>
</html>