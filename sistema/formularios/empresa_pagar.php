<?php 
$boton=$_POST['boton'];
$codg_aper=$_GET['codg_aper']; 
$codg_empr=$_SESSION['codg_empr'];
$cedula_usuario=$_SESSION['cedula_usuario'];
$inscripcion = registro_valor("vista_inscripciones","*","WHERE codg_aper=".$codg_aper." AND codg_empr='".$codg_empr."'");

/// valores para usar auditoría en caso de eliminar y modificar
$auditoria=$_POST['campo_auditoria'];
/// Preparando datos para guardar
$tabla = "pagos";
$key_entabla = 'codg_pago';
$key_enpantalla = $codg_pago;

//// Preparando la imagen 

$archivo1 = $_FILES["imag_pago1"]["tmp_name"];
$tami_pago1 = $_FILES["imag_pago1"]["size"];
$tipi_pago1 = $_FILES["imag_pago1"]["type"];
$nomi_pago1 = $_FILES["imag_pago1"]["name"];
if($tami_pago1>0){
	if($imgr_pago1!=""){
		unlink($imgr_pago1);
	}
    $destino1 = "sistema/imagenes/imagenes_pagos/b".$_POST['codg_banc'].'_r'.$_POST['refe_pago'].'_'.$nomi_pago1;
	$destino1 = str_replace(" ","_",$destino1);
    if(move_uploaded_file($archivo1, $destino1)) {
		echo "";
	}else{
          echo "archivo no movido";
	}
}

$datos[0] = prepara_datos ("fech_pago",$_POST['fech_pago'],'fecha');
$datos[1] = prepara_datos ("codg_tpag",$_POST['codg_tpag'],'');
$datos[2] = prepara_datos ("codg_banc",$_POST['codg_banc'],'');
$datos[3] = prepara_datos ("refe_pago",$_POST['refe_pago'],'');
$datos[4] = prepara_datos ("mont_pago",$_POST['mont_pago'],'');
$datos[5] = prepara_datos ("apro_pago",'E','');
$datos[6] = prepara_datos ("codg_insc",0,'');
$datos[7] = prepara_datos ("imag_pago1",$destino1,'');
$datos[8] = prepara_datos ("codg_empr",$codg_empr,'');
$datos[9] = prepara_datos ("codg_aper",$codg_aper,'');



if ($boton=='Registrar'){
		$ejec_guardar = guardar($datos,$tabla);
		if ($ejec_guardar[0]!=''){
			$existente='si';
			$$key_entabla = $ejec_guardar[0];
			$con2 = buscar($tabla,$key_entabla,$ejec_guardar[0],'individual');
			//$con=$con2[0];
			$auditoria=$con2[3];
		}
	$mensaje_mostrar=$ejec_guardar[1];
}

?>

<meta charset="utf-8" />
	<div class="titulo_formulario" align="center">REGISTRAR PAGO DE <?PHP echo $inscripcion['nomb_evnt']; ?></div>
	<?php include('sistema/general/mensaje.php'); ?>
	<form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="" enctype="multipart/form-data">
<?php if (!$_GET['pago']){ ?>
	  <table cellpaddig="0" cellspacing="0" border="0" align="center">
      </br>
      <?php 
         /// No Borrar campo usado para auditoría    
	      echo "<input type='hidden' name='campo_auditoria' value='".$auditoria."'>";
         ////////////////////////////////////////       		  	   
      		echo '
				<tr>
					<td>
						<input  type="hidden" value="'.$codg_insc.'" name="codg_insc" id="codg_insc" placeholder="Codigo de inscripcion"/>
						<input  class="validate[required,custom[date]] text-input datepicker cajas_entrada es" type="text" value="'.ordernar_fecha($con[fech_pago]).'" name="fech_pago" id="fech_pago" placeholder="Fecha de pago"/>
					</td>
				</tr>

				<tr>
          		<td  align="center">
						<select name="codg_tpag" id="codg_tpag"  class="validate[required], combo_form" >';
						echo ' <option value="" selected disabled style="display:none;">Seleccione tipo de pago</option>';
						$consulta_bancos = mysql_query("SELECT * FROM pagos_tipos order by nomb_tpag ");
						while($fila=mysql_fetch_array($consulta_bancos))
						{
			                      echo "<option value=".$fila[codg_tpag].">".$fila[nomb_tpag]."</option>";
                  			}
						echo '</select>

         		</td>

				</tr>
				<tr>
          		<td  align="center">
						<select name="codg_banc" id="codg_banc"  class="validate[required], combo_form" >';
						echo ' <option value="" selected disabled style="display:none;">Seleccione el Banco</option>';
						$consulta_bancos = mysql_query("SELECT * FROM banco order by nomb_banc ");
						while($fila=mysql_fetch_array($consulta_bancos))
						{
			                      echo "<option value=".$fila[codg_banc].">".$fila[nomb_banc]." (".$fila[numr_cuen].")</option>";
                  			}
						echo '</select>

         		</td>

				</tr>
				<tr>
					<td align="center">
						<input type="text" class="validate[required, custom[integer] , minSize[3],maxSize[30]] text-input,  cajas_entrada" value="'.$con[refe_pago].'" id="refe_pago" name="refe_pago" placeholder="Nro de referencia o deposito" />
					</td>
				</tr>
				<tr>
					<td  align="center">
						<input type="text" class="validate[required,custom[number]] text-input, cajas_entrada" value="'.$con[mont_pago].'" id="mont_pago" name="mont_pago" placeholder="Monto del pago" />
					</td>
				</tr>
				<tr> <td aling="center"><label id="etiqueta">Imagen del Pago</label><br><input type="file" class="cajas_entrada"  id="imag_pago1" name="imag_pago1" placeholder="Imagen Apertura uno" />';


?>
	<tr>
	  <td align="right">&nbsp;<input name="boton" type="submit" value="Registrar" class="buscar_curso"/>&nbsp;</td>
   </tr>
<?PHP 
echo '<tr><td>&nbsp;</td></tr></table>';
}
else { echo '<br>'; }
////// verificar que tipo de inscripcion (contado o financiada)
$sql_tip = "SELECT * FROM inscripcion ins WHERE codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."' GROUP BY codg_aper";
$res_tip = mysql_fetch_array(mysql_query($sql_tip));
$tipo = $res_tip[tipo_insc];

if ($tipo == 'Financiada'){

	$sql_deuda = "SELECT vi.tipo_insc, vi.codg_aper, vi.pgen_aper, vi.prec_aper, vi.prep_aper, vi.prem_aper, (SELECT SUM(empr_cuot) FROM aperturas_cuotas WHERE codg_aper = vi.codg_aper ) as monto_cuotas FROM vista_inscripciones vi WHERE codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."'";
	$bus_deuda = mysql_query($sql_deuda);
	$res_deuda = mysql_fetch_array($bus_deuda);
	if ($res_deuda[monto_cuotas]>0){
		$deuda = $res_deuda[monto_cuotas];
		echo '<table align="center" width="80%" cellspacing="0">
		      <tr><td align="center"><div class="titulo_formulario" align="center">V A L O R&nbsp;&nbsp;&nbsp;&nbsp;P O R&nbsp;&nbsp;&nbsp;&nbsp;P A R T I C I P A N T E</div></td></tr>
			  </table>';
	  	echo '<table align="center" width="80%" cellspacing="0" id="listados2" >';
		echo '<tr">
			<th style="font-size: 12px;" id="etiqueta" align="center">Descripción del Pago</th>
			<th style="font-size: 12px;" id="etiqueta" width="100px" align="center">Fecha Límite</th>
			<th style="font-size: 12px;" id="etiqueta" width="150px" align="right">Monto del Pago</th>
		</tr>';
		$sql_cuotas = "SELECT * FROM aperturas_cuotas WHERE codg_aper = ".$res_deuda[codg_aper];
		$bus_cuotas = mysql_query($sql_cuotas);
		while ($res_cuotas = mysql_fetch_array($bus_cuotas)){
			$totalizar += $res_cuotas[empr_cuot];
			echo '<tr">
				<td style="font-size: 12px;" id="etiqueta" align="left">'.$res_cuotas[conc_cuot].'</td>
				<td style="font-size: 12px;" id="etiqueta" width="100px" align="center">'.ordernar_fecha($res_cuotas[ftop_cuot]).'</td>
				<td style="font-size: 12px;" id="etiqueta" width="100px" align="right">'.number_format($res_cuotas[empr_cuot],2,",",".").'</td>
			</tr>';
		}
		echo '<tr>
			<th colspan="2">TOTAL DEL VALOR POR PARTICIPANTE</th>
			<th align="right">Bs. '.number_format($totalizar,2,',','.').'&nbsp;</th>
		</tr>';
		echo '</table><br><BR>';
	}
}


// Buscar cuantos hay inscritos para la empresa actual en el evento actual
//	$sql_aper="select ((pgen_aper - (pgen_aper * prem_aper / 100)) * count(codg_part)) as inscritos from vista_inscripciones where codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."'";
	$sql_aper="select SUM(base_insc) as inscritos from vista_inscripciones where codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."'";
	$busq_aper=mysql_query($sql_aper);
	$reg_aper=mysql_fetch_array($busq_aper);

$sql_deberes = "SELECT * FROM vista_inscripciones ins, participantes par where ins.codg_empr = ".$codg_empr." AND ins.codg_aper=".$codg_aper. " AND ins.codg_part=par.codg_part ORDER BY nomb_secc,apel_part,nomb_part";
$bus_deberes = mysql_query($sql_deberes);
?>
<table align="center" width="80%" cellspacing="0">
      <tr><td align="center"><div class="titulo_formulario" align="center">P A R T I C I P A N T E S&nbsp;&nbsp;&nbsp;&nbsp;I N S C R I T O S</div></td></tr>
</table>
<table width="80%" border="0" align="center" cellspacing="0">
	<tr class="cajas_entrada"  align="center">
	  <td width="30px">&nbsp;Nº</td>
	  <td width="100px">C&eacute;dula</td>
	  <td width="30px">&nbsp;</td>
	  <td align="left">Apellidos y Nombres</td>
	  <td align="center">Secci&oacute;n</td>
   	</tr>
<?php
$i=0;
while ($res_deberes = mysql_fetch_array($bus_deberes)){
	$i+=1;
	$res=$i%2;if($res==0){ $clase="lista_tabla2"; }else{ $clase="lista_tabla1"; }
	echo '<tr class="'.$clase.'">
	  <td width="30px" align="right">'.$i.'</td>
	  <td width="100px" align="right">'.number_format($res_deberes[cedu_part],0,",",".").'</td>
	  <td width="30px">&nbsp;</td>
	  <td>'.$res_deberes[apel_part].' '.$res_deberes[nomb_part].'</td>
	  <td align="center">'.$res_deberes[nomb_secc].'</td>
   	</tr>';
}
if ($totalizar>0){ $reg_aper[inscritos] = $totalizar*$i; }
echo '<tr id="listados"  align="center">
  <th width="30px" colspan="5">&nbsp;TOTAL DE LA INVERSI&Oacute;N '.number_format($reg_aper[inscritos],2,",",".").'</th>
</tr>';
echo '</table><br><br>';
///// Descuento recibido en esta Apertura
$sql_desc = "SELECT * FROM aperturas_descuentos WHERE codg_empr = ".$codg_empr." AND codg_aper=".$codg_aper.";";
$bus_desc = mysql_query($sql_desc);
if ($res_desc = mysql_fetch_array($bus_desc)){
	if ($res_desc[tipo_desc]=='%'){ $mostrar_desc = '('.$res_desc[mont_desc].'%)'; $descuento = $reg_aper[inscritos] * $res_desc[mont_desc] / 100; }
	if ($res_desc[tipo_desc]=='Bs.') { $descuento = $res_desc[mont_desc]; }
	$reg_aper[inscritos] = $reg_aper[inscritos] - $descuento;
	echo '<table align="center" width="80%" cellspacing="0">';
	echo '<tr><td align="center"><div class="titulo_formulario" align="center">D E S C U E N T O S</div></td></tr>
		  </table>';
	echo '<table align="center" width="80%" cellspacing="0" id="listados2" >';
	echo '<tr>
		<th id="etiqueta" align="center">Descuento Especial Recibido '.$mostrar_desc.'</th>
		<th id="etiqueta" width="150px" align="right">Bs. '.number_format($descuento,"2",",",".").'&nbsp;</th>
	</tr></table><br>';
} 
///// pagos efectuados
?>

	<table align="center" width="80%" cellspacing="0">
	      <tr><td align="center"><div class="titulo_formulario" align="center">P A G O S&nbsp;&nbsp;&nbsp;&nbsp;R E G I S T R A D O S</div></td></tr>
	</table>
  <table width="80%" border="0" align="center" cellspacing="0">
 	<tr>
	  <td height="24">
  <table width="100%" border="0" align="center" cellspacing="0">
	<tr class="cajas_entrada" align="center">
	  <td width="30px">&nbsp;Nº</td>
	  <td width="80px">&nbsp;Fecha</td>
	  <td align="left">&nbsp;Banco</td>
	  <td width="80px">&nbsp;Referencia</td>
	  <td width="80px">&nbsp;Monto</td>
	  <td width="100px">&nbsp;Conformado</td>
   </tr>
   
<?PHP
		$sql_pago="select pa.*, CONCAT(bn.nomb_banc,' (',bn.numr_cuen,')') as banc_pago from pagos pa, banco bn where codg_empr=".$codg_empr." AND codg_aper=".$codg_aper." AND pa.codg_banc=bn.codg_banc ORDER BY codg_insc DESC";
		$busq_pago=mysql_query($sql_pago);
		if($reg_pago=mysql_fetch_array($busq_pago)){
			$i=0;
			do{
				$i+=1;
				$res=$i%2;
				if($res==0){ $clase="lista_tabla2"; }else{ $clase="lista_tabla1"; }
				echo '<tr class="'.$clase.'">
						<td align="right">&nbsp;'.$i.'</td>
						<td align="center">&nbsp;'.ordernar_fecha($reg_pago[fech_pago]).'</td>
						<td>&nbsp;'.$reg_pago[banc_pago].'</td>
						<td align="center">&nbsp;'.$reg_pago[refe_pago].'</td>
						<td align="right">&nbsp;'.number_format($reg_pago[mont_pago],2,",",".").'&nbsp;</td>
						<td align="center">&nbsp;'; if($reg_pago[apro_pago]=="A"){ echo "Aprobado"; $total_conformado += $reg_pago[mont_pago]; }elseif($reg_pago[apro_pago]=="R"){ echo "Rechazado: <br>".$reg_pago[rech_pago]; }else{ echo "En&nbsp;Espera"; } echo '&nbsp;</td>
					  </tr>';
			}while($reg_pago=mysql_fetch_array($busq_pago));
		}
echo '<tr id="listados" align="center">
  <th width="30px" colspan="6">TOTAL CONFORMADO '.number_format($total_conformado,2,",",".").'</th>
</tr>';
echo '<tr id="listados"  align="center">
  <th width="30px" colspan="6">PENDIENTE POR PAGAR '.number_format($reg_aper[inscritos]-$total_conformado,2,",",".").'</th>
</tr>';
?>
</table>
	</td>
   </tr>
	<tr>
	  <td align="right">&nbsp;</td>
   </tr>
   </tr>

</table>

</form>
