<?php 
	session_start();
	$url_base = "../../";
	$url_base2 = "../";
	include($url_base.'sistema/comunes/conexion.php');
	include_once($url_base.'sistema/comunes/funciones_php.php'); 
	$codg_aper=$_POST['codg_aper']; 
	$codg_empr=$_SESSION['codg_empr'];
	$cedula_usuario=$_SESSION['cedula_usuario'];
	$inscripcion = registro_valor("vista_inscripciones","*","WHERE codg_aper=".$codg_aper." AND codg_empr='".$codg_empr."'");

?>
<meta charset="utf-8" />
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
   	<link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
   	<script src="../js/calendario/bootstrap-datepicker.min.js"></script>
   	<script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
   	<script>
   		//Precargar imagen timer
		jQuery.preloadImages = function() {
			for(var i = 0; i<arguments.length; i++){
				jQuery("<img>").attr("src", arguments[i]);
			}
		}
		$.preloadImages('../sistema/imagenes/cargando.gif');

	    $(document).ready(function() {
	        $('.datepicker')
	            .datepicker({
	              format: 'dd-mm-yyyy',
	              autoclose: true,
	              language: 'es'
	            });
	    });
	    function guardar_pago(){
			if ($("#form1").validationEngine('validate')){
				var url="../sistema/comunes/funcion_guardar.php"; 
				$.ajax
				({
				    type: "POST",
				    url: url,
				    data: $("#form1").serialize(),
		          	beforeSend: function () {
		          		$('#etiqueta_boton').html('Guardando...');
				    },
				    success: function(data)
				    {
				      var codigo, datatemp, mensaje;
				      datatemp=data;
				      datatemp=datatemp.split(":::");
				      codigo=datatemp[0];
				      mensaje=datatemp[1];
				      $("#resultado").html(mensaje);
  		              $('html,body').animate({
				        scrollTop: $("#inicio").offset().top
				      }, 1000);
				      setTimeout(function() {
				        $("#msg_act").fadeOut(1500);
				      },3000);
	          		  $('#etiqueta_boton').html('Guardar');
	          		  if(codigo==001){
	          		  	$("#form1")[0].reset();
	          		  }
	          		  setTimeout(function() {
				        pagar_inscripcion(<?php echo $codg_aper; ?>);
				      },3000);
				    }
				});
				return false;
			}
		} 
	</script>
</head>
    <form id="form1" onsubmit="return jQuery(this).validationEngine('validate');"  method="post" action="" enctype="multipart/form-data">
	<span class="titulo-perfil">Registrar un Pago de la Empresa</span><br>
		<span id="subtitulo" class="subtitulo-perfil"><?php echo $inscripcion['nomb_evnt'];?></span>
		<div id="resultado"></div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="row-fluid">
				<div class="col-md-11 col-xs-11">	
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-11 col-xs-11">
				<input type="hidden" name="apro_pago" id="apro_pago" value="E">
				<input type="hidden" name="var_tabla" id="var_tabla" value="pagos">
				<input type="hidden" name="codg_aper" id="codg_aper" value="<?php echo $codg_aper; ?>">
				<input type="hidden" name="codg_empr" id="codg_empr" value="<?php echo $codg_empr; ?>">
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton" ><div class="hidden-xs negritas" style="min-width: 110px;">Fecha</div><span class="visible-xs glyphicon glyphicon-calendar" width="20"></span></span>
	                <input type="text" name="fech_pago" id="fech_pago" placeholder="Fecha del Pago" class="validate[required, custom[date]] text-input form-control datepicker"  value="<?php echo date('d-m-Y'); ?>">
				</div>
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton hidden-xs"><div class="hidden-xs negritas"  style="min-width: 110px;">Tipo de pago</div></span>
		    		<select name="codg_tpag" id="codg_tpag"  class="validate[required], text-input form-control" >
						<?php 
							echo ' <option value="" selected disabled style="display:none;">Seleccione tipo de pago</option>';
							$consulta_bancos = mysql_query("SELECT * FROM pagos_tipos order by nomb_tpag ");
							while($fila=mysql_fetch_array($consulta_bancos))
							{
								echo "<option value=".$fila[codg_tpag].">".$fila[nomb_tpag]."</option>";
	                  		}
		    			?>
		    		</select>
				</div>
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton hidden-xs" ><div class="hidden-xs negritas"  style="min-width: 110px;">Banco Destino</div></span>
		    		<select name="codg_banc" id="codg_banc"  class="validate[required], text-input form-control" >
						<?php 
							echo ' <option value="" selected disabled style="display:none;">Seleccione el Banco</option>';
							$consulta_bancos = mysql_query("SELECT * FROM banco WHERE trans_cuen = 1 order by nomb_banc ");
							while($fila=mysql_fetch_array($consulta_bancos))
							{
								echo "<option value=".$fila[codg_banc].">".$fila[nomb_banc]." (".$fila[numr_cuen].")</option>";
                  			}
		    			?>
		    		</select>
				</div>
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton hidden-xs" ><div class="hidden-xs negritas"  style="min-width: 110px;">Referencia</div></span>
					<input type="text" class="validate[required, custom[integer] , minSize[3],maxSize[30]] text-input form-control" value="" id="refe_pago" name="refe_pago" placeholder="Nro de referencia o deposito" />
				</div>
				<div class="input-group" style="margin-top: 0.8em;">
		    		<span class="input-group-addon fondo_boton hidden-xs" ><div class="hidden-xs negritas" style="min-width: 110px;">Monto del Pago</div></span>
					<input type="text" class="validate[required,custom[number]] text-input, form-control" value="" id="mont_pago" name="mont_pago" placeholder="Monto del pago" />
				</div>
		    </div>
		</div>
		<div class="row-fluid">
			<div class="col-md-12 col-xs-12" style="margin-top: 2em;">
				<div class="text-center"><button id="guardar" onclick="guardar_pago();return;" class="btn fondo_boton" style="margin-top: 0.3em; font-weight: bold;"><span id="etiqueta_boton">Guardar</span>&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk"></span></button></div>
		    </div>
		</div>
		<div class="row-fluid" style="margin-top: 1em;">
			<div class="col-md-11 col-xs-11">&nbsp;</div> 
		</div>

<?php
////// verificar que tipo de inscripcion (contado o financiada)
$sql_tip = "SELECT * FROM inscripcion ins WHERE codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."' GROUP BY codg_aper";
$res_tip = mysql_fetch_array(mysql_query($sql_tip));
$tipo = $res_tip[tipo_insc];

if ($tipo == 'Financiada'){

	$sql_deuda = "SELECT vi.tipo_insc, vi.codg_aper, vi.pgen_aper, vi.prec_aper, vi.prep_aper, vi.prem_aper, (SELECT SUM(empr_cuot) FROM aperturas_cuotas WHERE codg_aper = vi.codg_aper ) as monto_cuotas FROM vista_inscripciones vi WHERE codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."'";
	$bus_deuda = mysql_query($sql_deuda);
	$res_deuda = mysql_fetch_array($bus_deuda);
	if ($res_deuda[monto_cuotas]>0){
		$deuda = $res_deuda[monto_cuotas];
		echo '<table align="center" width="80%" cellspacing="0">
		      <tr><td align="center"><div class="titulo_formulario" align="center">V A L O R&nbsp;&nbsp;&nbsp;&nbsp;P O R&nbsp;&nbsp;&nbsp;&nbsp;P A R T I C I P A N T E</div></td></tr>
			  </table>';
	  	echo '<table align="center" width="80%" cellspacing="0" id="listados2" >';
		echo '<tr">
			<th style="font-size: 12px;" id="etiqueta" align="center">Descripción del Pago</th>
			<th style="font-size: 12px;" id="etiqueta" width="100px" align="center">Fecha Límite</th>
			<th style="font-size: 12px;" id="etiqueta" width="150px" align="right">Monto del Pago</th>
		</tr>';
		$sql_cuotas = "SELECT * FROM aperturas_cuotas WHERE codg_aper = ".$res_deuda[codg_aper];
		$bus_cuotas = mysql_query($sql_cuotas);
		while ($res_cuotas = mysql_fetch_array($bus_cuotas)){
			$totalizar += $res_cuotas[empr_cuot];
			echo '<tr">
				<td style="font-size: 12px;" id="etiqueta" align="left">'.$res_cuotas[conc_cuot].'</td>
				<td style="font-size: 12px;" id="etiqueta" width="100px" align="center">'.ordernar_fecha($res_cuotas[ftop_cuot]).'</td>
				<td style="font-size: 12px;" id="etiqueta" width="100px" align="right">'.number_format($res_cuotas[empr_cuot],2,",",".").'</td>
			</tr>';
		}
		echo '<tr>
			<th colspan="2">TOTAL DEL VALOR POR PARTICIPANTE</th>
			<th align="right">Bs. '.number_format($totalizar,2,',','.').'&nbsp;</th>
		</tr>';
		echo '</table><br><BR>';
	}
}


// Buscar cuantos hay inscritos para la empresa actual en el evento actual
//	$sql_aper="select ((pgen_aper - (pgen_aper * prem_aper / 100)) * count(codg_part)) as inscritos from vista_inscripciones where codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."'";
	$sql_aper="select SUM(base_insc) as inscritos from vista_inscripciones where codg_empr='".$codg_empr."' AND codg_aper='".$codg_aper."'";
	$busq_aper=mysql_query($sql_aper);
	$reg_aper=mysql_fetch_array($busq_aper);
	$sql_deberes = "SELECT * FROM vista_inscripciones ins, participantes par where ins.codg_empr = ".$codg_empr." AND ins.codg_aper=".$codg_aper. " AND ins.codg_part=par.codg_part ORDER BY nomb_secc,apel_part,nomb_part";
	$bus_deberes = mysql_query($sql_deberes);
	echo '<div class="col-md-12 col-xs-12">
		<div class="titulo-perfil" align="center">P A R T I C I P A N T E S&nbsp;&nbsp;&nbsp;&nbsp;I N S C R I T O S</div>
	</div>';
?>
<table width="100%" border="0" align="center" cellspacing="0" id="lista-table" style="font-size: 12px;">
	<tr class="cajas_entrada"  align="center">
	  <th width="30px">&nbsp;Nº</th>
	  <th width="100px">C&eacute;dula</th>
	  <th width="30px">&nbsp;</th>
	  <th align="left">Apellidos y Nombres</th>
	  <th align="center">Secci&oacute;n</th>
   	</tr>
<?php
$i=0;
while ($res_deberes = mysql_fetch_array($bus_deberes)){
	$i+=1;
	$res=$i%2;if($res==0){ $clase="lista_tabla2"; }else{ $clase="lista_tabla1"; }
	echo '<tr class="'.$clase.'">
	  <td width="30px" align="right">'.$i.'</td>
	  <td width="100px" align="right">'.number_format($res_deberes[cedu_part],0,",",".").'</td>
	  <td width="30px">&nbsp;</td>
	  <td>'.$res_deberes[apel_part].' '.$res_deberes[nomb_part].'</td>
	  <td align="center">'.$res_deberes[nomb_secc].'</td>
   	</tr>';
}
if ($totalizar>0){ $reg_aper[inscritos] = $totalizar*$i; }
echo '<tr id="listados"  align="center">
  <th width="30px" colspan="5">&nbsp;TOTAL DE LA INVERSI&Oacute;N '.number_format($reg_aper[inscritos],2,",",".").'</th>
</tr>';
echo '</table><br><br>';
///// Descuento recibido en esta Apertura
$sql_desc = "SELECT * FROM aperturas_descuentos WHERE codg_empr = ".$codg_empr." AND codg_aper=".$codg_aper.";";
$bus_desc = mysql_query($sql_desc);
if ($res_desc = mysql_fetch_array($bus_desc)){
	if ($res_desc[tipo_desc]=='%'){ $mostrar_desc = '('.$res_desc[mont_desc].'%)'; $descuento = $reg_aper[inscritos] * $res_desc[mont_desc] / 100; }
	if ($res_desc[tipo_desc]=='Bs.') { $descuento = $res_desc[mont_desc]; }
	$reg_aper[inscritos] = $reg_aper[inscritos] - $descuento;
	echo '<div class="col-md-12 col-xs-12">
		<div class="titulo-perfil" align="center">D E S C U E N T O S</div>
	</div>';

	echo '<table align="center" width="100%" cellspacing="0"  id="lista-table">';
	echo '<tr>
		<th id="etiqueta" align="center">Descuento Especial Recibido '.$mostrar_desc.'</th>
		<th id="etiqueta" width="150px" align="right">Bs. '.number_format($descuento,"2",",",".").'&nbsp;</th>
	</tr></table><br>';
} 
///// pagos efectuados

	echo '<div class="col-md-12 col-xs-12">
		<div class="titulo-perfil" align="center">P A G O S&nbsp;&nbsp;&nbsp;&nbsp;R E G I S T R A D O S</div>
	</div>';
?>
  <table width="100%" border="0" align="center" cellspacing="0" id="lista-table" style="font-size: 12px;">
	<tr class="cajas_entrada" align="center">
	  <th width="30px">&nbsp;Nº</th>
	  <th width="80px">&nbsp;Fecha</th>
	  <th align="left">&nbsp;Banco</th>
	  <th width="80px">&nbsp;Referencia</th>
	  <th width="80px">&nbsp;Monto</th>
	  <th width="100px">&nbsp;Conformado</th>
   </tr>
   
<?PHP
		$sql_pago="select pa.*, CONCAT(bn.nomb_banc,' (',bn.numr_cuen,')') as banc_pago from pagos pa, banco bn where codg_empr=".$codg_empr." AND codg_aper=".$codg_aper." AND pa.codg_banc=bn.codg_banc ORDER BY fech_pago DESC, codg_pago DESC";
		$busq_pago=mysql_query($sql_pago);
		if($reg_pago=mysql_fetch_array($busq_pago)){
			$i=0;
			do{
				$i+=1;
				$res=$i%2;
				if($res==0){ $clase="lista_tabla2"; }else{ $clase="lista_tabla1"; }
				echo '<tr class="'.$clase.'">
						<td align="right">&nbsp;'.$i.'</td>
						<td align="center">&nbsp;'.ordernar_fecha($reg_pago[fech_pago]).'</td>
						<td>&nbsp;'.$reg_pago[banc_pago].'</td>
						<td align="center">&nbsp;'.$reg_pago[refe_pago].'</td>
						<td align="right">&nbsp;'.number_format($reg_pago[mont_pago],2,",",".").'&nbsp;</td>
						<td align="center">&nbsp;'; if($reg_pago[apro_pago]=="A"){ echo "Aprobado"; $total_conformado += $reg_pago[mont_pago]; }elseif($reg_pago[apro_pago]=="R"){ echo "Rechazado: <br>".$reg_pago[rech_pago]; }else{ echo "En&nbsp;Espera"; } echo '&nbsp;</td>
					  </tr>';
			}while($reg_pago=mysql_fetch_array($busq_pago));
		}
echo '<tr id="listados" align="center">
  <th width="30px" colspan="6">TOTAL CONFORMADO '.number_format($total_conformado,2,",",".").'</th>
</tr>';
echo '<tr id="listados"  align="center">
  <th width="30px" colspan="6">PENDIENTE POR PAGAR '.number_format($reg_aper[inscritos]-$total_conformado,2,",",".").'</th>
</tr>';
?>
</table>
<div class="row-fluid">
	<div class="col-md-12 col-xs-12">&nbsp;</div>
</div>
<script type="text/javascript">
	$('#mont_pago').attr("placeholder", 'Monto pendiente por pagar Bs. <?php echo number_format($reg_aper[inscritos]-$total_conformado,2,"","."); ?>');	
</script>
</form>
