-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-11-2015 a las 15:56:36
-- Versión del servidor: 5.5.43
-- Versión de PHP: 5.4.41-0+deb7u1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `siems`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aperturas_cuotas`
--

CREATE TABLE IF NOT EXISTS `aperturas_cuotas` (
  `codg_cuot` int(11) NOT NULL AUTO_INCREMENT,
  `codg_aper` int(11) NOT NULL,
  `conc_cuot` varchar(255) NOT NULL,
  `ftop_cuot` date NOT NULL,
  `mont_cuot` double(9,2) NOT NULL,
  `estu_cuot` double(9,2) NOT NULL,
  `prof_cuot` double(9,2) NOT NULL,
  `empr_cuot` double(9,2) NOT NULL,
  PRIMARY KEY (`codg_cuot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aperturas_descuentos`
--

CREATE TABLE IF NOT EXISTS `aperturas_descuentos` (
  `codg_desc` int(11) NOT NULL AUTO_INCREMENT,
  `mont_desc` double(9,2) NOT NULL,
  `tipo_desc` varchar(5) NOT NULL COMMENT 'Tipo de Descuentos',
  `codg_insc` int(11) DEFAULT NULL,
  `codg_aper` int(11) NOT NULL,
  `codg_empr` int(11) DEFAULT NULL,
  PRIMARY KEY (`codg_desc`),
  UNIQUE KEY `codg_insc` (`codg_insc`),
  UNIQUE KEY `codg_aper` (`codg_aper`,`codg_empr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de los descuentos' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE IF NOT EXISTS `auditoria` (
  `codg_audi` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Auditoria',
  `nomb_usua` varchar(60) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre del usuario',
  `acci_audi` varchar(15) CHARACTER SET latin1 NOT NULL COMMENT 'Accion Realizada',
  `deta_audi` longtext CHARACTER SET latin1 NOT NULL COMMENT 'Detalles de la Accion',
  `tabl_audi` varchar(50) CHARACTER SET latin1 NOT NULL COMMENT 'Tabla sobre la que se efectuo la accion',
  `fech_audi` date NOT NULL COMMENT 'fecha de la accion',
  `hora_audi` time NOT NULL COMMENT 'hora de la accion',
  PRIMARY KEY (`codg_audi`),
  KEY `cod_usr` (`nomb_usua`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Datos de las acciones efectuadas en el sistema' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE IF NOT EXISTS `banco` (
  `codg_banc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de la cuenta bancaria y banco',
  `nomb_banc` varchar(60) NOT NULL COMMENT 'Nombre del banco al que pertenece la cuenta',
  `numr_cuen` varchar(20) NOT NULL COMMENT 'Numero de cuenta de 20 digitos',
  `tipo_cuen` char(1) NOT NULL COMMENT 'Tipo de cuenta',
  `titu_cuen` varchar(60) NOT NULL COMMENT 'Titular de la cuenta',
  `rift_cuen` varchar(12) NOT NULL COMMENT 'Rif del titular de la cuenta',
  `cort_cuen` varchar(60) NOT NULL COMMENT 'correo del titular de la cuenta',
  `trans_cuen` int(11) NOT NULL COMMENT 'SI la cuenta es usada para transferencia o no',
  PRIMARY KEY (`codg_banc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificados`
--

CREATE TABLE IF NOT EXISTS `certificados` (
  `id_cert` int(11) NOT NULL AUTO_INCREMENT,
  `cedu_part` varchar(13) NOT NULL,
  `nomb_apel_part` varchar(250) NOT NULL,
  `nomb_evnt` varchar(250) NOT NULL,
  `codg_aper` int(11) NOT NULL,
  `cedu_faci` varchar(13) NOT NULL,
  `nomb_apel_faci` int(250) NOT NULL,
  `usuario` varchar(250) NOT NULL,
  `fech_cert` date NOT NULL,
  `nume_cert` varchar(250) NOT NULL,
  `cali_cert` varchar(250) NOT NULL,
  PRIMARY KEY (`id_cert`),
  UNIQUE KEY `nume_cert` (`nume_cert`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `id_contacto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `correo` varchar(250) NOT NULL,
  `contenido` varchar(250) NOT NULL,
  `fecha` date NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `status_cont` varchar(30) NOT NULL,
  PRIMARY KEY (`id_contacto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `codg_empr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la empresa',
  `trif_empr` char(1) NOT NULL COMMENT 'Tipo de Rif de la empresa',
  `rifd_empr` varchar(12) NOT NULL COMMENT 'Rif de la empresa',
  `nomb_empr` varchar(60) NOT NULL COMMENT 'Razon Social o Nombre de la empresa',
  `dirc_empr` varchar(255) NOT NULL COMMENT 'Direccion de la empresa',
  `tlfn_empr` varchar(13) NOT NULL COMMENT 'Telefono de la empresa',
  `emai_empr` varchar(70) NOT NULL COMMENT 'Email de la empresa',
  `cont_empr` varchar(60) NOT NULL COMMENT 'Persona de contacto de la empresa',
  `tlfc_empr` varchar(13) NOT NULL COMMENT 'Telefono de la persona de contacto de la empresa',
  PRIMARY KEY (`codg_empr`),
  UNIQUE KEY `emai_empr` (`emai_empr`),
  UNIQUE KEY `rifd_empr` (`rifd_empr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE IF NOT EXISTS `eventos` (
  `codg_evnt` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del Evento',
  `nomb_evnt` varchar(255) NOT NULL COMMENT 'Nombre del Evento',
  `desc_evnt` varchar(255) NOT NULL COMMENT 'Descripcion detallda del evento',
  `codg_area` int(11) NOT NULL COMMENT 'Codigo del Area a la que pertenece el evento',
  `codg_tipo` int(11) NOT NULL COMMENT 'Codigo del tipo de Evento al que pertenece',
  `stat_evnt` varchar(30) NOT NULL COMMENT 'Status del evento',
  `imag_evnt` varchar(100) NOT NULL COMMENT 'Tipo de imagen del evento',
  PRIMARY KEY (`codg_evnt`),
  KEY `codg_area` (`codg_area`),
  KEY `codg_tipo` (`codg_tipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=300 ;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`codg_evnt`, `nomb_evnt`, `desc_evnt`, `codg_area`, `codg_tipo`, `stat_evnt`, `imag_evnt`) VALUES
(1, 'Programa de Formación Integral en Asistente Administrativo', '', 1, 1, '', ''),
(2, 'Gerencia del Talento Humano', '', 1, 1, '', ''),
(3, 'Auxiliar contable', '', 1, 1, '', ''),
(4, 'Diseño y Cálculo de Nómina', '', 1, 1, '', ''),
(5, 'Elaboracion y analisis de estados', '', 1, 1, '', ''),
(6, 'EPCP', '', 1, 1, '', ''),
(7, 'Formulacion y Evaluacion de Proyectos', '', 1, 1, '', ''),
(8, 'LOPCYMAT', '', 4, 1, '', ''),
(9, 'Secretariado computarizado', '', 1, 1, '', ''),
(10, 'Indicadores de Gestión', '', 8, 1, '', ''),
(11, 'Diplomado en Gerencia del Talento Humano', '', 5, 1, '', ''),
(12, 'Diplomado en Gerencia Estrategica de Negocios', '', 5, 1, '', ''),
(13, 'Diplomado en Gerencia Publica', '', 5, 1, '', ''),
(14, 'Diplomado en Normas Internacionales de Informacion Financiera NIF', '', 5, 1, '', ''),
(15, 'Diplomado en Tributos', '', 5, 1, '', ''),
(16, 'Diplomado en Direccion Estrategica de Marketing y Comunicacion Integral', '', 5, 1, '', ''),
(17, 'Aplicacion practica de la ley organica del trabajo', '', 1, 1, '', ''),
(18, 'Gestion del cambio organizacional', '', 1, 1, '', ''),
(19, 'Negociación', '', 1, 1, '', ''),
(20, 'Diseño y Elaboración de Manuales de Procedimientos -Normas ISO-', '', 1, 1, '', ''),
(21, 'ISO 9000', '', 1, 1, '', ''),
(22, 'Gerencia de Inventarios', '', 8, 1, '', ''),
(23, 'Empowerment', '', 1, 1, '', ''),
(24, 'Programa de Formación Integral en Contabilidad (Sección 1)', '', 2, 1, '', ''),
(25, 'Asesor Tributario', '', 1, 1, '', ''),
(26, 'Conciliacion Bancaria', '', 1, 1, '', ''),
(27, 'Tratamiento Fiscal y Contable de las Empresas del Sector Construcción', '', 1, 1, '', ''),
(28, 'Analisis e Interpretacion de Estados Financieros', '', 1, 1, '', ''),
(29, 'Como enfrentar una fiscalizacion tributaria', '', 1, 1, '', ''),
(30, 'Estado de flujos de efectivo', '', 1, 1, '', ''),
(31, 'Programas de Actualización en Normas Internacionales de Contabilidad VEN-NIF', '', 2, 1, '', ''),
(32, 'Principios de contabilidad Generalmente Aceptados VEN-NIF para PYMES', '', 1, 1, '', ''),
(33, 'Ejercicio Profesional del Contador Publico', '', 2, 1, '', ''),
(35, 'Prevencion de Accidentes de Transito', '', 1, 1, '', ''),
(36, 'Prevencion de Riesgos del Trabajo', '', 1, 1, '', ''),
(37, 'El Rol de los Delegados de Prevencion', '', 1, 1, '', ''),
(38, 'Prevencion de Riesgo Electrico', '', 1, 1, '', ''),
(39, 'Uso de Elementos de Proteccion Personal', '', 4, 1, '', ''),
(40, 'Ergonomia en el Trabajo', '', 1, 1, '', ''),
(41, 'Prevencion de Enfermedades Profesionales', '', 1, 1, '', ''),
(42, 'Prevencion en la Via Publica', '', 1, 1, '', ''),
(43, 'Prevencion de Riesgos Laborales y Enfermedades Ocupacionales', '', 1, 1, '', ''),
(44, 'Planificacion y Control de la Seguridad Industrial', '', 1, 1, '', ''),
(45, 'Comportamiento en caso de incendios y Normas para el desalojo en evento adverso', '', 1, 1, '', ''),
(46, 'Normas de Higiene y Seguridad Industrial', '', 1, 1, '', ''),
(47, 'Ley Organica de Prevencion Condiciones y Medio Ambiente de Trabajo', '', 1, 1, '', ''),
(48, 'Prevencion de Incendios', '', 1, 1, '', ''),
(49, 'Motivacion para el cumplimiento de los procedimientos de Higiene y Seguridad', '', 1, 1, '', ''),
(50, 'La Seguridad y el Supervisor', '', 1, 1, '', ''),
(51, 'Cuidados de la Espalda', '', 1, 1, '', ''),
(52, 'Primeros Auxilios', '', 1, 1, '', ''),
(53, 'Analisis de Riesgos', '', 1, 1, '', ''),
(54, 'Proteccion en Trabajos en Alturas', '', 1, 1, '', ''),
(55, 'Prevencion de Riesgos Ergonomicos', '', 1, 1, '', ''),
(56, 'Protección Respiratoria', '', 1, 1, '', ''),
(57, 'Diseño y Elaboración de Programas de Seguridad  y Salud Laboral', '', 4, 1, '', ''),
(58, 'Gerencia de Seguridad Salud Higiene y Ambiente', '', 1, 1, '', ''),
(59, 'Protección Auditiva', '', 1, 1, '', ''),
(60, 'Seguridad en Laboratorios', '', 1, 1, '', ''),
(61, 'Seguridad y Salud Laboral', '', 4, 1, '', ''),
(64, 'Diplomado en Gerencia Social', '', 5, 1, '', ''),
(65, 'Diplomado de Coaching Ontológico', '', 5, 1, '', ''),
(66, 'Diplomado en Seguridad, Salud y Ergonomia Laboral', '', 5, 1, '', ''),
(67, 'Coaching Corporal. Un Viaje del Cuerpo a las Emociones', '', 1, 1, '', ''),
(68, 'Régimen Tributario y Deberes Formales de los Contribuyentes Especiales', '', 3, 1, '', ''),
(69, 'Planificación de Remuneración y Beneficios', '', 1, 1, '', ''),
(70, 'Legislación Laboral Práctica', '', 1, 1, '', ''),
(71, 'Régimen Tributario de Facturación', '', 3, 1, '', ''),
(73, 'Ley de Costos y Precios Justos', '', 8, 1, '', ''),
(74, 'Determinación de la Estructura de Costo y Gastos en el Marco de la Ley de Costos', '', 8, 1, '', ''),
(75, 'Contrato Marco del Decreto-Ley de Contrataciones Públicas ', '', 8, 1, '', ''),
(76, 'PRUEBAS!! NO EDITAR!!', '', 1, 1, '', ''),
(77, 'Auditoria Interna y Prevención de Fraudes', '', 8, 1, '', ''),
(78, 'Ventas Exitosas con PNL', '', 8, 1, '', ''),
(79, 'Calidad de Servicio en la Atención Telefónica', '', 8, 1, '', ''),
(80, 'Pensamiento Lateral. Potencia tu Creatividad', '', 1, 1, '', ''),
(81, 'Logística y Planificación del Transporte', '', 8, 1, '', ''),
(82, 'Presupuesto y Flujo de Caja Proyectado', '', 8, 1, '', ''),
(83, 'Gestión de Crédito y Cobranza', '', 8, 1, '', ''),
(84, 'Marketing para Hoteles y Restaurantes.', '', 8, 1, '', ''),
(87, 'Marketing Político', '', 14, 1, '', ''),
(88, 'Impuesto Sobre la Renta', '', 2, 1, '', ''),
(89, 'Retenciones de ISLR', '', 2, 1, '', ''),
(90, 'Presupuesto Público', '', 8, 1, '', ''),
(91, 'Diplomado en Logística (Supply Chain Management)', '', 5, 1, '', ''),
(92, 'Obligaciones Laborales del Sector Público', '', 8, 1, '', ''),
(93, 'Normatividad en Buenas Prácticas de Manufactura', '', 8, 1, '', ''),
(94, 'Determinación de la Estructura de Costo y Gastos en el Marco de la Ley de Costos', '', 3, 1, '', ''),
(95, 'Ajuste por Inflación Fiscal', '', 3, 1, '', ''),
(96, 'Nueva Ley Orgánica del Trabajo. Los Trabajadores y Trabajadoras', '', 8, 1, '', ''),
(97, 'Auditoria de Gestión', '', 8, 1, '', ''),
(98, 'Buenas Prácticas en Gestión del Talento Humano: Captación, mantenimiento y de', '', 1, 1, '', ''),
(99, 'Buenas Prácticas en Gestión del Talento Humano: Captación, mantenimiento y...', '', 8, 1, '', ''),
(100, 'Calidad de Servicio y Atención al Cliente', '', 8, 1, '', ''),
(101, 'Gestión de Compras y Stock', '', 8, 1, '', ''),
(102, 'Gerencia de Almacenes y Control de Inventarios', '', 8, 1, '', ''),
(103, 'Gestión Organizacional por Competencias', '', 1, 1, '', ''),
(104, 'Modelado de Organizaciones y sus Puestos de Trabajo', '', 1, 1, '', ''),
(105, 'Métodos y Técnicas de Comunicación Efectiva. ', '', 1, 1, '', ''),
(106, 'Negociación y Manejo de Conflictos', '', 1, 1, '', ''),
(107, 'Gerencia del Tiempo', '', 8, 1, '', ''),
(108, 'Cálculo de Prestaciones S. Vacaciones y Utilidades adaptado a LOTTT    ', '', 8, 1, '', ''),
(109, 'Seminario: Autoestima  y Cultura Organizacional, Dr. Manuel Barroso', '', 8, 1, '', ''),
(110, 'Seminario: Autoestima  y Cultura Organizacional, Dr. Manuel Barroso', '', 1, 1, '', ''),
(111, 'Seminario: Autoestima  y Cultura Organizacional, ,Dr. Manuel Barroso', '', 8, 1, '', ''),
(112, 'Selección, Capacitación y Evaluación del Talento Humano', '', 1, 1, '', ''),
(113, 'Toma Física y Control de Inventarios', '', 8, 1, '', ''),
(114, 'Formación Gerencial Avanzada en Logística - Supply Chain Management', '', 8, 1, '', ''),
(115, 'Analisis e Interpretacion de Estados Financieros', '', 2, 1, '', ''),
(116, 'Gestión Estratégica de la Tesoreria', '', 8, 1, '', ''),
(117, 'Nueva Ley Orgánica del Trabajo. Los Trabajadores y Trabajadoras', '', 1, 1, '', ''),
(118, 'El Vigía: Nueva Ley Orgánica del Trabajo. Los Trabajadores y Trabajadoras', '', 8, 1, '', ''),
(119, 'El Vigía: Nueva Ley Orgánica del Trabajo. Los Trabajadores y Trabajadoras', '', 8, 1, '', ''),
(120, 'Desarrollo de Equipos de Alto Desempeño', '', 1, 1, '', ''),
(121, 'Contratos, Desvinculación y Liquidación Laboral Adaptado a la nueva LOTTT', '', 8, 1, '', ''),
(122, 'Sistemas Integrales de Evaluación del Talento Humano', '', 1, 1, '', ''),
(123, 'Diplomado en Ergonomía Laboral', '', 5, 1, '', ''),
(124, 'Impuesto al Valor Agregado', '', 3, 1, '', ''),
(125, 'Cálculo de Nómina adaptado a la Nueva LOTTT', '', 8, 1, '', ''),
(126, 'Diplomado Internacional  en Avances en Gestión Financiera', '', 9, 1, '', ''),
(127, 'Diplomado Internacional  en Dirección y Planificación Estratégica', '', 9, 1, '', ''),
(128, 'Diplomado Internacional de Avances en Marketing', '', 9, 1, '', ''),
(129, 'Diplomado Internacional en Gerencia de Logística', '', 9, 1, '', ''),
(130, 'Diplomado Internacional en Dirección y Planificación Estratégica', '', 9, 1, '', ''),
(131, 'Diplomado Internacional en Management', '', 10, 1, '', ''),
(132, 'Diplomado Internacional en Avances en Gestión Financiera', '', 9, 1, '', ''),
(133, 'Analisis de Riesgos.', '', 4, 1, '', ''),
(134, 'Diplomado Internacional de Avances en Marketing', '', 9, 1, '', ''),
(135, 'Diplomado Internacional en Gerencia de Logística', '', 9, 1, '', ''),
(136, 'Diplomado Internacional en Gerencia Social.', '', 10, 1, '', ''),
(137, 'Diplomado Internacional en Creatividad y Habilidades Gerenciales.', '', 10, 1, '', ''),
(138, 'Diplomado Internacional en Avances en Gestión Empresarial', '', 10, 1, '', ''),
(139, 'Diplomado Internacional en Dirección y Planificación Estratégica.', '', 10, 1, '', ''),
(140, 'Diplomado Internacional en E-Business y Comercio Electrónico.', '', 10, 1, '', ''),
(141, 'Diplomado Internacional en Global Management.', '', 10, 1, '', ''),
(142, 'Diplomado Internacional en Técnicas de Negociación y Manejo de Conflictos.', '', 10, 1, '', ''),
(143, 'Diplomado Internacional en Avances en Recursos Humanos', '', 10, 1, '', ''),
(144, 'Diplomado Internacional en Gestión del Clima Organizacional.', '', 10, 1, '', ''),
(145, 'Diplomado Internacional en Gestión Integral del Capital Humano.', '', 10, 1, '', ''),
(146, 'Diplomado Internacional en Comunicación Empresarial y Corporativa.', '', 10, 1, '', ''),
(147, 'Diplomado Internacional en Estrategias de Marketing Internacional.', '', 10, 1, '', ''),
(148, 'Diplomado Internacional en Estrategias de Promoción y Ventas.', '', 10, 1, '', ''),
(149, 'Diplomado Internacional en Estudios de Publicidad.', '', 10, 1, '', ''),
(150, 'Diplomado Internacional en Ética y Estrategia de Marketing.', '', 10, 1, '', ''),
(151, 'Diplomado Internacional en Gestión de Servicio al Cliente.', '', 10, 1, '', ''),
(152, 'Diplomado Internacional en Manejo de Crisis.', '', 10, 1, '', ''),
(153, 'Diplomado Internacional en Marketing y Mercadotecnia Política.', '', 10, 1, '', ''),
(154, 'Diplomado Internacional en Marketing Relacional y Fidelización de Clientes.', '', 10, 1, '', ''),
(155, 'Diplomado Internacional en Relaciones Públicas y Protocolo.', '', 10, 1, '', ''),
(156, 'Diplomado Internacional en Responsabilidad Social, Empresarial y Corporativa.', '', 10, 1, '', ''),
(157, 'Diplomado Internacional en Tecnologías de la Comunicación e Información.', '', 10, 1, '', ''),
(158, 'Diplomado Internacional en Desarrollo Sustentable.', '', 10, 1, '', ''),
(159, 'Diplomado Internacional en Desarrollo Local', '', 10, 1, '', ''),
(160, 'Diplomado Internacional en Estudios de Evaluación del Impacto Ambiental.', '', 10, 1, '', ''),
(161, 'Diplomado Internacional en Estudios de Macroeconomía.', '', 10, 1, '', ''),
(162, 'Diplomado Internacional en Estudios de Microeconomía.', '', 10, 1, '', ''),
(163, 'Diplomado Internacional en Experto en Cooperación Internacional.', '', 10, 1, '', ''),
(164, 'Diplomado Internacional en Globalización', '', 10, 1, '', ''),
(165, 'Diplomado Internacional en Manejo de Emergencias Químicas.', '', 10, 1, '', ''),
(166, 'Diplomado Int.en Incidentes Biológ.Quím.Radiológ.Mercanc.Pelig. y Terrorismo', '', 10, 1, '', ''),
(167, 'Diplomado Internacional en Prevención de Riesgos Laborales.', '', 10, 1, '', ''),
(168, 'Convenio Cambiario N. 20, para acceso a divisas y cuentas en moneda extranjera', '', 8, 1, '', ''),
(169, 'Tributación Municipal', '', 3, 1, '', ''),
(170, 'Legislación Aduanera', '', 3, 1, '', ''),
(171, 'Cálculo y Manejo de Obligaciones Laborales', '', 1, 1, '', ''),
(172, 'Finanzas y Manejo Estratégico de la Tesoreria', '', 8, 1, '', ''),
(173, 'Sistemas de Indicadores Logísticos', '', 8, 1, '', ''),
(174, 'Log', '', 8, 1, '', ''),
(175, 'Logística del Fullfillment', '', 8, 1, '', ''),
(176, 'Estrategias Logísticas en el Contexto de Globalización de los Negocios', '', 8, 1, '', ''),
(177, 'Excel Fundamentals', '', 12, 1, '', ''),
(178, 'Excel Intermediate', '', 12, 1, '', ''),
(179, 'Excel Advanced', '', 12, 1, '', ''),
(180, 'Programa de Formación Integral en Contabilidad', '', 2, 1, '', ''),
(181, 'Microsoft Office', '', 12, 1, '', ''),
(182, 'Macros con Excel', '', 12, 1, '', ''),
(183, '¿Cómo hacer negocios con China?', '', 8, 1, '', ''),
(184, 'Herramientas para la Planificación Estratégica', '', 1, 1, '', ''),
(185, 'Cómo hacer negocios con China', '', 1, 1, '', ''),
(186, 'Herramientas para la Planificación Estratégica', '', 8, 1, '', ''),
(187, 'Planificación por Escenarios y Estrategias ante la Incertidumbre', '', 8, 1, '', ''),
(188, 'Coaching y Planificación Estratégica', '', 8, 1, '', ''),
(189, 'Cómo obtener Divisas: Sicad, Sicad 2, Aladi, Sucre, Cencoex', '', 13, 1, '', ''),
(190, 'Excelencia en el Servicio de Telemarketing', '', 14, 1, '', ''),
(191, 'Calidad en el Servicio y Atención al Cliente', '', 8, 1, '', ''),
(192, 'Calidad de Servicio en la Atención Telefónica', '', 1, 1, '', ''),
(193, 'Procedimientos Administrativos en Materia Laboral', '', 8, 1, '', ''),
(194, 'Programa de Formación Integral en Asistente Administrativo (Sección 2)', '', 13, 1, '', ''),
(196, 'Marketing para Hoteles y Restaurantes', '', 14, 1, '', ''),
(197, 'Servicio de Alojamiento para Hoteles. Formación de Camareras', '', 13, 1, '', ''),
(198, 'Como negociar con el MERCOSUR - ALBA - ALADI', '', 8, 1, '', ''),
(199, 'Gestión de Compras Internacionales', '', 8, 1, '', ''),
(200, 'SICAD', '', 8, 1, '', ''),
(201, 'Indicadores de Gestión para Recursos Humanos', '', 8, 1, '', ''),
(202, 'Trade Marketing: De la Industria al Anaquel', '', 14, 1, '', ''),
(203, 'prueba01', '', 5, 1, '', ''),
(204, 'Nuevo Reglamento Parcial de la LOTTT', '', 8, 1, '', ''),
(205, 'Libro de Compras y Ventas: Análisis y Normativas para su Elaboración', '', 3, 1, '', ''),
(206, 'Contabilidad Tributaria', '', 3, 1, '', ''),
(207, 'Registro de prueba', '', 1, 1, '', ''),
(208, 'Inscripción y Actualización en el Servicio Nacional de Contratistas', '', 2, 1, '', ''),
(209, 'Diplomado de Formación Avanzada en Gerencia', '', 5, 1, '', ''),
(210, 'Remuneración de Cargos:Diseño de Estructuras Salariales, Curvas y Tabuladores', '', 8, 1, '', ''),
(211, 'Estructura de Costos en el Marco de la Ley de Costos', '', 8, 1, '', ''),
(212, 'La Planificación y Control de la Producción en base al Pronóstico de Ventas', '', 8, 1, '', ''),
(213, 'Incoterms 2010', '', 8, 1, '', ''),
(214, 'Arancel de Aduanas', '', 8, 1, '', ''),
(215, 'Programa de Formación de Almacenistas', '', 8, 1, '', ''),
(216, 'Presupuesto y Control de la Gestión Empresarial', '', 8, 1, '', ''),
(217, 'Conteo Cíclico', '', 8, 1, '', ''),
(218, 'Cómo Gestionar con Éxito tu Empresa Familiar', '', 8, 1, '', ''),
(220, 'Programa de Entrenamiento Corporal para el Desempeño Ejecutivo', '', 8, 1, '', ''),
(221, 'Retorno Sobre la Inversión (ROI) en la Capacitación de Personal', '', 8, 1, '', ''),
(222, 'Formulación de Indicadores de Gestión', '', 8, 1, '', ''),
(223, 'Liderazgo con Inteligencia Emocional', '', 8, 1, '', ''),
(224, ' Cómo  Hacer Negocios  con Panamá', '', 8, 1, '', ''),
(225, 'Cómo Hacer Negocios con Curazao', '', 8, 1, '', ''),
(226, 'Programa de Certificación Internacional: Licenced Practitioner de PNL', '', 5, 1, '', ''),
(227, 'Ley Orgánica de Precios Justos', '', 8, 1, '', ''),
(228, 'Cálculo de Nómina y Obligaciones Parafiscales', '', 8, 1, '', ''),
(229, 'Resolución de Conflictos Laborales', '', 8, 1, '', ''),
(230, 'FOREX Inversión a través de la Bolsa', '', 8, 1, '', ''),
(231, 'Cómo obtener DÓLARES mediante el Centro Nacional de Comercio Exterior', '', 8, 1, '', ''),
(232, 'Seguridad Informática', '', 12, 1, '', ''),
(233, 'Procedimiento para cargar la Estructura de Costos en el sistema RUPDAE', '', 8, 1, '', ''),
(234, 'Protocolo y Cultura de Negocios en China', '', 8, 1, '', ''),
(235, 'Ajuste por Inflación Financiero (sección 31 de NIIF Pymes)', '', 8, 1, '', ''),
(236, 'Contabilidad para Empresas Cooperativas', '', 2, 1, '', ''),
(237, 'Actualización de Normas de Información Financiera VEN-NIF para Pymes', '', 5, 1, '', ''),
(238, 'Transición a la VEN-NIF para Pymes', '', 8, 1, '', ''),
(239, 'Gerencia de Proyectos - Project Management', '', 5, 1, '', ''),
(240, 'Indicadores de Gestión y Balanced Scorecard', '', 8, 1, '', ''),
(241, 'Programa Especializado en Gerencia de Ventas', '', 5, 1, '', ''),
(242, 'Diseño Organizacional - Modelado de las Organizaciones y sus Puestos de Trabajo', '', 1, 1, '', ''),
(243, 'Gerencia de la calidad Normas ISO 9000', '', 8, 1, '', ''),
(244, 'Los Pliegos de Condiciones, Matrices de Calificación y Evaluación ', '', 10, 1, '', ''),
(245, 'prueba0001', '', 10, 1, '', ''),
(246, 'Cálculos Laborales Empresas Constructoras - Contrato Colectivo 2013/2015', '', 8, 1, '', ''),
(247, 'Marketing Interno. Motivación Laboral y Equipos de Alto Rendimiento', '', 14, 1, '', ''),
(248, 'Estrategia del punto G.Venta emocional para desarrollar fidelidad en clientes', '', 8, 1, '', ''),
(249, 'Excelencia en Ventas y atención al Cliente', '', 14, 1, '', ''),
(250, 'Notas Revelatorias a los Estados Financieros', '', 8, 1, '', ''),
(251, 'Normas Interprofesionales para la Función del Comisario', '', 8, 1, '', ''),
(252, 'Presentaciones Efectivas con Power Point', '', 12, 1, '', ''),
(253, 'Lean Manufacturing: Manufactura Esbelta', '', 8, 1, '', ''),
(254, 'Ventas desde la Logística', '', 8, 1, '', ''),
(255, 'Formación de Supervisores', '', 8, 1, '', ''),
(256, 'Curso de Especialización en Microexpresiones y Lenguaje Corporal', '', 8, 1, '', ''),
(257, 'Gerencia del Personal y Trabajo en Equipo', '', 1, 1, '', ''),
(258, 'Lean Warehouse - Almacén Esbelto', '', 8, 1, '', ''),
(259, 'Conciliación Fiscal de Rentas con VEN-NIF-PYME', '', 8, 1, '', ''),
(260, 'Impuesto Diferido', '', 8, 1, '', ''),
(261, 'Cumplimiento Fiscal para Gerentes de Tienda. Acciones frente a Fiscalización', '', 8, 1, '', ''),
(262, 'Marketing para Vendedores', '', 14, 1, '', ''),
(263, 'Contabilidad para Empresas Constructoras bajo ambiente VEN-NIF-Pymes', '', 8, 1, '', ''),
(264, 'Valoración de Cargos: Diseño de Estructuras Salariales, Curvas y Tabuladores', '', 8, 1, '', ''),
(265, 'Oratoria y lenguaje corporal', '', 14, 1, '', ''),
(266, 'Fundamentos del Control Fiscal', '', 8, 1, '', ''),
(267, 'Régimen Legal y Contable del Sistema de Bienes Municipales', '', 8, 1, '', ''),
(268, 'Gestiona con Éxito tu Marca Personal. Marketing Personal', '', 14, 1, '', ''),
(269, 'El World Café', '', 8, 1, '', ''),
(270, 'El Gerente Coach', '', 8, 1, '', ''),
(271, 'Reexpresión de Estados Financieros y el Impuesto Diferido bajo VEN-NIF-Pymes', '', 8, 1, '', ''),
(272, 'Declaración del ISLR 2014 bajo VEN-NIF-Pyme', '', 8, 1, '', ''),
(273, 'Reforma Tributaria 2015', '', 8, 1, '', ''),
(274, 'Perspectivas Económicas 2015, La Ola Perfecta', '', 8, 1, '', ''),
(275, 'Programa de Certificación Internacional: Licensed Master de PNL', '', 5, 1, '', ''),
(276, 'Nuevo Decreto Ley de Contrataciones Públicas 2014', '', 8, 1, '', ''),
(277, 'Reforma Tributaria Empresas Cooperativas.  Nuevo Modelo de Gestión', '', 8, 1, '', ''),
(278, 'SIMADI, Sicad, Cencoex 2015', '', 8, 1, '', ''),
(279, 'Competencias Conversacionales para Gerentes', '', 8, 1, '', ''),
(280, 'Auditoría de Gestión del Talento Humano', '', 8, 1, '', ''),
(281, 'Ausentismo Laboral', '', 8, 1, '', ''),
(282, 'Coaching de Equipos', '', 8, 1, '', ''),
(283, 'Optimización de la Gestión de Distribución y Bodegas de Medicamentos', '', 8, 1, '', ''),
(284, 'Gestión de Costos y Variables en el Transporte Terrestre', '', 8, 1, '', ''),
(285, 'Mercadeo Digital y Redes Sociales para las Empresas', '', 8, 1, '', ''),
(286, 'Escritura Estratégica para Redes Sociales', '', 8, 1, '', ''),
(287, 'Mercadeo de Contenidos', '', 8, 1, '', ''),
(288, 'Community Management', '', 8, 1, '', ''),
(289, 'Certificado de Formación en Mercadeo Digital y Community Management', '', 5, 1, '', ''),
(290, 'Ley Orgánica de Procedimientos Administrativos', '', 8, 1, '', ''),
(291, 'Coaching Corporal. Un Viaje del Cuerpo a las Emociones', '', 8, 1, '', ''),
(292, 'Liderazgo y Coaching para la Organización que Aprende', '', 8, 1, '', ''),
(293, 'Nuevo Código Orgánico Tributario', '', 2, 1, '', ''),
(294, 'Conferencia: Me voy o me quedo', '', 8, 1, '', ''),
(295, 'Taller Vivencial: Terapia Gestalt', '', 8, 1, '', ''),
(296, 'Planificación Fiscal en Materia I.V.A.', '', 2, 1, '', ''),
(297, 'Planificación Fiscal en Materia I.S.L.R.', '', 1, 1, '', ''),
(298, 'Exportación de Productos. ¡Expande tu negocio y Genera Divisas!', '', 8, 1, '', ''),
(299, 'Taller de Terapia Gestalt: Un beso para el Mañana', '', 8, 1, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos_apertura`
--

CREATE TABLE IF NOT EXISTS `eventos_apertura` (
  `codg_aper` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de apertura del evento.',
  `fini_aper` date NOT NULL COMMENT 'Fecha en que inicia la apartura del evento',
  `ffin_aper` date NOT NULL COMMENT 'Fecha en que finaliza la apartura del evento',
  `obsr_aper` varchar(255) NOT NULL COMMENT 'Observaciones sobre la apartura del evento',
  `codg_evnt` int(11) NOT NULL COMMENT 'Codigo del evento con el que esta relacionada esta apertetura',
  `pgen_aper` double(9,2) NOT NULL COMMENT 'Precio General ',
  `prec_aper` double(9,2) NOT NULL COMMENT 'Precio para estudiantes',
  `prep_aper` double(9,2) NOT NULL COMMENT 'Precio para profesionales',
  `prem_aper` double(9,2) NOT NULL COMMENT 'Precio para empresas',
  `pmin_aper` double(9,2) NOT NULL COMMENT 'Pago Mínimo Requerido para Aprobar Inscripción',
  `hras_aper` int(3) NOT NULL COMMENT 'cantidad de horas del evento-apertura',
  `stat_aper` varchar(30) NOT NULL COMMENT 'Estatus de apertura dos valores Activo o desactivo',
  `imag_aper1` varchar(100) NOT NULL COMMENT 'Ruta de la imagen 1',
  `imag_aper2` varchar(100) NOT NULL COMMENT 'Ruta de la imagen 2',
  `ciudad_aper` varchar(250) NOT NULL,
  `desc_aper` double(9,2) NOT NULL COMMENT 'Porcentaje de descuento para pago decontado',
  `estu_desc` int(11) NOT NULL DEFAULT '0',
  `prof_desc` int(11) NOT NULL DEFAULT '0',
  `empr_desc` int(11) NOT NULL DEFAULT '0',
  `dcon_aper` varchar(255) NOT NULL COMMENT 'Condicion del Porcentaje de descuento para pago decontado',
  `dura_aper` varchar(255) NOT NULL COMMENT 'Duración en días, meses, años',
  `evnt_aper` varchar(2) NOT NULL COMMENT 'Si se desea publicar en eventos',
  PRIMARY KEY (`codg_aper`),
  UNIQUE KEY `fini_aper` (`fini_aper`,`codg_evnt`),
  KEY `codg_evnt` (`codg_evnt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos_areas`
--

CREATE TABLE IF NOT EXISTS `eventos_areas` (
  `codg_area` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Area',
  `nomb_area` varchar(50) NOT NULL COMMENT 'Nombre del Area',
  `imag_area` varchar(255) NOT NULL COMMENT 'imagen del area',
  `stat_area` varchar(10) NOT NULL COMMENT 'Estatus del área',
  PRIMARY KEY (`codg_area`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `eventos_areas`
--

INSERT INTO `eventos_areas` (`codg_area`, `nomb_area`, `imag_area`, `stat_area`) VALUES
(1, 'Administrativa', '', ''),
(2, 'Contable', '', ''),
(3, 'Tributaria', '', ''),
(4, 'Seguridad y Salud Laboral', '', ''),
(5, 'Diplomados', '', ''),
(6, '', '', ''),
(7, 'Ingenieria', '', ''),
(8, 'Especializados', '', ''),
(9, 'Diplomados Internacionales Semi-Presenciales', '', ''),
(10, 'Diplomados Internacionales 100% Virtuales', '', ''),
(11, '', '', ''),
(12, 'Informática para la Gerencia', '', ''),
(13, 'Hotelería y Turismo', '', ''),
(14, 'Marketing', '', ''),
(15, 'EVENTOS', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos_secciones`
--

CREATE TABLE IF NOT EXISTS `eventos_secciones` (
  `codg_secc` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_secc` char(1) NOT NULL,
  `hora_secc` varchar(120) NOT NULL,
  `lugr_secc` varchar(120) NOT NULL,
  `obsr_secc` varchar(255) NOT NULL,
  `codg_aper` int(11) NOT NULL,
  `codg_faci` int(11) NOT NULL,
  PRIMARY KEY (`codg_secc`),
  UNIQUE KEY `nomb_secc` (`nomb_secc`,`codg_aper`),
  UNIQUE KEY `nomb_secc_2` (`nomb_secc`,`codg_aper`),
  KEY `codg_faci` (`codg_faci`),
  KEY `codg_aper` (`codg_aper`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos_tipos`
--

CREATE TABLE IF NOT EXISTS `eventos_tipos` (
  `codg_tipo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de evento',
  `nomb_tipo` varchar(80) NOT NULL COMMENT 'Nombre del tipo de evento',
  PRIMARY KEY (`codg_tipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `eventos_tipos`
--

INSERT INTO `eventos_tipos` (`codg_tipo`, `nomb_tipo`) VALUES
(1, 'Cursos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facilitadores`
--

CREATE TABLE IF NOT EXISTS `facilitadores` (
  `codg_faci` int(11) NOT NULL AUTO_INCREMENT,
  `naci_faci` char(1) NOT NULL,
  `cedu_faci` varchar(10) NOT NULL,
  `nomb_faci` varchar(50) NOT NULL,
  `apel_faci` varchar(50) NOT NULL,
  `fnac_faci` date NOT NULL COMMENT 'fecha de nacimiento del facilitador',
  `tlfn_faci` varchar(12) NOT NULL,
  `celu_faci` varchar(12) NOT NULL,
  `emai_faci` varchar(100) NOT NULL,
  `obsr_faci` varchar(100) NOT NULL,
  `titu_faci` varchar(50) NOT NULL COMMENT 'Titulo o especialidad del facilitador',
  `perf_faci` text NOT NULL COMMENT 'Perfil profesional del Facilitador',
  PRIMARY KEY (`codg_faci`),
  UNIQUE KEY `cedu_faci` (`cedu_faci`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `codg_feed` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`codg_feed`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestion_contacto`
--

CREATE TABLE IF NOT EXISTS `gestion_contacto` (
  `codg_gest` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo de la gestion',
  `codg_rela` int(11) NOT NULL COMMENT 'codigo en la tabla relacionada',
  `orgn_rela` varchar(100) NOT NULL COMMENT 'Origen de la relacion',
  `obsr_gest` longtext NOT NULL COMMENT 'Observación',
  `dest_gest` varchar(50) NOT NULL COMMENT 'Destino de la gestión',
  `fcha_gest` date NOT NULL COMMENT 'Fecha de registro de la gestión',
  `codg_usua` int(11) NOT NULL,
  `stat_gest` varchar(2) NOT NULL,
  PRIMARY KEY (`codg_gest`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos de las gestiones de contacto' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE IF NOT EXISTS `inscripcion` (
  `codg_insc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la inscripcion',
  `codg_part` int(11) NOT NULL COMMENT 'id del participante para el sistema y base de datos',
  `codg_aper` int(11) NOT NULL COMMENT 'Id de la apertura del evento',
  `codg_secc` int(11) NOT NULL COMMENT 'Id de la seccion de la apertura',
  `fech_insc` date NOT NULL COMMENT 'Fecha en que se realiza la inscripcion',
  `apro_insc` char(1) NOT NULL COMMENT 'Aprobacion de la mincripcion, debe contener A si esta aprobada y N sino lo esta',
  `codg_empr` int(11) DEFAULT NULL COMMENT 'código empresa que inscribe',
  `obsr_insc` varchar(100) NOT NULL,
  `tipo_insc` varchar(20) NOT NULL,
  `base_insc` double(9,2) NOT NULL,
  PRIMARY KEY (`codg_insc`),
  UNIQUE KEY `codg_part` (`codg_part`,`codg_aper`),
  KEY `codg_aper` (`codg_aper`),
  KEY `codg_secc` (`codg_secc`),
  KEY `codg_empr` (`codg_empr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interesados`
--

CREATE TABLE IF NOT EXISTS `interesados` (
  `codg_intr` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de interesado',
  `nomb_intr` varchar(60) NOT NULL COMMENT 'Nombre del interesado',
  `tlfn_intr` varchar(12) NOT NULL COMMENT 'Telefono del interesado',
  `corr_intr` varchar(60) NOT NULL COMMENT 'Direccion de correo del interesado',
  `fech_intr` date NOT NULL COMMENT 'Fecha de registro de interes',
  `codg_aper` int(11) NOT NULL COMMENT 'Apertura del evento al cual se encuentra interesado - a traves de este codigo se acede al evento y por ende al area y al tipo',
  `codg_insc` int(11) DEFAULT NULL COMMENT 'Si existe es la relación a una inscripción',
  `codg_curso` int(11) DEFAULT NULL,
  `codg_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`codg_intr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `num_menu` int(2) NOT NULL,
  `nom_menu` varchar(20) NOT NULL,
  `num_opcion` int(2) NOT NULL,
  `nom_opcion` varchar(50) NOT NULL,
  `ico_opcion` longtext NOT NULL,
  `dir_opcion` longtext NOT NULL,
  `tar_opcion` varchar(50) NOT NULL,
  `tipo_usua` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`),
  UNIQUE KEY `num_menu` (`num_menu`,`num_opcion`,`tipo_usua`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id_menu`, `num_menu`, `nom_menu`, `num_opcion`, `nom_opcion`, `ico_opcion`, `dir_opcion`, `tar_opcion`, `tipo_usua`) VALUES
(1, 1, 'Participantes', 0, 'Participantes', 'sistema/imagenes/menu/participantes.png', 'sys.php?menu=participantes&formulario=participantes', '', 1),
(5, 2, 'Eventos', 0, 'Aperturas', 'sistema/imagenes/menu/areas.png', 'sys.php?menu=eventos&formulario=aperturas', '', 1),
(7, 3, 'Pagos', 0, 'Pagos', 'sistema/imagenes/menu/areas.png', 'sys.php?menu=pagos', '', 1),
(8, 1, 'Participantes', 1, 'Participantes', 'sistema/imagenes/menu/participantes.png', 'sys.php?menu=participantes&formulario=participantes', '', 1),
(10, 1, 'Participantes', 2, 'Empresas', 'sistema/imagenes/menu/empresas.png', 'sys.php?menu=participantes&formulario=empresas', '', 1),
(13, 1, 'Participantes', 4, 'Usuarios', 'sistema/imagenes/menu/usuarios.png', 'sys.php?menu=participantes&formulario=usuarios', '', 1),
(14, 1, 'Participantes', 5, 'Comunicación / Contacto', 'sistema/imagenes/menu/comunicacion.png', 'sistema/formularios/gestion_contacto.php', '_blank', 1),
(15, 2, 'Eventos', 1, 'Tipo de Eventos', 'sistema/imagenes/menu/eventos_tipos.png', 'sys.php?menu=eventos&formulario=eventos_tipos', '', 1),
(16, 2, 'Eventos', 2, 'Áreas de Eventos', 'sistema/imagenes/menu/eventos_areas.png', 'sys.php?menu=eventos&formulario=eventos_areas', '', 1),
(18, 2, 'Eventos', 3, 'Facilitadores', 'sistema/imagenes/menu/profesores.png', 'sys.php?menu=eventos&formulario=facilitadores', '', 1),
(20, 2, 'Eventos', 4, 'Eventos', 'sistema/imagenes/menu/eventos.png', 'sys.php?menu=eventos&formulario=eventos', '', 1),
(21, 2, 'Eventos', 5, 'Aperturas de Eventos', 'sistema/imagenes/menu/areas.png', 'sys.php?menu=eventos&formulario=aperturas', '', 1),
(22, 2, 'Eventos', 6, 'Creación de Secciones', 'sistema/imagenes/menu/secciones.png', 'sys.php?menu=eventos&formulario=secciones', '', 1),
(23, 3, 'Pagos', 1, 'Bancos', 'sistema/imagenes/menu/bancos.png', 'sys.php?menu=pagos&formulario=bancos', '', 1),
(24, 3, 'Pagos', 2, 'Tipo de Pagos', 'sistema/imagenes/menu/pagos_tipos.png', 'sys.php?menu=pagos&formulario=pagos_tipos', '', 1),
(26, 1, 'Participante', 0, 'Mi Perfil', 'sistema/imagenes/menu/participantes.png', 'user.php?menu=participante&formulario=participantes_perfil&boton=Buscar', '', 2),
(27, 1, 'Participante', 1, 'Mi Perfil', 'sistema/imagenes/menu/participantes.png', 'user.php?menu=participante&formulario=participantes_perfil&boton=Buscar', '', 2),
(28, 1, 'Participante', 2, 'Historial', 'sistema/imagenes/menu/eventos.png', 'user.php?menu=participante&formulario=participantes_historial', '', 2),
(29, 1, 'Participante', 3, 'Pagos', 'sistema/imagenes/menu/pagos.png', 'user.php?menu=participante&formulario=participantes_pagos', '', 2),
(31, 1, 'Empresa', 0, 'Empresa', 'sistema/imagenes/menu/empresas.png', 'user.php?menu=empresa&formulario=empresas_perfil&boton=Buscar', '', 3),
(32, 1, 'Empresa', 1, 'Empresa', 'sistema/imagenes/menu/empresas.png', 'user.php?menu=empresa&formulario=empresas_perfil&boton=Buscar', '', 3),
(33, 1, 'Participante', 4, 'Cambio de Contraseña', 'sistema/imagenes/menu/cambio_pass.png', 'user.php?menu=participante&formulario=cambiopass', '', 2),
(34, 1, 'Participantes', 99, 'Cambio de Contraseña', 'sistema/imagenes/menu/cambio_pass.png', 'user.php?menu=participantes&formulario=cambiopass', '', 1),
(35, 1, 'Empresa', 4, 'Cambio de Contraseña', 'sistema/imagenes/menu/cambio_pass.png', 'user.php?menu=empresa&formulario=cambiopass', '', 3),
(37, 1, 'Empresa', 2, 'Participantes', 'sistema/imagenes/menu/participantes.png', 'user.php?menu=empresa&formulario=empresa_participantes', '', 3),
(38, 1, 'Empresa', 3, 'Pagos', 'sistema/imagenes/menu/pagos.png', 'user.php?menu=empresa&formulario=empresa_pagos', '', 3),
(39, 3, 'Pagos', 4, 'Confirmar Pagos', 'sistema/imagenes/menu/pagos_confirmar.png', 'sys.php?menu=pagos&formulario=pagos_confirmar', '', 1),
(40, 1, 'Participantes', 7, 'Aprobar Inscripciones', 'sistema/imagenes/menu/inscripcion_aprobar.png', 'sys.php?menu=participantes&formulario=inscripcion_aprobar', '', 1),
(41, 1, 'Participantes', 6, 'Inscripciones', 'sistema/imagenes/menu/inscripcion.png', 'sys.php?menu=participantes&formulario=inscripcion_admin', '', 1),
(45, 3, 'Pagos', 3, 'Pagos Inscripciones', 'sistema/imagenes/menu/pagos.png', 'sys.php?menu=pagos&formulario=pagar_admin', '', 1),
(46, 2, 'Eventos', 7, 'Listado de Participantes', 'sistema/imagenes/menu/rep_sec.png', 'sistema/formularios/reporte_participantes.php', '_blank', 1),
(47, 2, 'Eventos', 8, 'Evaluación de Participantes', 'sistema/imagenes/menu/evaluacion.png', 'sys.php?menu=eventos&formulario=evaluacion_participantes', '', 1),
(48, 2, 'Eventos', 9, 'Cambio de imágenes de los eventos', 'sistema/imagenes/menu/slides.png', 'sys.php?menu=eventos&formulario=cambio_slides', '', 1),
(49, 2, 'Eventos', 10, 'Generar Certificados', 'sistema/imagenes/menu/certifica.png', 'sys.php?menu=eventos&formulario=certificados', '', 1),
(50, 1, 'Diseñador', 0, 'Aperturas', 'sistema/imagenes/menu/areas.png', 'user.php?menu=diseñador&formulario=aperturas', '', 5),
(51, 1, 'Diseñador', 1, 'Tipo de Eventos', 'sistema/imagenes/menu/eventos_tipos.png', 'user.php?menu=diseñador&formulario=eventos_tipos', '', 5),
(52, 1, 'Diseñador', 2, 'Áreas de Eventos', 'sistema/imagenes/menu/eventos_areas.png', 'user.php?menu=diseñador&formulario=eventos_areas', '', 5),
(53, 1, 'Diseñador', 3, 'Eventos', 'sistema/imagenes/menu/eventos.png', 'user.php?menu=diseñador&formulario=eventos', '', 5),
(54, 1, 'Diseñador', 4, 'Aperturas de Eventos', 'sistema/imagenes/menu/areas.png', 'user.php?menu=diseñador&formulario=aperturas', '', 5),
(55, 1, 'Diseñador', 5, 'Cambio de imágenes de los eventos', 'sistema/imagenes/menu/slides.png', 'user.php?menu=diseñador&formulario=cambio_slides', '', 5),
(56, 1, 'Diseñador', 99, 'Cambio de Contraseña', 'sistema/imagenes/menu/cambio_pass.png', 'user.php?menu=diseñador&formulario=cambiopass', '', 5),
(57, 1, 'Vendedor', 0, 'Participantes', 'sistema/imagenes/menu/participantes.png', 'user.php?menu=vendedor&formulario=participantes', '', 4),
(58, 1, 'Vendedor', 1, 'Participantes', 'sistema/imagenes/menu/participantes.png', 'user.php?menu=vendedor&formulario=participantes', '', 4),
(59, 1, 'Vendedor', 3, 'Comunicación / Contacto', 'sistema/imagenes/menu/comunicacion.png', 'sistema/formularios/gestion_contacto.php', '_blank', 4),
(60, 1, 'Vendedor', 4, 'Inscripciones', 'sistema/imagenes/menu/inscripcion.png', 'user.php?menu=vendedor&formulario=inscripcion_admin', '', 4),
(61, 1, 'Vendedor', 5, 'Aprobar Inscripciones', 'sistema/imagenes/menu/inscripcion_aprobar.png', 'user.php?menu=vendedor&formulario=inscripcion_aprobar', '', 4),
(62, 1, 'Vendedor', 99, 'Cambio de Contraseña', 'sistema/imagenes/menu/cambio_pass.png', 'user.php?menu=vendedor&formulario=cambiopass', '', 4),
(64, 1, 'Vendedor', 10, 'Pagos Inscripciones', 'sistema/imagenes/menu/pagos.png', 'user.php?menu=vendedor&formulario=pagar_admin', '', 4),
(65, 1, 'Administrador', 0, 'Pagos', 'sistema/imagenes/menu/areas.png', 'user.php', '', 6),
(66, 1, 'Administrador', 1, 'Bancos', 'sistema/imagenes/menu/bancos.png', 'user.php?menu=administrador&formulario=bancos', '', 6),
(67, 1, 'Administrador', 2, 'Tipo de Pagos', 'sistema/imagenes/menu/pagos_tipos.png', 'user.php?menu=administrador&formulario=pagos_tipos', '', 6),
(68, 1, 'Administrador', 4, 'Confirmar Pagos', 'sistema/imagenes/menu/pagos_confirmar.png', 'user.php?menu=administrador&formulario=pagos_confirmar', '', 6),
(69, 1, 'Administrador', 3, 'Pagos Inscripciones', 'sistema/imagenes/menu/pagos.png', 'user.php?menu=administrador&formulario=pagar_admin', '', 6),
(70, 1, 'Vendedor', 2, 'Empresas', 'sistema/imagenes/menu/empresas.png', 'user.php?menu=vendedor&formulario=empresas', '', 4),
(71, 3, 'Pagos', 5, 'Descuentos', 'sistema/imagenes/menu/descuentos.png', 'sys.php?menu=pagos&formulario=descuentos', '', 1),
(72, 3, 'Pagos', 6, 'Facturas', 'sistema/imagenes/menu/factura.png', 'sys.php?menu=pagos&formulario=facturas', '', 1),
(73, 1, 'Empresa', 5, 'Solcitud de Presupuesto', 'sistema/imagenes/menu/presupuesto.png', 'user.php?menu=empresa&formulario=solicitud_presupuesto', '', 3),
(74, 1, 'Participantes', 10, 'Ver Solicitudes de Presupuesto', 'sistema/imagenes/menu/presupuesto2.png', 'sys.php?menu=participantes&formulario=ver_presupuestos', '', 1),
(76, 1, 'Vendedor', 6, 'Listado de Participantes', 'sistema/imagenes/menu/rep_sec.png', 'sistema/formularios/reporte_participantes.php', '_blank', 4),
(77, 1, 'Diseñador', 6, 'Generar Certificados', 'sistema/imagenes/menu/certifica.png', 'user.php?menu=diseñador&formulario=certificados', '', 5),
(78, 1, 'Administrador', 99, 'Cambio de Contraseña', 'sistema/imagenes/menu/cambio_pass.png', 'user.php?menu=administrador&formulario=cambiopass', '', 6),
(79, 1, 'AtcEst', 0, 'Participantes', 'sistema/imagenes/menu/participantes.png', 'user.php?menu=atcest&formulario=participantes', '', 7),
(80, 1, 'AtcEst', 1, 'Participantes', 'sistema/imagenes/menu/participantes.png', 'user.php?menu=atcest&formulario=participantes', '', 7),
(81, 1, 'AtcEst', 3, 'Comunicación / Contacto', 'sistema/imagenes/menu/comunicacion.png', 'sistema/formularios/gestion_contacto.php', '_blank', 7),
(82, 1, 'AtcEst', 4, 'Inscripciones', 'sistema/imagenes/menu/inscripcion.png', 'user.php?menu=atcest&formulario=inscripcion_admin', '', 7),
(83, 1, 'AtcEst', 5, 'Aprobar Inscripciones', 'sistema/imagenes/menu/inscripcion_aprobar.png', 'user.php?menu=atcest&formulario=inscripcion_aprobar', '', 7),
(84, 1, 'AtcEst', 99, 'Cambio de Contraseña', 'sistema/imagenes/menu/cambio_pass.png', 'user.php?menu=atcest&formulario=cambiopass', '', 7),
(86, 1, 'AtcEst', 10, 'Pagos Inscripciones', 'sistema/imagenes/menu/pagos.png', 'user.php?menu=atcest&formulario=pagar_admin', '', 7),
(87, 1, 'AtcEst', 2, 'Empresas', 'sistema/imagenes/menu/empresas.png', 'user.php?menu=atcest&formulario=empresas', '', 7),
(88, 1, 'AtcEst', 6, 'Listado de Participantes', 'sistema/imagenes/menu/rep_sec.png', 'sistema/formularios/reporte_participantes.php', '_blank', 7),
(89, 1, 'AtcEst', 7, 'Generar Certificados', 'sistema/imagenes/menu/certifica.png', 'user.php?menu=atcest&formulario=certificados', '', 7),
(90, 1, 'Participantes', 8, 'Ver contacto Correo', 'sistema/imagenes/menu/contacto.png', 'sys.php?menu=participantes&formulario=ver_contactos', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
  `codg_pago` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del pago realizado',
  `fech_pago` date NOT NULL COMMENT 'Fecha de pago',
  `codg_tpag` int(11) DEFAULT NULL COMMENT 'Codigo del tipo de pago',
  `codg_banc` int(11) DEFAULT NULL COMMENT 'Código delBanco en que se realizo el pago',
  `refe_pago` varchar(20) NOT NULL COMMENT 'Numero de referencia del pago',
  `mont_pago` double(9,2) NOT NULL COMMENT 'Monto del pago',
  `apro_pago` char(1) NOT NULL COMMENT 'Aprobacion de pago',
  `codg_insc` int(11) NOT NULL COMMENT 'Codigo de la inscripcion asociada, E en espera y A Aprobado',
  `imag_pago1` varchar(100) NOT NULL DEFAULT '',
  `codg_aper` int(11) DEFAULT NULL COMMENT 'Se utiliza para relacionar el pago con la inscripcion de participantes miembros de una empresa',
  `codg_empr` int(11) DEFAULT NULL COMMENT 'Se utiliza para relacionar el pago con la inscripcion de participantes miembros de una empresa',
  `rech_pago` varchar(255) NOT NULL COMMENT 'Motivo del rechazo',
  `nfac_pago` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`codg_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_tipos`
--

CREATE TABLE IF NOT EXISTS `pagos_tipos` (
  `codg_tpag` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del tipo de pago',
  `nomb_tpag` varchar(30) NOT NULL COMMENT 'Nombre del tipo de pago',
  PRIMARY KEY (`codg_tpag`),
  UNIQUE KEY `nomb_tpag` (`nomb_tpag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `pagos_tipos`
--

INSERT INTO `pagos_tipos` (`codg_tpag`, `nomb_tpag`) VALUES
(4, 'Cheque'),
(2, 'Credito'),
(1, 'Debito'),
(5, 'Deposito'),
(6, 'Efectivo'),
(3, 'Transferencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participantes`
--

CREATE TABLE IF NOT EXISTS `participantes` (
  `codg_part` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id del participante para el sistema y base de datos',
  `naci_part` char(1) NOT NULL COMMENT 'Nacionalidad del Participante',
  `cedu_part` varchar(13) NOT NULL COMMENT 'Numero de CÃ©dula del participante',
  `nomb_part` varchar(30) NOT NULL COMMENT 'Nombres del participante',
  `apel_part` varchar(30) NOT NULL COMMENT 'Apellidos del participante',
  `sexo_part` char(1) NOT NULL COMMENT 'Tipo de sexo del participante',
  `tlfn_part` varchar(30) NOT NULL COMMENT 'Numero telefonico del participante',
  `corr_part` varchar(100) NOT NULL COMMENT 'Correo electronico del participante',
  `fchn_part` date NOT NULL COMMENT 'Fecha de nacimiento del participante',
  `codg_tpar` int(11) NOT NULL COMMENT 'Codigo relacional de tipo de participante',
  `codg_empr` int(11) DEFAULT NULL COMMENT 'Código relacional de la empresa que pertenece',
  PRIMARY KEY (`codg_part`),
  UNIQUE KEY `cedula_part` (`cedu_part`),
  UNIQUE KEY `corr_part` (`corr_part`),
  KEY `codg_tpar` (`codg_tpar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participantes_certificado`
--

CREATE TABLE IF NOT EXISTS `participantes_certificado` (
  `codg_cert` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del certificado',
  `codg_part` int(11) NOT NULL COMMENT 'Id del Participante al que pertenece el certificado',
  `codg_aper` int(11) NOT NULL COMMENT 'Codigo de la apertura del evento (curso) por el cual se otorga el certificado',
  `codg_faci` int(11) NOT NULL COMMENT 'Codigo del Facilitador de la apertura del evento',
  `obsr_cert` varchar(255) NOT NULL COMMENT 'Observaciones adicionales sobre el certificado.',
  PRIMARY KEY (`codg_cert`),
  KEY `codg_part` (`codg_part`),
  KEY `codg_aper` (`codg_aper`),
  KEY `codg_faci` (`codg_faci`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participantes_evaluacion`
--

CREATE TABLE IF NOT EXISTS `participantes_evaluacion` (
  `codg_insc` int(11) NOT NULL,
  `nota_eval` double(9,2) NOT NULL,
  PRIMARY KEY (`codg_insc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participantes_tipos`
--

CREATE TABLE IF NOT EXISTS `participantes_tipos` (
  `codg_tpar` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del tipo de participante',
  `nomb_tpar` varchar(60) NOT NULL COMMENT 'Nombre del tipo de participante',
  PRIMARY KEY (`codg_tpar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `codg_slide` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_slide` varchar(255) NOT NULL,
  `pred_slide` varchar(2) NOT NULL,
  PRIMARY KEY (`codg_slide`),
  UNIQUE KEY `nomb_slide` (`nomb_slide`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slides_images`
--

CREATE TABLE IF NOT EXISTS `slides_images` (
  `codg_imag` int(11) NOT NULL AUTO_INCREMENT,
  `codg_slide` int(11) NOT NULL,
  `ordn_imag` int(11) NOT NULL,
  `imag_imag` longtext NOT NULL,
  `urls_imag` longtext NOT NULL,
  `targ_imag` varchar(255) NOT NULL,
  `titl_imag` varchar(255) NOT NULL,
  `stat_imag` varchar(30) NOT NULL COMMENT 'Status de una imagen en el slide',
  PRIMARY KEY (`codg_imag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Datos para las imágenes rotativas' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_presupuesto`
--

CREATE TABLE IF NOT EXISTS `solicitud_presupuesto` (
  `id_pres` int(11) NOT NULL AUTO_INCREMENT,
  `tema_pres` varchar(250) NOT NULL,
  `desc_pres` varchar(250) NOT NULL,
  `npar_pres` int(5) NOT NULL,
  `fech_pres` date NOT NULL,
  `obsr_pres` varchar(250) NOT NULL,
  `codg_empr` int(11) NOT NULL,
  `status_pres` varchar(30) NOT NULL,
  PRIMARY KEY (`id_pres`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `codg_usua` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Campo Clave de la Tabla Usuarios',
  `cedu_usua` varchar(13) NOT NULL COMMENT 'Numero de cedula del usuario',
  `nomb_usua` varchar(30) NOT NULL COMMENT 'Nombres del Usuario',
  `apel_usua` varchar(30) NOT NULL COMMENT 'Apellidos del Usuario',
  `corr_usua` varchar(100) NOT NULL COMMENT 'Correo Electronico del Usuario',
  `logi_usua` varchar(30) NOT NULL COMMENT 'Nombre de usuario',
  `pass_usua` varchar(40) NOT NULL COMMENT 'Clave de Usuario',
  `tipo_usua` int(11) NOT NULL,
  `codg_empr` int(11) DEFAULT NULL,
  `stat_usua` varchar(250) NOT NULL,
  PRIMARY KEY (`codg_usua`),
  UNIQUE KEY `corr_usua` (`corr_usua`),
  UNIQUE KEY `logi_usua` (`logi_usua`),
  UNIQUE KEY `cedu_usua` (`cedu_usua`),
  KEY `tipo_usu` (`tipo_usua`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Manejo de Usuarios del Sistema' AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`codg_usua`, `cedu_usua`, `nomb_usua`, `apel_usua`, `corr_usua`, `logi_usua`, `pass_usua`, `tipo_usua`, `codg_empr`, `stat_usua`) VALUES
(7, '11111111', 'ROOT', '', 'jjmt', 'jjmt', '88eb1c0803e43de250f54e4f2b99781d', 1, NULL, 'Activo'),
(8, '401363202', 'Merintec', 'Merintec', 'felixpe09@gmail.com', 'felixpe09@gmail.com', 'bc20c150f40ab3ad76624a7a0deff489', 3, 1, 'Activo'),
(9, '14832319', 'Zulay', 'Dávila', 'felixpe09@hotmail.com', 'felixpe09@hotmail.com', '8c694cce51ae8014fbcf18b6efcaaed6', 2, NULL, 'Activo'),
(10, '18966499', 'Johana', 'Valecillos', 'johana_2890@hotmail.com', 'johana_2890@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', 7, NULL, 'Activo'),
(11, '15074749', 'Yulymar', 'Gutierrez', 'yulymar@siems.com.ve', 'yulymar@siems.com.ve', 'e10adc3949ba59abbe56e057f20f883e', 6, NULL, 'Activo'),
(12, '14832318', 'Zulay', 'davila', 'felixpe09@yahoo.com', 'felixpe09@yahoo.com', 'c21c1e766f35f630069c34081344b34b', 2, NULL, 'Activo'),
(13, '16657064', 'Juan', 'Marquez', 'juanjmt@gmail.com', 'juanjmt@gmail.com', '303c00e1bc7a293e6f9de70789547962', 2, NULL, ''),
(14, '17895221', 'Alejandra', 'Benavides', 'siemsmarketing@gmail.com', 'siemsmarketing@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 5, NULL, 'Activo'),
(17, '', '', '', '', '', '15553c774e9a53bd3c06cc23c268c101', 2, NULL, ''),
(18, '1879637', 'Pedro', 'Quintero', 'pedro@pedro.com', 'pedro@pedro.com', '165db19c177759cc85199fc0be138eed', 2, NULL, ''),
(19, '121212', 'Zulia', 'Caracas', 'zulia@caracas.com', 'zulia@caracas.com', '0741c6697f95b4e7276688313beb4915', 2, NULL, ''),
(20, '1515151', 'Sabado', 'Perez', 'sabado@caracas.com', 'sabado@caracas.com', '827ccb0eea8a706c4c34a16891f84e7b', 1, NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_tipos`
--

CREATE TABLE IF NOT EXISTS `usuarios_tipos` (
  `codg_tusu` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id del tipo de usuario del sistema',
  `nomb_tusu` varchar(35) NOT NULL COMMENT 'Nombre del tipo de usuario del sistema',
  PRIMARY KEY (`codg_tusu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `usuarios_tipos`
--

INSERT INTO `usuarios_tipos` (`codg_tusu`, `nomb_tusu`) VALUES
(1, 'Administrador de Sistema'),
(2, 'Usuario Regular'),
(3, 'Empresa'),
(4, 'Atención al Público'),
(5, 'Diseñador'),
(6, 'Administración'),
(7, 'Atención al Estudiante');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_inscripciones`
--
CREATE TABLE IF NOT EXISTS `vista_inscripciones` (
`codg_insc` int(11)
,`codg_part` int(11)
,`codg_aper` int(11)
,`codg_secc` int(11)
,`fech_insc` date
,`apro_insc` char(1)
,`codg_empr` int(11)
,`obsr_insc` varchar(100)
,`tipo_insc` varchar(20)
,`base_insc` double(9,2)
,`icodg_evnt` bigint(11)
,`fini_aper` date
,`ffin_aper` date
,`pgen_aper` double(9,2)
,`prec_aper` double(9,2)
,`prep_aper` double(9,2)
,`prem_aper` double(9,2)
,`pmin_aper` double(9,2)
,`nomb_evnt` varchar(255)
,`corr_part` varchar(100)
,`nomb_secc` varchar(1)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_secciones_aperturas`
--
CREATE TABLE IF NOT EXISTS `vista_secciones_aperturas` (
`codg_secc` int(11)
,`nomb_secc` char(1)
,`hora_secc` varchar(120)
,`lugr_secc` varchar(120)
,`obsr_secc` varchar(255)
,`codg_faci` int(11)
,`nomb_faci` varchar(50)
,`apel_faci` varchar(50)
,`codg_aper` int(11)
,`fini_aper` date
,`ffin_aper` date
,`obsr_aper` varchar(255)
,`codg_evnt` int(11)
,`pgen_aper` double(9,2)
,`prec_aper` double(9,2)
,`prep_aper` double(9,2)
,`prem_aper` double(9,2)
,`pmin_aper` double(9,2)
,`hras_aper` int(3)
,`stat_aper` varchar(30)
,`imag_aper1` varchar(100)
,`imag_aper2` varchar(100)
,`ciudad_aper` varchar(250)
,`desc_aper` double(9,2)
,`estu_desc` int(11)
,`prof_desc` int(11)
,`empr_desc` int(11)
,`dcon_aper` varchar(255)
,`dura_aper` varchar(255)
,`nomb_evnt` varchar(255)
);
-- --------------------------------------------------------

--
-- Estructura para la vista `vista_inscripciones`
--
DROP TABLE IF EXISTS `vista_inscripciones`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_inscripciones` AS select `i`.`codg_insc` AS `codg_insc`,`i`.`codg_part` AS `codg_part`,`i`.`codg_aper` AS `codg_aper`,`i`.`codg_secc` AS `codg_secc`,`i`.`fech_insc` AS `fech_insc`,`i`.`apro_insc` AS `apro_insc`,`i`.`codg_empr` AS `codg_empr`,`i`.`obsr_insc` AS `obsr_insc`,`i`.`tipo_insc` AS `tipo_insc`,`i`.`base_insc` AS `base_insc`,(select `eventos_apertura`.`codg_evnt` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `icodg_evnt`,(select `eventos_apertura`.`fini_aper` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `fini_aper`,(select `eventos_apertura`.`ffin_aper` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `ffin_aper`,(select `eventos_apertura`.`pgen_aper` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `pgen_aper`,(select `eventos_apertura`.`prec_aper` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `prec_aper`,(select `eventos_apertura`.`prep_aper` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `prep_aper`,(select `eventos_apertura`.`prem_aper` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `prem_aper`,(select `eventos_apertura`.`pmin_aper` from `eventos_apertura` where (`eventos_apertura`.`codg_aper` = `i`.`codg_aper`)) AS `pmin_aper`,(select `eventos`.`nomb_evnt` from `eventos` where (`eventos`.`codg_evnt` = `icodg_evnt`)) AS `nomb_evnt`,(select `participantes`.`corr_part` from `participantes` where (`participantes`.`codg_part` = `i`.`codg_part`)) AS `corr_part`,(select `eventos_secciones`.`nomb_secc` from `eventos_secciones` where (`eventos_secciones`.`codg_secc` = `i`.`codg_secc`)) AS `nomb_secc` from `inscripcion` `i` order by `i`.`codg_insc` desc;

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_secciones_aperturas`
--
DROP TABLE IF EXISTS `vista_secciones_aperturas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_secciones_aperturas` AS select `s`.`codg_secc` AS `codg_secc`,`s`.`nomb_secc` AS `nomb_secc`,`s`.`hora_secc` AS `hora_secc`,`s`.`lugr_secc` AS `lugr_secc`,`s`.`obsr_secc` AS `obsr_secc`,`s`.`codg_faci` AS `codg_faci`,(select `facilitadores`.`nomb_faci` from `facilitadores` where (`facilitadores`.`codg_faci` = `s`.`codg_faci`)) AS `nomb_faci`,(select `facilitadores`.`apel_faci` from `facilitadores` where (`facilitadores`.`codg_faci` = `s`.`codg_faci`)) AS `apel_faci`,`a`.`codg_aper` AS `codg_aper`,`a`.`fini_aper` AS `fini_aper`,`a`.`ffin_aper` AS `ffin_aper`,`a`.`obsr_aper` AS `obsr_aper`,`a`.`codg_evnt` AS `codg_evnt`,`a`.`pgen_aper` AS `pgen_aper`,`a`.`prec_aper` AS `prec_aper`,`a`.`prep_aper` AS `prep_aper`,`a`.`prem_aper` AS `prem_aper`,`a`.`pmin_aper` AS `pmin_aper`,`a`.`hras_aper` AS `hras_aper`,`a`.`stat_aper` AS `stat_aper`,`a`.`imag_aper1` AS `imag_aper1`,`a`.`imag_aper2` AS `imag_aper2`,`a`.`ciudad_aper` AS `ciudad_aper`,`a`.`desc_aper` AS `desc_aper`,`a`.`estu_desc` AS `estu_desc`,`a`.`prof_desc` AS `prof_desc`,`a`.`empr_desc` AS `empr_desc`,`a`.`dcon_aper` AS `dcon_aper`,`a`.`dura_aper` AS `dura_aper`,(select `eventos`.`nomb_evnt` from `eventos` where (`eventos`.`codg_evnt` = `a`.`codg_evnt`)) AS `nomb_evnt` from (`eventos_secciones` `s` join `eventos_apertura` `a`) where (`a`.`codg_aper` = `s`.`codg_aper`) order by `s`.`codg_secc`;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `eventos_ibfk_2` FOREIGN KEY (`codg_tipo`) REFERENCES `eventos_tipos` (`codg_tipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eventos_ibfk_1` FOREIGN KEY (`codg_area`) REFERENCES `eventos_areas` (`codg_area`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `eventos_apertura`
--
ALTER TABLE `eventos_apertura`
  ADD CONSTRAINT `eventos_apertura_ibfk_1` FOREIGN KEY (`codg_evnt`) REFERENCES `eventos` (`codg_evnt`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `eventos_secciones`
--
ALTER TABLE `eventos_secciones`
  ADD CONSTRAINT `eventos_secciones_ibfk_1` FOREIGN KEY (`codg_aper`) REFERENCES `eventos_apertura` (`codg_aper`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eventos_secciones_ibfk_2` FOREIGN KEY (`codg_faci`) REFERENCES `facilitadores` (`codg_faci`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `inscripcion_ibfk_4` FOREIGN KEY (`codg_secc`) REFERENCES `eventos_secciones` (`codg_secc`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inscripcion_ibfk_5` FOREIGN KEY (`codg_empr`) REFERENCES `empresas` (`codg_empr`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `participantes_certificado`
--
ALTER TABLE `participantes_certificado`
  ADD CONSTRAINT `participantes_certificado_ibfk_1` FOREIGN KEY (`codg_part`) REFERENCES `participantes` (`codg_part`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `participantes_certificado_ibfk_2` FOREIGN KEY (`codg_aper`) REFERENCES `eventos_apertura` (`codg_aper`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `participantes_certificado_ibfk_3` FOREIGN KEY (`codg_faci`) REFERENCES `facilitadores` (`codg_faci`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
