-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-12-2016 a las 20:08:32
-- Versión del servidor: 5.5.38-0+wheezy1
-- Versión de PHP: 5.4.45-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `siems`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exalumnos_xls`
--

CREATE TABLE IF NOT EXISTS `exalumnos_xls` (
  `codg_part` int(11) NOT NULL,
  `nomb_part` varchar(60) NOT NULL,
  `apel_part` varchar(30) NOT NULL,
  `nomb_evnt` varchar(255) NOT NULL,
  `tlfn_part` varchar(30) NOT NULL,
  `corr_part` varchar(100) NOT NULL,
  `fech_insc` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Exalumnos importados de xls';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `exalumnos_xls`
--
ALTER TABLE `exalumnos_xls`
  ADD PRIMARY KEY (`codg_part`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `exalumnos_xls`
--
ALTER TABLE `exalumnos_xls`
  MODIFY `codg_part` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
