-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-12-2016 a las 10:07:21
-- Versión del servidor: 5.5.38-0+wheezy1
-- Versión de PHP: 5.4.45-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `siems`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestion_exalumnos`
--

CREATE TABLE IF NOT EXISTS `gestion_exalumnos` (
  `codg_gest` int(11) NOT NULL COMMENT 'codigo de la gestion',
  `codg_rela` int(11) NOT NULL COMMENT 'codigo en la tabla relacionada',
  `orgn_rela` varchar(100) NOT NULL COMMENT 'Origen de la relacion',
  `obsr_gest` longtext NOT NULL COMMENT 'Observación',
  `dest_gest` varchar(50) NOT NULL COMMENT 'Destino de la gestión',
  `fcha_gest` date NOT NULL COMMENT 'Fecha de registro de la gestión',
  `codg_usua` int(11) NOT NULL,
  `stat_gest` varchar(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='Datos de las gestiones de contacto';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `gestion_exalumnos`
--
ALTER TABLE `gestion_exalumnos`
  ADD PRIMARY KEY (`codg_gest`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `gestion_exalumnos`
--
ALTER TABLE `gestion_exalumnos`
  MODIFY `codg_gest` int(11) NOT NULL AUTO_INCREMENT COMMENT 'codigo de la gestion',AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
