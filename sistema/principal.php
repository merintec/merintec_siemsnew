<?php
include("comunes/verificar_sesion.php");
include("comunes/conexion.php");
include("comunes/funciones_php.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>.:: SIEMS Instituto Gerencial ::.</title>
        <link href="css/estilos.css" rel="stylesheet" type="text/css">
        <script src="validacion/js/jquery-1.8.2.min.js" type="text/javascript"></script>
		  <script src="validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
		  <script src="validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
		  <link rel="stylesheet" href="validacion/css/validationEngine.jquery.css" type="text/css"/>
		  <link rel="stylesheet" href="validacion/css/template.css" type="text/css"/>
		  <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    </head>
    

<link href="css/estilos.css" rel="stylesheet" type="text/css">

<!-- validacion en vivo -->
<script >

	jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
		});

</script>
<!-- CALENDARIO-->
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			$( ".datepicker" ).datepicker();
			jQuery("form1").validationEngine();
		});
</script>
  <body>
  <?php include('comunes/usuario_logueado.php'); ?> 
  <?php include('general/menu.php'); ?>
  <?php include('general/tipos.php'); ?>
  </body>
</html>
